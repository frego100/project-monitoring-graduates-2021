ssh -i $PRIVATE_KEY_STG ubuntu@$SERVER_IP_STG << 'ENDSSH'
  cd ../ubuntu/projects/egresados
  cd project-monitoring-graduates-2021/
  git pull
  sudo supervisorctl restart djangoegresados
ENDSSH
