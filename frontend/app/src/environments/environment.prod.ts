export const environment = {
  production: true,
  app: 'http://localhost:5000',
  api: 'https://dev.unsa-egresados.eesp-system.net',
  root: 'https://dev.unsa-egresados.web.eesp-system.net'
};
