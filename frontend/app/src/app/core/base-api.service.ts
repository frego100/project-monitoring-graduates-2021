import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { getToken } from '../shared/helpers/auth-utils';
import { environment } from '@env/environment';

@Injectable({
    providedIn: 'root'
})

/**
 * This abstract class is API tools
 *
 * @class BaseAPIService
 */
export abstract class BaseAPIService<T> {

    private headers = new HttpHeaders();

    constructor(private http: HttpClient) {
        this.authorizationHeader();
    }

    /**
     * Method that adds the authentication token of the user in session,
     * but if it was already assigned it does nothing
     *
     * @method authorizationHeader
     */
    authorizationHeader(): void {
        const token = getToken();
        if (token != '')
            this.headers = new HttpHeaders({'Authorization': 'token ' + token});
    }

    /**
     * Operation that queries the API by GET method, in general view a record
     *
     * @method get
     * @param url It is the query URL
     * @return Any record or records in the database wrapped asynchronously
     */
    get<T>(url: string): Observable<T> {
        this.authorizationHeader();
        return this.http.get<T>(environment.api + '/' +url, { headers: this.headers });
    }

    /**
     * Operation that queries the API by GET method, in general view a record
     *
     * @method get
     * @param url It is the query URL
     * @return Any record or records in the database wrapped asynchronously
     */
     getWithParams<T>(url: string , params ): Observable<T> {
        this.authorizationHeader();
        return this.http.get<T>(environment.api + '/' +url, { headers: this.headers ,
            params : params });
    }

    /**
     * Operation that queries the API by POST method, in general add a record
     *
     * @method post
     * @param url It is the query URL
     * @param body It is the body of the message
     * @return Any record or records in the database wrapped asynchronously
     */
    post<T>(url: string, body: object): Observable<T> {
        this.authorizationHeader();
        return this.http.post<T>(environment.api + '/' + url, body, { headers: this.headers });
    }

  /**
   * Operation that queries the API by POST method and responseType blob, in general upload a file binary
   *
   * @method post
   * @param url It is the query URL
   * @param body It is the body of the message
   * @return Any record or records in the database wrapped asynchronously
   */
    postBlob(url: string, body: object): Observable<any> {
      this.authorizationHeader();
      return this.http.post(environment.api + '/' + url, body,
      { headers: this.headers, responseType: 'blob' });
    }

    /**
     * Operation that queries the API by PUT method, in general modify a record
     *
     * @method put
     * @param url It is the query URL
     * @param body It is the body of the message
     * @return Any record or records in the database wrapped asynchronously
     */
    put<T>(url: string, body: object): Observable<T> {
        this.authorizationHeader();
        return this.http.put<T>(environment.api + '/' + url, body, { headers: this.headers });
    }

    /**
     * Operation that queries the API by PATCH method, in general modify parts of a record
     *
     * @method patch
     * @param url It is the query URL
     * @param body It is the body of the message
     * @return Any record or records in the database wrapped asynchronously
     */
    patch<T>(url: string, body: object): Observable<T> {
        this.authorizationHeader();
        return this.http.patch<T>(environment.api + '/' + url, body, { headers: this.headers });
    }

    /**
     * Operation that queries the API by DELETE method, in general
     * logically or physically delete a record
     *
     * @method delete
     * @param url It is the query URL
     * @return Any record or records in the database wrapped asynchronously
     */
    delete<T>(url: string): Observable<T> {
        this.authorizationHeader();
        return this.http.delete<T>(environment.api + '/' + url, { headers: this.headers });
    }
    deleteRegister<T>(url: string,id:number): Observable<T> {
        this.authorizationHeader();
        return this.http.delete<T>(environment.api + '/' + url +  id, { headers: this.headers });
    }
}
