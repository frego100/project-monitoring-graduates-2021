import { BaseAPIService } from './base-api.service';
import { BaseModel } from './base-model';

export {
   BaseAPIService,
   BaseModel
};