import { Injectable } from '@angular/core';
import { Faculty } from '@app/models/faculty';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
const url = 'instance/faculty' as string;
@Injectable({
  providedIn: 'root',
})
export class FacultyService {
  facultyControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<Faculty>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url+'?is_active=true');
  }

  getLite(area_field: number): Observable<any> {
    var _url = `${url}/lite/`;
    if (!IsNullUndefinedBlank(area_field)) {
        _url = _url + `?area_field=${area_field}`;
    }
    return this.API.get<any>(_url);
  }

  getById(id: String) {
    return this.API.get<Faculty>(url + '/' + id);
  }

  getByArea(id: String): Observable<any> {
    return this.API.get<any>(url + '/by/area/' + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    formData.append('area_field', data.area_field);
    return this.API.post<any>(url, formData);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    formData.append('area_field', data.area_field);
    return this.API.patch(url + '/' + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + '/' + id);
  }

  notify(object) {
    return this.API.post<any>(url + '/' + 'notify/', object);
  }
}
