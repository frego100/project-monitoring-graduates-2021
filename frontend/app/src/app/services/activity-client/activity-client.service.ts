import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from "@env/environment";


@Injectable({
  providedIn: 'root'
})
export class ActivityClientService {

  baseUrl = environment.api

  constructor(private http: HttpClient) {}

  getActivities(){
    return this.http.get<any>(`${this.baseUrl}/activity/public/`);
  }
}
