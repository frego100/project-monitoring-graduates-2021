import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from "@env/environment";
import { formatDate } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class AnnouncementClientService {

  baseUrl = environment.api;
  
  constructor(private http: HttpClient) {}

  getAnnouncement(){
    return this.http.get<any>(`${this.baseUrl}/announcements/public/`);
  }
  getAnnouncementOrder(){
    
    let month: number = new Date().getMonth()+1;
    return this.http.get<any>(`${this.baseUrl}/announcements/public?date_start__gte=2021-`+month+`-01+00:00`);
  }
}
