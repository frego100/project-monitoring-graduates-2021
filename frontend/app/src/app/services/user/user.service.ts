import { Injectable, Output, EventEmitter } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { User } from '@models/user';
import { map } from 'rxjs/operators';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import {environment} from "@env/environment";

const url = 'user' as string;

@Injectable({
  providedIn: 'root',
})
export class UserService {
  formControl = new Subject();
  userControl = new BehaviorSubject(null);
  userRegister: any;
  userRegisterBack: any;
  listUsers: User[] = [];

  constructor(private API: BaseAPIService<any>) {}

  setUserPreRegister(data) {
    this.userRegister = data;
  }
  setUserPreRegisterBack(data) {
    this.userRegisterBack = data;
  }
  getUserPreRegister() {
    return this.userRegister;
  }
  getUserPreRegisterBack() {
    return this.userRegisterBack;
  }
  getAll(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true');
  }

  preRegisterUser(data: any): Observable<any> {
    return this.API.post<any>(url + '/create-pre-registration/', data);
  }

  getWithParams(limit, offset, groups, search, ordering_list): Observable<any> {
    let _url = `${url}/?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(groups)) {
      _url = _url + `&groups=${groups}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    if (!IsNullUndefinedBlank(ordering_list)) {
      ordering_list.forEach((value: string) => {
        if(!IsNullUndefinedBlank(value)) {
          _url = _url + `&ordering=${value}`;
        }
      });
    }
    return this.API.get<any>(_url);
  }

  getById(id: String) {
    return this.API.get<any>(url + '/' + id);
  }

  getAllUsers() {
    return this.listUsers.slice();
  }

  create(data: any): Observable<any> {
    delete data.pk;
    return this.API.post<any>(url + '/', data);
  }

  update(_id: string, data: any): Observable<any> {
    const body = Object.assign({}, data);
    delete body._id;
    delete body.company;
    return this.API.put(url + '/' + _id, body).pipe(map((res: any) => Object.assign({ _id }, res)));
  }

  getUserCreationRequest() {
    return this.API.get<any>(url + '/list-pre-registration/?state=false');
  }
  getList(
    limit,
    offset,
    filter1,
    search,
    ordering_list: Map<string, string>
  ): Observable<any> {
    var _url = `${url}/?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(filter1)) {
      _url = _url + `&filter1=${filter1}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    if (!IsNullUndefinedBlank(ordering_list)) {
      ordering_list.forEach((value: string) => {
        if (!IsNullUndefinedBlank(value)) {
          _url = _url + `&ordering=${value}`;
        }
      });
    }
    return this.API.get<any>(_url);
  }
  validateUser(id: string, data: any) {
    return this.API.put<any>(url + '/validate/' + id, data);
  }
  uploadExcel(data: any) {
    var formData: any = new FormData();
    formData.append("file", data.file);
    return this.API.post<any>(url + "/excel_upload/", formData);
  }

  /**
  * Operation list user lite
  *
  * @method getUserLite
  * @param groups filter user for groups
  * @return Simple Data of the  users
  */
  getUserLite2 (): Observable<any> {
    return this.API.get<any>(`${url}/lite/`);
  }
  getUserLite (groups): Observable<any> {
    var _url = `${url}/lite/`;
    if (groups !== '') {
      _url = _url + `?groups=${groups}`;
    }
    return this.API.get<any>(_url);
  }
  disabledAccount() {
    return this.API.delete<any>(`auth/deactivate/`)
  }
}
