import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Observable } from 'rxjs';
import { History } from '@app/models/history';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const url = "history/" as string;

@Injectable({
    providedIn: 'root'
})

/**
 * This service provides information on user permissions and profiling
 * 
 * @class AuthUserService
 */
export class TrackingUserService {

    constructor (private API: BaseAPIService<History>) {}

    /**
     * Operation history user with filters and pagination
     * 
     * @method getHistory
     * @param ordering_list its hashmap of the ordering
     * @param limit page size
     * @param offset start item page
     * @param therest diferent filters
     * @return Data of the tracking history users
     */
    getHistory (
        limit, offset, user__groups__name, user__username,
        created__lte, created__gte, search, ordering_list:Map<string,string>
    ): Observable<any> {
        var _url = `${url}?limit=${limit}&offset=${offset}`
        if (!IsNullUndefinedBlank(user__groups__name)) {
            _url = _url + `&user__groups__name=${user__groups__name}`;
        }
        if (!IsNullUndefinedBlank(user__username)) {
            _url = _url + `&user__username=${user__username}`;
        }
        if (!IsNullUndefinedBlank(created__lte)) {
            _url = _url + `&created__lte=${created__lte}`;
        }
        if (!IsNullUndefinedBlank(created__gte)) {
            _url = _url + `&created__gte=${created__gte}`;
        }
        if (!IsNullUndefinedBlank(search)) {
            _url = _url + `&search=${search}`;
        }
        if (!IsNullUndefinedBlank(ordering_list)) {
            ordering_list.forEach((value: string) => {
                if(!IsNullUndefinedBlank(value)) {
                    _url = _url + `&ordering=${value}`;
                }
            });
        }
        return this.API.get<any>(_url);
    }

}

