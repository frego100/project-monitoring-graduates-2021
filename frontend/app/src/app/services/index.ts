import { ActivityService } from './activity/activity.service';
import { ActivityClientService } from './activity-client/activity-client.service';
import { AnnouncementService } from './announcement/announcements.service';
import { AreaService } from './area/area.service';
import { AuthService } from './auth/auth.service';
import { AuthUserService } from './auth-user/auth-user.service';
import { BaseAPIService } from '../core/base-api.service';
import { ContentTypeProfileService } from './content-type-profile/content-type-profile.service';
import { TypeEmployeeService } from './type-employee/type-employee.service';
import { FacultyService } from './faculty/faculty.service';
import { IdentifierService } from './identifier/identifier.service';
import { JobsService } from './job/job.service';
import { ManualsService } from './manual/manual.service';
import { PermissionService } from './permission/permission.service';
import { ProgramService } from './program/program.service';
import { RolesService } from './rol/rol.service';
import { TrackingUserService } from './tracking-history-user/tracking-history-user.service';
import { UserService } from './user/user.service';
import { ProfileService } from './profile/profile.service';
import { CompanyService } from './company/company.service';
import { ReportService } from './report/report.service';
import { PageConfigService} from "@app/services/page-config/page-config.service";

export {
  ActivityService,
  ActivityClientService,
  AnnouncementService,
  AreaService,
  AuthService,
  AuthUserService,
  BaseAPIService,
  CompanyService,
  ContentTypeProfileService,
  TypeEmployeeService,
  FacultyService,
  IdentifierService,
  JobsService,
  ManualsService,
  PermissionService,
  ProgramService,
  RolesService,
  TrackingUserService,
  UserService,
  ProfileService,
  ReportService,
  PageConfigService
}
