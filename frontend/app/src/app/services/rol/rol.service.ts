import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Roles } from '@models/rol'

const url = 'group/' as string;
@Injectable({
  providedIn: 'root',
})
export class RolesService {
  formControl = new Subject();
  rolesControl = new BehaviorSubject(null);
  listRoles: Roles[] = [];

  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url + '?state=true');
  }

  getAllPublic(): Observable<any> {
    return this.API.get<any>(url + 'public/');
  }
  
  create(data: any): Observable<any> {
    delete data.id;
    return this.API.post<any>(url, data);
  }
  getById(id: String) {
    return this.API.get<any>(url + id);
  }

  update(_id: string, data: any): Observable<any> {
    return this.API.put(url + _id + '/', data).pipe(map((res: any) => Object.assign({ _id }, res)));
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  /**
  * Operation list group 
  * 
  * @method getGroupLite
  * @return Simple Data of the groups
  */
  getGroupLite (): Observable<any> {
    var _url = `${url}lite/`;
    return this.API.get<any>(_url);
  }
}
