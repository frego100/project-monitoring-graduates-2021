import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import {
  SuperAdminProfile,
  AdminProfile,
  Profile,
  GraduateProfile,
  CompanyProfile,
} from '@app/models/profile';
import { map } from 'rxjs/operators';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const admin_url = 'profile/admin' as string;
const super_admin_url = 'profile/super-admin' as string;
const graduate_url = 'profile/graduate' as string;
const company_url = 'profile/company' as string;

@Injectable({
  providedIn: 'root',
})

/**
 * This service provides information about different profiles
 *
 * @class ProfileService
 */
export class ProfileService {
  formControl = new Subject();
  profileControl = new BehaviorSubject(null);

  constructor(private API: BaseAPIService<any>) {}

  getAdmin(id): Observable<any> {
    let _url = `${admin_url}/${id}`;
    return this.API.get<any>(_url);
  }

  postAdmin(body): Observable<AdminProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('instance', body.instance);
    formData.append('job_position', body.job_position);
    formData.append('type_employee', body.type_employee);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    formData.append('picture', body.picture);
    formData.append('is_active', body.is_active);
    return this.API.post<AdminProfile>(admin_url + '/', formData);
  }

  putAdmin(id, body): Observable<AdminProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('instance', body.instance);
    formData.append('job_position', body.job_position);
    formData.append('type_employee', body.type_employee);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    formData.append('picture', body.picture);
    formData.append('is_active', body.is_active);
    var _url = `${admin_url}/${id}`;
    return this.API.put<AdminProfile>(_url, formData);
  }

  patchAdmin(id, body): Observable<AdminProfile> {
    delete body.id;
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('instance', body.instance);
    formData.append('job_position', body.job_position);
    formData.append('type_employee', body.type_employee);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    if (typeof body.picture != 'string') {
      formData.append('image', body.picture);
    }
    formData.append('is_active', body.is_active);
    var _url = `${admin_url}/${id}`;
    return this.API.patch<AdminProfile>(_url, formData);
  }

  deleteAdmin(id: string): Observable<AdminProfile> {
    return this.API.delete(admin_url + id);
  }

  getGraduate(id): Observable<GraduateProfile> {
    var _url = `${graduate_url}/${id}`;
    return this.API.get<GraduateProfile>(_url);
  }

  postGraduate(body): Observable<GraduateProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('date_birthday', body.date_birthday);
    formData.append('address', body.address);
    formData.append('company', body.company);
    formData.append('cui', body.cui);
    formData.append('document', body.document);
    formData.append('description_dcp', body.description_dcp);
    formData.append('is_dcp', body.is_dcp);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    if (!IsNullUndefinedBlank(body.picture)) {
      formData.append('picture', body.picture);
    }
    if (!IsNullUndefinedBlank(body.email)) {
      formData.append('email', body.email);
    }
    formData.append('is_active', body.is_active);
    return this.API.post<GraduateProfile>(graduate_url + '/', formData);
  }

  putGraduate(id, body): Observable<GraduateProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('gender', body.gender);
    formData.append('date_birthday', body.date_birthday);
    formData.append('address', body.address);
    formData.append('company', body.company);
    formData.append('cui', body.cui);
    formData.append('web', body.web);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    if (!IsNullUndefinedBlank(body.email)) {
      formData.append('email', body.email);
    }
    if (!IsNullUndefinedBlank(body.picture)) {
      formData.append('picture', body.picture);
    }
    formData.append('is_active', body.is_active);
    var _url = `${graduate_url}/${id}`;
    return this.API.put<GraduateProfile>(_url, formData);
  }

  patchGraduate(id, body): Observable<GraduateProfile> {
    delete body.id;
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('gender', body.gender);
    formData.append('date_birthday', body.date_birthday);
    formData.append('address', body.address);
    formData.append('company', body.company);
    formData.append('cui', body.cui);
    formData.append('web', body.web);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    if (!IsNullUndefinedBlank(body.email)) {
      formData.append('email', body.email);
    }
    if (!IsNullUndefinedBlank(body.picture)) {
      formData.append('picture', body.picture);
    }
    formData.append('is_active', body.is_active);
    var _url = `${graduate_url}/${id}`;
    return this.API.patch<GraduateProfile>(_url, formData);
  }

  getAllSuperAdmin(): Observable<any> {
    var _url = `${super_admin_url}/`;
    return this.API.get<any>(_url);
  }

  getSuperAdmin(id): Observable<any> {
    let _url = `${super_admin_url}/${id}`;
    return this.API.get<any>(_url);
  }

  postCompany(body): Observable<any> {
    return this.API.post<any>(company_url + "/", body);
  }

  putCompany(id, body): Observable<CompanyProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('is_active', body.is_active);
    formData.append('company', body.company);
    var _url = `${company_url}/${id}`;
    return this.API.put<CompanyProfile>(_url, formData);
  }

  patchCompanyProfile(id, body): Observable<CompanyProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('is_active', body.is_active);
    formData.append('company', body.company);
    var _url = `${company_url}/update/${id}`;
    return this.API.patch<CompanyProfile>(_url, formData);
  }

  patchCompany(id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('link', data.link);
    if (typeof data.file != 'string') {
      formData.append('file', data.file);
    }
    formData.append('is_active', data.is_active);
    return this.API.patch(company_url + "/" +id, formData).pipe(
      map((res: any) => Object.assign({ id }, res))
    );
  }

  getCompany(id): Observable<any> {
    let _url = `${company_url}/${id}`;
    return this.API.get<any>(_url);
  }

  postSuperAdmin(body): Observable<SuperAdminProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('job_position', body.job_position);
    formData.append('type_employee', body.type_employee);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    if (!IsNullUndefinedBlank(body.picture)) {
      formData.append('picture', body.picture);
    }
    formData.append('is_active', body.is_active);
    return this.API.post<SuperAdminProfile>(super_admin_url + '/', formData);
  }

  putSuperAdmin(id, body): Observable<SuperAdminProfile> {
    var formData: any = new FormData();
    formData.append('user', body.user);
    formData.append('identificator', body.identificator);
    formData.append('job_position', body.job_position);
    formData.append('type_employee', body.type_employee);
    formData.append('document', body.document);
    formData.append('cell_phone', body.cell_phone);
    formData.append('phone', body.phone);
    if (!IsNullUndefinedBlank(body.picture)) {
      formData.append('picture', body.picture);
    }
    formData.append('is_active', body.is_active);
    var _url = `${super_admin_url}/${id}`;
    return this.API.put<SuperAdminProfile>(_url, formData);
  }

  patchSuperAdmin(id, body): Observable<SuperAdminProfile> {
    var _url = `${super_admin_url}/${id}`;
    return this.API.patch<SuperAdminProfile>(_url, body);
  }

  patchProfileSelf(body): Observable<Profile> {
    var _url = 'profile/update-current-profile/';
    return this.API.patch<Profile>(_url, body);
  }

  updateLogo(data: any): Observable<any> {
    var _url = 'profile/company/update-logo/auth/';
    var formData: any = new FormData();
    formData.append('logo', data);
    return this.API.put<any>(_url, formData);
  }

  updatePicture(data: any): Observable<any> {
    var _url = 'profile/update-picture/auth/';
    var formData: any = new FormData();
    formData.append('picture', data);
    return this.API.put<any>(_url, formData);
  }

  validateData(data: any): Observable<any>{
    let _url = 'profile/validate-data/';
    return this.API.post<any>(_url, data);
  }
}
