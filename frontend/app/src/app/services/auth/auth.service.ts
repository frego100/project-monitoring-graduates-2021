import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError, BehaviorSubject, Subject } from 'rxjs';
import { UserResponse, UserApply } from '@app/models/user';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
const helper = new JwtHelperService();

const base_api_url = environment.api + 'auth';

@Injectable({
  providedIn: 'root',
})

/*
  Methods of log in and log out.
  Methods related to authorization token
*/

export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
  }

  headers: HttpHeaders = new HttpHeaders({
    authorization: 'token ' + this.getToken(),
  });

  get isLogged(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  getToken() {
    return localStorage.getItem('token') as string;
  }

  login(authData: UserApply): Observable<UserResponse | void> {
    return this.http
      .post<UserResponse>(`${environment.api}/auth/`, authData)
      .pipe(
        map((res: UserResponse) => {
          this.saveToken(res.token);
          this.loggedIn.next(true);
          return res;
        })
      );
  }
  
  logout(): void {
    
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      this.loggedIn.next(false);

  }

  private saveToken(token: string): void {
    localStorage.setItem('token', token);
  }

}
