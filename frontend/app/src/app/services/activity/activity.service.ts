import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const url = 'activity/' as string;

@Injectable({
  providedIn: 'root',
})

/**
 * This service provides information on activities, and sending emails for activities
 *
 * @class ActivityService
 */
export class ActivityService {
  formControl = new Subject();
  activityControl = new BehaviorSubject(null);

  constructor(private API: BaseAPIService<any>) {}

  /**
   * Operation that returns all activities
   * @method getAll
   * @return Data of the activitys
   */
  getAll(limit, offset, status, search): Observable<any> {
    var _url = `${url}?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(status)) {
      _url = _url + `&status=${status}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    return this.API.get<any>(_url);
  }

  /**
   * Operation that returns my activities
   *
   * @method getMyActivities
   * @return Data of the activitys
   */
  getMyActivities(limit, offset, status, search): Observable<any> {
    var _url = `${url}my/?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(status)) {
      _url = _url + `&status=${status}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    return this.API.get<any>(_url);
  }

  getAllKindActivity() {
    return this.API.get<any>(url + 'types/');
  }

  /**
   * Operation that returns  activities pending for validation
   *
   * @method getActivityCreationRequest
   * @return Data of the activitys that are pending for validation
   */
  getActivityCreationRequest() {
    return this.API.get<any>(url + 'to_validate/');
  }

  /**
   * Operation that returns the detail of an activity
   *
   * @method getById
   * @param id identifier of activty
   * @return One activity for id
   */
  getById(id: String) {
    return this.API.get<any>(url + id);
  }

  /**
   * Operation that returns activity validate to client
   *
   * @method getActivityClient
   * @return Data of the activitys that are is_validate and is_active
   */
  getActivityClient() {
    return this.API.get<any>(url + 'public/');
  }

  /**
   * Operation that creates a new activity, formdata is used to send an image
   *
   * @method create
   * @param body information of activity
   * @return Aggregate activity data
   */
  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('title', data.title);
    formData.append('description', data.description);
    formData.append('start_date', data.start_date);
    formData.append('end_date', data.end_date);
    formData.append('link', data.link);
    formData.append('stream_link', data.stream_link);
    formData.append('meet_link', data.meet_link);
    formData.append('can_register', data.can_register);
    formData.append('image', data.image);
    formData.append('is_validated', data.is_validated);
    formData.append('status', data.status);
    formData.append('activity_type', data.activity_type);
    return this.API.post<any>(url, formData);
  }

  /**
   * Operation that edits an activity, formdata is used to send
   * an image, it is also verified that the image field is of
   * type File, if it is type String it is not sent,
   * in this method we only use patch
   *
   * @method update
   * @param data information of activity
   * @param _id identifier of activity
   * @return Edited activity data
   */
  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('title', data.title);
    formData.append('description', data.description);
    formData.append('start_date', data.start_date);
    formData.append('end_date', data.end_date);
    formData.append('link', data.link);
    formData.append('stream_link', data.stream_link);
    formData.append('meet_link', data.meet_link);
    formData.append('can_register', data.can_register);
    formData.append('activity_type', data.activity_type);
    formData.append('stream_link', data.stream_link);
    if (typeof data.image != 'string') {
      formData.append('image', data.image);
    }
    formData.append('is_validated', data.is_validated);
    formData.append('status', data.status);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }
  updateMyActivity(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('title', data.title);
    formData.append('description', data.description);
    formData.append('start_date', data.start_date);
    formData.append('end_date', data.end_date);
    formData.append('meet_link', data.meet_link);
    formData.append('link', data.link);
    formData.append('activity_type', data.activity_type);
    formData.append('can_register', data.can_register);
    formData.append('stream_link', data.stream_link);
    if (typeof data.image != 'string') {
      formData.append('image', data.image);
    }
    formData.append('is_validated', data.is_validated);
    formData.append('status', data.status);
    return this.API.patch(url + 'my/' + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }
  /**
   * Operation that deactivates, logically eliminates an activity
   *
   * @method remove
   * @param id identifier of activity
   * @return Deleted activity data
   */
  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  /**
   * Email sending operation to notify activity
   *
   * @method notify
   * @param object mail list
   * @return No response, but if it is correct it returns 200
   */
  notify(object) {
    return this.API.post<any>(url + 'notify/', object);
  }
  notifyGroup(object) {
    return this.API.post<any>(url + 'notify-group/', object);
  }

  /**
   * Operation that deactivates, logically eliminates an activity
   *
   * @method enroll
   * @param id identifier of activity
   * @return Deleted activity data
   */
  enroll(id: string): Observable<any> {
    return this.API.post(url + 'enroll/' + id, {});
  }

  /**
   * Operation that enroll user in activity
   *
   * @method enrollActivityUser
   * @param body is data about activity and user
   * @return activity user data
   */
  enrollActivityUser(body): Observable<any> {
    return this.API.post(url + 'user/', body);
  }

  /**
   * Operation that enroll user in activity
   *
   * @method enrollActivityUser
   * @param body is data about activity and user
   * @return activity user data
   */
  enrollActivityMultipleUser(body): Observable<any> {
    return this.API.post(url + 'user-multiple/', body);
  }
  getAllObjectiveGroup(id: string): Observable<any> {
    return this.API.get(url + 'group_objective/' + '?activity=' + id);
  }
  createObjectiveGroup(data: any): Observable<any> {
    return this.API.post(url + 'group_objective/', data);
  }
  validateActivity(id: string, data: any) {
    return this.API.put<any>(url + 'validate/' + id, data);
  }
  invalidateActivity(id: string, data: any) {
    return this.API.put<any>(url + 'invalidate/' + id, data);
  }
  listUsers() {
    return this.API.get<any>(url + 'user');
  }
}
