import { Injectable } from '@angular/core';
import { BaseAPIService } from '@app/core';
import { Poll } from '@app/models/poll';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
const url = 'poll/' as string;
@Injectable({
  providedIn: 'root'
})
export class PollService {
  pollControl = new BehaviorSubject(null);
  formControl = new Subject();

  constructor(private API: BaseAPIService<Poll>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true');
  }
  getList(
    limit,
    offset,
    filter1,
    search,
    ordering_list: Map<string, string>
  ): Observable<any> {
    var _url = `${url}?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(filter1)) {
      _url = _url + `&filter1=${filter1}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    if (!IsNullUndefinedBlank(ordering_list)) {
      ordering_list.forEach((value: string) => {
        if (!IsNullUndefinedBlank(value)) {
          _url = _url + `&ordering=${value}`;
        }
      });
    }
    return this.API.get<any>(_url);
  }
  getPublics(): Observable<any> {
    return this.API.get<any>(url + 'public/');
  }

  getById(id: String) {
    return this.API.get<Poll>(url + id);
  }
  
  create(data: any): Observable<any> {
    delete data.id;

    return this.API.post<any>(url, data);
  }

  update(_id: string, data: any): Observable<any> {
    
    return this.API.patch(url + _id, data).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  notify(object) {
    return this.API.post<any>(url + 'notify/', object);
  }
}
