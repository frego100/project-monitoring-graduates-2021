import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Observable } from 'rxjs';
import { History } from '@app/models/history';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { Identifier } from '@app/models/identifier';

const url = 'identificator' as string;

@Injectable({
  providedIn: 'root',
})

/**
 * This service provides information on user permissions and profiling
 *
 * @class AuthUserService
 */
export class IdentifierService {
  constructor(private API: BaseAPIService<History>) {}

  /**
   * Operation that response with filters and pagination about identifiers
   *
   * @method getList
   * @param ordering_list its hashmap of the ordering
   * @param limit page size
   * @param offset start item page
   * @param therest diferent filters
   * @return Data of the identifiers
   */
  getList(
    limit,
    offset,
    filter1,
    search,
    ordering_list: Map<string, string>
  ): Observable<any> {
    var _url = `${url}/?limit=${limit}&offset=${offset}` + '&is_active=true';
    if (!IsNullUndefinedBlank(filter1)) {
      _url = _url + `&filter1=${filter1}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    if (!IsNullUndefinedBlank(ordering_list)) {
      ordering_list.forEach((value: string) => {
        if (!IsNullUndefinedBlank(value)) {
          _url = _url + `&ordering=${value}`;
        }
      });
    }
    return this.API.get<any>(_url);
  }

  /**
   * Operation specific identifier
   *
   * @method getPost
   * @return get data about one identifier
   */
  get(id): Observable<Identifier> {
    var _url = `${url}/${id}`;
    return this.API.get<Identifier>(_url);
  }

  /**
   * Operation specific identifier
   *
   * @method post
   * @return get data about one identifier
   */
  post(body): Observable<Identifier> {
    return this.API.post<Identifier>(url + '/', body);
  }

  /**
   * Operation specific identifier
   *
   * @method put
   * @return get data about one identifier
   */
  put(id, body): Observable<Identifier> {
    var _url = `${url}/${id}`;
    return this.API.put<Identifier>(_url, body);
  }

  /**
   * Operation specific identifier
   *
   * @method delete
   * @return delete data about one identifier
   */
  delete(id): Observable<Identifier> {
    return this.API.delete<Identifier>(`${url}/${id}`);
  }

  /**
   * Operation specific identifier
   *
   * @method patch
   * @return get data about one identifier
   */
  patch(id, body): Observable<Identifier> {
    var _url = `${url}/${id}`;
    return this.API.patch<Identifier>(_url, body);
  }

  /**
   * Operation list user lite
   * IMPORTANT, This method is temporary, just for development.
   *
   * @method getUserLiteforHistory
   * @param groups filter user for groups
   * @return Simple Data of the  users
   */
  getIdentifierLiteforHistory(is_active): Observable<any> {
    const _url = url + `?is_active=${is_active}`;
    return this.API.get<any>(_url);
  }
  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }
}
