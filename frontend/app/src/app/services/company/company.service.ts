import { Injectable } from '@angular/core';
import { Company } from '@app/models/company';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { BaseAPIService } from '../../core/base-api.service';

const url = 'company/' as string;

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  companyControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<Company>) {}

  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('identificator', data.identificator);
    formData.append('company_name', data.company_name);
    formData.append('social_reason', data.social_reason);
    formData.append('ruc', data.ruc);
    formData.append('company_activity', data.company_activity);
    formData.append('logo', data.logo);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }

  getActive(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true&limit=500');
  }

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }

  getById(id: String) {
    return this.API.get<Company>(url + id);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('identificator', data.identificator);
    formData.append('company_name', data.company_name);
    formData.append('social_reason', data.social_reason);
    formData.append('ruc', data.ruc);
    formData.append('company_activity', data.company_activity);
    formData.append('logo', data.logo);
    formData.append('is_active', data.is_active);

    return this.API.put(url + _id, formData);

  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }
}
