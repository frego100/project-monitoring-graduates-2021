import { Injectable } from '@angular/core';
import { Jobs } from '@app/models/job';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';

const url = 'job-position/' as string;

@Injectable({
  providedIn: 'root',
})
export class JobsService {
  jobsControl = new BehaviorSubject(null);
  formControl = new Subject();

  constructor(private API: BaseAPIService<Jobs>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true');
  }
  
  getById(id: String) {
    return this.API.get<Jobs>(url + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  notify(object) {
    return this.API.post<any>(url + 'notify/', object);
  }
}
