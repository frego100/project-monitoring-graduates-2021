import { Injectable } from '@angular/core';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
import {Announcement} from "@models/announcement";
const url = 'announcements/' as string;
const urlAuth ='announcements/postulate/';
const urlMy ='announcements/my/';
@Injectable({
  providedIn: 'root',
})
export class AnnouncementService {
  annoucementControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }
  getMyAnnouncements(): Observable<any> {
    return this.API.get<any>(urlMy);
  }
  getUserListMyAnnoucements(id) {
    return this.API.get<any>(url + "postulates/" + id)
  }
  getAnnouncementsValidated(): Observable<any> {
    return this.API.get<any>(url+"?is_validated=true");
  }
  getAnnouncementsCreationRequest(): Observable<any> {
    return this.API.get<any>(url+"?is_validated=false");
  }

  get(id): Observable<any> {
    var _url = `${url}/${id}`;
    return this.API.get<any>(_url);
  }
  getById(id: String) {
    return this.API.get<Announcement>(url + id);
  }
  getAllObjectiveGroup(id: string): Observable<any> {
    return this.API.get(url + 'group_objective/'+'?announcement='+id);
  }
  getMyApplications(): Observable<any> {
    return this.API.get<any>(url + 'postulate/');
  }
  registerAnnouncemenetAuth(body){
    return this.API.post<any>(urlAuth, body);
  }
  createAnnoucement(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('date_start', data.date_start);
    formData.append('date_end', data.date_end);
    formData.append('status', data.status);
    formData.append('is_validated', data.is_validated);
    formData.append('company_pk', data.company_pk);
    formData.append('extra_file', data.extra_file);
    return this.API.post<any>(url, formData);
  }
  notifyGroup(object) {
    return this.API.post<any>(url + 'notify-group/', object);
  }
  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('text_invitation', data.text_invitation);
    formData.append('instance', data.instance);
    formData.append('announcement', data.announcement);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }

  post(body): Observable<any> {
    return this.API.post<any>(url + '/', body);
  }

  put(id, body): Observable<any> {
    var _url = `${url}/${id}`;
    return this.API.put<any>(_url, body);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('date_start', data.date_start);
    formData.append('date_end', data.date_end);
    formData.append('status', data.status);
    formData.append('is_validated', data.is_validated);
    formData.append('company_pk', data.company_pk);
    formData.append('extra_file', data.extra_file);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  removeMyApplication(id: string): Observable<any> {
    return this.API.delete(url + 'quit/' + id);
  }

  getList(
    limit,
    offset,
    filter1,
    search,
    ordering_list: Map<string, string>
  ): Observable<any> {
    var _url = `${url}?limit=${limit}&offset=${offset}` + '&is_active=true';
    if (!IsNullUndefinedBlank(filter1)) {
      _url = _url + `&filter1=${filter1}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    if (!IsNullUndefinedBlank(ordering_list)) {
      ordering_list.forEach((value: string) => {
        if (!IsNullUndefinedBlank(value)) {
          _url = _url + `&ordering=${value}`;
        }
      });
    }
    return this.API.get<any>(_url);
  }

  getTargetGroupLite(is_active): Observable<any> {
    const _url = url + `?is_active=${is_active}`;
    return this.API.get<any>(_url);
  }
  validateAnnoucement(id: string, data: any) {
    return this.API.post<any>(url + 'validate_email/' + id, data);
  }

}
