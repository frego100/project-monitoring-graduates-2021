import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { map } from "rxjs/operators";

const url = 'group/content-type-profile/' as string;
@Injectable({
  providedIn: 'root'
})
export class ContentTypeProfileService{
  formControl = new Subject();
  rolesControl = new BehaviorSubject(null);

  constructor(private API: BaseAPIService<any>) { }

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }
  
  create(data:any): Observable<any> {
    delete data.id;
    return this.API.post<any>(url, data);

  }
  getById(id:String){
    return this.API.get<any>(url+id);
  }

  update(_id: string, data: any): Observable<any> {
    const body = Object.assign({}, data);
    return this.API.put(url+_id+'/', body).pipe(map((res: any ) => Object.assign({ _id }, res)));
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }
}
