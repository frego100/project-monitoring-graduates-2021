import { Injectable } from '@angular/core';
import { getItemNameinArraybyId } from '@app/shared/helpers/general-utils';

@Injectable({
  providedIn: 'root',
})
export class GraduateExtrasService {

  private projectListType = [
    { id: 1, name: 'Desarrollo Experimental' },
    { id: 2, name: 'Desarrollo Tecnológico' },
    { id: 3, name: 'Investigación aplicada' },
    { id: 4, name: 'Investigación Básica' }
  ];

  private projectListRole = [
    {id:1, name:'Investigador principal'},
    {id:2, name:'Investigador Asociado'},
    {id:3, name:'Investigador Colaborador'},
    {id:4, name:'Desarrollador Tecnológico'},
    {id:5, name:'Técnico'},
    {id:6, name:'Innovador'},
    {id:7, name:'Emprendedor'},
    {id:8, name:'Otros'}
  ];

  constructor() {}

  getTypesProject(): any {
    return this.projectListType;
  }

  getTypesRole(): any {
    return this.projectListRole;
  }

  getItemTypeProjectbyId(_id){
    return getItemNameinArraybyId(_id, this.projectListType);
  }

  getItemTypeRolebyId(_id){
    return getItemNameinArraybyId(_id, this.projectListRole);
  }
}
