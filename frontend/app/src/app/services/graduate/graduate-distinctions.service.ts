import { Injectable } from '@angular/core';
import { GraduateDistinction } from '@app/models/graduate';
import { PaginationResponse } from '@app/models/pagination';
import { Observable } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const url_administration = 'distinctions/' as string;
const url_auth = 'distinctions/auth/' as string;

@Injectable({
  providedIn: 'root',
})

export class GraduateDistinctionService {

  private url = url_administration;

  constructor(private API: BaseAPIService<any>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration
    } else {
      this.url = url_auth
    }
  }


  /**
   * Operation that response with filters and pagination about Graduate Distinction
   * 
   * @method getList
   * @param limit page size
   * @param offset start item page
   * @param graduate_profile_pk graduate profile
   * @param country country
   * @return Data of the Graduate's Distinctions
   */
  getList(limit: number, offset: number, graduate_profile_pk, country: string): Observable<PaginationResponse<GraduateDistinction>> {
    var _url = `${this.url}?limit=${limit}&offset=${offset}`
    if (!IsNullUndefinedBlank(graduate_profile_pk)) {
        _url = _url + `&graduate_profile_pk=${graduate_profile_pk}`;
    }
    if (!IsNullUndefinedBlank(country)) {
        _url = _url + `&country=${country}`;
    }
    return this.API.get<PaginationResponse<GraduateDistinction>>(_url);
  }

  /**
   * Operation add specific Graduate Distinction
   * 
   * @method post
   * @return returns aggregate record data 
   */
  post (body: any): Observable<GraduateDistinction> {
    return this.API.post<GraduateDistinction>(this.url,body);
  }

  /**
   * Operation get specific Graduate Distinction
   * 
   * @method get
   * @return returns requested record data
   */
   get (id: number): Observable<GraduateDistinction> {
      return this.API.get<GraduateDistinction>(`${this.url}${id}`);
   }

   /**
    * Operation update specific Graduate Distinction
    * 
    * @method put
    * @return returns data from the edited record
    */
    put (id: number, body: any): Observable<GraduateDistinction> {
       return this.API.put<GraduateDistinction>(`${this.url}${id}`,body);
    }

    /**
     * Operation update specific Graduate Distinction
     * 
     * @method patch
     * @return returns data from the edited record
     */
     patch (id: number, body: any): Observable<GraduateDistinction> {
        return this.API.patch<GraduateDistinction>(`${this.url}${id}`,body);
     }

     /**
      * Operation deleted specific Graduate Distinction
      * 
      * @method delete
      * @return no return data
      */
      delete (id: number): Observable<any> {
         return this.API.delete<any>(`${this.url}${id}`);
      }
}
