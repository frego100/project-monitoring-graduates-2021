import { Injectable } from '@angular/core';
import { Grade, GraduateAchievement } from '@app/models/graduate';
import { PaginationResponse } from '@app/models/pagination';
import { Observable } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const url_administration = 'achievements/' as string;
const url_auth = 'achievements/auth/' as string;

@Injectable({
  providedIn: 'root',
})
export class GraduateAchievementService {
  private url = url_administration;

  constructor(private API: BaseAPIService<any>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration;
    } else {
      this.url = url_auth;
    }
  }

  /**
   * Operation that response with filters and pagination about Graduate Achievement
   *
   * @method getList
   * @param limit page size
   * @param offset start item page
   * @param graduate_profile_pk graduate profile
   * @param country country
   * @return Data of the Graduate's Achievements
   */
  getList(
    limit: number,
    offset: number,
    graduate_profile,
    country: string
  ): Observable<PaginationResponse<GraduateAchievement>> {
    var _url = `${this.url}?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(graduate_profile)) {
      _url = _url + `&graduate_profile_pk=${graduate_profile}`;
    }
    if (!IsNullUndefinedBlank(country)) {
      _url = _url + `&country=${country}`;
    }
    return this.API.get<PaginationResponse<GraduateAchievement>>(_url);
  }

  /**
   * Operation add specific Graduate Achievement
   *
   * @method post
   * @return returns aggregate record data
   */
  post(body: any): Observable<GraduateAchievement> {
    return this.API.post<GraduateAchievement>(this.url, body);
  }

  /**
   * Operation get specific Graduate Achievement
   *
   * @method get
   * @return returns requested record data
   */
  get(id: number): Observable<GraduateAchievement> {
    return this.API.get<GraduateAchievement>(`${this.url}${id}`);
  }

  /**
   * Operation update specific Graduate Achievement
   *
   * @method put
   * @return returns data from the edited record
   */
  put(id: number, body: any): Observable<GraduateAchievement> {
    return this.API.put<GraduateAchievement>(`${this.url}${id}`, body);
  }

  /**
   * Operation update specific Graduate Achievement
   *
   * @method patch
   * @return returns data from the edited record
   */
  patch(id: number, body: any): Observable<GraduateAchievement> {
    return this.API.patch<GraduateAchievement>(`${this.url}${id}`, body);
  }

  /**
   * Operation deleted specific Graduate Achievement
   *
   * @method delete
   * @return no return data
   */
  delete(id: number): Observable<any> {
    return this.API.delete<any>(`${this.url}${id}`);
  }

  /**
   * Operation get specific Graduate Achievement
   *
   * @method get
   * @return returns requested record data
   */
  getGrades(): Observable<Grade[]> {
    return this.API.get<Grade[]>(`${url_administration}grades/`);
  }
}
