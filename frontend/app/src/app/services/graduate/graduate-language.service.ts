import { Injectable } from '@angular/core';
import { GraduateLanguage } from '@app/models/graduate';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { PaginationResponse } from '@app/models/pagination';
import { Language } from '@app/models/language';

const url_administration = 'language/' as string;
const url_auth = 'language/auth/' as string;

@Injectable({
  providedIn: 'root',
})
export class GraduateLanguageService {
  private url = url_administration;
  jobsControl = new BehaviorSubject(null);
  formControl = new Subject();

  constructor(private API: BaseAPIService<GraduateLanguage>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration;
    } else {
      this.url = url_auth;
    }
  }

  getAllAuth(): Observable<any> {
    return this.API.get<any>(url_auth);
  }
  allLenguages():Observable<any>{
      return this.API.get<Language>('language/allLanguages/');
   }
  getById(id: String) {
    return this.API.get<GraduateLanguage>(this.url + id);
  }

  create(data: any): Observable<any> {
    return this.API.post<GraduateLanguage>(this.url,data);
  }
  delete(id:number): Observable<any> {
    return this.API.deleteRegister<GraduateLanguage>(this.url,id);
  }


  update(_id: string, data: any): Observable<any> {
    delete data.id;
     return this.API.put(this.url + _id, data);
  }

  remove(id: string): Observable<any> {
    return this.API.delete(this.url + id);
  }

  /**
   * Operation that response with filters and pagination about Graduate Data Academic
   * 
   * @method getList
   * @param limit page size
   * @param offset start item page
   * @param graduate_profile_pk graduate profile
   * @param is_student is_student
   * @param is_graduate is_graduate
   * @return Data of the Graduate's Data Academic
   */
  getList(limit: number, offset: number, graduate_profile_pk, is_student: boolean, is_graduate: boolean): Observable<PaginationResponse<GraduateLanguage>> {
    var _url = `${this.url}?limit=${limit}&offset=${offset}`
    if (!IsNullUndefinedBlank(graduate_profile_pk)) {
        _url = _url + `&graduate_profile_pk=${graduate_profile_pk}`;
    }
    if (!IsNullUndefinedBlank(is_student)) {
        _url = _url + `&is_student=${is_student}`;
    }
    if (!IsNullUndefinedBlank(is_graduate)) {
        _url = _url + `&is_graduate=${is_graduate}`;
    }
    return this.API.get<PaginationResponse<GraduateLanguage>>(_url);
  }
}
