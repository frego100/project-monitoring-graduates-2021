import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { PaginationResponse } from '@app/models/pagination';
import { Graduate } from '@app/models/graduate';

const url = 'profile/graduate-user/' as string;

@Injectable({
  providedIn: 'root',
})
export class GraduateListService {

  constructor(private API: BaseAPIService<PaginationResponse<Graduate>>) {}

  /**
   * Operation that response with filters and pagination about Graduate
   * 
   * @method getList
   * @param limit page size
   * @param offset start item page
   * @return Data of the Graduates
   */
  getList(
      limit: number, offset: number, professional_program_id: number, 
      area_id: number, faculty_id: number, user__username: string, 
      user__email: string, cui: string, search: any, not_in_activity_id: number): Observable<PaginationResponse<Graduate>> {
    var _url = `${url}?limit=${limit}&offset=${offset}`
    if (!IsNullUndefinedBlank(professional_program_id)) {
        _url = _url + `&professional_program_id=${professional_program_id}`;
    }
    if (!IsNullUndefinedBlank(area_id)) {
        _url = _url + `&area_id=${area_id}`;
    }
    if (!IsNullUndefinedBlank(faculty_id)) {
        _url = _url + `&faculty_id=${faculty_id}`;
    }
    if (!IsNullUndefinedBlank(user__email)) {
        _url = _url + `&user__email=${user__email}`;
    }
    if (!IsNullUndefinedBlank(user__username)) {
        _url = _url + `&user__username=${user__username}`;
    }
    if (!IsNullUndefinedBlank(cui)) {
        _url = _url + `&cui=${cui}`;
    }
    if (!IsNullUndefinedBlank(not_in_activity_id)) {
        _url = _url + `&not_in_activity_id=${not_in_activity_id}`;
    }
    if (!IsNullUndefinedBlank(search)) {
        _url = _url + `&search=${search}`;
    }
    return this.API.get<PaginationResponse<Graduate>>(_url);
  }
}
