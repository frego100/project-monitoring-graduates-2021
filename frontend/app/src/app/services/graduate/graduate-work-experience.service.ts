import { Injectable } from '@angular/core';
import {GraduateWorkExperience} from '@app/models/graduate';
import { PaginationResponse } from '@app/models/pagination';
import { GraduateWorkExperienceComponent } from '@app/pages/admin/profiles/form-graduate/graduate-work-experience/graduate-work-experience.component';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';

const url_administration = 'work-experience/' as string;
const url_auth = 'work-experience/auth/' as string;

@Injectable({
  providedIn: 'root',
})
export class GraduateWorkExperienceService {
  private url = url_administration;
  workExperienceControl = new BehaviorSubject(null);
  formControl = new Subject();

  constructor(private API: BaseAPIService<GraduateWorkExperience>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration;
    } else {
      this.url = url_auth;
    }
  }

  getList(
    limit: number,
    offset: number,
    graduate_profile,
    country: string
  ): Observable<PaginationResponse<GraduateWorkExperience>> {
    var _url = `${this.url}?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(graduate_profile)) {
      _url = _url + `&graduate_profile_pk=${graduate_profile}`;
    }
    if (!IsNullUndefinedBlank(country)) {
      _url = _url + `&country=${country}`;
    }
    return this.API.get<PaginationResponse<GraduateWorkExperience>>(_url);
  }

  getAllAuth(): Observable<any> {
    return this.API.get<any>(this.url);
  }

  getById(id: String) {
    return this.API.get<GraduateWorkExperience>(this.url + id);
  }

  create(data: any): Observable<any> {
    return this.API.post<GraduateWorkExperience>(this.url,data);
  }
  delete(id:number): Observable<any> {
    return this.API.deleteRegister<GraduateWorkExperience>(this.url,id);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
     return this.API.put(this.url + _id, data);
  }

  remove(id: string): Observable<any> {
    return this.API.delete(this.url + id);
  }

  notify(object) {
    return this.API.post<any>(this.url + 'notify/', object);
  }
}
