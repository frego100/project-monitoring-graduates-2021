import { Injectable } from '@angular/core';
import { GraduateProject } from '@app/models/graduate';
import { PaginationResponse } from '@app/models/pagination';
import { Observable } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const url_administration = 'projects/' as string;
const url_auth = 'projects/auth/' as string;

@Injectable({
  providedIn: 'root',
})

export class GraduateProjectService {

  private url = url_administration;

  constructor(private API: BaseAPIService<any>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration
    } else {
      this.url = url_auth
    }
  }


  /**
   * Operation that response with filters and pagination about GraduateProject
   * 
   * @method getList
   * @param limit page size
   * @param offset start item page
   * @param graduate_profile_pk graduate profile
   * @return Data of the GraduateProject
   */
  getList(limit: number, offset: number, graduate_profile_pk): Observable<PaginationResponse<GraduateProject>> {
    var _url = `${this.url}?limit=${limit}&offset=${offset}`
    if (!IsNullUndefinedBlank(graduate_profile_pk)) {
        _url = _url + `&graduate_profile_pk=${graduate_profile_pk}`;
    }
    return this.API.get<PaginationResponse<GraduateProject>>(_url);
  }

  /**
   * Operation add specific GraduateProject
   * 
   * @method post
   * @return returns aggregate record data 
   */
  post (body: GraduateProject): Observable<GraduateProject> {
    return this.API.post<GraduateProject>(this.url,body);
  }

  /**
   * Operation get specific GraduateProject
   * 
   * @method get
   * @return returns requested record data
   */
   get (id: number): Observable<GraduateProject> {
      return this.API.get<GraduateProject>(`${this.url}${id}`);
   }

   /**
    * Operation update specific GraduateProject
    * 
    * @method put
    * @return returns data from the edited record
    */
    put (id: number, body: any): Observable<GraduateProject> {
       return this.API.put<GraduateProject>(`${this.url}${id}`,body);
    }

    /**
     * Operation update specific GraduateProject
     * 
     * @method patch
     * @return returns data from the edited record
     */
     patch (id: number, body: any): Observable<GraduateProject> {
        return this.API.patch<GraduateProject>(`${this.url}${id}`,body);
     }

     /**
      * Operation deleted specific GraduateProject
      * 
      * @method delete
      * @return no return data
      */
      delete (id: number): Observable<any> {
         return this.API.delete<any>(`${this.url}${id}`);
      }
}
