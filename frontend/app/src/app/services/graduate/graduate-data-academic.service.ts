import { Injectable } from '@angular/core';
import { GraduateDataAcademic } from '@app/models/graduate';
import { PaginationResponse } from '@app/models/pagination';
import { Observable } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

const url_administration = 'academic/' as string;
const url_auth = 'academic/auth/' as string;

@Injectable({
  providedIn: 'root',
})
export class GraduateDataAcademicService {
  private url = url_administration;

  constructor(private API: BaseAPIService<any>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration;
    } else {
      this.url = url_auth;
    }
  }

  /**
   * Operation that response with filters and pagination about Graduate Data Academic
   *
   * @method getList
   * @param limit page size
   * @param offset start item page
   * @param graduate_profile_pk graduate profile
   * @param is_student is_student
   * @param is_graduate is_graduate
   * @return Data of the Graduate's Data Academic
   */
  getList(
    limit: number,
    offset: number,
    graduate_profile_pk,
    is_student: boolean,
    is_graduate: boolean
  ): Observable<PaginationResponse<GraduateDataAcademic>> {
    var _url = `${this.url}?limit=${limit}&offset=${offset}`;
    if (!IsNullUndefinedBlank(graduate_profile_pk)) {
      _url = _url + `&graduate_profile_pk=${graduate_profile_pk}`;
    }
    
    if (!IsNullUndefinedBlank(is_student)) {
      _url = _url + `&is_student=${is_student}`;
    }
    if (!IsNullUndefinedBlank(is_graduate)) {
      _url = _url + `&is_graduate=${is_graduate}`;
    }
    return this.API.get<PaginationResponse<GraduateDataAcademic>>(_url);
  }

  /**
   * Operation add specific Graduate Data Academic
   *
   * @method post
   * @return returns aggregate record data
   */
  post(
    body: GraduateDataAcademic
  ): Observable<GraduateDataAcademic> {
    
    return this.API.post<GraduateDataAcademic>(this.url, body);
  }

  /**
   * Operation get specific Graduate Data Academic
   *
   * @method get
   * @return returns requested record data
   */
  get(id: number): Observable<GraduateDataAcademic> {
    return this.API.get<GraduateDataAcademic>(`${this.url}${id}`);
  }

  /**
   * Operation update specific Graduate Data Academic
   *
   * @method put
   * @return returns data from the edited record
   */
  put(id: number, body: any): Observable<GraduateDataAcademic> {
    
    return this.API.put<GraduateDataAcademic>(`${this.url}${id}`, body);
  }

  /**
   * Operation update specific Graduate Data Academic
   *
   * @method patch
   * @return returns data from the edited record
   */
  patch(id: number, body: any): Observable<GraduateDataAcademic> {
    
    return this.API.patch<GraduateDataAcademic>(`${this.url}${id}`, body);
  }

  /**
   * Operation deleted specific Graduate Data Academic
   *
   * @method delete
   * @return no return data
   */
  delete(id: number): Observable<any> {
    return this.API.delete<any>(`${this.url}${id}`);
  }
}
