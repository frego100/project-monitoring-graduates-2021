import { Injectable } from '@angular/core';
import {GraduatePublications} from '@app/models/graduate';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';

const url_administration = 'publications/' as string;
const url_auth = 'publications/auth/' as string;

@Injectable({
  providedIn: 'root',
})
export class GraduatePublicationsService {
  private url = url_administration;
  publicationsControl = new BehaviorSubject(null);
  formControl = new Subject();

  constructor(private API: BaseAPIService<GraduatePublications>) {}

  set(auth: boolean) {
    if (!auth) {
      this.url = url_administration;
    } else {
      this.url = url_auth;
    }
  }

  getAll(graduate_profile_pk):Observable<any>{
    var _url = url_administration
    if (!IsNullUndefinedBlank(graduate_profile_pk)) {
      _url = _url + `?graduate_profile_pk=${graduate_profile_pk}`;
    }
    return this.API.get<any>(_url);
  }
  getAllAuth(): Observable<any> {
    return this.API.get<any>(url_auth);
  }
  getAllTypePublications():Observable<any>{
    return this.API.get<any>(url_administration+"type_publication/");
  }
  getAllFunctionType():Observable<any>{
    return this.API.get<any>(url_administration+"function_type/");
  }

  getById(id: String) {
    return this.API.get<GraduatePublications>(this.url + id);
  }

  create(data: any): Observable<any> {
    return this.API.post<GraduatePublications>(this.url , data);
  }

  delete(id:number): Observable<any> {
    return this.API.deleteRegister<GraduatePublications>(this.url , id);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
     return this.API.put(this.url + _id, data);
  }

  remove(id: string): Observable<any> {
    return this.API.delete(this.url + id);
  }

  notify(object) {
    return this.API.post<any>(this.url + 'notify/', object);
  }
}
