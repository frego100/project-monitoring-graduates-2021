import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';

const url = 'permission' as string;

@Injectable({
  providedIn: 'root',
})
export class PermissionService {
  formControl = new Subject();
  activityControl = new BehaviorSubject(null);

  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }

  getById(id: String) {
    return this.API.get<any>(url + '/' + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    return this.API.post<any>(url + '/', data);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    return this.API.patch(url + '/' + _id, data).pipe(map((res: any) => Object.assign({ _id }, res)));
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + '/' + id);
  }
}
