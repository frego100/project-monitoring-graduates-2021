import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from "@env/environment";
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  baseUrl = environment.api
  userRegister: any;
  userRegisterBack: any;
  constructor(private http: HttpClient) {}

  getActivities(){
    return this.http.get<any>(`${this.baseUrl}/activity/public/`);
  }
  getRoles(){
    return this.http.get<any>(`${this.baseUrl}/group/public/`);
  }
  getIdentificator(){
    return this.http.get<any>(`${this.baseUrl}/identificator/`);
  }
  getJobs(){
    return this.http.get<any>(`${this.baseUrl}/job-position/`+'?is_active=true');
  }
  getTypeEmployee(){
    return this.http.get<any>(`${this.baseUrl}/employee/type-employee/`+'?is_active=true');
  }
  getPrograms(){
    return this.http.get<any>(`${this.baseUrl}/instance/professional-program/`+'?is_active=true');
  }
  postGraduate(body):Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/profile/graduate/`,body);
  }
  postCompany(body):Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/profile/company/`,body);
  }
  postAdmin(body):Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/profile/admin/`,body);
  }
  validateData(body):Observable<any>{
    return this.http.post<any>(`${this.baseUrl}/profile/validate-data/`,body);
  }
  setUserPreRegister(data) {
    this.userRegister = data;
  }
  setUserPreRegisterBack(data) {
    this.userRegisterBack = data;
  }
  getUserPreRegister() {
    return this.userRegister;
  }
  getUserPreRegisterBack() {
    return this.userRegisterBack;
  }
  preRegisterUser(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/user/create-pre-registration/`, data);
  }
  forgotPassword(data: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/reset_password/`, data);
  }
  resetPassword(data: any, uuid: string): Observable<any> {
    return this.http.put<any>(`${this.baseUrl}/reset_password/${uuid}`, data);
  }
}
