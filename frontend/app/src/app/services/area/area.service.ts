import { Injectable } from '@angular/core';
import { Area } from '@app/models/area';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
import { HttpParams } from '@angular/common/http';

const url = 'instance/area' as string;

@Injectable({
  providedIn: 'root',
})
export class AreaService {
  areaControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<Area>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url+'?is_active=true');
  }

  getLite(): Observable<any> {
    return this.API.get<any>(url+'/lite/');
  }

  getAllEnabled(): Observable<any> {
    let params = new HttpParams().set('is_active', true);
    return this.API.getWithParams<any>(url , params);
  }

  getById(id: String) {
    return this.API.get<Area>(url + '/' + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    return this.API.patch(url + '/' + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + '/' + id);
  }

  notify(object) {
    return this.API.post<any>(url + '/' + 'notify/', object);
  }
}
