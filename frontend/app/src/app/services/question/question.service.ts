import { Injectable } from '@angular/core';
import { Area } from '@app/models/area';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
import { HttpParams } from '@angular/common/http';

const url = 'question/' as string;
const urlAuth = 'graduate-questions/auth/' as string;

@Injectable({
  providedIn: 'root',
})
export class QuestionService {
  questionControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }
  getAllActive(): Observable<any> {
    return this.API.get<any>(url+"?is_active=true");
  }
  getAllGraduate(): Observable<any> {
    return this.API.get<any>(urlAuth);
  }

  getById(id: String) {
    return this.API.get<any>(url + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    return this.API.post<any>(url, data);
  }
  update(_id: string, data: any): Observable<any> {
    delete data.id;
    return this.API.put<any>(url + _id, data);
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }
}
