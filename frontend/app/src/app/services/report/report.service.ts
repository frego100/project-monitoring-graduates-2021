import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Observable } from 'rxjs';

const url_profiles = 'profile/company/report/' as string;
const url_reports = 'report/' as string;

@Injectable({
  providedIn: 'root',
})
export class ReportService {

  constructor(private API: BaseAPIService<any>) {}

  /**
   * Operation report xlsx file to companies profile
   *
   * @method getReportCompany
   * @return BinaryType File report
   */
  getReportCompany (data): Observable<any> {
    return this.API.postBlob(url_profiles, data);
  }

  /**
   * Operation report xlsx file to graduate profile
   *
   * @method getReportGraduate
   * @return BinaryType File report
   */
  getReportGraduate (data): Observable<any> {
    return this.API.postBlob(url_reports + 'graduate/', data);
  }

  /**
   * Operation report xlsx file to admin profile
   *
   * @method getReportAdmin
   * @return BinaryType File report
   */
  getReportAdmin (data): Observable<any> {
    return this.API.postBlob(url_reports + 'admin/', data);
  }

  /**
   * Operation report xlsx file to super admin profile
   *
   * @method getReportSuperAdmin
   * @return BinaryType File report
   */
  getReportSuperAdmin (data): Observable<any> {
    return this.API.postBlob(url_reports + 'superadmin/', data);
  }
}
