import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { HttpParams } from '@angular/common/http';
import {Institution} from "@models/institution";
//OJO CON EL "/"
const url = 'institution' as string;
@Injectable({
  providedIn: 'root',
})
export class InstitutionService {
  institutionControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<Institution>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url+"/");
  }

  getAllEnabled(): Observable<any> {
    let params = new HttpParams().set('is_active', true);
    return this.API.getWithParams<any>(url , params);
  }

  getById(id: String) {
    return this.API.get<Institution>(url + '/' + id);
  }
  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('identificator', data.identificator);
    formData.append('institution_name', data.institution_name);
    formData.append('social_reason', data.social_reason);
    formData.append('ruc', data.ruc);
    formData.append('institution_activity', data.institution_activity);
    formData.append('logo', data.logo);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url +"/", formData);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('identificator', data.identificator);
    formData.append('institution_name', data.institution_name);
    formData.append('social_reason', data.social_reason);
    formData.append('ruc', data.ruc);
    formData.append('institution_activity', data.institution_activity);
    formData.append('logo', data.logo);
    formData.append('is_active', data.is_active);

    return this.API.put(url + "/" + _id, formData);

  }
  remove(id: string): Observable<any> {
    return this.API.delete(url + '/' + id);
  }
  notify(object) {
    return this.API.post<any>(url + '/' + 'notify/', object);
  }
}
