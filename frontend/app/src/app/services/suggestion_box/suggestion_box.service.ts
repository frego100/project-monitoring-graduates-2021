import { Injectable } from '@angular/core';
import { Area } from '@app/models/area';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
import { HttpParams } from '@angular/common/http';

const url = 'suggestion' as string;
let rol = JSON.parse(localStorage.getItem('user'));;

@Injectable({
  providedIn: 'root',
})
export class SuggestionService {
  suggestionControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url+'/');
  }
  getAllAuth(): Observable<any> {
    return this.API.get<any>(url+'/auth/');
  }
  getById(id: String) {
    return this.API.get<any>(url + '/auth/' + id);
  }
  create(data: any): Observable<any> {
    delete data.id;
    return this.API.post<any>(url+"/auth/", data);
  }
  update(_id: string, data: any): Observable<any> {
    delete data.id;
    return this.API.put(url + '/auth/' + _id, data);
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + '/auth/' + id);
  }
  notify(object) {
    return this.API.post<any>(url + '/' + 'notify/', object);
  }
}
