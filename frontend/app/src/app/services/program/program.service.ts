import { Injectable } from '@angular/core';
import { Program } from '@app/models/program';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
const url = 'instance/professional-program/' as string;
@Injectable({
  providedIn: 'root',
})
export class ProgramService {
  programControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<Program>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true');
  }
  
  /**
   * Delete pagination updating limit
   * @returns Program data
   */
  getWitOutPagination(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true&limit=10000');
  }

  /**
   * Delete pagination updating limit and serialize lite data
   * @returns Program data
   */
  getLite(faculty_field: number, faculty_field__area_field: number): Observable<any> {
    var _url = `${url}lite/?nope=nope`;
    if (!IsNullUndefinedBlank(faculty_field)) {
        _url = _url + `&faculty_field=${faculty_field}`;
    }
    if (!IsNullUndefinedBlank(faculty_field__area_field)) {
      _url = _url + `&faculty_field__area_field=${faculty_field__area_field}`;
    }
    return this.API.get<any>(_url);
  }
  
  getById(id: String) {
    return this.API.get<Program>(url + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    formData.append('faculty_field', data.faculty_field);
    return this.API.post<any>(url, formData);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    formData.append('faculty_field', data.faculty_field);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  notify(object) {
    return this.API.post<any>(url + 'notify/', object);
  }
}
