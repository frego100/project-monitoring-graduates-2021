import { Injectable } from '@angular/core';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
const url = 'target-group-announcement' as string;

@Injectable({
  providedIn: 'root',
})
export class TargetGroupAnnouncementService {

  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }
  get(id): Observable<any> {
    var _url = `${url}/${id}`;
    return this.API.get<any>(_url);
  }
  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('text_invitation', data.text_invitation);
    formData.append('instance', data.instance);
    formData.append('announcement', data.announcement);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }
  post(body): Observable<any> {
    return this.API.post<any>(url + '/', body);
  }
  put(id, body): Observable<any> {
    var _url = `${url}/${id}`;
    return this.API.put<any>(_url, body);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    formData.append('faculty_field', data.faculty_field);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url+"/" + id);
  }
  getList(
    limit,
    offset,
    filter1,
    search,
    ordering_list: Map<string, string>
  ): Observable<any> {
    var _url = `${url}/?limit=${limit}&offset=${offset}` + '&is_active=true';
    if (!IsNullUndefinedBlank(filter1)) {
      _url = _url + `&filter1=${filter1}`;
    }
    if (!IsNullUndefinedBlank(search)) {
      _url = _url + `&search=${search}`;
    }
    if (!IsNullUndefinedBlank(ordering_list)) {
      ordering_list.forEach((value: string) => {
        if (!IsNullUndefinedBlank(value)) {
          _url = _url + `&ordering=${value}`;
        }
      });
    }
    return this.API.get<any>(_url);
  }
  getTargetGroupLite(is_active): Observable<any> {
    const _url = url + `?is_active=${is_active}`;
    return this.API.get<any>(_url);
  }
  getAllObjectiveGroup(id: string): Observable<any> {
    return this.API.get(url + '/'+'?announcement='+id);
  }
  createObjectiveGroup(data:any): Observable<any> {
    return this.API.post(url + 'group_objective/',data);
  }
}
