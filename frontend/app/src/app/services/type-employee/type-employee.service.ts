import { Injectable } from '@angular/core';
import { Employee } from '@app/models/employee';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';

const url = 'employee/type-employee/' as string;

@Injectable({
  providedIn: 'root',
})
export class TypeEmployeeService {
  employedControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<Employee>) {}

  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }

  getAll(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true');
  }
  
  getById(id: String) {
    return this.API.get<Employee>(url + id);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('is_active', data.is_active);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  notify(object) {
    return this.API.post<any>(url + 'notify/', object);
  }
}
