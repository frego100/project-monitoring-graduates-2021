import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
import { Manual } from '@app/models/manual';
const url = 'manual/' as string;
@Injectable({
  providedIn: 'root',
})
export class ManualsService {
  manualControl = new BehaviorSubject(null);
  formControl = new Subject();

  constructor(private API: BaseAPIService<Manual>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url + '?is_active=true');
  }

  getPublics(): Observable<any> {
    return this.API.get<any>(url + 'public/');
  }

  getById(id: String) {
    return this.API.get<Manual>(url + id);
  }
  
  create(data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('link', data.link);
    formData.append('file', data.file);
    formData.append('is_active', data.is_active);
    return this.API.post<any>(url, formData);
  }

  update(_id: string, data: any): Observable<any> {
    delete data.id;
    var formData: any = new FormData();
    formData.append('name', data.name);
    formData.append('description', data.description);
    formData.append('link', data.link);
    if (typeof data.file != 'string') {
      formData.append('file', data.file);
    }
    formData.append('is_active', data.is_active);
    return this.API.patch(url + _id, formData).pipe(
      map((res: any) => Object.assign({ _id }, res))
    );
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }

  notify(object) {
    return this.API.post<any>(url + 'notify/', object);
  }
}
