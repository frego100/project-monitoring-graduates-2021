import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseAPIService } from '../../core/base-api.service';
import { HttpParams } from '@angular/common/http';

const url = 'campus/' as string;

@Injectable({
  providedIn: 'root',
})
export class SedeService {
  sedeControl = new BehaviorSubject(null);
  formControl = new Subject();
  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url+'?is_active=true');
  }

  getById(id: String) {
    return this.API.get<any>(url + id);
  }

  create(data: any): Observable<any> {
    delete data.id;
    return this.API.post<any>(url, data);
  }

  update(_id: string, data: any): Observable<any> {
    return this.API.put<any>(url + _id, data);
  }

  remove(id: string): Observable<any> {
    return this.API.delete(url + id);
  }
}
