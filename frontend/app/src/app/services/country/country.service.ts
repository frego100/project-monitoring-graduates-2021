import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseAPIService } from '../../core/base-api.service';
const url = 'country/' as string;

@Injectable({
  providedIn: 'root',
})
export class CountryService {

  constructor(private API: BaseAPIService<any>) {}

  getAll(): Observable<any> {
    return this.API.get<any>(url);
  }
}
