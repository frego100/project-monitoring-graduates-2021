import { Injectable } from '@angular/core';
import { BaseAPIService } from '../../core/base-api.service';
import { Observable } from 'rxjs';
import { AuthUser } from '@app/models/user';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';
import { getLocalUser, setLocalUser } from '@app/shared/helpers/auth-utils';

const url = "auth/user/" as string;

@Injectable({
    providedIn: 'root'
})

/**
 * This service provides information on user permissions and profiling
 * @class AuthUserService
 */
export class AuthUserService {

    private user: any;
    private user_permissions: Map<String, Boolean>;

    constructor(private API: BaseAPIService<AuthUser>) {
        this.user_permissions = new Map<String, Boolean>();
    }

    /**
     * Operation that verifies the user's data in session
     *
     * @method getUserVerify
     * @return Data of the user in session, group to which
     * he belongs together with permissions
     */
    getUserVerify(): Observable<AuthUser> {
        return this.API.get<AuthUser>(url);
    }

    /**
     * It is the user in session
     *
     * @method getUser
     * @return User
     */
    getUser(): AuthUser {
        return this.user;
    }

    getUserAdmin() {
        return this.API.get<any>(url);
    }

    getUserGeneral() {
        return this.API.get<any>(url);
    }

    updateUserAuth(data: any) {
        var formData: any = new FormData();
        formData.append('username', data.username);
        formData.append('first_name', data.first_name);
        formData.append('last_name', data.last_name);
        formData.append('is_active', data.is_active);
        return this.API.patch<any>("auth/update/information/", formData);
    }

    getUserCompany() {
        return this.API.get<any>(url);
    }

    /**
     * This operation updates the data of the user in session as
     * well as the list of permissions.
     *
     * @method setUser
     * @param user It is new user
     */
    setUser(user: AuthUser): void {
        this.user = user;
        setLocalUser(this.user);
        this.readPermissions(this.user);
    }

    /**
     * Operation that reads the permissions of the user
     * in session and adds them to a key-value list
     *
     * @method readPermissions
     * @param user Is in session
     */
    readPermissions(user: AuthUser): void {
        this.user_permissions.clear();
        if (IsNullUndefined(user)) {
            return;
        }
        for (let i = 0; i < user.groups.length; i++) {
            for (let j = 0; j < user.groups[i].permissions.length; j++) {
                this.user_permissions.set(user.groups[i].permissions[j].codename, true);
            }
        }
    }

    /**
     * Operation that reviews the permission list
     *
     * @method checkPermissions
     * @param val It is a wanted value
     * @return If the value is in the list the method returns
     * true otherwise it returns false
     */
    checkPermissions(val: string): boolean {
        if (IsNullUndefined(this.user)) {
            const u = getLocalUser();
            if (u != null) {
                this.readPermissions(u);
            }
        }
        if (IsNullUndefined(this.user_permissions.get(val))) {
            return false;
        }
        if (this.user_permissions.get(val)) {
            return true;
        }
        return false;
    }
}

