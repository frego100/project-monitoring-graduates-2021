import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import { SelectTargetModalComponent } from './modal/modal.component';
import { BehaviorSubject, Subject } from 'rxjs';

@Component({
   selector: 'app-select-target',
   templateUrl: './component.html',
   styleUrls: ['./component.scss']
})
export class SelectTargetComponent implements OnInit {
   @Output() select = new EventEmitter();
   @Output() new = new EventEmitter();

   complete: any;
   progress: any;
   selectedUsers = [];

   constructor(private dialog: MatDialog) {}

   ngOnInit(): void {
      this.progress = new Subject();
      this.complete = new BehaviorSubject(null);
   }

   showDialog() {
      this.progress = new Subject();
      this.complete = new BehaviorSubject(null);

      if (this.select) {
         this.select.emit();
      }
   }

   config(data: any, openDialog = true) {
      setTimeout(() => {
         if(openDialog) {
            const dialogRef = this.dialog.open(SelectTargetModalComponent, {
               width: '1024px',
               data
            }).componentInstance;

            dialogRef.afterSelected.subscribe((items : any) =>{
               this.selectedUsers=items.users;
   
               this.complete.next(items)
            });
   
            dialogRef.onNewItem.subscribe((title) => {
               this.new.emit(title);
               this.dismissModal();
            });
   
            this.progress.subscribe(() => dialogRef.updateProgress());
         } else {
            this.selectedUsers = data.selected.users;
         }        
      }, 100);

      return this;
   }

   afterSelected(cb: any) {
      this.complete.subscribe((items: any) => cb(items));
   }

   dismissModal() {
      this.dialog.closeAll();
   }

   updateProgress() {
      this.progress.next();
   }
}
