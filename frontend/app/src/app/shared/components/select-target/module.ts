import { NgModule } from '@angular/core';

import { AppCommonModule } from '@app/app-common.module';
import { AppMaterialModule } from '@app/app.material.module';

import { SelectTargetComponent } from './component';
import { SelectTargetModalComponent } from './modal/modal.component';

@NgModule({
   declarations: [SelectTargetComponent, SelectTargetModalComponent],
   imports: [AppCommonModule, AppMaterialModule],
   exports: [SelectTargetComponent, SelectTargetModalComponent],
   entryComponents: [SelectTargetModalComponent]
})
export class SelectTargetModule {}
