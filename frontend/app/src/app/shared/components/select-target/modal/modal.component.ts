import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';

@Component({
   selector: 'app-add-modal',
   templateUrl: './modal.component.html',
   styleUrls: ['./modal.component.scss']
})
export class SelectTargetModalComponent implements OnInit {
   public afterSelected = new Subject();
   public onNewItem = new Subject();

   searchCtrl = new FormControl();
   selection: SelectionModel<any>;
   dataSource: MatTableDataSource<any>;
   displayColumns: string[];
   title = '';
   checkbox = false;
   sticky = false;
   columns: any[];
   progress = -1;
   selectedUsers = [];

   constructor(
      public dialogRef: MatDialogRef<SelectTargetModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
   ) {
      this.title = data.title;
      this.sticky = data.sticky;
      this.checkbox = data.checkbox;
      this.selection = new SelectionModel<any>(true, []);
      this.columns = data.columns;
      this.displayColumns = data.columns.reduce(
         (prev, d) => (d.show === true ? prev.concat(d.name) : prev),
         []
      );

      if(data.selected) {
         this.selectedUsers = data.selected.users;
      }      

      data.dataSource.subscribe((res: any) => {
         this.dataSource = new MatTableDataSource<any>(res.results);         
         this.searchCtrl.setValue(data.textSearch);
         this.dataSource.filteredData.forEach(row => {
            if(this.selectedUsers.some(sr => row.id === sr.id)) {
               this.selection.select(row)
            }            
         });

         this.bindDTFilter();
      });

      if (this.checkbox) {
         this.displayColumns.unshift('select');
      }
   }

   getCellValue(item: any, col: any) {
      return col.split('.').reduce((prev: any, crr: any) => prev && prev[crr], item);
   }

   isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;

      return numSelected === numRows;
   }

   masterToggle() {
      this.isAllSelected()
         ? this.selection.clear()
         : this.dataSource.filteredData.forEach((row) =>
              this.selection.select(row)
           );
   }

   checkboxLabel(row?: any): string {
      if (!row) {
         return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
         row.position + 1
      }`;
   }

   ngOnInit(): void {
      this.searchCtrl.valueChanges.subscribe((str) => {
         this.dataSource.filter = str;
      });
   }

   bindDTFilter() {
      this.dataSource.filterPredicate = (data, str) =>
         this.displayColumns.some((dc) => {
            const value = this.getCellValue(data, dc);

            if (!value) {
               return;
            }

            return value.trim().toLowerCase().indexOf(str.toLowerCase()) > -1;
         });
   }

   add() {
      this.progress++;
      this.afterSelected.next({
         users: this.selection.selected,
      });
   }

   updateProgress() {
      this.progress++;
   }
}
