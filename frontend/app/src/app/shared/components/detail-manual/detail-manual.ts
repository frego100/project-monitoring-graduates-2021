import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './detail-manual.html',
  styleUrls: ['./detail-manual.scss']
})
export class DetailManual implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DetailManual>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }
  onClickNo(): void {
    this.dialogRef.close();
  }

}
