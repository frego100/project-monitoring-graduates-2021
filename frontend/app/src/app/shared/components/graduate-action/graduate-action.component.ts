import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';

import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GraduateListService } from '@app/services/graduate/graduate-list.service';
import { AreaService, FacultyService, ProgramService } from '@app/services';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { PaginationResponse } from '@app/models/pagination';
import { Graduate } from '@app/models/graduate';

@Component({
   selector: 'app-add-graduate-action',
   templateUrl: './graduate-action.component.html',
   styleUrls: ['./graduate-action.component.scss']
})
export class GraduateSelectActionModalComponent implements OnInit {
   public afterSelected = new Subject();
   public onNewItem = new Subject();

   selection: SelectionModel<any>;
   search: string = '';
   dataSource: MatTableDataSource<any>;
   displayColumns: string[];
   title = '';
   checkbox = false;
   sticky = false;
   columns: any[];
   in_progress = false;
   selectedUsers = [];
   loading = false;
   totalSize = 0;
   pageSize = 10;
   currentPage = 0;

   selectProgram: number;
   selectFaculty: number;
   selectArea: number;
   selectActivity: number;

   listProgram: any;
   listArea: any;
   listFaculty: any;

   @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

   constructor(
      public dialogRef: MatDialogRef<GraduateSelectActionModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any, 
      public graduateService: GraduateListService,
      public programsService: ProgramService,
      private snackBar: MatSnackBar,
      public facultyService: FacultyService,
      public areaService: AreaService
   ) {
      this.title = data.title;
      this.sticky = data.sticky;
      this.checkbox = data.checkbox;
      this.selection = new SelectionModel<any>(true, []);
      this.columns = data.columns;
      this.selectActivity = data.activity;
      this.displayColumns = data.columns.reduce(
         (prev, d) => (d.show === true ? prev.concat(d.name) : prev), []
      );
      if(data.selected) {
         this.selectedUsers = data.selected.users;
      }
      if (this.checkbox) {
         this.displayColumns.unshift('select');
      }
   }

   getCellValue(item: any, col: any) {
      return col.split('.').reduce((prev: any, crr: any) => prev && prev[crr], item);
   }

   isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
   }

   masterToggle() {
      this.isAllSelected()
         ? this.selection.clear()
         : this.dataSource.filteredData.forEach((row) =>  this.selection.select(row));
   }

   checkboxLabel(row?: any): string {
      if (!row) {
         return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
   }

   ngOnInit(): void {
      this.getData();
      this.getExtraDataAreas();
      this.getExtraDataFaculties();
      this.getExtraDataPrograms();
   }

   getData(){
      this.loading = true;
      this.graduateService.getList(
         this.pageSize, this.currentPage * this.pageSize,
         this.selectProgram, this.selectArea, this.selectFaculty,
         null,null,null, this.search, this.selectActivity).subscribe((res: any) => {
         this.dataSource = new MatTableDataSource<PaginationResponse<Graduate>>(res.results);
         this.totalSize = res.count;
         this.loading = false;
         if (this.totalSize === 0) {
            this.snackBar.open('No hay egresados disponibles!', 'Cerrar', {
               duration: 3000,
             });
         }
      });
   }

   getExtraDataPrograms(){
      this.programsService.getLite(this.selectFaculty,this.selectArea).subscribe((res: any) => {
         this.listProgram = res;
      });
   }

   getExtraDataFaculties(){
      this.facultyService.getLite(this.selectArea).subscribe((res: any) => {
         this.listFaculty = res;
      });
   }

   getExtraDataAreas(){
      this.areaService.getLite().subscribe((res: any) => {
         this.listArea = res;
      });
   }

   bindDTFilter() {
      this.dataSource.filterPredicate = (data, str) =>
         this.displayColumns.some((dc) => {
            const value = this.getCellValue(data, dc);
            if (!value) {
               return;
            }
            return value.trim().toLowerCase().indexOf(str.toLowerCase()) > -1;
         });
   }
  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
   handlePage(e: any) {
      this.currentPage = e.pageIndex;
      this.pageSize = e.pageSize;
      this.getData();
   }
  
   add() {
      this.in_progress = true;
      var list_user_id = []
      for (let i = 0; i<this.selection.selected.length; i++){
         list_user_id.push(this.selection.selected[i].user.id)
      }
      this.afterSelected.next({
         users: list_user_id,
      });
   }

   close(): void {
      this.dialogRef.close();
   }

  /**
  * This event occurs when the user selects a value in the combo box,
  * then we get the selected value and
  * finally we ask the server for the data again.
  *
  * @method onSelectProgram
  * @param id is the value selected in the combobox
  */
   onSelectProgram(id: any): void {
      this.selectProgram = id;
      if (!IsNullUndefinedBlank(this.paginator))
         this.paginator.firstPage();
      this.getData();
   }

  /**
  * This event occurs when the user selects a value in the combo box,
  * then we get the selected value and
  * finally we ask the server for the data again.
  *
  * @method onSelectFaculty
  * @param id is the value selected in the combobox
  */
   onSelectFaculty(id: any): void {
      this.selectFaculty = id;
      this.selectProgram = null;
      this.getExtraDataPrograms();
      if (!IsNullUndefinedBlank(this.paginator))
         this.paginator.firstPage();
      this.getData();
   }

  /**
  * This event occurs when the user selects a value in the combo box,
  * then we get the selected value and
  * finally we ask the server for the data again.
  *
  * @method onSelectArea
  * @param id is the value selected in the combobox
  */
   onSelectArea(id: any): void {
      
      this.selectArea = id;
      this.selectProgram = null;
      this.selectFaculty = null;
      this.getExtraDataFaculties();
      this.getExtraDataPrograms();
      if (!IsNullUndefinedBlank(this.paginator))
         this.paginator.firstPage();
      this.getData();
   }

   /**
    * This method applies a text search filter when the
    * user presses the enter key in the search field
    *
    * @method applyFilter
    * @param key keyboard event
    */
   applyFilter(key: any) {
      var keycode = key.keyCode || key.which;
      if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getData();
      if (!IsNullUndefinedBlank(this.paginator))
         this.paginator.firstPage();
      }
   }
}
