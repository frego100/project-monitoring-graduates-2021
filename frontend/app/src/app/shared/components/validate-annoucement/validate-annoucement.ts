import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AnnouncementService} from "@services";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './validate-annoucement.html',
  styleUrls: ['./validate-annoucement.scss']
})
export class ValidateAnnoucement implements OnInit {

  annoucementForm: FormGroup;
  editor = ClassicEditor;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  constructor(
    private annoucementService: AnnouncementService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ValidateAnnoucement>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.annoucementForm = this.formBuilder.group({
      justification: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }
  onClickNo(): void {
    this.dialogRef.close();
  }
  confirmAnnoucement(id: any) {
    let annoucement = this.annoucementForm.value;
    annoucement.is_validated = true
    this.annoucementService.validateAnnoucement(id, annoucement).subscribe(
      (res) => {
        this.onClickNo()
        this.snackBar.open('Validado correctamente!', 'Cerrar', {
          duration: 3000
        });
      },
      (err) => {
        this.snackBar.open('No se pudo validar el componente', 'Cerrar', {
          duration: 3000
        });
      });
  }
  discardAnnoucement(id: any) {

    let annoucement = this.annoucementForm.value;
    annoucement.is_validated = false
    this.annoucementService.validateAnnoucement(id, annoucement).subscribe(
      (res) => {
        this.onClickNo()
        this.snackBar.open('Validado correctamente!', 'Cerrar', {
          duration: 3000
        });
      },
      (err) => {
        this.snackBar.open('No se pudo validar el componente', 'Cerrar', {
          duration: 3000
        });
      });
  }
}
