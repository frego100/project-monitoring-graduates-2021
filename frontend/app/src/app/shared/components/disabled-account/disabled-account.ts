import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {AuthService, UserService} from "@services";
import { MatSnackBar } from "@angular/material/snack-bar";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './disabled-account.html',
  styleUrls: ['./disabled-acoount.scss']
})
export class DisabledAccount implements OnInit {

  annoucementForm: FormGroup;
  editor = ClassicEditor;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    public dialogRef: MatDialogRef<DisabledAccount>,
    private routes: Router,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.annoucementForm = this.formBuilder.group({
      justification: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }
  onClickNo(): void {
    this.dialogRef.close();
  }
  confirmDisabled() {
    this.userService.disabledAccount().subscribe(
      (res) => {
        this.snackBar.open('Cuenta Deshabilitado con Exito!', 'Cerrar', {
          duration: 3000
        });
        this.authService.logout()
        this.routes.navigate(['']);
      },
      (err) => {
        this.snackBar.open('No se puede deshabilitar la cuenta', 'Cerrar', {
          duration: 3000
        });
      });
  }
}
