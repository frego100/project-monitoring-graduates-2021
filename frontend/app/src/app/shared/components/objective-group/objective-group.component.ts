import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import {ActivityService} from '@services';
import {MatSort} from "@angular/material/sort";
import {ObjectiveGroupFormComponent} from "@shared/components/objective-group/form/form.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
   selector: 'app-objective-group',
   templateUrl: './objective-group.component.html',
   styleUrls: ['./objective-group.component.scss']
})
export class ObjectiveGroupModalComponent implements OnInit {
   public afterSelected = new Subject();

   selection: SelectionModel<any>;
   search: string = '';
   title = '';
   checkbox = false;
   sticky = false;
   columns: any[];
   selectedUsers = [];
   selectActivity: number;

   listObjectiveGroup: any;

    pageSize = 10;
    loading: boolean = false;
    currentPage = 0;
    totalSize = 0;
    displayedColumns: string[] = [
      //'nro',
      'name','instance'];
    dataSource :MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;

   constructor(
      public dialogRef: MatDialogRef<ObjectiveGroupModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public activityService: ActivityService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog,
   ) {
      this.title = data.title;
   }
   ngOnInit(): void {
     this.getAllObjectiveGroups();

   }
   getAllObjectiveGroups() {
     let cont = 1
     let kind_instance = ''
     this.activityService.getAllObjectiveGroup(this.data.activity).subscribe((response: any) => {
       let aux = Object.values(response.results).map((item) =>{
         if(item['instance'].faculty_field){
           kind_instance = "PROGRAMA PROFESIONAL"
         }else if(item['instance'].area_field){
           kind_instance = "FACULTAD"
         }else{
           kind_instance = "ÁREA"
         }
         let recurso = {
           id: item['id'],
           name: kind_instance,
           text_invitation: item['text_invitation'],
           activity: item['activity'],
           instance: item['instance'],
         }
         cont++
         return recurso;
       } );
       this.listObjectiveGroup = aux
       this.dataSource = new MatTableDataSource(this.listObjectiveGroup);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
     })
   }
   sendMails(){
     let listAreas = []
     let listFaculties = []
     let listPrograms = []
     this.listObjectiveGroup.forEach(element =>{
       //Escuela profesional
       if(element.instance.faculty_field){
         listPrograms.push(element.instance.id)
       }else if(element.instance.area_field){
         listFaculties.push(element.instance.id)
       }else{
         listAreas.push(element.instance.id)
       }
       let group_invitation = {
         group_objective_program: listPrograms,
         group_objective_faculty: listFaculties,
         group_objective_area: listAreas,
         activity: this.data.activity
       }
       this.activityService.notifyGroup(group_invitation).subscribe((res) =>{
         this.snackBar.open(res.message, 'Cerrar', {
           duration: 3000,
         });
       }, (error) =>{
       })
     } )
   }
   addObjectiveGroup(openDialog = true) {
     const dialogRefForm= this.dialog.open(ObjectiveGroupFormComponent, {
       width: '800px',
       data:{
         activity: this.data.activity,
         links: {
           link: this.data.links.link,
           stream_link: this.data.links.stream_link,
           meet_link: this.data.links.meet_link,
         },
       }
     }).componentInstance;
     dialogRefForm.afterSelected.subscribe((items: any) =>{
       if(items.aux === true){
         this.getAllObjectiveGroups()
       }
     })
   }
  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
   handlePage(e: any) {
      this.currentPage = e.pageIndex;
      this.pageSize = e.pageSize;
   }

   close(): void {
     this.afterSelected.next(
       {
         listObjectiveGroups: this.listObjectiveGroup
       }
     )
      this.dialogRef.close();
   }
}
