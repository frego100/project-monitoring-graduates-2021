import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ConfirmDisablingComponent } from '../confirm-disabling/confirm-disabling.component';

@Component({
  selector: 'table-view-simple',
  templateUrl: './table-view-simple.html',
  styleUrls: ['./table-view-simple.scss']
})
export class TableViewSimpleComponent implements OnInit {

  @Input() title = '';
  @Input() columns = [];
  @Input() data = [];
  @Input() displayedColumns = [];
  @Input() loading = false;
  @Input() totalSize = 0;
  @Input() pageSize = 10;
  @Input() currentPage = 0;
  @Output() paginationEvent = new EventEmitter<any>();
  @Output() formCallEvent = new EventEmitter<boolean>();
  @Output() deleteCallEvent = new EventEmitter<boolean>();
  @Output() formDataCallEvent = new EventEmitter<any>();
  
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void { }

  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
   handlePage(e: any) {
    this.paginationEvent.emit(e);
  }

  add(): void {
    this.formCallEvent.emit(false);
  }

  edit(row: any): void {
    this.formCallEvent.emit(true);
    this.formDataCallEvent.emit(row);
  }

  delete(row: any): void {
    const dialogRef = this.dialog.open(ConfirmDisablingComponent,
      { data: 'Esta seguro de que ¿Quiere eliminar este registro?'}
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteCallEvent.emit(row);
      }
    });
  }
  
  findColumnValue = (element:unknown, column:string):
    string => <string>column.split('.').reduce((acc, cur) => acc[cur], element);
}