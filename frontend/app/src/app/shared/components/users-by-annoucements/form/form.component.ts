import {Component, OnInit, Inject} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ActivityService, AreaService, FacultyService, ProgramService} from '@services';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {Subscription, Subject} from 'rxjs';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { environment } from '@env/environment';

@Component({
  selector: 'objective-group-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class ObjectiveGroupFormComponent implements OnInit {

  public afterSelected = new Subject();

  objectiveForm: FormGroup;
  editor = ClassicEditor;
  mediaType = 'image';
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  activitySubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  aux_create: boolean = false;
  activity: any;
  searchCtrl: any;
  selectedUsers = [];
  showBeforeImage: boolean = false;
  public listKindInstance: any [] = [];
  public listAreas: any [] = [];
  public listFaculties: any [] = [];
  public listProfessionalPrograms: any [] = [];
  public listSelectInstance: any [] = [];
  text: String;
  url_act: String;

  constructor(
    public dialogRef: MatDialogRef<ObjectiveGroupFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private activityService: ActivityService,
    private areaService: AreaService,
    private facultyService: FacultyService,
    private programaService: ProgramService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService,
  ) {
    this.listKindInstance = [
      {id: '1', name: 'Area'},
      {id: '2', name: 'Facultad'},
      {id: '3', name: 'Programa Profesional'}
    ]
    if(this.data.links.link === null){
      this.data.links.link = "Link no disponible"
    }
    if(this.data.links.stream_link === null){
      this.data.links.stream_link = "Link no disponible"
    }
    if(this.data.links.meet_link === null){
      this.data.links.meet_link = "Link no disponible"
    }
    /*this.text = `<p> <b>Link: </b> <a href="${this.data.links.link}">${this.data.links.link}</a> </p>
                <br> <p> <b>Stream Link: </b> <a href="${this.data.links.stream_link}">${this.data.links.stream_link}</a> </p>
                <br> <p> <b>Meet Link: </b> <a href="${this.data.links.meet_link}">${this.data.links.meet_link}</a> </p>`*/
    this.url_act = "/cliente/actividad/"
    this.text = `<p>Para ver a más detalle la actividad acceda al siguiente link <br> <b>Link de la Actividad: </b> <a href="${environment.root + this.url_act + this.data.activity}">${environment.root + this.url_act + this.data.activity}</a> </p>`
    this.objectiveForm = this.formBuilder.group({
      text_invitation: [this.text, Validators.required],
      activity: [''],
      instance: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getAllAreas()
    this.getAllFaculties()
    this.getAllPrograms()
  }
  private getAllAreas(){
    this.areaService.getAll().subscribe((res: any)=> {
      this.listAreas  = res.results
    })
  }
  private getAllFaculties(){
    this.facultyService.getAll().subscribe((res: any)=> {
      this.listFaculties  = res.results
    })
  }
  private getAllPrograms(){
    this.programaService.getAll().subscribe((res: any)=> {
      this.listProfessionalPrograms  = res.results
    })
  }
  public changeInstance(kind: any){
    if(kind.id === '1') {
      this.listSelectInstance = this.listAreas
    }else if(kind.id === '2'){
      this.listSelectInstance = this.listFaculties
    }else if(kind.id === '3'){
      this.listSelectInstance = this.listProfessionalPrograms
    }
  }
  create() {
    let objectiveGroup = this.objectiveForm.value
    objectiveGroup.activity = this.data.activity
    objectiveGroup.instance = objectiveGroup.instance.id
    let arr = [
      objectiveGroup,
    ]
    this.activityService.createObjectiveGroup(arr).subscribe(
      (res) => {
        this.afterSelected.next(
          {
            aux: true,
          }
        )
      },
      (error) => {
        this.aux_create = false
      }
    )
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }
}
