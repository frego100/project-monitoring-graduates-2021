import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import {AnnouncementService} from '@services';
import {MatSort} from "@angular/material/sort";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
   selector: 'app-users-by-annoucements',
   templateUrl: './users-by-annoucements.component.html',
   styleUrls: ['./users-by-annoucements.component.scss']
})
export class UsersByAnnoucementsModalComponent implements OnInit {
   public afterSelected = new Subject();

   selection: SelectionModel<any>;
   search: string = '';
   title = '';
   checkbox = false;
   sticky = false;
   columns: any[];
   selectedUsers = [];
   selectActivity: number;

   listUsersByAnnoucements: any;

    pageSize = 10;
    loading: boolean = false;
    currentPage = 0;
    totalSize = 0;
    displayedColumns: string[] = [
      //'nro',
      'user.first_name','user.last_name','user.profile.cell_phone','user.profile.document','user.profile.email'];
    dataSource :MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;

   constructor(
      public dialogRef: MatDialogRef<UsersByAnnoucementsModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public annoucementService: AnnouncementService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog,
   ) {
   }
   ngOnInit(): void {
     this.getAllUsers();

   }
   getAllUsers() {
     let cont = 1
     this.annoucementService.getUserListMyAnnoucements(this.data.annoucement).subscribe((response: any) => {
       this.listUsersByAnnoucements = response
       this.dataSource = new MatTableDataSource(this.listUsersByAnnoucements);
       this.dataSource.paginator = this.paginator;
       this.dataSource.sort = this.sort;
     })
   }
   handlePage(e: any) {
      this.currentPage = e.pageIndex;
      this.pageSize = e.pageSize;
   }

   close(): void {
      this.dialogRef.close();
   }
}
