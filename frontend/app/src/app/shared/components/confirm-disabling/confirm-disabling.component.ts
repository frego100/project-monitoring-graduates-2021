import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './confirm-disabling.component.html',
  styleUrls: ['./confirm-disabling.component.scss']
})
export class ConfirmDisablingComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDisablingComponent>,
    @Inject(MAT_DIALOG_DATA) public message: string) { }

  ngOnInit(): void {
  }
  onClickNo(): void {
    this.dialogRef.close();
  }

}
