import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivityService, AuthService, AuthUserService } from '@app/services';

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './detail-activity.html',
  styleUrls: ['./detail-activity.scss'],
})
export class DetailActivity implements OnInit {
  dataUser: any;

  constructor(
    public dialogRef: MatDialogRef<DetailActivity>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public authService: AuthService,
    public authUserService: AuthUserService,
    private activityService: ActivityService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    
  }

  onClickNo(): void {
    this.dialogRef.close();
  }

  enroll(): void {
   
    this.activityService.enroll(this.data.id).subscribe(
      () => {
        this.snackBar.open('Inscrito satisfactoriamente!', 'Cerrar', {
          duration: 3000,
        });
      },
      (err) => {
        if(err.error.message){
          this.snackBar.open(err.error.message, 'Cerrar', {
            duration: 3000,
          });
        }
        else{
          this.snackBar.open('No fue posible su inscripción!', 'Cerrar', {
            duration: 3000,
          });
        }
        
      }
    );
  }
}
