import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {ActivityService} from "@services";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './validate-activity.html',
  styleUrls: ['./validate-activity.scss']
})
export class ValidateActivity implements OnInit {
  constructor(
    private activityService: ActivityService,
    public dialogRef: MatDialogRef<ValidateActivity>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }
  onClickNo(): void {
    this.dialogRef.close();
  }
  discardActivity(id: any, data: any) {
    let request = {
      validated: false
    }
    this.activityService.invalidateActivity(id, request).subscribe(
      (res) => {
        this.onClickNo()
        this.snackBar.open('Se rechazo la solicitud de registro', 'Cerrar', {
          duration: 3000
        });
      },
      (err) => {
        this.snackBar.open('No se pudo validar el componente', 'Cerrar', {
          duration: 3000
        });
      });
  }
  confirmActivity(id: any) {
    const request = {
      validated: true
    }
    this.activityService.validateActivity(id, request).subscribe(
      (res) => {
        res.data = true;
        this.onClickNo()
        this.snackBar.open('Validado correctamente!', 'Cerrar', {
          duration: 3000
        });
      },
      (err) => {
        this.snackBar.open('No se pudo validar el componente', 'Cerrar', {
          duration: 3000
        });
      });
  }
}
