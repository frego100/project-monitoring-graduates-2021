import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {ActivityService} from "@services";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'view-suggestion-modal',
  templateUrl: './view-suggestion.html',
  styleUrls: ['./view-suggestion.scss']
})
export class ViewSuggestionModal implements OnInit {
  constructor(
    private activityService: ActivityService,
    public dialogRef: MatDialogRef<ViewSuggestionModal>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }
  onClickNo(): void {
    this.dialogRef.close();
  }
}
