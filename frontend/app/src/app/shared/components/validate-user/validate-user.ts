import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UserService } from "@services";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './validate-user.html',
  styleUrls: ['./validate-user.scss']
})
export class ValidateUser implements OnInit {
  value_profile_admin: boolean;
  value_profile_company: boolean;
  value_profile_graduate: boolean;
  is_dcp_value: boolean;
  value_without_profile: boolean;
  is_dcp:string;
  constructor(
    private userService: UserService,
    public dialogRef: MatDialogRef<ValidateUser>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data);
    if (this.data.aux.groups[0].profile_type == null) {
      this.value_profile_company = false;
      this.value_profile_admin = false;
      this.value_profile_graduate = false;
      this.value_without_profile = true;
    }
    else if (this.data.aux.groups[0].profile_type.model == "companyprofile") {
      this.value_profile_company = true;
      this.value_profile_admin = false;
      this.value_profile_graduate = false;
      this.value_without_profile = false;
    }
    else if (this.data.aux.groups[0].profile_type.model == "adminprofile") {
      this.value_profile_company = false;
      this.value_profile_admin = true;
      this.value_profile_graduate = false;
      this.value_without_profile = false;
    }
    else if (this.data.aux.groups[0].profile_type.model == "graduateprofile") {
      if(this.data.aux.profile.is_dcp){
        this.is_dcp="Si";
        this.is_dcp_value=true;
      }
      else{
        this.is_dcp="No"
        this.is_dcp_value=false;
      }
      this.value_profile_company = false;
      this.value_profile_admin = false;
      this.value_profile_graduate = true;
      this.value_without_profile = false;
    }
  }
  onClickNo(): void {
    this.dialogRef.close();
  }
  discardUser(id: any, data: any) {


    let request = {
      validated: false
    }
    this.userService.validateUser(id, request).subscribe(
      (res) => {
        this.onClickNo()
        this.snackBar.open('Se rechazo la solicitud de registro', 'Cerrar', {
          duration: 3000
        });
      },
      (err) => {
        this.snackBar.open('No se pudo validar el componente', 'Cerrar', {
          duration: 3000
        });
      });
  }
  confirmUser(id: any) {

    const request = {
      validated: true
    }
    this.userService.validateUser(id, request).subscribe(
      (res) => {
        res.data = true;
        this.onClickNo()
        this.snackBar.open('Validado correctamente!', 'Cerrar', {
          duration: 3000
        });
      },
      (err) => {
        this.snackBar.open('No se pudo validar el componente', 'Cerrar', {
          duration: 3000
        });
      });
  }
}
