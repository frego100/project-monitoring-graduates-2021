import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import {ActivityService, AnnouncementService} from '@services';
import {MatSort} from "@angular/material/sort";
import {MatSnackBar} from "@angular/material/snack-bar";
import { TargetGroupFormComponent } from './form/form.component';
import { TargetGroupAnnouncementService } from '@app/services/target-group-announcement/target-group-announcement.service';
import { Router } from '@angular/router';

@Component({
   selector: 'app-objective-group',
   templateUrl: './target-group-announcement.component.html',
   styleUrls: ['./target-group-announcement.component.scss']
})
export class TargetGroupModalComponent implements OnInit {
   public afterSelected = new Subject();

   selection: SelectionModel<any>;
   search: string = '';
   title = '';
   checkbox = false;
   sticky = false;
   columns: any[];
   selectedUsers = [];
   selectActivity: number;

   listObjectiveGroup: any;
   aux_create: boolean = false;
    pageSize = 10;
    loading: boolean = false;
    currentPage = 0;
    totalSize = 0;
    displayedColumns: string[] = [
      //'nro',
      'name','instance','actions'];
    dataSource :MatTableDataSource<any>;
    @ViewChild(MatPaginator) paginator!: MatPaginator;
    @ViewChild(MatSort) sort!: MatSort;

   constructor(
      public dialogRef: MatDialogRef<TargetGroupFormComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public activityService: ActivityService,
      private router: Router,
      public announcementService: AnnouncementService,
      public targetgroupservice: TargetGroupAnnouncementService,
      private snackBar: MatSnackBar,
      private dialog: MatDialog,
   ) {
      this.title = data.title;
   }
   ngOnInit(): void {
     this.getAllObjectiveGroups();

   }
   getAllObjectiveGroups() {
   
     let cont = 1
     let kind_instance = ''
     this.targetgroupservice.getAllObjectiveGroup(this.data.announcement).subscribe((response: any) => {
       let aux = Object.values(response.results).map((item) =>{
         
        if(item['instance'].faculty_field){
          kind_instance = "PROGRAMA PROFESIONAL"
        }else if(item['instance'].area_field){
          kind_instance = "FACULTAD"
        }else{
          kind_instance = "ÁREA"
        }
         let recurso = {
           id: item['id'],
           name: kind_instance,
           text_invitation: item['text_invitation'],
           is_active:item['is_active'],
           announcement: item['announcement'],
           instance: item['instance'],
         }
         cont++
         return recurso;
       } );
       this.listObjectiveGroup = aux;
       const list=this.listObjectiveGroup
       const listGroup=list.filter(item=>{
         
        if(item.is_active===true){
          return true;
        }
        else{
          return false;
        }
      });
      this.dataSource = new MatTableDataSource(listGroup);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      
 
     })
   }
   sendMails(){
     let listAreas = []
     let listFaculties = []
     let listPrograms = []
     this.listObjectiveGroup.forEach(element =>{
       
       //Escuela profesional
       if(element.instance.faculty_field){
         listPrograms.push(element.id)
       }else if(element.instance.area_field){
         listFaculties.push(element.id)
       }else{
         listAreas.push(element.id)
       }
     
     } );
     let group_invitation = {
      group_objective_program: listPrograms,
      group_objective_faculty: listFaculties,
      group_objective_area:listAreas,
      announcement: this.data.announcement
    }
    this.announcementService.notifyGroup(group_invitation).subscribe((res) =>{
      this.snackBar.open(res.message, 'Cerrar', {
        duration: 3000,
      });
    }, (error) =>{
    })
   }
   edit(element: any){

   }
   delete(element: any){
      this.targetgroupservice.remove(element).subscribe((res)=>{
        this.getAllObjectiveGroups();
        this.snackBar.open(res.message, 'Cerrar', {
          duration: 3000,
        });
       
       
      });
     
     
     // this.dialogRef.close();
     
  }
   addObjectiveGroup(openDialog = true) {
     const dialogRefForm= this.dialog.open(TargetGroupFormComponent, {
       width: '800px',
       data:{
         announcement: this.data.announcement,
         links: {
           company_pk: this.data.company_pk,
           description: this.data.description,
           date_end: this.data.date_end,
           date_start: this.data.date_start,
         },
       }
     }).componentInstance;
     
     dialogRefForm.afterSelected.subscribe((items: any) =>{
       if(items.aux === true){
         this.getAllObjectiveGroups()
       }
     })
   }
  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
   handlePage(e: any) {
      this.currentPage = e.pageIndex;
      this.pageSize = e.pageSize;
   }

   close(): void {
     this.afterSelected.next(
       {
         listObjectiveGroups: this.listObjectiveGroup
       }
     )
      this.dialogRef.close();
   }
}
