import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivityService, AnnouncementService, AuthService, AuthUserService } from '@app/services';
import { AnnouncementClientService } from '@app/services/announcement-client/annoucement-client.service';


@Component({
  selector: 'app-confirm-disabling',
  templateUrl: './detail-announcement.html',
  styleUrls: ['./detail-announcement.scss'],
})
export class AnnouncementDetail implements OnInit {
  dataUser: any;

  constructor(
    public dialogRef: MatDialogRef<AnnouncementDetail>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public authService: AuthService,
    public authUserService: AuthUserService,
    public announcementService: AnnouncementService,
    public announcementClienteService: AnnouncementClientService,
    private activityService: ActivityService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {

  }

  onClickNo(): void {
    this.dialogRef.close();
  }

  register(id):void{
    if(this.authService.getToken()==null){
      this.snackBar.open('Debe iniciar sesión para poder marcar esta convocatoria!', 'Cerrar', {
        duration: 4000,
      });
    }else{
      let body={
        announcement:id
      };
        this.announcementService.registerAnnouncemenetAuth(body).subscribe(()=> {
          this.snackBar.open('Operación realizada exitosamente!', 'Cerrar', {
            duration: 4000,
          });
          this.dialogRef.close();
       }, (err) => {
           this.snackBar.open('Usted ya indicó su preferencia por esta convocatoria anteriormente!', 'Cerrar', {
            duration: 4000,
          });
       });
    }
  }

  enroll(): void {
    this.activityService.enroll(this.data.id).subscribe(
      () => {
        this.snackBar.open('Inscrito satisfactoriamente!', 'Cerrar', {
          duration: 3000,
        });
      },
      (err) => {
        if(err.error.message){
          this.snackBar.open(err.error.message, 'Cerrar', {
            duration: 3000,
          });
        }
        else{
          this.snackBar.open('No fue posible su elección de interés!', 'Cerrar', {
            duration: 3000,
          });
        }

      }
    );
  }
}
