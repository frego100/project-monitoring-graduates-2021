import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
@Injectable({
  providedIn: 'root'
})
export class CheckProfileGuard implements CanActivate {

  usrProfile: any;

  constructor(private userProfileService: AuthUserService, private route: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    return this.verifyProfile(route);
  }

  verifyProfile(route: ActivatedRouteSnapshot): boolean {
    this.userProfileService.getUserAdmin().subscribe((res: any) => {
      this.usrProfile = res.groups[0].id;
      if (this.usrProfile === 1 && route.data.profile === 'admin') {
        return true;
      }
      else if (this.usrProfile === 13 && route.data.profile === 'empresa') {
        return true;
      }
    });
    return true;
  }
}
