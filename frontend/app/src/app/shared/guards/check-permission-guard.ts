import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';

@Injectable({
    providedIn: 'root'
})

/**
 * This class it's a custom routes guard
 *
 * @class CanActivateAuthGuard
 */
export class CanActivateAuthGuard implements CanActivate {

  constructor(private authService: AuthUserService) {
  }

  /**
   * Operation that verifies that the user in session has
   * permissions to access a specific route
   *
   * @method canActivate
   * @param route It is the route I want to access and data.permission
   * is the permission I am reviewing
   * @return true if I have permissions, false otherwise
   */
  canActivate(route: ActivatedRouteSnapshot) {
    let permission = route.data.permission as Array<string>;
    return this.authService.checkPermissions(permission[0]);
  }
}
