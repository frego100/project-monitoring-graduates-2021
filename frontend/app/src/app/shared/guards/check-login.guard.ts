import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthService } from '@app/services/auth/auth.service';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CheckLoginGuard implements CanActivate {

  constructor(private authSvc:AuthService, private cookieService:CookieService){}
  
  canActivate( ):Observable<boolean>  {
    return this.authSvc.isLogged.pipe(
      take(1),
      map((isLogged: boolean)=>isLogged)
      );
  }
  
}
