import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'orderby'
})
export class PipeOrderBy implements PipeTransform {
   getFlat(item :  any, props : any) {
      return props
         .map((attr : any) => attr.split('.').reduce((p : any, c : any) => p && p[c], item))
         .filter((value : any) => typeof value === 'string' && value.trim() !== '')
         .join(' - ');
   }

   transform(options: any[], attrs: any, args: string): any {
      options.sort((optA, optB) => {
         if (args === 'asc') {
            return this.getFlat(optA, attrs).localeCompare(this.getFlat(optB, attrs));
         } else {
            return this.getFlat(optB, attrs).localeCompare(this.getFlat(optA, attrs));
         }
      });

      return options;
   }
}
