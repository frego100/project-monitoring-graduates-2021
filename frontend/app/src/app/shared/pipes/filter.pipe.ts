import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'filter'
})
export class PipeFilter implements PipeTransform {
   transform(options: any[], attrs: any, args: string): any {
      if (!args || args === '') {
         return options;
      }

      const words = args
         .split(' ')
         .map((a) => a.replace(new RegExp(' ', 'g'), '').trim());
      const fields = attrs instanceof Array ? attrs : [attrs];

      return options.filter((option) => {
         const values = fields
            .map((f) =>
               f.split('.').reduce((prev : any, crr: any) => prev && prev[crr] ? prev[crr] : prev, option)
            )
            .filter((f) => f && f !== undefined);

         return words.every((w) => {
            return (
               values &&
               values.some((v) => {
                  if (typeof v === 'string') {
                     return v && v.toLowerCase().indexOf(w.toLowerCase()) > -1;
                  }
               })
            );
         });
      });
   }
}
