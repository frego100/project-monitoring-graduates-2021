import { NgModule } from '@angular/core';
import { SafePipe } from './safe.pipe';
import { PipeFilter } from './filter.pipe';
import { PipeOrderBy } from './orderby.pipe';

@NgModule({
   declarations: [SafePipe, PipeFilter, PipeOrderBy],
   exports: [SafePipe, PipeFilter, PipeOrderBy],
   providers: [SafePipe, PipeFilter, PipeOrderBy]
})
export class PipesModule {}
