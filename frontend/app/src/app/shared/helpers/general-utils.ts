export function IsNullUndefined(value: any): Boolean {
    if (value == null || value == undefined) {
        return true
    }
    return false;
}

export function IsNullUndefinedBlank(value: any): Boolean {
    if (value == null || value == undefined || value == '') {
        return true
    }
    return false;
}

export function addHour(value: any, moment: string): any {
    if (moment == 'start') {
        return value + '+00:00';
    } else if (moment == 'end') {
        return value + '+23:59';
    }
}

export function formateMyDate(value: any): any {
    const date = new Date(value);
    const formatDate = (date) => {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        if (year < 10) {
            year = '0' + year;
        }
        if (month < 10) {
            month = '0' + month;
        }
        if (day < 10) {
            day = '0' + day;
        }
        let formatted_date = year + "-" + month + "-" + day;
        return formatted_date;
    }
    return formatDate(date);
}

export function isFormatISODate(value: any): boolean {
    var date_regex = /^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/;
    if (date_regex.test(value)) {
        return true;
    }
    else{
        return false;
    }
}

export function getYears() {
  const max = new Date().getFullYear()
  var years:any = []
  for (let i = 1950; i <= max + 10; i++) {
      years.push(i)
  }
  return years
}

export function getYearofUniversity() {
    let years:any = []
    for (let i = 1; i <= 11 ; i++) {
        years.push(i)
    }
    return years
  }

export function getItemNameinArraybyId(_id, array){
    var results = array.filter(function (item) { return item.id == _id; });
    var firstObj = (results.length > 0) ? results[0] : null;
    return firstObj.name;
}

export function makeMessageErrorHttp(error){
  let message = ""
  try {
    Object.keys(error).forEach(
      (key) => {
        if(Array.isArray(error[key])) {
          message = message + error[key][0];
        } else {
          message = message + makeMessageErrorHttp(error[key]);
        }
      }
    );
  } catch {
    message = "Encontramos un error desconocido"
  }
  return message;
}

export function isObject (val) {
  if (val === null) {
    return false
  }
  return ((typeof val === 'function') || (typeof val === 'object'))
}
