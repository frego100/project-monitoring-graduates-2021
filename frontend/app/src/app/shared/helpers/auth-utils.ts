import { AuthUser } from "@app/models/user";

export function getToken(): string {
    var token = localStorage.getItem('token');
    if (token == null || token == undefined) {
        return '';
    }
    return token;
}

export function setToken(token: string) : void {
    localStorage.setItem('token',token);
}

export function getLocalUser(): AuthUser | null {
    const json = localStorage.getItem('user');
    if (!json) {
        return null;
    }
    const user: AuthUser = JSON.parse(json);
    return user;
}

export function setLocalUser(user: AuthUser) : void {
    localStorage.setItem('user',JSON.stringify(user));
}