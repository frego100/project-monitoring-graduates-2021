export interface Program {
    id: number;
    is_active: boolean;
    name: string;
    faculty_field: number;
  }
  