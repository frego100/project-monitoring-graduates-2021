export interface Enabled {
    id: number;
    name: string;
}
