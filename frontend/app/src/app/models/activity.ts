export interface Activity {
  id: number;
  title: string;
  description: string;
  start_date: string;
  end_date: string;
  link: string;
  stream_link: string;
  image: string;
  status: boolean;
  is_validated: boolean;
  activity_type:number;
  can_register:boolean;
}
 export interface TypeActivity{
   id: number;
   name: string;
 }