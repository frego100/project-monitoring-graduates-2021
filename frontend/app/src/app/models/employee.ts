export interface Employee {
    id: number;
    name: string;
    is_active: boolean;
  }
  