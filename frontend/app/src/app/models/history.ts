export interface History {
    id:number;
    description:string;
    created:string;
    object_id:number;
    content_type:number;
    user:number;
}