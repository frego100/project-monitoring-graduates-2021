export interface Poll {
    id: number;
    name: string;
    link_poll:string;
    description: string;
    period_days: number;
    is_to_company: boolean;
    is_to_graduate: boolean;
  }
  