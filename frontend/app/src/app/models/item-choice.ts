export interface Choice {
    value: string;
    text: string;
}
  