export interface Institution {
    id: number;
    identificator: number;
    institution_name: string;
    social_reason: string;
    ruc: number;
    institution_activity: string;
    logo: string;
    is_active: boolean;
  }
