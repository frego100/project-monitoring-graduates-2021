export interface Profile {
    id: number;
    user: string;
    identificator : number;
    document :string;
    cell_phone :string;
    phone :string;
    picture :string;
    is_active :boolean;
}

export interface SuperAdminProfile extends Profile {
    job_position: number;
    type_employee: number;
}

export interface AdminProfile extends Profile {
    area: number;
    faculty: number;
    school: number;
    job_position: number;
    type_employee: number;
}

export interface GraduateProfile extends Profile {
    gender: boolean;
    cui: number;
    date_birtday: Date;
    company: number;
    web: number;
    address: number;
}
export interface CompanyProfile extends Profile {
  company: number;
}

export interface ProfileSelf {
    document: string;
    cell_phone :string;
    identificator : number;
    job_position:number;
    type_employee:number;
    instance:number;
}