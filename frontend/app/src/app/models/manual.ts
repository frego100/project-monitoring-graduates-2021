export interface Manual {
  id: number;
  name: string;
  description: string;
  file: string;
  link: string;
  is_active: boolean;
}
