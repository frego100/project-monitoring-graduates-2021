import { AuthGroup } from '@app/models/group';
import { Profile } from './profile';
import { Roles } from "./rol";

export interface User {
    id:number;
    username:string;
    first_name:string;
    last_name:string;
    email:string;
    is_staff:boolean;
    is_superuser:boolean;
    is_active:boolean;
    last_login:null;
    date_joined:string;
    groups: [];
    user_permissions:[];
}

export interface AuthUser {
    username:string;
    first_name:string;
    last_name:string;
    groups:AuthGroup[];
    user_profile: Profile;
}

export interface UserLite {
    id:number;
    username:string;
    first_name:string;
    last_name:string;
}

export interface UserApply{
    username:string;
    password:string;
    groups: Roles;
}

export interface UserResponse extends User{
    message: string;
    token: string;
    userId: number;
    
}
