export interface Area {
    id: number;
    is_active: boolean;
    name: string;
  }
  