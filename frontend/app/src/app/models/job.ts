export interface Jobs {
    id: number;
    name: string;
    is_active: boolean;
  }
  