export interface Announcement {
  id: number;
  name: string;
  description: string;
  date_start: string;
  date_end: string;
  extra_file: string;
  status: boolean;
  is_validated: boolean;
  company_pk: number;
}