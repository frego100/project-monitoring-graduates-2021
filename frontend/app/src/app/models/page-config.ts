export interface PageConfig {
  id: number;
  link_facebook: string;
  link_twitter: string;
  link_youtube: string;
  link_linkedin: string;
  link_instagram: string;
  link_other: string;
  principal_text: string;
  principal_announcement: string;
}
