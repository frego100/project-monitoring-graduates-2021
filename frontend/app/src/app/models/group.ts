import { Permission } from "./permission";

export interface AuthGroup {
    id:number;
    name:string;
    permissions:Permission[];
}