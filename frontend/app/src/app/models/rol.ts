export interface Roles {
    id: number;
    name: string;
    state: boolean;
    permissions: [];
}

export interface RolLite {
    id: number;
    name: string;
}