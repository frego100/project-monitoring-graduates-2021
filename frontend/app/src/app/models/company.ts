export interface Company {
    identificator: number;
    company_name: string;
    social_reason: string;
    ruc: string;
    company_activity: string;
    is_active: boolean;
    logo: string;
  }
  