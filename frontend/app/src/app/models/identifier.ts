export interface Identifier {
    id: number;
    code: string;
    max_length: number;
    name: string;
    alphabet: string;
    is_active: boolean;
}
  