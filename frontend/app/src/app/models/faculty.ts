export interface Faculty {
    id: number;
    is_active: boolean;
    name: string;
    area_field: number;
  }
  