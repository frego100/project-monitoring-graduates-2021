import { ContentType } from "./content-type";

export interface Permission {
    id:number;
    content_type:ContentType;
    name:string;
    codename:string;
}