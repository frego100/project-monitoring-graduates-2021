export interface TargetGroupAnnouncement {
    id: number;
    text_invitation: string;
    announcement: number;
    instance: number;
    is_active: boolean;
}
  