export interface PaginationResponse<E> {
    count:number;
    next:string;
    previous:string;
    results:Array<E>;
}