import { Url } from "url";
import { Country } from "./country";
import { Language } from "./language";
import { Identifier } from "./identifier";
import { User } from "./user";

export interface Graduate {
    id: number;
    user: User;
    identificator: Identifier;
    document: string;
    cell_phone: string;
    phone: string;
    picture: string;
    is_active: boolean;
    gender: boolean;
    cui: number;
    date_birthday: string;
    web: Url;
    address: string;
}

export interface GraduateDistinction {
    id: number;
    name: string;
    date: string;
    reference_url: string;
    country: Country;
    graduate_profile_pk: number;
}

export interface GraduateDataAcademic {
    id: number;
    period_graduate: string;
    is_student: boolean;
    is_graduate: boolean;
    year_graduate: number;
    profesional_program_pk: number;
    graduate_profile_pk: number;
}
export interface GraduateWorkExperience {
  id: number;
  institution: number;
  charge: string;
  date_start: string;
  date_end: string;
  graduate_profile_pk: number;
  is_current: boolean;
}
export interface GraduatePublications {
  type_publication: number;
  title: string;
  description: string;
  function_type: number;
  DOI: string;
  reference_url: string;
  graduate_profile_pk: number;
}


export class GraduateLanguage {
    id: number;
    level_type: string;
    language: Language;
}

export class GraduateAchievement {
    id: number;
    grade: number;
    title: string;
    country: Country;
}

export class Grade {
    id: number;
    name: string;
}
export interface GraduateProject {
    id: number;
    code: string;
    type_project: number;
    title: string;
    description: string;
    type_role: number;
    date_start: string;
    date_end: string;
    reference_url: string;
    graduate_profile_pk: number;
}
