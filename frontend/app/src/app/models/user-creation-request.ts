import { UserApply } from "./user";

export interface UserCreationRequest {
    id: number;
    user: UserApply;
    state: boolean
  }
  