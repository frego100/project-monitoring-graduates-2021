import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PollFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CanActivateAuthGuard } from '@app/shared/guards/check-permission-guard';
import { PollListComponent } from './list/list.component';
export const PollRoutes: Routes = [
   {
      path: 'encuestas',
      children: [
         {
            path: '',
            component: PollListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_poll']},
         },
         {
            path: 'nuevo',
            component: PollFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['add_poll']},
         },
         {
            path: ':poll',
            component: PollFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_poll']},     
         }
      ]
   }

];

@NgModule({
   declarations: [ PollListComponent, PollFormComponent],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      CKEditorModule,
      AppCommonModule,
      BrowserAnimationsModule,
      PipesModule
   ],
   entryComponents: []
})
export class UserModule {}
