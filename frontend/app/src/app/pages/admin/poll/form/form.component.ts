import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityService, RolesService, UserService } from '@app/services';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';
import { PollService } from '@app/services/poll/poll.service';
@Component({
  selector: 'app-poll-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class PollFormComponent implements OnInit {
  imageSrc: string = '';
  pollForm: FormGroup;
  editor = ClassicEditor;
  mediaType = 'image';
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  pollSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  poll: any;
  showBeforeManual: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private pollService: PollService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.pollForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      description: ['', Validators.required],
      file: [null],
      link_poll: [''],
      period_days:['', Validators.required],
      is_to_company: [false],
      is_to_graduate: [false],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.poll && params.poll !== 'nuevo') {
        this.getManualById();
        this.getManualByPathId(params.poll);
      }
    });
  }

  private getManualById() {
    this.pollService.pollControl.subscribe((res: any) => {
      this.pollSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.pollForm.reset();

      this.pollSubscription = this.getSelectedItem(res).subscribe(
        (poll: any) => {
          this.pollForm.patchValue(poll);
          this.pollService.formControl.next({
            data: poll,
            event: 'select',
          });
        }
      );
    });
  }

  private getManualByPathId(id) {
    this.pollService.getById(id).subscribe((res) => {
      this.poll = Object.assign({}, this.poll, res);
      if (!IsNullUndefined(this.poll.file)) {
        this.showBeforeManual = true;
      } else {
        this.showBeforeManual = false;
      }
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.pollService
            .getById(params.poll)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, poll: any) {
    return new Promise((resolve, reject) => {
      this.pollService.update(_id, poll).subscribe(
        (res) => {
          resolve({
            data: poll,
            event: 'update',
          });
          this.router.navigate(['/admin/encuestas']);
        },
        (err) => {
          let keys = Object.keys(err.error)
          let values = Object.values(err.error)
          let allErrors = ""
          for(let i = 0; i < keys.length; i++){
            if(keys[i] === "name"){
              keys[i] = "Título"
            }
            allErrors += keys[i] + ": " + values[i] + "\n"
          }
          this.snackBar.open(allErrors, 'Cerrar', {
            duration: 6000,
          });
          reject(err);
        }
      );
    });
  }

  create(poll: any) {
    return new Promise((resolve, reject) => {
      this.pollService.create(poll).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/encuestas']);
        },
        (err) => {
          let keys = Object.keys(err.error)
          let values = Object.values(err.error)
          let allErrors = ""
          for(let i = 0; i < keys.length; i++){
            if(keys[i] === "name"){
              keys[i] = "Título"
            }
            allErrors += keys[i] + ": " + values[i] + "\n"
          }
          this.snackBar.open(allErrors, 'Cerrar', {
            duration: 6000,
          });
          reject(err);
        }
      );
    });
  }

  save() {
    const poll = this.pollForm.value;
    const send = poll.id
      ? this.update(poll.id, poll)
      : this.create(poll);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.pollService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        /*this.snackBar.open('Ops...algo no esta funcionando!', 'Cerrar', {
          duration: 3000,
        });*/
      });
  }

  remove() {}
  onFileChange(event: any) {
    // const reader = new FileReader();

    // if(event.target.files && event.target.files.length) {
    //   const [file] = event.target.files;
    //   reader.readAsDataURL(file);

    //   reader.onload = () => {

    //     this.imageSrc = reader.result as string;

    //     this.activityForm.patchValue({
    //       fileSource: reader.result
    //     });

    //   };

    // }

    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.pollForm.patchValue({
        file: file,
      });
      this.pollForm.get('file').updateValueAndValidity();
    }
  }
  removeAction() {
    const poll = this.pollForm.value;

    this.pollService.remove(poll.id).subscribe(
      () => {
        this.pollService.pollControl = new BehaviorSubject(null);

        this.pollService.formControl.next({
          data: poll.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.router.navigate(['..', 'nuevo'], { relativeTo: this.route });
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
