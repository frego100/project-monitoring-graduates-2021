import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Enabled } from '@app/models/enabled';
import { Poll } from '@app/models/poll';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { PollService } from '@app/services/poll/poll.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
@Component({
  selector: 'app-manual-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class PollListComponent implements OnInit {
   selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Dirigida a Empresas"
    },
    {
      id:2,
      name: "Dirigida a Egresados"
    },
   ]
   pageSize = 10;
   currentPage = 0;
   totalSize = 0;
   search: string = '';
   public listPolls:Poll[]=[];
   displayedColumns: string[] = [
      'name','isCompany','isGraduate' ,'period_days', 'date_next_notify','actions'
   ];
   dataSource !:MatTableDataSource<Poll>;
   private ordering_list: Map<string, string>;
   loading: boolean = false;
   selectGroup: string = '';
   @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
   @ViewChild(MatSort) sort!: MatSort;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pollService: PollService,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  check(){
    return this.authUserService.checkPermissions("view_manual");
  }
  getData() {
    this.loading = true;
    // @ts-ignore
    this.pollService
      .getList(
        this.pageSize,
        this.currentPage * this.pageSize,
        '',
        this.search,
        this.ordering_list
      )
      .subscribe((poll) => {
        this.listPolls = poll.results;
        this.dataSource = new MatTableDataSource(this.listPolls);
        this.loading = false;
        this.totalSize = poll.count;
      });
  }

  edit(poll: any) {
    this.router.navigate([poll.id], {
       relativeTo: this.route
    });
  }

  add() {
    this.pollService.pollControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onChangeSort(e: any): void {
    if (!IsNullUndefinedBlank(e)) {
      let direct = '';
      if (!IsNullUndefinedBlank(e.direction)) {
        if (e.direction == 'asc') {
          direct = '+';
        } else if (e.direction == 'desc') {
          direct = '-';
        }
        this.ordering_list.set(e.active, direct + '' + e.active);
      } else {
        this.ordering_list.set(e.active, '');
      }
    }
    this.getData();
  }
  delete(id:any){
    this.pollService.remove(id).subscribe((res)=>{
      this.getData();
      this.snackBar.open('Operación exitosa', 'Cerrar', {
        duration: 3000,
      });
    });
  }
  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getData();
  }
}
