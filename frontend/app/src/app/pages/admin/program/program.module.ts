import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramListComponent } from './list/list.component';
import { ProgramFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from '../../../shared/guards/check-permission-guard';
export const ProgramRoutes: Routes = [
    {
       path: 'programas',
       children: [
          {
             path: '',
             component: ProgramListComponent,
              canActivate: [CanActivateAuthGuard],
              data: {permission: ['view_professionalprogram']},
          },
          {
             path: 'nuevo',
             component: ProgramFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['add_professionalprogram']},
          },
          {
             path: ':program',
             component: ProgramFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['change_professionalprogram']},
          }
       ]
    }
 
 ];
 
 @NgModule({
    declarations: [ ProgramListComponent, ProgramFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class UserModule {}
 