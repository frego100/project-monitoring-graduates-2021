import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Faculty } from '@app/models/faculty';
import { Enabled } from '@app/models/enabled';
import { FacultyService, ProgramService } from '@services';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class ProgramFormComponent implements OnInit {
  programForm: FormGroup;
  idTemp: number;
  editor = ClassicEditorBuild;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };

  public listFaculty: Faculty[] = [];
  programSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  program: any;
  showBeforeManual: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private programService: ProgramService,
    private facultadService: FacultyService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.programForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      is_active: [true],
      faculty_field: [null],
    });
  }

  ngOnInit(): void {
    this.getAllFaculty();
    this.route.params.subscribe((params) => {
      if (params.program && params.program !== 'nuevo') {
        this.getProgramById();
        this.getProgramByPathId(params.program);
      }
    });
  }

  private getProgramById() {
    this.programService.programControl.subscribe((res: any) => {
      this.programSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.programForm.reset();

      this.programSubscription = this.getSelectedItem(res).subscribe(
        (program: any) => {
          this.programForm.patchValue(program);

          this.programService.formControl.next({
            data: program,
            event: 'select',

          });
          this.programForm.patchValue({
            faculty_field: program.faculty_field.id
          });
        }
      );
    });
  }

  private getProgramByPathId(id) {
    this.programService.getById(id).subscribe((res) => {
      this.program = Object.assign({}, this.program, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.programService
            .getById(params.program)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, program: any) {
    return new Promise((resolve, reject) => {
      this.programService.update(_id, program).subscribe(
        (res) => {
          resolve({
            data: program,
            event: 'update',
          });
          this.router.navigate(['/admin/programas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(program: any) {
    return new Promise((resolve, reject) => {
      this.programService.create(program).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/programas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      if(keys[i] === "faculty_field")
        keys[i] = "Facultad"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    this.programForm.patchValue({
      faculty_field: this.idTemp,
    });
    const program = this.programForm.value;
    const send = program.id
      ? this.update(program.id, program)
      : this.create(program);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.programService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() { }

  removeAction() {
    const puesto = this.programForm.value;

    this.programService.remove(puesto.id).subscribe(
      () => {
        this.programService.programControl = new BehaviorSubject(null);

        this.programService.formControl.next({
          data: puesto.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.router.navigate(['/admin/programas']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
  onSelect(id: any): void {


    //this.programForm=program;

    this.idTemp = id;
  }
  getAllFaculty() {
    this.facultadService.getAll().subscribe((faculty: any) => {
      this.listFaculty = faculty.results;
      const list = this.listFaculty;
      const enabled = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listFaculty = enabled;
    });
  }
}
