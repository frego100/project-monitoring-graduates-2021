import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Enabled } from '@app/models/enabled';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { Program } from '@app/models/program';
import { ProgramService } from '@app/services/program/program.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ProgramListComponent implements OnInit {
  selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Habilitados"
    },
    {
      id:2,
      name: "Deshabilitados"
    },
    {
      id:3,
      name: "Todos"
    }
  ]

  public listProgram:Program[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      'name','actions'
  ];
   dataSource !:MatTableDataSource<Program>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private programServices: ProgramService,
    public authUserService: AuthUserService

  ) {}

  ngOnInit(): void {
    this.getAllPrograms();
   // this.dataSource.filterPredicate = this.getFilterPredicate();
  }

  check(){
    return this.authUserService.checkPermissions("view_area");
  }
  getAllPrograms(){
    this.programServices.getAll().subscribe((program:any) => {
      this.listProgram = program.results;
      this.dataSource = new MatTableDataSource(this.listProgram);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  onSelect(id:any):void{

  if(id==1){
    const list=this.listProgram;
    const habilitit=list.filter(item=>{
      if(item.is_active===true){
        return true;
      }
      else{
        return false;
      }
    })
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  if(id==2){
    const list=this.listProgram;
    const habilitit=list.filter(item=>{
      if(item.is_active===false){
        return true;
      }
      else{
        return false;
      }
    })
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  if(id==3){
    const list=this.listProgram;
    const habilitit=list;
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
       relativeTo: this.route
    });
 }

  add() {
    this.programServices.programControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
