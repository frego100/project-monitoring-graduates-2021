import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrackingUserListComponent } from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';

export const TrackingUserRoutes: Routes = [
   {
      path: 'historial',
      children: [
         {
            path: '',
            component: TrackingUserListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_historyuser']},
         },
      ]
   }

];

@NgModule({
   declarations: [ TrackingUserListComponent ],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      BrowserAnimationsModule,
      PipesModule
   ],
   entryComponents: [],
})
export class UserModule {}
