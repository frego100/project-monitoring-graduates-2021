import { Component, OnInit, ViewChild } from '@angular/core';
import { UserLite } from '@app/models/user';
import { TrackingUserService } from '@services';
import { RolesService } from '@services';
import { UserService } from '@services';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { History } from '@app/models/history';
import { RolLite } from '@app/models/rol';
import { formateMyDate, IsNullUndefinedBlank, isFormatISODate, addHour } from '@app/shared/helpers/general-utils';

@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})

export class TrackingUserListComponent implements OnInit {

  gridColumns = 3;
  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }

  loading: boolean = false;
  listGroup: RolLite[];
  selectGroup: string = '';
  listUser: UserLite[];
  selectUser: string = '';
  history: History[] = [];
  search: string = '';
  startDate: any = '';
  selectStartDate: string = '';
  endDate: any = '';
  selectEndDate: string = '';
  pageSize = 10;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'user__username', 'user__email', 'description', 'created', 'content_type'
  ];
  dataSource !: MatTableDataSource<History>;
  pageEvent: PageEvent;

  private ordering_list: Map<string, string>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    private trackingUserService: TrackingUserService,
    private roleService: RolesService,
    private userService: UserService,
    public authUserService: AuthUserService
  ) {
    this.ordering_list = new Map<string, string>();
  }

  /**
  * We load the data from the view previously
  *
  * @method ngOnInit
  */
  ngOnInit(): void {
    this.getData();
    this.getDataComboGroup();
    this.getDataComboUser('');
  }

  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getData();
  }

  /**
  * This event occurs when the user selects a value in the combo box,
  * then we get the selected value and
  * finally we ask the server for the data again.
  *
  * @method onSelectUser
  * @param username is the username selected in the combobox
  */
  onSelectUser(username: any): void {
    this.selectUser = username;
    this.paginator.firstPage();
    this.getData();
  }

  /**
  * This event occurs when the user clicks on the table headers, ascending
  * or descending ordering is applied.
  * It can be sorted based on many fields so we use a hashmap.
  * Finally we ask the server for the data again
  *
  * @method onChangeSort
  * @param e its a event - headers and direction
  */
  onChangeSort(e: any): void {
    if (!IsNullUndefinedBlank(e)) {
      var direct = '';
      if (!IsNullUndefinedBlank(e.direction)) {
        if (e.direction == 'asc') {
          direct = '+';
        } else if (e.direction == 'desc') {
          direct = '-';
        }
        this.ordering_list.set(e.active, direct + '' + e.active);
      } else {
        this.ordering_list.set(e.active, '');
      }
    }
    this.getData();
  }

  /**
  * This event occurs when the user selects a value in the combo box,
  * then we get the selected value and
  * finally we ask the server for the data again.
  *
  * @method onSelectGroup
  * @param group is the group's name selected in the combobox
  */
  onSelectGroup(group: any): void {
    if (!IsNullUndefinedBlank(group)) {
      this.selectGroup = group.name;
      this.getDataComboUser(group.id);
    } else {
      this.selectGroup = ''
      this.getDataComboUser('');
    }
    this.getData();
    this.paginator.firstPage();
  }

  /**
  * Request historical data from the server
  *
  * @method getData
  */
  getData() {
    this.loading = true;
    this.trackingUserService.getHistory(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.selectGroup,
      this.selectUser,
      this.selectEndDate,
      this.selectStartDate,
      this.search,
      this.ordering_list
    ).subscribe((history) => {
      this.history = history.results;
      this.dataSource = new MatTableDataSource(this.history);
      this.loading = false;
      this.totalSize = history.count;
    });
  }

  /**
  * Request group data from the server
  *
  * @method getDataComboGroup
  */
  getDataComboGroup() {
    this.roleService.getGroupLite().subscribe((groups) => {
      this.listGroup = groups;
    });
  }

  /**
  * Request user data from the server
  *
  * @method getDataComboUser
  */
  getDataComboUser(group: string) {
    this.userService.getUserLite(group).subscribe((users) => {
      this.listUser = users;
    });
  }

  /**
  * This method clears the filter values by dates.
  * Finally we ask the server for the data again.
  *
  * @method resetRange
  */
  resetRange(): void {
    this.startDate = '';
    this.endDate = '';
    this.selectEndDate = '';
    this.selectStartDate = '';
    this.getData();
  }

  /**
  * This method applies a text search filter when the
  * user presses the enter key in the search field
  *
  * @method applyFilter
  * @param key keyboard event
  */
  applyFilter(key: any) {
    var keycode = key.keyCode || key.which;
    if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getData();
      this.paginator.firstPage();
    }
  }

  /**
  * This event occurs when the user selects or writes a new start date,
  * it is also verified that the date is correct and is in standard ISO format.
  * Finally we ask the server for the data again.
  *
  * @method StartDateChange
  */
  StartDateChange() {
    if (!IsNullUndefinedBlank(this.startDate)) {
      var pickStartDate = formateMyDate(this.startDate);
      if (isFormatISODate(pickStartDate)) {
        this.selectStartDate = addHour(pickStartDate, 'start');
        this.getData();
        this.paginator.firstPage();
      }
    }
  }

  /**
  * This event occurs when the user selects or writes a new end date,
  * it is also verified that the date is correct and is in standard ISO format.
  * Finally we ask the server for the data again.
  *
  * @method EndDateChange
  */
  EndDateChange() {
    if (!IsNullUndefinedBlank(this.endDate)) {
      var pickEndDate = formateMyDate(this.endDate);
      if (isFormatISODate(pickEndDate)) {
        this.selectEndDate = addHour(pickEndDate, 'end');
        this.getData();
        this.paginator.firstPage();
      }
    }
  }
}
