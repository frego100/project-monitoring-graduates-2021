import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {Enabled} from '@app/models/enabled';
import {Faculty} from '@app/models/faculty';
import {AuthUserService} from '@app/services/auth-user/auth-user.service';
import {AreaService, FacultyService} from '@services';

@Component({
  selector: 'app-faculty-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class FacultyListComponent implements OnInit {
  selectedItem: any;
  searchCtrl: any;
  public enablementOptions: Enabled[] = [
    {
      id: 1,
      name: 'Habilitados',
    },
    {
      id: 2,
      name: 'Deshabilitados',
    },
    {
      id: 3,
      name: 'Todos',
    },
  ];

  public listFaculty: Faculty[] = [];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'name',
    'actions',
  ];
  dataSource!: MatTableDataSource<Faculty>;
  areas: Array<any>;
  selectArea = '';

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private facultyService: FacultyService,
    private areaService: AreaService,
    public authUserService: AuthUserService
  ) {}

  ngOnInit(): void {
    this.getAllFaculties();
    this.areaService.getAll().subscribe((res) => {
      this.areas = res.results;
    });
  }

  check() {
    return this.authUserService.checkPermissions('view_faculty');
  }
  getAllFaculties() {
    this.facultyService.getAll().subscribe((res: any) => {
      this.listFaculty = res.results;
      this.dataSource = new MatTableDataSource(this.listFaculty);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  getFacultiesByArea() {
    this.loading = true;
    this.facultyService.getByArea(this.selectArea).subscribe((res) => {
      this.listFaculty = res;
      this.dataSource = new MatTableDataSource(this.listFaculty);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  /**
   * This event occurs when the user selects a value in the combo box,
   * then we get the selected value and
   * finally we ask the server for the data again.
   *
   * @method onSelectArea
   * @param area is the area selected in the combobox
   */
  onSelectArea(area: any = ""): void {
    if(area == ""){
      this.getAllFaculties();
    }
    else{
      this.selectArea = area;
      this.paginator.firstPage();
      this.getFacultiesByArea();
    }
  }

  onSelect(id: any): void {
    if (id == 1) {
      const list = this.listFaculty;
      const enabled = list.filter((item) => {
        return item.is_active === true;
      });
      this.dataSource = new MatTableDataSource(enabled);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if (id == 2) {
      const list = this.listFaculty;
      const enabled = list.filter((item) => {
        return item.is_active === false;
      });
      this.dataSource = new MatTableDataSource(enabled);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if (id == 3) {
      const enabled = this.listFaculty;
      this.dataSource = new MatTableDataSource(enabled);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
      relativeTo: this.route,
    });
  }

  add() {
    this.facultyService.facultyControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
