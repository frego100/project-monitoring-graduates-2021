import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FacultyService, AreaService } from '@services';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-faculty-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FacultyFormComponent implements OnInit {
  facultyForm: FormGroup;
  editor = ClassicEditorBuild;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  areas: Array<any>;
  facultySubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  faculty: any;
  constructor(
    private formBuilder: FormBuilder,
    private facultyService: FacultyService,
    private areaService: AreaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.facultyForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      is_active: [true],
      area_field: [null],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.faculty && params.faculty !== 'nuevo') {
        this.getFacultyById();
        this.getFacultyByPathId(params.faculty);
      }
    });
    this.areaService.getAllEnabled().subscribe((res) => {
      this.areas = res.results;
    });
  }

  private getFacultyById() {
    this.facultyService.facultyControl.subscribe((res: any) => {
      this.facultySubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.facultyForm.reset();
      this.facultySubscription = this.getSelectedItem(res).subscribe(
        (faculty) => {
          this.facultyForm.patchValue(faculty);
          this.facultyService.formControl.next({
            data: faculty,
            event: 'select',
          });
        }
      );
    });
  }

  private getFacultyByPathId(id) {
    this.facultyService.getById(id).subscribe((res) => {
      this.faculty = Object.assign({}, this.faculty, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.facultyService
            .getById(params.faculty)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, faculty: any) {
    return new Promise((resolve, reject) => {
      this.facultyService.update(_id, faculty).subscribe(
        (res) => {
          resolve({
            data: faculty,
            event: 'update',
          });
          this.router.navigate(['/admin/facultades']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(faculty: any) {
    return new Promise((resolve, reject) => {
      this.facultyService.create(faculty).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/facultades']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      if(keys[i] === "area_field")
        keys[i] = "Área"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const faculty = this.facultyForm.value;
    const send = faculty.id
      ? this.update(faculty.id, faculty)
      : this.create(faculty);
    send.then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.facultyService.formControl.next(res);
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() {}

  removeAction() {
    const faculty = this.facultyForm.value;
    this.facultyService.remove(faculty.id).subscribe(
      () => {
        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['..'], { relativeTo: this.route });
      },
      () => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
