import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FacultyListComponent } from './list/list.component';
import { FacultyFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
export const FacultyRoutes: Routes = [
  {
    path: 'facultades',
    children: [
      {
        path: '',
        component: FacultyListComponent,
        canActivate: [CanActivateAuthGuard],
        data: {permission: ['view_faculty']},
      },
      {
        path: 'nuevo',
        component: FacultyFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: {permission: ['add_faculty']},
      },
      {
        path: ':faculty',
        component: FacultyFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: {permission: ['change_faculty']},
      },
    ],
  },
];

@NgModule({
  declarations: [FacultyListComponent, FacultyFormComponent],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule,
  ],
  entryComponents: [],
})
export class UserModule {}
