import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityListComponent } from './list/list.component';
import { ActivityFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SelectTargetModule } from '@app/shared/components/select-target/module';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
import { GraduateSelectActionModalComponent } from '@app/shared/components/graduate-action/graduate-action.component';
import {ObjectiveGroupModalComponent} from "@shared/components/objective-group/objective-group.component";
import {ObjectiveGroupFormComponent} from "@shared/components/objective-group/form/form.component";


export const ActivityRoutes: Routes = [
   {
      path: 'actividades',
      children: [
         {
            path: '',
            component: ActivityListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_activity']},

         },
         {
            path: 'nuevo',
            component: ActivityFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['add_activity']},
         },
         {
            path: ':activity',
            component: ActivityFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_activity']},
         }
      ]
   }
];

@NgModule({
   declarations: [ ActivityListComponent, ActivityFormComponent, GraduateSelectActionModalComponent, ObjectiveGroupModalComponent, ObjectiveGroupFormComponent],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      PipesModule,
      CKEditorModule,
      SelectTargetModule,

   ],
   entryComponents: [],
   exports: [GraduateSelectActionModalComponent, ObjectiveGroupModalComponent, ObjectiveGroupFormComponent ]
})
export class ActivityModule {}
