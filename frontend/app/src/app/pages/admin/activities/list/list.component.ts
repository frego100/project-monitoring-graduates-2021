import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { Activity } from '@app/models/activity';
import { ActivityService, UserService } from '@services';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Enabled } from '@app/models/enabled';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { MatDialog } from '@angular/material/dialog';
import { GraduateSelectActionModalComponent } from '@app/shared/components/graduate-action/graduate-action.component';
import { GraduateListService } from '@app/services/graduate/graduate-list.service';
import {ObjectiveGroupModalComponent} from "@shared/components/objective-group/objective-group.component";

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-activity-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ActivityListComponent implements OnInit {

  @ViewChild('selectTarget', { static: false }) selectTarget: any;
  complete: any;
  progress: any;
  selectedUsers = [];


  selectedItem: any;
  searchCtrl: any;
  loading: boolean = false;
  public selectHabilitado: Enabled = { id: 0, name: "" };
  public habilitado: Enabled[] = [
    {
      id: 1,
      name: "Habilitados"
    },
    {
      id: 2,
      name: "Deshabilitados"
    },
    {
      id: 3,
      name: "Todos"
    }
  ]
  public listActivitys: Activity[] = [];
  displayedColumns: string[] = [
    //  'nro',
    'title',
    'startDate',
    'endDate',
    'validated',
    'actions',
  ];
  dataSource!: MatTableDataSource<Activity>;
  search = '';
  pageSize = 10;
  currentPage = 0;
  totalSize = 0;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activityService: ActivityService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private userService: UserService,
    public authUserService: AuthUserService,
    public graduateService: GraduateListService
  ) { }


  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
   handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getAllActivitys();
  }

  ngOnInit(): void {
    this.getAllActivitys();
  }

  onSelect(id: any): void {
    if (id == 1) {
      const list = this.listActivitys;
      const habilitit = list.filter(item => {
        if (item.is_validated === true) {
          return true;
        }
        else {
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if (id == 2) {
      const list = this.listActivitys;
      const habilitit = list.filter(item => {
        if (item.is_validated === false) {
          return true;
        }
        else {
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if (id == 3) {
      const list = this.listActivitys;
      const habilitit = list;
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  }

  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnDestroy(): void {
    this.activityService.activityControl = new BehaviorSubject(null);
  }

  getAllActivitys() {
    this.loading = true;
    this.activityService.getAll(
      this.pageSize,
      this.currentPage * this.pageSize,
      true,
      this.search
    ).subscribe((activities: any) => {
      this.listActivitys = activities.results;
      this.dataSource = new MatTableDataSource(activities.results);
      this.totalSize = activities.count;
      this.loading = false;
    });

  }

  edit(activity: any) {
    this.router.navigate([activity.id], {
      relativeTo: this.route
    });
  }

  add() {
    this.activityService.activityControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(key: any) {
    var keycode = key.keyCode || key.which;
    if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getAllActivitys();
      this.paginator.firstPage();
    }
    // const filterValue = (event.target as HTMLInputElement).value;
    // this.datasoruce.filter = filterValue.trim().toLowerCase();
  }
 objectiveGroup(data: any, openDialog = true) {
    const dialogRef = this.dialog.open(ObjectiveGroupModalComponent, {
      width: '1024px',
      data:{
        title: 'Grupos Objetivos',
        activity: data.id,
        links: {
          link: data.link,
          stream_link: data.stream_link,
          meet_link: data.meet_link,
        },
      }
    }).componentInstance;
   dialogRef.afterSelected.subscribe(result => {
   });
  }


  enroll(data: any, openDialog = true) {
    // this.progress = new Subject();
    // this.complete = new BehaviorSubject(null);

    const columns = [
      { name: 'id', title: 'id', show: false },
      { name: 'user.first_name', title: 'Nombre', show: true },
      { name: 'user.last_name', title: 'Apellido', show: true },
      { name: 'user.email', title: 'Correo', show: true },
      { name: 'cui', title: 'CUI', show: true },
      { name: 'user.username', title: 'Usuario', show: true },
      { name: 'cell_phone', title: 'Contacto', show: true },
    ];

    const dialogRef = this.dialog.open(GraduateSelectActionModalComponent, {
       width: '1024px',
       data:{
        title: 'Inscribir egresados',
        activity: data.id,
        checkbox: true,
        sticky: true,
        columns,
        selected: {
          users: this.selectedUsers,
        },
      }
    }).componentInstance;

    dialogRef.afterSelected.subscribe((items : any) =>{
       this.selectedUsers=items.users;

       this.activityService.enrollActivityMultipleUser({
         "activity":data.id,
         "users":this.selectedUsers,
       }).subscribe((res)=>{
          dialogRef.in_progress = false;
          // dialogRef.close();
          this.snackBar.open('Usuarios inscritos correctamente!', 'Cerrar', {
            duration: 3000,
          });
          dialogRef.getData();
       })
       // this.complete.next(items)
    });

  }
}
