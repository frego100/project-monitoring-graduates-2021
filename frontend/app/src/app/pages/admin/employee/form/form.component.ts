import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';
import { TypeEmployeeService } from '@app/services/type-employee/type-employee.service';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-employee-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class EmployeeFormComponent implements OnInit {
  imageSrc: string = '';
  employeeForm: FormGroup;
  editor = ClassicEditor;
  mediaType = 'image';
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  employeeSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  employee: any;
  showBeforeManual: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private employeeService: TypeEmployeeService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.employeeForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.employee && params.employee !== 'nuevo') {
        this.getManualById();
        this.getManualByPathId(params.employee);
      }
    });
  }

  private getManualById() {
    this.employeeService.employedControl.subscribe((res: any) => {
      this.employeeSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.employeeForm.reset();

      this.employeeSubscription = this.getSelectedItem(res).subscribe(
        (employee: any) => {
          
          this.employeeForm.patchValue(employee);
          this.employeeService.formControl.next({
            data: employee,
            event: 'select',
          });
        }
      );
    });
  }

  private getManualByPathId(id) {
    this.employeeService.getById(id).subscribe((res) => {
      this.employee = Object.assign({}, this.employee, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.employeeService
            .getById(params.employee)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, employee: any) {
    return new Promise((resolve, reject) => {
      this.employeeService.update(_id, employee).subscribe(
        (res) => {
          resolve({
            data: employee,
            event: 'update',
          });
          this.router.navigate(['/admin/empleados']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(employee: any) {
    return new Promise((resolve, reject) => {
      this.employeeService.create(employee).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/empleados']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const empl = this.employeeForm.value;
    const send = empl.id ? this.update(empl.id, empl) : this.create(empl);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.employeeService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() {}

  removeAction() {
    const emplead = this.employeeForm.value;

    this.employeeService.remove(emplead.id).subscribe(
      () => {
        this.employeeService.employedControl = new BehaviorSubject(null);

        this.employeeService.formControl.next({
          data: emplead.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.router.navigate(['..'], { relativeTo: this.route });
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
