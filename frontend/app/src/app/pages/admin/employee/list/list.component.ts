import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {Employee} from '@app/models/employee';
import {Enabled} from '@app/models/enabled';
import {AuthUserService} from '@app/services/auth-user/auth-user.service';
import {TypeEmployeeService} from '@app/services/type-employee/type-employee.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class EmployeeListComponent implements OnInit {


  selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Habilitados"
    },
    {
      id:2,
      name: "Deshabilitados"
    },
    {
      id:3,
      name: "Todos"
    }
  ]

  public listManuals:Employee[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      'name','actions'];
   dataSource !:MatTableDataSource<Employee>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private employeeService: TypeEmployeeService,
    public authUserService: AuthUserService

  ) {}

  ngOnInit(): void {
    this.getAll();
  }

  check(){
    return this.authUserService.checkPermissions("view_manual");
  }
  getAll(){
    this.employeeService.getAll().subscribe((manual:any) => {
      this.listManuals = manual.results;
      this.dataSource = new MatTableDataSource(this.listManuals);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  onSelect(id:any):void{
    if(id==1){
      const list=this.listManuals;
      const habilitit=list.filter(item=>{
        return item.is_active === true;
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==2){
      const list=this.listManuals;
      const habilitit=list.filter(item=>{
        return item.is_active === false;
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==3){
      const list=this.listManuals;
      this.dataSource = new MatTableDataSource(list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
       relativeTo: this.route
    });
 }

  add() {
    this.employeeService.employedControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
