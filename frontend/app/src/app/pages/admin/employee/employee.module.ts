import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './list/list.component';
import { EmployeeFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
export const EmployeeRoutes: Routes = [
  {
    path: 'empleados',
    children: [
      {
        path: '',
        component: EmployeeListComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['view_typeemployee'] },
      },
      {
        path: 'nuevo',
        component: EmployeeFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['add_typeemployee'] },
      },
      {
        path: ':employee',
        component: EmployeeFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['change_typeemployee'] },
      },
    ],
  },
];

@NgModule({
  declarations: [EmployeeListComponent, EmployeeFormComponent],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule,
  ],
  entryComponents: [],
})
export class EmployeeModule {}
