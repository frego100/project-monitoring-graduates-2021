import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AreaService } from '@app/services/area/area.service';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-area-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class AreaFormComponent implements OnInit {
  areaForm: FormGroup;
  editor = ClassicEditorBuild;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  puestoTrSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  area: any;
  showBeforeManual: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private areaService: AreaService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.areaForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.area && params.area !== 'nuevo') {
        this.getAreaById();
        this.getAreaByPathId(params.area);
      }
    });
  }

  private getAreaById() {
    this.areaService.areaControl.subscribe((res: any) => {
      this.puestoTrSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.areaForm.reset();

      this.puestoTrSubscription = this.getSelectedItem(res).subscribe(
        (puestoTr: any) => {
          this.areaForm.patchValue(puestoTr);
          this.areaService.formControl.next({
            data: puestoTr,
            event: 'select',
          });
        }
      );
    });
  }

  private getAreaByPathId(id) {
    this.areaService.getById(id).subscribe((res) => {
      this.area = Object.assign({}, this.area, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.areaService
            .getById(params.area)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, area: any) {
    return new Promise((resolve, reject) => {
      this.areaService.update(_id, area).subscribe(
        (res) => {
          resolve({
            data: area,
            event: 'update',
          });
          this.router.navigate(['/admin/areas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(puesto: any) {
    return new Promise((resolve, reject) => {
      this.areaService.create(puesto).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/areas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const puesto = this.areaForm.value;
    const send = puesto.id
      ? this.update(puesto.id, puesto)
      : this.create(puesto);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.areaService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() { }

  removeAction() {
    const puesto = this.areaForm.value;

    this.areaService.remove(puesto.id).subscribe(
      () => {
        this.areaService.areaControl = new BehaviorSubject(null);

        this.areaService.formControl.next({
          data: puesto.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/areas']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
