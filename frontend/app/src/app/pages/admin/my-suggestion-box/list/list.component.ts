import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import {SuggestionService} from "@app/services/suggestion_box/suggestion_box.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class MySuggestionBoxListComponent implements OnInit {

  selectedItem: any;
  searchCtrl: any;

  rol: any;

  public listSuggestion: any[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      //'nro',
      'text','datetime','user_pk.username' ,'actions'];
   dataSource !:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private suggestionServices: SuggestionService,
    public authUserService: AuthUserService
  ) {
    this.rol = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit(): void {
    this.getAllAuthSuggestions();
  }

  check(){
    return this.authUserService.checkPermissions("view_suggestion");
  }
  getAllAuthSuggestions(){
    this.suggestionServices.getAllAuth().subscribe((response:any) => {
      this.listSuggestion = response.results;
      this.dataSource = new MatTableDataSource(this.listSuggestion);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  edit(suggestion: any) {
    this.router.navigate([suggestion.id], {
       relativeTo: this.route
    });
 }

  add() {
    this.suggestionServices.suggestionControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
