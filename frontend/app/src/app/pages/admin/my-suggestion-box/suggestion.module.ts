import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MySuggestionBoxFormComponent} from "@pages/admin/my-suggestion-box/form/form.component";
import {MySuggestionBoxListComponent} from "@pages/admin/my-suggestion-box/list/list.component";
export const MySuggestionRoutes: Routes = [
    {
       path: 'mis_sugerencias',
       children: [
          {
             path: '',
             component: MySuggestionBoxListComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['view_suggestion']},
          },
          {
             path: 'nuevo',
             component: MySuggestionBoxFormComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['add_suggestion']},
          },
          {
             path: ':mis_sugerencias',
             component: MySuggestionBoxFormComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['change_suggestion']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [ MySuggestionBoxListComponent, MySuggestionBoxFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class SuggestionModule {}
