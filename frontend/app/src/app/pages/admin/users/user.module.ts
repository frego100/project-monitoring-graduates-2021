import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './list/list.component';
import { UserFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
import { ProfileSuperadminComponent } from './form/profiles/profile-superadmin/profile-superadmin.component';
import { ProfileCompanyComponent } from './form/profiles/profile-company/profile-company.component';
import { ProfileAdminComponent } from './form/profiles/profile-admin/profile-admin.component';
import { ProfileGraduateComponent } from './form/profiles/profile-graduate/profile-graduate.component';
import { ProfileModule } from '../profiles/form-graduate/profiles.graduate.module';

export const UserRoutes: Routes = [
  {
    path: 'usuarios',
    children: [
      {
        path: '',
        component: UserListComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['view_user'] },
      },
      {
        path: 'nuevo',
        component: UserFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['add_user'] },
      },
      {
        path: ':user',
        component: UserFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['change_user'] },
      },
    ],
  },
];

@NgModule({
  declarations: [
    UserListComponent,
    ProfileAdminComponent,
    ProfileGraduateComponent,
    UserFormComponent,
    ProfileSuperadminComponent,
    ProfileCompanyComponent,
  ],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule,
    ProfileModule
  ],
  entryComponents: [],
})
export class UserModule {}
