import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Identifier} from "@app/models/identifier";
import {Jobs} from "@app/models/job";
import {Employee} from "@models/employee";
import {TypeEmployeeService, IdentifierService, JobsService, ProfileService} from "@services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute, Router} from "@angular/router";
import {IsNullUndefined, isObject} from "@shared/helpers/general-utils";

@Component({
  selector: 'profile-superadmin',
  templateUrl: './profile-superadmin.component.html',
  styleUrls: ['./profile-superadmin.component.scss']
})
export class ProfileSuperadminComponent implements OnInit {


  public listIdentifiers: Identifier[] = [];
  public listJobs: Jobs[] = [];
  public listEmployee: Employee[] = [];

  showBeforeImage: boolean = false;
  profile: any;

  user;
  profileForm: FormGroup;

  createFlag = true;
  @Output() redirect = new EventEmitter();
  @Output() profileReady = new EventEmitter<any>();
  @Input()
  set userItem(value) {
    if (value) {
      this.user = value;
    }
  }
  constructor(
    private formBuilder: FormBuilder,
    private identifierService: IdentifierService,
    private jobsService: JobsService,
    private employeeService: TypeEmployeeService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar
  ) {
    this.profileForm = this.formBuilder.group({
      id: [null],
      identificator: ['', Validators.required],
      job_position: ['', Validators.required],
      type_employee: ['', Validators.required],
      document: ['', Validators.required],
      cell_phone: ['', Validators.required],
      phone: ['', Validators.compose([Validators.min(111111),Validators.max(999999)])],
      picture: [null],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.getAllIdentifiers();
    this.getAllJobPosition();
    this.getAllEmployee();
    if (this.user && this.user.profile !== null) {
      this.profileService.getSuperAdmin(this.user.profile).subscribe((res) => {
        let data = Object.assign({}, res, res);
        data.job_position = data.job_position.id;
        data.type_employee = data.type_employee.id;
        data.identificator = data.identificator.id;
        this.profileForm.patchValue(data);
        this.profile =res;
        this.profileService.formControl.next({
          data: res,
          event: 'select',
        });
        this.createFlag = false;
        this.showBeforeImage = !IsNullUndefined(res.picture);
      });
    }
  }
  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.listIdentifiers = identifiers.results;
    });
  }
  getAllJobPosition() {
    this.jobsService.getAll().subscribe((jobPosition) => {
      this.listJobs = jobPosition.results;
    });
  }
  getAllEmployee() {
    this.employeeService.getAll().subscribe((employee) => {
      this.listEmployee = employee.results;
    });
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.profileForm.patchValue({
        picture: file,
      });
      this.profileForm.get('picture').updateValueAndValidity();
    }
  }
  save() {
    if(!this.profileForm.valid) {
      this.snackBar.open('Faltan completar correctamente algunos datos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
    const profileFormValue = this.profileForm.value;
    if (isObject(profileFormValue.identificator)) {
      profileFormValue.identificator = profileFormValue.identificator.id;
    }
    if (isObject(profileFormValue.job_position)) {
      profileFormValue.job_position = profileFormValue.job_position.id;
    }
    if (isObject(profileFormValue.type_employee)) {
      profileFormValue.type_employee = profileFormValue.type_employee.id;
    }
    delete profileFormValue.picture;
    this.profileReady.emit(profileFormValue);
  }
  create(profile: any){
    return new Promise((resolve, reject) => {
      this.profileService.postSuperAdmin(profile).subscribe(
        (res) => {
          resolve({
            data: profile,
            event: 'create',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  update(_id: any, user: any){
    const profile = Object.assign({}, user, {
      identificator: user.identificator.id,
      job_position: user.job_position.id,
      type_employee: user.type_employee.id
    });
    return new Promise((resolve, reject) => {
      this.profileService.putCompany(_id, profile).subscribe(
        (res) => {
          resolve({
            data: user,
            event: 'update',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }

}
