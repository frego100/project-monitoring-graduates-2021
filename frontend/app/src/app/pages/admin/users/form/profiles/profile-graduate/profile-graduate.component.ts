import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Identifier } from '@app/models/identifier';
import {
  CompanyService,
  IdentifierService,
  ProfileService
} from '@services';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';
import { Gender } from '@app/models/gender';
import { Company } from '@app/models/company';
const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

@Component({
  selector: 'profile-graduate',
  templateUrl: './profile-graduate.component.html',
  styleUrls: ['./profile-graduate.component.scss'],
})
export class ProfileGraduateComponent implements OnInit {
  public identifierList: Identifier[] = [];
  public companyList: Company[] = [];
  showBeforeImage: boolean = false;
  profile: any;
  isDCP:number;
  user;
  profileForm: FormGroup;
  public dcpList:any[]=[
    {
      id:1,
      name: "Si",
      value:true,
      checked:false,
    },
    {
      id:0,
      name: "No",
      value:false,
      checked:false,
    }
  ]
  public genderList: Gender[] = [
    {
      id: 1,
      name: "Femenino",
      value: true
    },
    {
      id: 2,
      name: "Masculino",
      value: false
    }
  ]
  createFlag = true;
  @Output() redirect = new EventEmitter();
  @Output() profileReady = new EventEmitter<any>();
  @Input()
  set userItem(value) {
    if (value) {
      this.user = value;
    }
  }
  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private identifierService: IdentifierService,
    private companyService: CompanyService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar
  ) {
    this.profileForm = this.formBuilder.group({
      identificator: ['', Validators.required],
      document: ['', Validators.required],
      cell_phone: ['', Validators.compose([Validators.min(111111111), Validators.max(999999999), Validators.required])],
      phone: ['', Validators.compose([Validators.min(111111),Validators.max(999999)])],
      is_active: true,
      gender: [''],
      email: [''],
      description_dcp:[''],
      is_dcp:[''],
      cui: ['', Validators.compose([Validators.min(11111111),Validators.max(99999999), Validators.required])],
      date_birthday: ['', Validators.required],
      web: ['', [Validators.pattern(urlRegex)]],
      address: [''],
      picture: [null]
    });
  }

  ngOnInit(): void {
    this.getAllIdentifiers();
    this.getAllCompanies();
    if (this.user && this.user.profile !== null) {
      this.profileService.getGraduate(this.user.profile).subscribe((res) => {
        this.profileForm.reset();
        let graduate;
        if (res.gender) {
          graduate = Object.assign(res, {gender: this.genderList[0]})
        }
        else {
          graduate = Object.assign(res, {gender: this.genderList[1]})
        }
        graduate = Object.assign({}, graduate, graduate);
        graduate.identificator = graduate.identificator.id;
        this.profileForm.patchValue(graduate);
        this.profile =graduate;
        this.profileService.formControl.next({
          data: graduate,
          event: 'select',
        });
        this.createFlag = false;
        this.showBeforeImage = !IsNullUndefined(graduate.picture);
        if(graduate.is_dcp==false){
          this.dcpList[0].checked=false;
          this.dcpList[1].checked=true;
          this.profileForm.patchValue({
            description_dcp:null,
           });
          this.profileForm.controls['description_dcp'].disable();
         this.isDCP=0;
       }else{
         this.isDCP=1;
         this.dcpList[0].checked=true;
         this.dcpList[1].checked=false;
       }
      });
    }
  }
  radioButton(event: any) {
    if(event.value===1){
      this.isDCP=1;
      this.profileForm.controls['description_dcp'].enable();
      this.profileForm.patchValue({
        is_dcp:true,
       });

    }else if(event.value===0){
      this.isDCP=0;
      this.profileForm.patchValue({
        description_dcp:null,
       });
       this.profileForm.controls['description_dcp'].disable();

    }else{
      this.profileForm.patchValue({
        description_dcp:null,
       });
       this.profileForm.controls['description_dcp'].disable();

    }
}
  update(_id: any, user: any) {
    return new Promise((resolve, reject) => {
      this.profileService.patchGraduate(_id, user).subscribe(
        (res) => {
          resolve({
            data: user,
            event: 'update',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(profile: any) {
    return new Promise((resolve, reject) => {
      this.profileService.postGraduate(profile).subscribe(
        (res) => {
          resolve({
            data: profile,
            event: 'create',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  save() {
    if(!this.profileForm.valid) {
      this.snackBar.open('Faltan completar correctamente algunos datos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
    const dcpValue = this.dcpList.filter(item => {
      return item.id === this.isDCP;
    });
    this.profileForm.patchValue({
      is_dcp:dcpValue[0].value
    });
    const profileFormValue = this.profileForm.value;
    const profile = Object.assign({}, profileFormValue, {
      identificator: profileFormValue.identificator.id,
      gender: profileFormValue.gender.value,
    });
    this.profileReady.emit(profile);
  }

  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.identifierList = identifiers.results;
    });
  }

  getAllCompanies() {
    this.companyService.getActive().subscribe((res) => {
      this.companyList = res.results;
    });
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.profileForm.patchValue({
        picture: file,
      });
      this.profileForm.get('picture').updateValueAndValidity();
    }
  }

  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock.id === newBlock.id;
  }

}
