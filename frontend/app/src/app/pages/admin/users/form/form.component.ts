import { Component, OnInit, ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IdentifierService, RolesService, UserService } from '@services';
import { Subscription, of, Observable } from 'rxjs';
import { Roles } from '@app/models/rol';
import { Identifier } from '@app/models/identifier';
import { Jobs } from '@app/models/job';
import { Employee } from '@models/employee';
import { TypeEmployeeService } from '@app/services/type-employee/type-employee.service';
import { JobsService } from '@app/services/job/job.service';
import { Profile } from '@models/profile';
import { ProfileService } from '@app/services/profile/profile.service';
import {IsNullUndefinedBlank, isObject, makeMessageErrorHttp} from '@app/shared/helpers/general-utils';

@Component({
  selector: 'app-users-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class UserFormComponent implements OnInit {
  @ViewChild('stepper') private myStepper: MatStepper;
  usersForm: FormGroup;
  user: any;
  id_profile: number;
  userSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  secondFormGroup: FormGroup;

  public listRoles: Roles[] = [];
  public listIdentificadores: Identifier[] = [];
  public listPuestoTrabajo: Jobs[] = [];
  public listTipoEmpleado: Employee[] = [];
  public listSuperAdmin: Profile[] = [];
  id_user_created: any;

  modoEdit: boolean = false;
  showBeforeImage: boolean = false;
  show_check: boolean = false;
  create_profile: boolean = false;
  flag_edit_profile: boolean = true;
  exist_kind_profile: boolean = false;
  wait_for_roles: boolean = false;
  profile: any;
  profile_restore: any;
  isOptional = false;
  profile_aux : any;
  id_route: any;

  //Verifico el tipo de perfil
  model_kind_profile: any;

  public profileImage: any = [];
  profileSubscription: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private rolesService: RolesService,
    private identifierService: IdentifierService,
    private jobsService: JobsService,
    private employeeService: TypeEmployeeService,
    private profileService: ProfileService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private _formBuilder: FormBuilder,
  ) {
    this.usersForm = this.formBuilder.group({
      id: [null],
      username: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: [''],
      groups: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required],
    });
    this.route.params.subscribe((params) => {
      if (params.user && params.user !== 'nuevo') {
        this.modoEdit = true;
        this.id_route = params.user
      }
    });
    this.getAllRoles();
    this.getAllIdentifiers();
    this.getAllJobPosition();
    this.getAllEmployee();
  }

  getAllRoles() {
    this.rolesService.getAll().subscribe((roles) => {
      this.listRoles = roles.results;
      this.route.params.subscribe((params) => {
        if (params.user && params.user !== 'nuevo') {
          this.getUserByPathId(params.user);
        }
      });
    });
  }

  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.listIdentificadores = identifiers.results;
    });
  }

  getAllJobPosition() {
    this.jobsService.getAll().subscribe((jobPosition) => {
      this.listPuestoTrabajo = jobPosition.results;
    });
  }

  getAllEmployee() {
    this.employeeService.getAll().subscribe((employee) => {
      this.listTipoEmpleado = employee.results;
    });
  }

  changeRol(event: any){
    if(event.profile_type !== null){
      if (!this.exist_kind_profile){
        this.show_check = true
      }
      this.model_kind_profile = event.profile_type.model
    }else{
      this.show_check = false
      this.model_kind_profile = ''
    }
  }

  private getUserByPathId(id) {
    this.userService.getById(id).subscribe((res) => {
      this.user = Object.assign({}, this.user, res);
      if (this.user.groups) {
        this.show_check = !(this.user.groups[0].profile_type !== null && this.user.profile !== null);
        const group: any = this.listRoles.find((x) => x.id === this.user.groups[0].id);
        this.user = Object.assign({}, this.user, { groups: group });
        this.model_kind_profile = this.user.groups.profile_type.model
      }
      this.usersForm.patchValue(this.user);
      this.id_profile = this.user.profile;
      this.exist_kind_profile = !IsNullUndefinedBlank(this.id_profile);
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, user: any) {
    return new Promise((resolve, reject) => {
      this.userService.update(_id, user).subscribe(
        (res) => {
          if(this.profile!==undefined){
            this.profile.user = res.id
          }
          if (this.exist_kind_profile) {
            if (this.model_kind_profile == 'adminprofile'){
              this.profileService.patchAdmin(this.profile.id, this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'update' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'companyprofile') {
              let dataSend = this.profile;
              if(isObject(dataSend.identificator)){
                dataSend.identificator = dataSend.identificator.id;
              }
              this.profileService.patchCompanyProfile(this.profile.id, dataSend).subscribe(
                () => {
                  resolve({ data: user, event: 'update' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'superadminprofile') {
              this.profileService.patchSuperAdmin(this.profile.id, this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'update' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'graduateprofile') {
              this.profileService.patchGraduate(this.id_profile, this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'update' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            }
          } else {
            if (this.model_kind_profile == 'adminprofile'){
              this.profileService.postAdmin(this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'create' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'companyprofile') {
              this.profileService.postCompany(this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'create' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'superadminprofile') {
              this.profileService.postSuperAdmin(this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'create' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'graduateprofile') {
              this.profileService.postGraduate(this.profile).subscribe(
                (res) => {
                  resolve({ data: user, event: 'create' });
                  this.id_profile = res.id
                  this.snackBar.open('Operación exitosa!, ahora puede registrar la informacion de detalle', 'Cerrar', { duration: 3000 });
                  this.exist_kind_profile = true;
                },
                () => { (err) => { reject(err); } }
              );
            }
          }
        },
        (err) => { reject(err); }
      );
    });
  }

  create(user: any) {
    return new Promise((resolve, reject) => {
      this.userService.create(user).subscribe(
        (res) => {
          if (this.create_profile) {
            this.profile.user = res.id;
            this.id_route = res.id;
            if (this.model_kind_profile == 'adminprofile'){
              this.profileService.postAdmin(this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'create' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'companyprofile') {
              this.profileService.postCompany(this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'create' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'superadminprofile') {
              this.profileService.postSuperAdmin(this.profile).subscribe(
                () => {
                  resolve({ data: user, event: 'create' });
                  this.redirect();
                },
                () => { (err) => { reject(err); } }
              );
            } else if (this.model_kind_profile == 'graduateprofile') {
              this.profileService.postGraduate(this.profile).subscribe(
                (res) => {
                  resolve({ data: user, event: 'create' });
                  this.id_profile = res.id
                  this.snackBar.open('Operación exitosa!, ahora puede registrar la informacion de detalle', 'Cerrar', { duration: 3000 });
                  this.exist_kind_profile = true;
                },
                () => { (err) => { reject(err); } }
              );
            }
          } else {
            resolve({ data: user, event: 'create' });
          }
        },
        (err) => { reject(err); }
      );
    });
  }

  redirect(item = '') {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  saveUser(value) {
    if(!this.usersForm.valid) {
      this.myStepper.steps.get(0).completed = false;
      this.snackBar.open('Faltan completar correctamente algunos datos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
    this.myStepper.steps.get(0).completed = true;
    this.myStepper.next();
    if (this.model_kind_profile == 'graduateprofile'){
      let validate_email = this.usersForm.value.email.split("@");
        if (validate_email[1] != "unsa.edu.pe") {
          this.snackBar.open('Para egresados el correo PRINCIPAL en la seccion de DATOS DE USUARIO debe ser @unsa.edu.pe', 'Cerrar', {
            duration: 6000,
          });
          this.myStepper.steps.get(0).completed = false;
          return;
        }
    }
    if (value) {
      const userFormValue = this.usersForm.value;
      const group = [userFormValue.groups.id];
      const user = Object.assign({}, userFormValue, { groups: group });
      if (user.password && user.password === '') {
        delete user.password;
      }
      const send = (user.id||this.exist_kind_profile) ? this.update(this.id_route, user) : this.create(user);
      send.then(() => {
        if (!this.create_profile) {
          this.snackBar.open('Operación exitosa!', 'Cerrar', { duration: 3000 });
          this.redirect();
        } else {
          if (this.model_kind_profile != 'graduateprofile'){
            this.snackBar.open('Operación exitosa!', 'Cerrar', { duration: 3000 });
            this.redirect();
          }
        }
      }).catch((error) => {
        let message = ''
        let valores = Object.values(error.error);
        for (let i=0; i < valores.length; i++) {
          message = message + valores[i][0] + ' . ';
        }
        this.snackBar.open(message, 'Cerrar', { duration: 3000 });
      });
    }
  }

  updateStatus() {
    this.create_profile = !this.create_profile
  }

  profileReady(event){
    let dataProfile = JSON.parse(JSON.stringify(event))
    if (this.model_kind_profile==='companyprofile'){
      if(!isObject(dataProfile.company)){
        dataProfile.company = {
          company_activity: "mock",
          company_name: "mock",
          company_reason: "mock",
          social_reason: "mock",
          ruc: dataProfile.document
        }
      }
    }
    this.route.params.subscribe((params) => {
      if (params.user && params.user !== 'nuevo') {
        this.myStepper.steps.get(1).completed = true;
        this.myStepper.next();
        this.profile = event;
        this.saveUser(false)
      } else {
        this.profileService.validateData({ type: this.model_kind_profile, data: dataProfile}).subscribe(()=>{
          this.myStepper.steps.get(1).completed = true;
          this.myStepper.next();
          this.profile = event;
          this.saveUser(false)
        },(error)=>{
          this.myStepper.steps.get(1).completed = false;
          this.snackBar.open(makeMessageErrorHttp(error.error), 'Cerrar', {
            duration: 3000,
          });
        });
      }
    });
  }
}
