import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CompanyService, IdentifierService , ProfileService} from "@services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute, Router} from "@angular/router";
import {Identifier} from "@app/models/identifier";
import {Company} from "@models/company";
import {isObject} from "@shared/helpers/general-utils";

@Component({
  selector: 'profile-company',
  templateUrl: './profile-company.component.html',
  styleUrls: ['./profile-company.component.scss']
})
export class ProfileCompanyComponent implements OnInit {

  public listIdentifiers: Identifier[] = [];
  public companyList: Company[] = [];

  showBeforeImage: boolean = false;
  company: any;

  user;
  companyForm: FormGroup;

  createFlag = true;
  @Output() redirect = new EventEmitter();
  @Output() profileReady = new EventEmitter<any>();
  @Input()
  set userItem(value) {
    if (value) {
      this.user = value;
      /*this.profileService.getCompany(this.user.profile).subscribe((res) => {
        this.companyForm.reset();
        var graduate;
        this.companyForm.patchValue(graduate);
        this.company = graduate;
        this.profileService.formControl.next({
          data: graduate,
          event: 'select',
        });
        this.createFlag = false;
      });*/
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private identifierService: IdentifierService,
    private companyService: CompanyService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.companyForm = this.formBuilder.group({
      id: [null],
      identificator: ['', Validators.required],
      document: ['', Validators.required],
      cell_phone: ['', Validators.required],
      company: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.getAllIdentifiers()
    this.getAllCompanies()
    if (this.user && this.user.profile !== null) {
      this.profileService.getCompany(this.user.profile).subscribe((res) => {
        let data = Object.assign({}, res, res);
        data.identificator = data.identificator.id;
        data.company = data.company.id;
        this.companyForm.patchValue(data);
        this.profileService.formControl.next({
          data: res,
          event: 'select',
        });
        this.createFlag = false;
        /*if (!IsNullUndefined(res.picture)) {
          this.showBeforeImage = true;
        } else {
          this.showBeforeImage = false;
        }*/
      });
    }
  }
  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.listIdentifiers = identifiers.results;
    });
  }
  getAllCompanies() {
    this.companyService.getActive().subscribe((res) => {
      this.companyList = res.results;
    });
  }
  save() {
    if(!this.companyForm.valid) {
      this.snackBar.open('Faltan completar correctamente algunos datos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
    const profileFormValue = this.companyForm.value;
    if (isObject(profileFormValue.identificator)){
      profileFormValue.identificator = profileFormValue.identificator.id
    }
    if (isObject(profileFormValue.company)) {
      profileFormValue.company = profileFormValue.company.id;
    }
    const profile = Object.assign({}, profileFormValue, {
      company: profileFormValue.company,
    });
    this.profileReady.emit(profile);
  }
  create(profile: any){
    return new Promise((resolve, reject) => {
      this.profileService.postCompany(profile).subscribe(
        (res) => {
          resolve({
            data: profile,
            event: 'create',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  update(_id: any, user: any){
    const profile = Object.assign({}, user, {
      identificator: user.identificator.id,
      job_position: user.job_position.id,
      type_employee: user.type_employee.id
    });
    return new Promise((resolve, reject) => {
      this.profileService.putSuperAdmin(_id, profile).subscribe(
        (res) => {
          resolve({
            data: user,
            event: 'update',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.companyForm.patchValue({
        picture: file,
      });
      this.companyForm.get('picture').updateValueAndValidity();
    }
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }
}
