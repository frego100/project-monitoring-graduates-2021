import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Employee} from '@app/models/employee';
import {Identifier} from '@app/models/identifier';
import {Jobs} from '@app/models/job';
import {IdentifierService, JobsService, ProfileService, ProgramService, TypeEmployeeService,} from '@services';
import {IsNullUndefined, isObject} from '@app/shared/helpers/general-utils';

@Component({
  selector: 'profile-admin',
  templateUrl: './profile-admin.component.html',
  styleUrls: ['./profile-admin.component.scss'],
})
export class ProfileAdminComponent implements OnInit {
  public identifierList: Identifier[] = [];
  public programList: Identifier[] = [];
  public jobPositionList: Jobs[] = [];
  public typeEmployeeList: Employee[] = [];
  showBeforeImage: boolean = false;
  profile: any;

  user;
  profileForm: FormGroup;

  createFlag = true;
  @Output() redirect = new EventEmitter();
  @Output() profileReady = new EventEmitter<any>();
  @Input()
  set userItem(value) {
    if (value) {
      this.user = value;
    }
  }
  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private identifierService: IdentifierService,
    private jobsService: JobsService,
    private employeeService: TypeEmployeeService,
    private profileService: ProfileService,
    private programService: ProgramService,
    private snackBar: MatSnackBar
  ) {
    this.profileForm = this.formBuilder.group({
      id: [null],
      identificator: ['', Validators.required],
      instance: ['', Validators.required],
      job_position: ['', Validators.required],
      type_employee: ['', Validators.required],
      document: ['', Validators.required],
      cell_phone: ['',Validators.compose([Validators.minLength(9),Validators.max(999999999), Validators.required])],
      phone: ['', Validators.compose([Validators.min(111111),Validators.max(999999)])],
      picture: [null],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.getAllIdentifiers();
    this.getAllJobPosition();
    this.getAllEmployee();
    this.getAllPrograms();
    if (this.user && this.user.profile !== null) {
      this.profileService.getAdmin(this.user.profile).subscribe((res) => {
        let data = Object.assign({}, res, res);
        data.instance = data.instance.id;
        data.job_position = data.job_position.id;
        data.type_employee = data.type_employee.id;
        data.identificator = data.identificator.id;
        this.profileForm.patchValue(data);
        this.profile =res;
        this.profileService.formControl.next({
          data: res,
          event: 'select',
        });
        this.createFlag = false;
        this.showBeforeImage = !IsNullUndefined(res.picture);
      });
    }
  }

  update(_id: any, user: any) {
    return new Promise((resolve, reject) => {
      this.profileService.patchAdmin(_id, user).subscribe(
        (res) => {
          resolve({
            data: user,
            event: 'update',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(profile: any) {
    return new Promise((resolve, reject) => {
      this.profileService.postAdmin(profile).subscribe(
        (res) => {
          resolve({
            data: profile,
            event: 'create',
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  save() {
    if(!this.profileForm.valid) {
      this.snackBar.open('Faltan completar correctamente algunos datos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
    const profileFormValue = this.profileForm.value;
    if (isObject(profileFormValue.instance)) {
      profileFormValue.instance = profileFormValue.instance.id;
    }
    if (isObject(profileFormValue.identificator)) {
      profileFormValue.identificator = profileFormValue.identificator.id;
    }
    if (isObject(profileFormValue.job_position)) {
      profileFormValue.job_position = profileFormValue.job_position.id;
    }
    if (isObject(profileFormValue.type_employee)) {
      profileFormValue.type_employee = profileFormValue.type_employee.id;
    }
    const profile = Object.assign({}, profileFormValue, {
      identificator: profileFormValue.identificator,
      instance: profileFormValue.instance,
      job_position: profileFormValue.job_position,
      type_employee: profileFormValue.type_employee
    });
    this.profileReady.emit(profile);
  }

  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.identifierList = identifiers.results;
    });
  }
  getAllJobPosition() {
    this.jobsService.getAll().subscribe((jobPosition) => {
      this.jobPositionList = jobPosition.results;
    });
  }
  getAllEmployee() {
    this.employeeService.getAll().subscribe((employee) => {
      this.typeEmployeeList = employee.results;
    });
  }

  getAllPrograms() {
    this.programService.getAll().subscribe((ident) => {
      this.programList = ident.results;
      const list = this.programList;
      this.programList = list.filter(item => {
        return item.is_active === true;
      });
      console.log(this.programList)
    });
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.profileForm.patchValue({
        picture: file,
      });
      this.profileForm.get('picture').updateValueAndValidity();
    }
  }

  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }
}
