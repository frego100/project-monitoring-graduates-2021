import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '@app/models/user';
import { RolesService, UserService } from '@services';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Enabled } from '@app/models/enabled';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import {IsNullUndefinedBlank} from "@shared/helpers/general-utils";
import {RolLite} from "@models/rol";

@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class UserListComponent implements OnInit {
  pageSize = 100;
  currentPage = 0;
  totalSize = 0;
  selectedItem: any;
  searchCtrl: any;
  loading: boolean = false;
  listGroup: RolLite[];
  selectGroup: string = '';
  search: string = '';
  public selectHabilitado: Enabled = { id: 0, name: "" };
  public habilitado: Enabled[] = [
    {
      id: 1,
      name: "Habilitados"
    },
    {
      id: 2,
      name: "Deshabilitados"
    },
    {
      id: 3,
      name: "Todos"
    }
  ]
  public listUsers: User[] = [];
  displayedColumns: string[] = ['username', 'firstname', 'lastname', 'email', 'active', 'actions'];
  dataSource !: MatTableDataSource<User>;
  private ordering_list: Map<string, string>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private roleService: RolesService,
    private userService: UserService,
    public authUserService: AuthUserService,
  ) {
    this.ordering_list = new Map<string, string>();
  }

  ngOnInit(): void {
    this.getData();
    this.getDataComboGroup();
  }

  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getData();
  }

  getData() {
    this.loading = true;
    this.userService.getWithParams(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.selectGroup,
      this.search,
      this.ordering_list
    ).subscribe((users) => {
      this.listUsers = users.results;
      this.dataSource = new MatTableDataSource(this.listUsers);
      this.loading = false;
      this.totalSize = users.count;
    });
  }

  edit(user: any) {
    this.router.navigate([user.id], {
      relativeTo: this.route
    });
  }

  add() {
    this.userService.userControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  onChangeSort(e: any): void {
    if (!IsNullUndefinedBlank(e)) {
      var direct = '';
      if (!IsNullUndefinedBlank(e.direction)) {
        if (e.direction == 'asc') {
          direct = '+';
        } else if (e.direction == 'desc') {
          direct = '-';
        }
        this.ordering_list.set(e.active, direct + '' + e.active);
      } else {
        this.ordering_list.set(e.active, '');
      }
    }
    this.getData();
  }

  onSelectGroup(group: any): void {
    if (!IsNullUndefinedBlank(group)) {
      this.selectGroup = group.id;
    } else {
      this.selectGroup = ''
    }
    this.getData();
    this.paginator.firstPage();
  }

  getDataComboGroup() {
    this.roleService.getGroupLite().subscribe((groups) => {
      this.listGroup = groups;
    });
  }

  applyFilter(key: any) {
    let keycode = key.keyCode || key.which;
    if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getData();
      this.paginator.firstPage();
    }
  }
}
