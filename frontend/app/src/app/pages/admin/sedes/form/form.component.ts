import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import {SedeService} from "@app/services/sede/sede.service";

@Component({
  selector: 'app-sede-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class SedeFormComponent implements OnInit {
  sedeForm: FormGroup;
  sedeSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  sede: any;
  constructor(
    private formBuilder: FormBuilder,
    private sedeService: SedeService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.sedeForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.sedes && params.sedes !== 'nuevo') {
        this.getSedeById();
        this.getSedeByPathId(params.sedes);
      }
    });
  }

  private getSedeById() {
    this.sedeService.sedeControl.subscribe((res: any) => {
      this.sedeSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.sedeForm.reset();
      this.sedeSubscription = this.getSelectedItem(res).subscribe(
        (sede: any) => {
          this.sedeForm.patchValue(sede);
          this.sedeService.formControl.next({
            data: sede,
            event: 'select',
          });
        }
      );
    });
  }

  private getSedeByPathId(id) {
    this.sedeService.getById(id).subscribe((res) => {
      this.sede = Object.assign({}, this.sede, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }
    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.sedeService
            .getById(params.sedes)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, area: any) {
    return new Promise((resolve, reject) => {
      this.sedeService.update(_id, area).subscribe(
        (res) => {
          resolve({
            data: area,
            event: 'update',
          });
          this.router.navigate(['/admin/sedes']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(puesto: any) {
    return new Promise((resolve, reject) => {
      this.sedeService.create(puesto).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/sedes']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const puesto = this.sedeForm.value;
    const send = puesto.id
      ? this.update(puesto.id, puesto)
      : this.create(puesto);
    send.then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.sedeService.formControl.next(res);
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  removeAction() {
    const puesto = this.sedeForm.value;
    this.sedeService.remove(puesto.id).subscribe(
      () => {
        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/sedes']);
      },
      () => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
