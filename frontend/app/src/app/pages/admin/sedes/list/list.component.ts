import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import {SedeService} from "@app/services/sede/sede.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SedeListComponent implements OnInit {
  selectedItem: any;
  searchCtrl: any;
  public listSedes:any[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      'name','actions'
  ];
  dataSource !:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private sedeServices: SedeService,
    public authUserService: AuthUserService
  ) {}

  ngOnInit(): void {
    this.getAllSedes();
  }

  check(){
    return this.authUserService.checkPermissions("view_area");
  }

  getAllSedes(){
    this.sedeServices.getAll().subscribe((res:any) => {
      this.listSedes = res.results;
      this.dataSource = new MatTableDataSource(this.listSedes);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
      relativeTo: this.route
    });
  }
  add() {
    this.sedeServices.sedeControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
