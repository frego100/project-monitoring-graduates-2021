import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import {SuggestionService} from "@app/services/suggestion_box/suggestion_box.service";
import {ViewSuggestionModal} from "@shared/components/view-suggestion/view-suggestion";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SuggestionBoxListComponent implements OnInit {

  selectedItem: any;
  searchCtrl: any;

  rol: any;

  public listSuggestion: any[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      //'nro',
      'text','datetime','user_pk.username' ,'actions'];
   dataSource !:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private suggestionServices: SuggestionService,
    public authUserService: AuthUserService,
    private dialog: MatDialog,
  ) {
    this.rol = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit(): void {
    this.getAllSuggestions()
  }

  check(){
    return this.authUserService.checkPermissions("view_suggestion");
  }
  getAllSuggestions(){
    this.suggestionServices.getAll().subscribe((response:any) => {
      this.listSuggestion = response.results;
      this.dataSource = new MatTableDataSource(this.listSuggestion);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  viewSuggestion(item: any){
    const dialogRef = this.dialog.open(ViewSuggestionModal,{
      width: '500px',
      data: {
        item: item
      }
    })
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
