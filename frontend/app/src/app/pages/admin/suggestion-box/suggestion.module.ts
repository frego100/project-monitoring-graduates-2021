import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SuggestionBoxListComponent} from "@pages/admin/suggestion-box/list/list.component";
import {SuggestionBoxFormComponent} from "@pages/admin/suggestion-box/form/form.component";
import {ViewSuggestionModal} from "@shared/components/view-suggestion/view-suggestion";
export const SuggestionRoutes: Routes = [
    {
       path: 'sugerencias',
       children: [
          {
             path: '',
             component: SuggestionBoxListComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['view_suggestion']},
          },
          {
             path: 'nuevo',
             component: SuggestionBoxFormComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['add_suggestion']},
          },
          {
             path: ':sugerencias',
             component: SuggestionBoxFormComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['change_suggestion']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [ SuggestionBoxListComponent, SuggestionBoxFormComponent, ViewSuggestionModal],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class SuggestionModule {}
