import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import {SuggestionService} from "@app/services/suggestion_box/suggestion_box.service";
import {UserService} from "@services";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-suggestion-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class SuggestionBoxFormComponent implements OnInit {
  suggestionForm: FormGroup;
  suggestionSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  suggestion: any;
  listUsers: any [];
  rol: any;
  confs: {};

  constructor(
    private formBuilder: FormBuilder,
    private suggestionService: SuggestionService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.rol = JSON.parse(localStorage.getItem('user'));
    if(this.rol.groups[0].profile_type.model === 'superadminprofile'){
      this.confs = {
        id: [null],
        text: ['', Validators.required],
        user_pk: ['', Validators.required],
      }
    }else{
      this.confs = {
        id: [null],
        text: ['', Validators.required],
      }
    }
    this.suggestionForm = this.formBuilder.group(this.confs);
  }

  ngOnInit(): void {
    if(this.rol.groups[0].profile_type.model === 'superadminprofile'){
      this.getAllUsers()
    }
    this.route.params.subscribe((params) => {
      if (params.sugerencias && params.sugerencias !== 'nuevo') {
        this.getSuggestionById();
        this.getSuggestionByPathId(params.sugerencias);
      }
    });
  }
  private getAllUsers() {
    this.userService.getUserLite2().subscribe((response:any) => {
      this.listUsers = response;
    });
  }

  private getSuggestionById() {
    this.suggestionService.suggestionControl.subscribe((res: any) => {
      this.suggestionSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.suggestionForm.reset();
      this.suggestionSubscription = this.getSelectedItem(res).subscribe(
        (res: any) => {
          res.user_pk = res.user_pk.id
          this.suggestionForm.patchValue(res);
          this.suggestionService.formControl.next({
            data: res,
            event: 'select',
          });
        }
      );
    });
  }

  private getSuggestionByPathId(id) {
    this.suggestionService.getById(id).subscribe((res) => {
      this.suggestion = Object.assign({}, this.suggestion, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }
    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.suggestionService
            .getById(params.sugerencias)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }
  update(_id: any, suggestion: any) {
    return new Promise((resolve, reject) => {
      this.suggestionService.update(_id, suggestion).subscribe(
        (res) => {
          resolve({
            data: suggestion,
            event: 'update',
          });
          this.router.navigate(['/admin/sugerencias']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  create(suggestion: any) {
    return new Promise((resolve, reject) => {
      this.suggestionService.create(suggestion).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/sugerencias']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "text")
        keys[i] = "Sugerencia"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const suggestion = this.suggestionForm.value;
    const send = suggestion.id
      ? this.update(suggestion.id, suggestion)
      : this.create(suggestion);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.suggestionService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() { }

  removeAction() {
    const suggestion = this.suggestionForm.value;
    this.suggestionService.remove(suggestion.id).subscribe(
      () => {
        this.suggestionService.suggestionControl = new BehaviorSubject(null);

        this.suggestionService.formControl.next({
          data: suggestion.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/sugerencias']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }
}
