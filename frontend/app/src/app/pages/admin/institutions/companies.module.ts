import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InstitutionListComponent} from './list/list.component';
import {InstitutionFormComponent} from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from "@shared/guards/check-permission-guard";
export const InstitutionRoutes: Routes = [
    {
       path: 'instituciones',
       children: [
          {
             path: '',
             component: InstitutionListComponent,
              canActivate: [CanActivateAuthGuard],
              data: {permission: ['view_institution']},
          },
          {
             path: 'nuevo',
             component: InstitutionFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['add_institution']},
          },
          {
             path: ':instituciones',
             component: InstitutionFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['change_institution']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [ InstitutionListComponent, InstitutionFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class CompaniesModule {}
