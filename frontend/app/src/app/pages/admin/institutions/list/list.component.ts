import {Component, OnInit, ViewChild} from '@angular/core';
import {Company} from "@models/company";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthUserService} from "@services";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Institution} from "@models/institution";
import {InstitutionService} from "@app/services/institution/institution.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class InstitutionListComponent implements OnInit {

  public listInstitutions:Institution[]=[];

  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'institution_name', 'ruc','actions'
  ];
  dataSource !:MatTableDataSource<Institution>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private institutionService: InstitutionService,
    public authUserService: AuthUserService
  ) { }

  ngOnInit(): void {
    this.getAllInstitutions()
  }
  check(){
    return this.authUserService.checkPermissions("view_area");
  }
  getAllInstitutions(){
    this.institutionService.getAllEnabled().subscribe((institution: any) => {
      this.listInstitutions = institution.results;
      this.dataSource = new MatTableDataSource(this.listInstitutions);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }
  onSelect(id:any):void{

    if(id==1){
      const list=this.listInstitutions;
      const habilitit=list.filter(item=>{
        if(item.is_active===true){
          return true;
        }
        else{
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==2){
      const list=this.listInstitutions;
      const habilitit=list.filter(item=>{
        if(item.is_active===false){
          return true;
        }
        else{
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==3){
      const list=this.listInstitutions;
      const habilitit=list;
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  edit(company: any) {
    this.router.navigate([company.id], {
      relativeTo: this.route
    });
  }
  add() {
    this.institutionService.institutionControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
