import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IdentifierService } from '@services';
import { Identifier } from '@models/identifier';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { InstitutionService } from '@app/services/institution/institution.service';
import {IsNullUndefined} from "@shared/helpers/general-utils";

@Component({
  selector: 'app-company-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class InstitutionFormComponent implements OnInit {
  public listIdentifiers: Identifier[] = [];

  showBeforeImage: boolean = false;

  institutionForm: FormGroup;

  institutionSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();

  institution: any;

  constructor(
    private formBuilder: FormBuilder,
    private institutionService: InstitutionService,
    private identifierService: IdentifierService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.institutionForm = this.formBuilder.group({
      id: [null],
      institution_name: ['', Validators.required],
      social_reason: ['', Validators.required],
      ruc: ['', Validators.required],
      institution_activity: ['', Validators.required],
      logo: [null],
      identificator: ['', Validators.required],
      type: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.getAllIdentifiers();
    this.route.params.subscribe((params) => {
      if (params.instituciones && params.instituciones !== 'nuevo') {
        this.getInstitutionById();
        this.getInstitutionByPathId(params.instituciones);
      }else{
        this.showBeforeImage = false;
      }
    });
  }
  private getInstitutionById() {
    this.institutionService.institutionControl.subscribe((res: any) => {
      this.institutionSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.institutionForm.reset();
      this.institutionSubscription = this.getSelectedItem(res).subscribe(
        (institution: any) => {
          this.institutionForm.patchValue(institution);
          this.institutionService.formControl.next({
            data: institution,
            event: 'select',
          });
        }
      );
    });
  }
  private getInstitutionByPathId(id) {
    this.institutionService.getById(id).subscribe((res) => {
      this.institution = Object.assign({}, this.institution, res);
      if (!IsNullUndefined(this.institution.logo)) {
        this.showBeforeImage = true;
      } else {
        this.showBeforeImage = false;
      }
    });
  }
  cancel() {
    history.back();
  }
  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }
    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.institutionService
            .getById(params.instituciones)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.listIdentifiers = identifiers.results.filter((item)=>{
        return item.code === 'RUC';
      });
    });
  }

  update(_id: any, institution: any) {
    const comp = Object.assign({}, institution, {
      identificator: institution.identificator.id,
    });
    return new Promise((resolve, reject) => {
      this.institutionService.update(_id, comp).subscribe(
        (res) => {
          resolve({
            data: institution,
            event: 'update',
          });
          this.router.navigate(['/admin/instituciones']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(institution: any) {
    return new Promise((resolve, reject) => {
      this.institutionService.create(institution).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/instituciones']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any) {
    let keys = Object.keys(err.error);
    let values = Object.values(err.error);
    let allErrors = '';
    for (let i = 0; i < keys.length; i++) {
      if (keys[i] === 'institution_name') keys[i] = 'Nombre de la Institución';
      if (keys[i] === 'institution_activity')
        keys[i] = 'Actividad de la Institución';
      if (keys[i] === 'social_reason') keys[i] = 'Razón Social';
      if (keys[i] === 'identificator') keys[i] = 'Identificador';
      allErrors += keys[i] + ': ' + values[i] + '\n';
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 100000,
    });
  }

  save() {
    const company = this.institutionForm.value;
    const send = company.id
      ? this.update(company.id, company)
      : this.create(company);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.institutionService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err);
      });
  }
  removeAction() {
    const institution = this.institutionForm.value;

    this.institutionService.remove(institution.id).subscribe(
      () => {
        this.institutionService.institutionControl = new BehaviorSubject(null);

        this.institutionService.formControl.next({
          data: institution.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/instituciones']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.institutionForm.patchValue({
        logo: file,
      });
      this.institutionForm.get('logo').updateValueAndValidity();
    }
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }
}
