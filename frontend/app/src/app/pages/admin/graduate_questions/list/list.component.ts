import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { AreaService } from '@app/services/area/area.service';
import {GraduateQuestionService} from "@app/services/question/graduate_question.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class GraduateQuestionListComponent implements OnInit {



  selectedItem: any;
  searchCtrl: any;
  public listQuestion: any[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      //'nro',
      'name','is_active' ,'actions'];
   dataSource !:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private areaServices: AreaService,
    private questionServices: GraduateQuestionService,
    public authUserService: AuthUserService

  ) {}

  ngOnInit(): void {
    this.getAllQuestions();
  }

  check(){
    return this.authUserService.checkPermissions("view_graduatequestions");
  }
  getAllQuestions(){
    this.questionServices.getAllActive().subscribe((res:any) => {
      this.listQuestion = res.results;
      this.dataSource = new MatTableDataSource(this.listQuestion);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  edit(res: any) {
    this.router.navigate([res.id], {
       relativeTo: this.route
    });
 }

  add() {
    this.questionServices.questionControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
