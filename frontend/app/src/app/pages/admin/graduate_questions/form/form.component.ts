import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import {QuestionService} from "@app/services/question/question.service";

@Component({
  selector: 'app-question-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class GraduateQuestionFormComponent implements OnInit {
  questionForm: FormGroup;
  questionSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  question: any;
  constructor(
    private formBuilder: FormBuilder,
    private questionService: QuestionService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.questionForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.preguntas && params.preguntas !== 'nuevo') {
        this.getQuestionById();
        this.getQuestionByPathId(params.preguntas);
      }
    });
  }

  private getQuestionById() {
    this.questionService.questionControl.subscribe((res: any) => {
      this.questionSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.questionForm.reset();

      this.questionSubscription = this.getSelectedItem(res).subscribe(
        (question: any) => {
          this.questionForm.patchValue(question);
          this.questionService.formControl.next({
            data: question,
            event: 'select',
          });
        }
      );
    });
  }

  private getQuestionByPathId(id) {
    this.questionService.getById(id).subscribe((res) => {
      this.question = Object.assign({}, this.question, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.questionService
            .getById(params.preguntas)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, question: any) {
    return new Promise((resolve, reject) => {
      this.questionService.update(_id, question).subscribe(
        (res) => {
          resolve({
            data: question,
            event: 'update',
          });
          this.router.navigate(['/admin/preguntas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(question: any) {
    return new Promise((resolve, reject) => {
      this.questionService.create(question).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/preguntas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const question = this.questionForm.value;
    const send = question.id
      ? this.update(question.id, question)
      : this.create(question);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.questionService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() { }

  removeAction() {
    const puesto = this.questionForm.value;

    this.questionService.remove(puesto.id).subscribe(
      () => {
        this.questionService.questionControl = new BehaviorSubject(null);

        this.questionService.formControl.next({
          data: puesto.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/preguntas']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
