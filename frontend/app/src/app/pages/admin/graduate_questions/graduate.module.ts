import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GraduateQuestionListComponent} from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from '../../../shared/guards/check-permission-guard';
import {GraduateQuestionFormComponent} from "@pages/admin/graduate_questions/form/form.component";
export const GraduateQuestionRoutes: Routes = [
    {
       path: 'preguntas_egresado',
       children: [
          {
             path: '',
             component: GraduateQuestionListComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['view_graduatequestions']},
          },
          {
             path: 'nuevo',
             component: GraduateQuestionFormComponent,
              canActivate: [CanActivateAuthGuard],
              data: {permission: ['add_graduatequestions']},
          },
          {
             path: ':preguntas_egresado',
             component: GraduateQuestionFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['change_graduatequestions']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [ GraduateQuestionListComponent, GraduateQuestionFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class GraduateQuestionModule {}
