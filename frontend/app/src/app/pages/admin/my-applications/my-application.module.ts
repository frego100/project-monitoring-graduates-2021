import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationListComponent } from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SelectTargetModule } from '@app/shared/components/select-target/module';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';

export const MyApplicationRoutes: Routes = [
   {
      path: 'mis-postulaciones',
      children: [
         {
            path: '',
            component: ApplicationListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_userpostulant']},
         },
      ]
   }
];

@NgModule({
   declarations: [ ApplicationListComponent],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      PipesModule,
      CKEditorModule,
      SelectTargetModule
   ],
   entryComponents: []
})
export class MyApplicationModule {}
