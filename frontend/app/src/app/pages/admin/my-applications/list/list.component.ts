import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Announcement } from '@app/models/announcement';
import { AnnouncementService } from '@services';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Enabled } from '@app/models/enabled';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { MatDialog } from '@angular/material/dialog';
import { AnnouncementDetail } from '@app/shared/components/detail-announcement/detail-announcement';

@Component({
  selector: 'app-activity-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ApplicationListComponent implements OnInit {
  selectedItem: any;
  searchCtrl: any;
  loading: boolean = false;
  public selectHabilitado: Enabled = { id: 0, name: '' };
  public listAnnouncements: Announcement[] = [];
  displayedColumns: string[] = [
    //  'nro',
    'title',
    'startDate',
    'endDate',
    'actions',
  ];
  dataSource!: MatTableDataSource<Announcement>;
  search = '';
  pageSize = 10;
  currentPage = 0;
  totalSize = 0;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private announcementService: AnnouncementService,
    public dialog: MatDialog,
    public authUserService: AuthUserService
  ) {}

  /**
   * This event happens when the user changes the page or changes the page size,
   * so we update the values of the current page and the page size.
   * Finally we ask the server for the data again
   *
   * @method handlePage
   * @param e its a event
   */
  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getAllAnnouncements();
  }

  ngOnInit(): void {
    this.getAllAnnouncements();
  }

  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  getAllAnnouncements() {
    this.loading = true;
    this.announcementService
      .getMyApplications()
      .subscribe((activities: any) => {
        this.listAnnouncements = activities.results;
        this.dataSource = new MatTableDataSource(activities.results);
        this.totalSize = activities.count;
        this.loading = false;
      });
  }

  edit(activity: any) {
    this.router.navigate(['../mis-actividades', activity.id], {
      relativeTo: this.route,
    });
  }

  applyFilter(key: any) {
    var keycode = key.keyCode || key.which;
    if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getAllAnnouncements();
      this.paginator.firstPage();
    }
    // const filterValue = (event.target as HTMLInputElement).value;
    // this.datasoruce.filter = filterValue.trim().toLowerCase();
  }
  
  delete(applicationId){
    this.announcementService
      .removeMyApplication(applicationId)
      .subscribe(() => {
        this.getAllAnnouncements()
      })
  }

  openDialog(act: any): void {
    const dialogRef = this.dialog.open(AnnouncementDetail, {
      width: '50%',
      data: {
        id:act.id,
        name: act.name,
        description: act.description,
        date_start: act.date_start,
        date_end: act.date_end,
        link: act.link,
        company_pk: act.company_pk,
        extra_file: act.extra_file,
        can_register: act.can_register,
        view_only : true,
        created_by: act.created_by
      }
    });
    dialogRef.afterClosed().subscribe(res => {
    })
  }
}
