import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCreationRequest } from '@app/models/user-creation-request';
import { Roles } from '@app/models/rol';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { ActivityService, RolesService, UserService } from '@services';
import { ValidateActivity} from "@shared/components/validate-activity/validate-activity";

@Component({
  selector: 'app-user-creation-request-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ActivityCreationRequestListComponent implements OnInit {
  selectedItem: any;
  searchCtrl: any;
  all: any;
  public listRequest: UserCreationRequest[] = [];
  public listRoles: Roles[] = [];
  public listKindActivity: any [] = [];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    //'nro',
    'name',
    'active',
    'actions',
  ];
  dataSource!: MatTableDataSource<UserCreationRequest>;
  areas: Array<any>;
  selectArea = '';

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private rolesService: RolesService,
    private activityService: ActivityService,
    public authUserService: AuthUserService
  ) {}

  ngOnInit(): void {
    this.getAllRequests();
    this.getAllRoles();
                                                                                                                                                }

  getAllRequests() {
    this.activityService.getActivityCreationRequest().subscribe((res: any) => {
      this.listRequest = res.results;
      this.dataSource = new MatTableDataSource(this.listRequest);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  openDialog(valActivity: any): void {
    const dialogRef = this.dialog.open(ValidateActivity, {
       data: {
         activityObj: valActivity,
       },
     });
     dialogRef.afterClosed().subscribe((res) => {
       this.getAllRequests();
     });
  }

  getAllRoles() {
    this.rolesService.getAll().subscribe((roles) => {
      this.listRoles = roles.results;
    });
  }
  onSelect(id: any): void {
    if (id == this.all) {
      this.getAllRequests();
    } else {
      const list = this.listRequest;

      const enabled = list.filter((item) => {
        if (item.user.groups[0].id === id) {
          return true;
        } else {
          return false;
        }
      });
      this.dataSource = new MatTableDataSource(enabled);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
