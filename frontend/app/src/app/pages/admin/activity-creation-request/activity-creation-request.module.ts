import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityCreationRequestListComponent } from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
export const ActivityCreationRequestRoutes: Routes = [
  {
    path: 'solicitudes-actividades',
    children: [
      {
        path: '',
        component: ActivityCreationRequestListComponent,
        canActivate: [CanActivateAuthGuard],
        data: {permission: ['validate_activity']},
      }
    ],
  },
];

@NgModule({
  declarations: [ActivityCreationRequestListComponent],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule,
  ],
  entryComponents: [],
})
export class ActivityCreationRequestModule {}
