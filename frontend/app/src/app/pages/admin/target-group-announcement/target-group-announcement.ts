import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TargetGroupAnnouncementFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { SelectTargetModule } from '@app/shared/components/select-target/module';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
import { TargetGroupAnnouncementListComponent } from './list/list.component';


export const TargetGroupAnnouncementRoutes: Routes = [
   {
      path: 'grupo-objetivo-convocatorias',
      children: [
         {
            path: '',
            component: TargetGroupAnnouncementListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_targetgroupannouncement']},

         },
         {
            path: 'nuevo',
            component: TargetGroupAnnouncementFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['add_targetgroupannouncement']},
         },
         {
            path: ':id',
            component: TargetGroupAnnouncementFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_targetgroupannouncement']},   
         }
      ]
   }
];

@NgModule({
   declarations: [ TargetGroupAnnouncementListComponent, TargetGroupAnnouncementFormComponent ],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      PipesModule,
      SelectTargetModule
   ],
   entryComponents: []
})
export class IdentifierModule {}
