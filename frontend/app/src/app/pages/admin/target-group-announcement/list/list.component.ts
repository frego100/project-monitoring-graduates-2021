import { Component, OnInit, ViewChild } from '@angular/core';
import { UserLite } from '@app/models/user';
import { IdentifierService } from '@services';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { RolLite } from '@app/models/rol';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { Identifier } from '@app/models/identifier';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { TargetGroupAnnouncement } from '@app/models/target-group-announcement';
import { TargetGroupAnnouncementService } from '@app/services/target-group-announcement/target-group-announcement.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class TargetGroupAnnouncementListComponent implements OnInit {
  gridColumns = 3;
  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }

  loading: boolean = false;
  listGroup: RolLite[];
  selectGroup: string = '';
  listUser: UserLite[];
  selectUser: string = '';
  identifier: any[] = [];
  search: string = '';
  startDate: any = '';
  selectStartDate: string = '';
  endDate: any = '';
  selectEndDate: string = '';
  pageSize = 10;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = ['text-invitation','name-announcement', 'instance', 'state', 'actions'];
  states = [
    {
      id: 1,
      name: 'Habilitados',
      state: true,
    },
    {
      id: 2,
      name: 'Deshabilitados',
      state: false,
    },
    {
      id: 3,
      name: 'Todos',
    },
  ];
  selectState;

  dataSource!: MatTableDataSource<TargetGroupAnnouncement>;
  pageEvent: PageEvent;

  private ordering_list: Map<string, string>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private targetGroupService: TargetGroupAnnouncementService,
    public authUserService: AuthUserService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.ordering_list = new Map<string, string>();
  }

  /**
   * We load the data from the view previously
   *
   * @method ngOnInit
   */
  ngOnInit(): void {
    this.getData();
   
  }

  /**
   * This event happens when the user changes the page or changes the page size,
   * so we update the values of the current page and the page size.
   * Finally we ask the server for the data again
   *
   * @method handlePage
   * @param e its a event
   */
  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getData();
  }

  /**
   * This event occurs when the user clicks on the table headers, ascending
   * or descending ordering is applied.
   * It can be sorted based on many fields so we use a hashmap.
   * Finally we ask the server for the data again
   *
   * @method onChangeSort
   * @param e its a event - headers and direction
   */
  onChangeSort(e: any): void {
    if (!IsNullUndefinedBlank(e)) {
      var direct = '';
      if (!IsNullUndefinedBlank(e.direction)) {
        if (e.direction == 'asc') {
          direct = '+';
        } else if (e.direction == 'desc') {
          direct = '-';
        }
        this.ordering_list.set(e.active, direct + '' + e.active);
      } else {
        this.ordering_list.set(e.active, '');
      }
    }
    this.getData();
  }

  /**
   * Request historical data from the server
   *
   * @method getData
   */
  getData() {
    this.loading = true;
    this.targetGroupService
      .getList(
        this.pageSize,
        this.currentPage * this.pageSize,
        this.selectGroup,
        this.search,
        this.ordering_list
      )
      .subscribe((identifier) => {
        this.identifier = identifier.results;
        this.dataSource = new MatTableDataSource(this.identifier);
        this.loading = false;
        this.totalSize = identifier.count;
      });
     
  }

  /**
   * This method applies a text search filter when the
   * user presses the enter key in the search field
   *
   * @method applyFilter
   * @param key keyboard event
   */
  applyFilter(key: any) {
    var keycode = key.keyCode || key.which;
    if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getData();
      this.paginator.firstPage();
    }
  }

  /**
   * This event occurs when the user selects a value in the combo box,
   * then we get the selected value and
   * finally we ask the server for the data again.
   *
   * @method onSelectState
   * @param group is the group's name selected in the combobox
   */
  onSelectState(state: any): void {
    if (!IsNullUndefinedBlank(state) && state != 3) {
      this.selectState = state.state;
      this.getIdentifiersFilterByState(this.selectState);
    } else {
      this.getData();
    }
    this.getData();
    this.paginator.firstPage();
  }

  /**
   * Request identifirs filtered by state
   *
   * @method getIdentifiersFilterByState
   */
  getIdentifiersFilterByState(is_active: boolean) {
    this.targetGroupService
      .getTargetGroupLite(is_active)
      .subscribe((identifier) => {
        this.identifier = identifier.results;
        this.dataSource = new MatTableDataSource(this.identifier);
        this.loading = false;
        this.totalSize = identifier.count;
      });
  }

  edit(identifier) {
    this.router.navigate([identifier.id], {
      relativeTo: this.route,
    });
  }
}
