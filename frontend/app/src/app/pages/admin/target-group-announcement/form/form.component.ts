import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormsModule  } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { Choice } from '@app/models/item-choice';
import { Identifier } from '@app/models/identifier';
import { IdentifierService } from '@app/services/identifier/identifier.service';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';
import { TargetGroupAnnouncementService } from '@app/services/target-group-announcement/target-group-announcement.service';
import { AreaService, FacultyService, ProgramService , AnnouncementService} from '@app/services';

@Component({
   selector: 'app-users-form',
   templateUrl: './form.component.html',
   styleUrls: ['./form.component.scss']
})

export class TargetGroupAnnouncementFormComponent implements OnInit {

   listAlphabet: Choice[] = [
      { value: 'NUM', text: 'NUMÉRICO' },
      { value: 'ALN', text: 'ALFANUMÉRICO' },
   ];
   modeEdit: boolean = false;
   selectedValue: string;
   currentTragetGroupC: any;
   listAnnouncement:  any[] = [];
   listProgram:  any[] = [];
   disabledProgram: boolean=true;
   disabledFaculty: boolean=true;
   disabledArea: boolean=true;
   public instancesList:any[]=[
      {
        id:1,
        name: "Área"
      },
      {
        id:2,
        name: "Facultad"
      },
      {
        id:3,
        name: "Programa Profesional"
      }
    ]
   listArea:  any[] = [];
   listFaculty:  any[] = [];
   targetGroupAnnouForm: FormGroup;
   myGroup:FormGroup;
   userSubscription: Subscription = new Subscription();
   routeParamsSubscription: Subscription = new Subscription();

   constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private targetGroupAnnouService: TargetGroupAnnouncementService,
      private announcementService: AnnouncementService,
      private facultyService: FacultyService,
      private programService: ProgramService,
      private areaService: AreaService,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar,
   ) {
      this.targetGroupAnnouForm = this.formBuilder.group({
         id: [null],
         text_invitation: ['', Validators.required],
         announcement: ['', Validators.required],
         instance: ['', Validators.required],
         is_active: [true]
      });
     
   }

   ngOnInit(): void {
      this.getComplements();
      this.modeEdit = false;
      this.getTargetGroupAnnouByRoute();
    
   
   }

   private getTargetGroupAnnouByRoute () {
      this.route.params.subscribe((params) => {
         if (!IsNullUndefined(params.id)) {
            this.getTargetGroupAnnouByService(params.id)
            this.modeEdit = true;
         }
        
      });
   
   }

   private getTargetGroupAnnouByService (id :number) {
      this.targetGroupAnnouService.get(id).subscribe((res) => {
         this.targetGroupAnnouForm.patchValue(Object.assign({},this.currentTragetGroupC, res));
         this.targetGroupAnnouForm.patchValue({
            announcement:this.targetGroupAnnouForm.value.announcement.id
         });
         
      },()=>{
         this.cancel();
        
      });
     
    
   }

   cancel() {
      history.back();
   }

   private update() {
     
      this.targetGroupAnnouService.put(this.currentTragetGroupC.id, this.currentTragetGroupC).subscribe(() => {
         this.showSnackBar('Editado correctamente!');
         this.router.navigate(['..'], { relativeTo: this.route });
      }, (err) => {
         this.allErrors(err)
      });
   }

   private create() {
      
      this.targetGroupAnnouService.post(this.currentTragetGroupC).subscribe(() => {
         this.showSnackBar('Guardado correctamente!');
         this.router.navigate(['..'], { relativeTo: this.route });
      }, (err) => {
         this.allErrors(err)
      });
      
   }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      if(keys[i] === "code")
        keys[i] = "Código"
      if(keys[i] === "max_length")
        keys[i] = "Longitud Máxima"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

   save() {
    
      this.currentTragetGroupC = this.targetGroupAnnouForm.value;
      this.currentTragetGroupC.id ? this.update() : this.create();
   }
   public getComplements(){
      this.announcementService.getAll().subscribe((announcement: any) => {
         
         this.listAnnouncement = announcement.results;
         const list = this.listAnnouncement;
         const enabled = list.filter(item => {
           if (item.status === true) {
             return true;
           }
           else {
             return false;
           }
         });
         this.listAnnouncement = enabled;
       });
       this.facultyService.getAll().subscribe((faculty: any) => {
         
         this.listFaculty = faculty.results;
         const list = this.listFaculty;
         const enabled = list.filter(item => {
           if (item.is_active === true) {
             return true;
           }
           else {
             return false;
           }
         });
         this.listFaculty = enabled;
       });
       this.areaService.getAll().subscribe((area: any) => {
         
         this.listArea = area.results;
         const list = this.listArea;
         const enabled = list.filter(item => {
           if (item.is_active === true) {
             return true;
           }
           else {
             return false;
           }
         });
         this.listArea = enabled;
       });
       this.programService.getAll().subscribe((program: any) => {
         
         this.listProgram = program.results;
         const list = this.listProgram;
         const enabled = list.filter(item => {
           if (item.is_active === true) {
             return true;
           }
           else {
             return false;
           }
         });
         this.listProgram = enabled;
       });
   }
   private showSnackBar(text) {
      this.snackBar.open(text, 'Cerrar', { duration: 3000 });
   }
   radioButton(event: any) {
      if(event.value===1){
         this.disabledArea=false;
         this.disabledFaculty=true;
         this.disabledProgram=true;
      }else if(event.value===2){
         this.disabledArea=true;
         this.disabledFaculty=false;
         this.disabledProgram=true;
      }else if(event.value===3){
         this.disabledArea=true;
         this.disabledFaculty=true;
         this.disabledProgram=false;
      }else{
         this.disabledArea=true;
         this.disabledFaculty=true;
         this.disabledProgram=true;
      }
      
  }
   
}
