import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompaniesListComponent } from './list/list.component';
import { CompaniesFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from "@shared/guards/check-permission-guard";
export const CompaniesRoutes: Routes = [
    {
       path: 'empresas',
       children: [
          {
             path: '',
             component: CompaniesListComponent,
              canActivate: [CanActivateAuthGuard],
              data: {permission: ['view_company']},
          },
          {
             path: 'nuevo',
             component: CompaniesFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['add_company']},
          },
          {
             path: ':empresas',
             component: CompaniesFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['change_company']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [ CompaniesListComponent, CompaniesFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class CompaniesModule {}
