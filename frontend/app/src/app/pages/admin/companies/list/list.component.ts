import {Component, OnInit, ViewChild} from '@angular/core';
import {Company} from "@models/company";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthUserService, CompanyService} from "@services";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class CompaniesListComponent implements OnInit {

  public listCompanies:Company[]=[];

  pageSize = 100;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'company_name','ruc','actions'
  ];
  dataSource !:MatTableDataSource<Company>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private companyServices: CompanyService,
    public authUserService: AuthUserService
  ) { }

  ngOnInit(): void {
    this.getAllCompanies()
  }
  check(){
    return this.authUserService.checkPermissions("view_area");
  }
  getAllCompanies(){
    this.companyServices.getActive().subscribe((company: any) => {

      this.listCompanies = company.results;
      this.dataSource = new MatTableDataSource(this.listCompanies);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    })
  }
  onSelect(id:any):void{

    if(id==1){
      const list=this.listCompanies;
      const habilitit=list.filter(item=>{
        return item.is_active === true;
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==2){
      const list=this.listCompanies;
      const habilitit=list.filter(item=>{
        return item.is_active === false;
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==3){
      const list=this.listCompanies;
      this.dataSource = new MatTableDataSource(list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  edit(company: any) {
    this.router.navigate([company.id], {
      relativeTo: this.route
    });
  }
  add() {
    this.companyServices.companyControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
