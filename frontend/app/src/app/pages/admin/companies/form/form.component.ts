import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CompanyService} from "@services";
import {IdentifierService} from "@services";
import {Identifier} from "@app/models/identifier";
import {BehaviorSubject, Observable, of, Subscription} from "rxjs";
import {IsNullUndefined} from "@shared/helpers/general-utils";

@Component({
  selector: 'app-company-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class CompaniesFormComponent implements OnInit {

  public listIdentifiers: Identifier[] = [];

  showBeforeImage: boolean = false;

  companyForm: FormGroup;

  companySubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();


  company: any

  constructor(
    private formBuilder: FormBuilder,
    private companyService: CompanyService,
    private identifierService: IdentifierService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.companyForm = this.formBuilder.group({
      id: [null],
      company_name: ['', Validators.required],
      social_reason: ['', Validators.required],
      ruc: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      company_activity: ['', Validators.required],
      logo: [null],
      identificator: ['', Validators.required],
      is_active: [true],
    })
  }

  ngOnInit(): void {
    this.getAllIdentifiers()
    this.route.params.subscribe((params) => {
      if (params.empresas && params.empresas !== 'nuevo') {
        this.getCompanyById();
        this.getCompanyByPathId(params.empresas);
      }else{
        this.showBeforeImage = false;
      }
    });
  }

  private getCompanyById() {

    this.companyService.companyControl.subscribe((res: any) => {
      this.companySubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.companyForm.reset();
      this.companySubscription = this.getSelectedItem(res).subscribe(
        (company: any) => {
          this.companyForm.patchValue(company);
          this.companyService.formControl.next({
            data: company,
            event: 'select',
          });
        }
      );
    });
  }
  private getCompanyByPathId(id) {
    this.companyService.getById(id).subscribe((res) => {
      this.company = Object.assign({}, this.company, res);
      if (!IsNullUndefined(this.company.logo)) {
        this.showBeforeImage = true;
      } else {
        this.showBeforeImage = false;
      }
    });
  }
  cancel() {
    history.back();
  }
  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }
    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.companyService
            .getById(params.empresas)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  getAllIdentifiers() {
    this.identifierService.getAll().subscribe((identifiers) => {
      this.listIdentifiers = identifiers.results.filter((item)=>{
        return item.code === 'RUC';
      });
    });
  }

  update(_id: any, company: any) {
    const comp = Object.assign({},company, {
      identificator: company.identificator.id,
    });
    return new Promise((resolve, reject) => {
      this.companyService.update(_id, comp).subscribe(
        (res) => {
          resolve({
            data: company,
            event: 'update',
          });
          this.router.navigate(['/admin/empresas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(company: any) {
    return new Promise((resolve, reject) => {
      this.companyService.create(company).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/empresas']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "company_name")
        keys[i] = "Nombre de la Empresa"
      if(keys[i] === "company_activity")
        keys[i] = "Rubro de la Empresa"
      if(keys[i] === "social_reason")
        keys[i] = "Razón Social"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 100000,
    });
  }
  save() {
    const company = this.companyForm.value;
    const send = company.id
      ? this.update(company.id, company)
      : this.create(company);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.companyService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }
  removeAction() {
    const company = this.companyForm.value;

    this.companyService.remove(company.id).subscribe(
      () => {
        this.companyService.companyControl = new BehaviorSubject(null);

        this.companyService.formControl.next({
          data: company.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/empresas']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.companyForm.patchValue({
        logo: file,
      });
      this.companyForm.get('logo').updateValueAndValidity();
    }
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }

}
