import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {PageConfigService} from "@services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {makeMessageErrorHttp} from "@shared/helpers/general-utils";

const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

@Component({
  selector: 'page-config',
  templateUrl: './page-config.component.html',
  styleUrls: ['./page-config.component.scss']
})
export class PageConfigComponent implements OnInit {

  loading: boolean = false;
  configForm: FormGroup;
  editor = ClassicEditor;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };

  constructor(
    private formBuilder: FormBuilder,
    private pageConfigService: PageConfigService,
    private snackBar: MatSnackBar,
  ) {
    this.configForm = this.formBuilder.group({
      id: [null],
      principal_text: ['', Validators.required],
      principal_announcement: ['', Validators.required],
      link_facebook: ['', [Validators.pattern(urlRegex)]],
      link_twitter: ['', [Validators.pattern(urlRegex)]],
      link_youtube: ['', [Validators.pattern(urlRegex)]],
      link_linkedin: ['', [Validators.pattern(urlRegex)]],
      link_instagram: ['', [Validators.pattern(urlRegex)]],
      link_other: ['', [Validators.pattern(urlRegex)]],
    });
  }

  ngOnInit(): void {
    this.getData();
  }
  getData(){
    this.loading = true;
    this.pageConfigService.get().subscribe((response)=>{
      this.configForm.patchValue(response)
      this.loading = false;
    });
  }
  update(){
    if(!this.configForm.valid) {
      this.snackBar.open('Datos no válidos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
    this.loading = true;
    this.pageConfigService.update(this.configForm.value).subscribe(()=>{
        this.snackBar.open('Operación Exitosa', 'Cerrar', {
          duration: 3000,
        });
        this.loading = false;
      },(error)=>{
        this.snackBar.open(makeMessageErrorHttp(error.error), 'Cerrar', {
          duration: 3000,
        });
        this.loading = false;
      }
    )
  }
}
