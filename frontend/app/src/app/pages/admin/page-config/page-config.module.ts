import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageConfigComponent } from './page-config.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from "@shared/guards/check-permission-guard";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
export const PageAdminRoute: Routes = [
  {
    path: 'page-config',
    children: [
      {
        path: '',
        component: PageConfigComponent,
      }
    ]
  }

];

@NgModule({
  declarations: [PageConfigComponent],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    CKEditorModule,
    BrowserAnimationsModule,
    PipesModule
  ],
  entryComponents: [],
})
export class CompaniesModule {}
