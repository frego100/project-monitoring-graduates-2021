import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Enabled } from '@app/models/enabled';
import { Manual } from '@app/models/manual';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { ManualsService } from '@app/services/manual/manual.service';
@Component({
  selector: 'app-manual-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ManualsListComponent implements OnInit {

  selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Habilitados"
    },
    {
      id:2,
      name: "Deshabilitados"
    },
    {
      id:3,
      name: "Todos"
    }
  ]

  public listManuals:Manual[]=[];
   displayedColumns: string[] = [
      'name','actions'
   ];
   dataSource !:MatTableDataSource<Manual>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private manualService: ManualsService,
    public authUserService: AuthUserService

  ) {}

  ngOnInit(): void {
    this.getAllManuals();
   // this.dataSource.filterPredicate = this.getFilterPredicate();
  }

  check(){
    return this.authUserService.checkPermissions("view_manual");
  }
  getAllManuals(){
    this.manualService.getAll().subscribe((manual:any) => {
      this.listManuals = manual.results;
      this.dataSource = new MatTableDataSource(this.listManuals);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  onSelect(id:any):void{

  if(id==1){
    const list=this.listManuals;
    const habilitit=list.filter(item=>{
      if(item.is_active===true){
        return true;
      }
      else{
        return false;
      }
    })
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  if(id==2){
    const list=this.listManuals;
    const habilitit=list.filter(item=>{
      return item.is_active === false;
    })
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  if(id==3){
    const list=this.listManuals;
    const habilitit=list;
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
       relativeTo: this.route
    });
 }

  add() {
    this.manualService.manualControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
