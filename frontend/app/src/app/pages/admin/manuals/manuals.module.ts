import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManualsListComponent } from './list/list.component';
import { ManualsFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CanActivateAuthGuard } from '@app/shared/guards/check-permission-guard';
export const ManualsRoutes: Routes = [
   {
      path: 'manuales',
      children: [
         {
            path: '',
            component: ManualsListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_manual']},
         },
         {
            path: 'nuevo',
            component: ManualsFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['add_manual']},
         },
         {
            path: ':manual',
            component: ManualsFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_manual']},     
         }
      ]
   }

];

@NgModule({
   declarations: [ ManualsListComponent, ManualsFormComponent],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      CKEditorModule,
      AppCommonModule,
      BrowserAnimationsModule,
      PipesModule
   ],
   entryComponents: []
})
export class UserModule {}
