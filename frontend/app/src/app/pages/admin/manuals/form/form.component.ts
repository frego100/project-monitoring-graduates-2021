import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ManualsService } from '@app/services/manual/manual.service';
import {IsNullUndefined, makeMessageErrorHttp} from '@app/shared/helpers/general-utils';
@Component({
  selector: 'app-manual-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class ManualsFormComponent implements OnInit {
  manualForm: FormGroup;
  editor = ClassicEditor;
  mediaType = 'image';
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  manualSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  manual: any;
  showBeforeManual: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private manualService: ManualsService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
  ) {
    this.manualForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      description: ['', Validators.required],
      file: [null],
      link: [''],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.manual && params.manual !== 'nuevo') {
        this.getManualById();
        this.getManualByPathId(params.manual);
      }
    });
  }

  private getManualById(){
    this.manualService.manualControl.subscribe((res: any) => {
      this.manualSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.manualForm.reset();
      this.manualSubscription = this.getSelectedItem(res).subscribe(
        (manual: any) => {
          this.manualForm.patchValue(manual);
          this.manualService.formControl.next({
            data: manual,
            event: 'select',
          });
        }
      );
    });
  }

  private getManualByPathId(id) {
    this.manualService.getById(id).subscribe((res) => {
      this.manual = Object.assign({}, this.manual, res);
      this.showBeforeManual = !IsNullUndefined(this.manual.file);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }
    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.manualService
            .getById(params.manual)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, manual: any) {
    return new Promise((resolve, reject) => {
      this.manualService.update(_id, manual).subscribe(
        (res) => {
          resolve({
            data: manual,
            event: 'update',
          });
          this.router.navigate(['..'], { relativeTo: this.route });
        },
        (err) => {
          this.snackBar.open(this.showErrorsIdentifier(err), 'Cerrar', {
            duration: 6000,
          });
          reject(err);
        }
      );
    });
  }

  create(manual: any) {
    return new Promise((resolve, reject) => {
      this.manualService.create(manual).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['..'], { relativeTo: this.route });
        },
        (err) => {
          this.snackBar.open(this.showErrorsIdentifier(err), 'Cerrar', {
            duration: 6000,
          });
          reject(err);
        }
      );
    });
  }

  save() {
    const manual = this.manualForm.value;
    const send = manual.id
      ? this.update(manual.id, manual)
      : this.create(manual);
    send.then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.manualService.formControl.next(res);
        if (res.event === 'create') {
        }
      })
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.manualForm.patchValue({
        file: file,
      });
      this.manualForm.get('file').updateValueAndValidity();
    }
  }
  removeAction() {
    const manual = this.manualForm.value;
    this.manualService.remove(manual.id).subscribe(
      () => {
        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['..'], { relativeTo: this.route });
      },
      () => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
  showErrorsIdentifier(err){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name"){
         keys[i] = "Título"
      }
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    return allErrors
  }
}
