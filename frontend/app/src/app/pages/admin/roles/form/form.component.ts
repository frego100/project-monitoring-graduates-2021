import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, of, Observable, BehaviorSubject } from 'rxjs';
import { RolesService, ContentTypeProfileService } from '@services';
import { ConfirmDisablingComponent } from '@shared/components/confirm-disabling/confirm-disabling.component';
import { DualListComponent } from 'angular-dual-listbox';
import { PermissionService } from '@app/services/permission/permission.service';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';

@Component({
  selector: 'app-roles-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class RolesFormComponent implements OnInit {
  status: any[] = [
    { value: true, viewValue: 'Activo' },
    { value: false, viewValue: 'Inactivo' },
  ];

  rolesForm: FormGroup;
  rol: any;
  profiles: Array<any>;
  rolesSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  confirmed: Array<any> = [];
  source: Array<any>;
  format = {
    add: 'Añadir',
    remove: 'Remover',
    all: 'Todos',
    none: 'Ninguno',
    direction: DualListComponent.LTR,
    draggable: true,
    locale: undefined,
  };

  constructor(
    private formBuilder: FormBuilder,
    private rolesService: RolesService,
    private permissionService: PermissionService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    public authUserService: AuthUserService,
    public contentTypeProfileService: ContentTypeProfileService
  ) {
    this.rolesForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      state: [true],
      profile_type: [null],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.roles && params.roles !== 'nuevo') {
        this.getRolById();
        this.getRolByPathId(params.roles);
      }
    });
    this.contentTypeProfileService.getAll().subscribe((res) => {
      this.profiles = res.results;
    });

    // Assign the permissions
    this.permissionService.getAll().subscribe((res) => {
      this.source = res.results;
    });
  }

  private getRolById() {
    this.rolesService.rolesControl.subscribe((res: any) => {
      this.rolesSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.rolesForm.reset();

      this.rolesSubscription = this.getSelectedItem(res).subscribe(
        (rol: any) => {
          if (rol.profile_type) {
            rol = Object.assign({}, rol, { profile_type: rol.profile_type.id });
          }

          this.rolesForm.patchValue(rol);
          this.rolesService.formControl.next({
            data: rol,
            event: 'select',
          });
        }
      );
    });
  }
  private getRolByPathId(id) {
    this.rolesService.getById(id).subscribe((res) => {
      this.rol = Object.assign({}, this.rol, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.rolesService.getById(params.roles).subscribe((res) => {
            observer.next(res);
            this.confirmed = res.permissions != null ? res.permissions : [];
          });
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, rol: any) {
    return new Promise((resolve, reject) => {
      this.rolesService.update(_id, rol).subscribe(
        (res) => {
          resolve({
            data: rol,
            event: 'update',
          });
          this.router.navigate(['/admin/roles']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(rol: any) {
    return new Promise((resolve, reject) => {
      this.rolesService.create(rol).subscribe(
        (res) => {
          resolve({
            data: rol,
            event: 'create',
          });
          this.router.navigate(['/admin/roles']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  save() {
    const rolFormValue = this.rolesForm.value;

    const permissionsId = this.confirmed.map((a) => a.id);

    const rol = Object.assign({}, rolFormValue, { permissions: permissionsId });

    const send = rol.id ? this.update(rol.id, rol) : this.create(rol);

    send.then((res: any) => {
      this.snackBar.open('Guardado correctamente!', 'Cerrar', {
        duration: 3000,
      });
      this.rolesService.formControl.next(res);
      if (res.event === 'create') {
        this.router.navigate(['..', res.data.id], {
          relativeTo: this.route,
        });
      }
    });
    /*.catch((err) => {
        this.snackBar.open('Ops...algo no esta funcionando!', 'Cerrar', {
           duration: 3000
        });
     });*/
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConfirmDisablingComponent, {
      width: '350px',
      data: 'Esta seguro que desea habilitar este rol',
    });
    dialogRef.afterClosed().subscribe((res) => {});
  }

  remove() {}

  removeAction() {
    const rol = this.rolesForm.value;

    this.rolesService.remove(rol.id).subscribe(
      () => {
        this.rolesService.rolesControl = new BehaviorSubject(null);

        this.rolesService.formControl.next({
          data: rol.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.router.navigate(['..', 'nuevo'], { relativeTo: this.route });
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
