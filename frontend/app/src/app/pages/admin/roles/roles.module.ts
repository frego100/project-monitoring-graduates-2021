import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RolesListComponent } from './list/list.component';
import { RolesFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AppCommonModule } from '@app/app-common.module';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';

export const RolesRoutes: Routes = [
  {
    path: 'roles',
    children: [
      {
        path: '',
        component: RolesListComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['view_usergroup'] },
      },
      {
        path: 'nuevo',
        component: RolesFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['add_usergroup'] },
      },
      {
        path: ':roles',
        component: RolesFormComponent,
        canActivate: [CanActivateAuthGuard],
        data: { permission: ['change_usergroup'] },
      },
    ],
  },
];

@NgModule({
  declarations: [RolesListComponent, RolesFormComponent],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    PipesModule,
    AngularDualListBoxModule,
  ],
  entryComponents: [],
})
export class RolesModule {}
