import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { RolesService } from '@services';
import { Roles } from '@app/models/rol';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';

@Component({
  selector: 'app-roles-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class RolesListComponent implements OnInit {
  roles: any[] = [];
  selectedItem: any;
  searchCtrl: any;

  public listRoles: Roles[] = [];
  displayedColumns: string[] = ['name', 'state', 'actions'];
  dataSource!: MatTableDataSource<Roles>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private rolesService: RolesService,
    public authUserService: AuthUserService
  ) {}

  ngOnInit(): void {
    this.getAllRoles();
  }
  getAllRoles() {
    this.rolesService.getAll().subscribe((roles) => {
      this.listRoles = roles.results;
      this.dataSource = new MatTableDataSource(roles.results);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy(): void {
    this.rolesService.rolesControl = new BehaviorSubject(null);
  }

  getAll() {
    this.roles = [];
    this.rolesService.getAll().subscribe((roles) => {
      this.roles = roles;
    });
  }

  edit(rol: any) {
    this.rolesService.rolesControl.next(rol);
    this.router.navigate([`${rol.id}`], { relativeTo: this.route });
  }

  add() {
    this.rolesService.rolesControl.next(null);
    this.router.navigate([`novo`], { relativeTo: this.route });
  }
}
