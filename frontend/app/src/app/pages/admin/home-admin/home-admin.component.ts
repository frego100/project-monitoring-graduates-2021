import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {PageConfigService} from '@app/services';
import { AnnouncementDetail } from '@app/shared/components/detail-announcement/detail-announcement';
import {PageConfig} from "@models/page-config";
import {AnnouncementClientService} from "@app/services/announcement-client/annoucement-client.service";

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.scss']
})
export class HomeAdminComponent implements OnInit {
  gridColumns = 3;
  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }
  public displayActivities: Array<any> = []
  configPage: PageConfig;

  constructor(
    private announcementService: AnnouncementClientService,
    public dialog: MatDialog,
    public pageConfigService: PageConfigService
  ) { }

  ngOnInit(): void {
    this.getConfig();
    this.getAnnouncement();
   }

  getConfig(){
    this.pageConfigService.getPublic().subscribe((response)=>{
      this.configPage = response
    })
  }
  getAnnouncement(){

    let activity;
    this.announcementService.getAnnouncement().subscribe(
      (res: any) => {
        //res.results = this.displayActivites;
        this.displayActivities = res.results;
      },
      err => {

      }
    );
  }

  getElement(act: any) {
  }
  openDialog(act: any): void {
    const dialogRef = this.dialog.open(AnnouncementDetail, {
      width: '50%',
      data: {
        id:act.id,
        name: act.name,
        description: act.description,
        date_start: act.date_start,
        date_end: act.date_end,
        link: act.link,
        company_pk: act.company_pk,
        extra_file: act.extra_file,
        can_register: act.can_register
      }
    });
    dialogRef.afterClosed().subscribe(res => {
    })
  }
}
