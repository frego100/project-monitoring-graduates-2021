import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsService } from '@app/services/job/job.service';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class JobsFormComponent implements OnInit {
  jobsForm: FormGroup;
  editor = ClassicEditorBuild;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  puestoTrSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  puestoTr: any;
  showBeforeManual: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private jobsService: JobsService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.jobsForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      is_active: [true],
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.jobPosition && params.jobPosition !== 'nuevo') {
        this.getJobsById();
        this.getJobsByPathId(params.jobPosition);
      }
    });
  }

  private getJobsById() {
    this.jobsService.jobsControl.subscribe((res: any) => {
      this.puestoTrSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.jobsForm.reset();

      this.puestoTrSubscription = this.getSelectedItem(res).subscribe(
        (puestoTr: any) => {
          
          this.jobsForm.patchValue(puestoTr);
          this.jobsService.formControl.next({
            data: puestoTr,
            event: 'select',
          });
        }
      );
    });
  }

  private getJobsByPathId(id) {
    this.jobsService.getById(id).subscribe((res) => {
      this.puestoTr = Object.assign({}, this.puestoTr, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.jobsService
            .getById(params.jobPosition)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, puestoTr: any) {
    return new Promise((resolve, reject) => {
      this.jobsService.update(_id, puestoTr).subscribe(
        (res) => {
          resolve({
            data: puestoTr,
            event: 'update',
          });
          this.router.navigate(['/admin/puestos-trabajo']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(puesto: any) {
    return new Promise((resolve, reject) => {
      this.jobsService.create(puesto).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/puestos-trabajo']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const puesto = this.jobsForm.value;
    
    const send = puesto.id
      ? this.update(puesto.id, puesto)
      : this.create(puesto);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.jobsService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }

  remove() {}

  removeAction() {
    this.jobsForm.patchValue({
      is_active:false
    });
    const puesto = this.jobsForm.value;

    this.jobsService.update(puesto.id,puesto).subscribe(
      () => {
        this.jobsService.jobsControl = new BehaviorSubject(null);

        this.jobsService.formControl.next({
          data: puesto.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.router.navigate(['..'], { relativeTo: this.route });
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
