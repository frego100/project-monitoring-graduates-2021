import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobsListComponent } from './list/list.component';
import { JobsFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from '../../../shared/guards/check-permission-guard';
export const JobsRoutes: Routes = [
    {
       path: 'puestos-trabajo',
       children: [
          {
             path: '',
             component: JobsListComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['view_jobposition']},
          },
          {
             path: 'nuevo',
             component: JobsFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['add_jobposition']},
          },
          {
             path: ':jobPosition',
              component: JobsFormComponent,
             canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_jobposition']},
          }
       ]
    }
 
 ];
 
 @NgModule({
    declarations: [ JobsListComponent, JobsFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class UserModule {}
 