import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {Enabled} from '@app/models/enabled';
import {Jobs} from '@app/models/job';
import {AuthUserService} from '@app/services/auth-user/auth-user.service';
import {JobsService} from '@app/services/job/job.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class JobsListComponent implements OnInit {
  selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Habilitados"
    },
    {
      id:2,
      name: "Deshabilitados"
    },
    {
      id:3,
      name: "Todos"
    }
  ]

  public listJobs:Jobs[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      'name','actions'];
   dataSource !:MatTableDataSource<Jobs>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private jobsService: JobsService,
    public authUserService: AuthUserService

  ) {}

  ngOnInit(): void {
    this.getAll();
  }

  check(){
    return this.authUserService.checkPermissions("view_manual");
  }
  getAll(){
    this.jobsService.getAll().subscribe((manual:any) => {
      this.listJobs = manual.results;
      this.dataSource = new MatTableDataSource(this.listJobs);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  onSelect(id:any):void{
    if(id==1){
      const list=this.listJobs;
      const habilitit=list.filter(item=>{
        return item.is_active === true;
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==2){
      const list=this.listJobs;
      const habilitit=list.filter(item=>{
        return item.is_active === false;
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if(id==3){
      const list=this.listJobs;
      this.dataSource = new MatTableDataSource(list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
       relativeTo: this.route
    });
 }

  add() {
    this.jobsService.jobsControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
