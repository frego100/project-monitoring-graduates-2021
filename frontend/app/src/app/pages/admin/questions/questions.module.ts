import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionListComponent} from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from '../../../shared/guards/check-permission-guard';
import {QuestionFormComponent} from "@pages/admin/questions/form/form.component";
export const QuestionRoutes: Routes = [
    {
       path: 'preguntas',
       children: [
          {
             path: '',
             component: QuestionListComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['view_question']},
          },
          {
             path: 'nuevo',
             component: QuestionFormComponent,
              canActivate: [CanActivateAuthGuard],
              data: {permission: ['add_question']},
          },
          {
             path: ':preguntas',
             component: QuestionFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['change_question']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [ QuestionListComponent, QuestionFormComponent],
    imports: [
       RouterModule,
       AppMaterialModule,
       ReactiveFormsModule,
       FormsModule,
       CommonModule,
       AppCommonModule,
       BrowserAnimationsModule,
       PipesModule
    ],
    entryComponents: [],
 })
 export class QuestionModule {}
