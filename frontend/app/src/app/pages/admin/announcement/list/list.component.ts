import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Enabled } from '@app/models/enabled';
import { TargetGroupModalComponent } from '@app/shared/components/target-group-announcement/target-group-announcement.component';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import {Announcement} from "@models/announcement";
import {AnnouncementService, AuthUserService} from "@services";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class AnnouncementListComponent implements OnInit {



  selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Habilitados"
    },
    {
      id:2,
      name: "Deshabilitados"
    },
    {
      id:3,
      name: "Todos"
    }
  ]
  private ordering_list: Map<string, string>;
  public listAnnouncement:Announcement[]=[];
  pageSize = 10;
  selectGroup: string = '';
  search: string = '';
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      //'nro',
      'name','date_start','date_end','actions'];
   dataSource !:MatTableDataSource<Announcement>;

   @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
   @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private announcementService: AnnouncementService,
    public authUserService: AuthUserService

  ) {
    this.ordering_list = new Map<string, string>();
  }

  ngOnInit(): void {
    this.getData();
   // this.dataSource.filterPredicate = this.getFilterPredicate();
  }

  check(){
    return this.authUserService.checkPermissions("view_area");
  }
  handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getData();
  }

  onChangeSort(e: any): void {
    if (!IsNullUndefinedBlank(e)) {
      var direct = '';
      if (!IsNullUndefinedBlank(e.direction)) {
        if (e.direction == 'asc') {
          direct = '+';
        } else if (e.direction == 'desc') {
          direct = '-';
        }
        this.ordering_list.set(e.active, direct + '' + e.active);
      } else {
        this.ordering_list.set(e.active, '');
      }
    }
    this.getData();
  }
  getData() {
    this.loading = true;
    this.announcementService
      .getList(
        this.pageSize,
        this.currentPage * this.pageSize,
        this.selectGroup,
        this.search,
        this.ordering_list
      )
      .subscribe((announcementList) => {
        this.listAnnouncement = announcementList.results;
        this.dataSource = new MatTableDataSource(this.listAnnouncement);
        this.loading = false;
        this.totalSize = announcementList.count;
      });
  }
  
 
  edit(manual: any) {
    this.router.navigate([manual.id], {
      relativeTo: this.route
    });
  }
  targetGroup(data:any, openDialog=true){
   
    const dialogRef = this.dialog.open(TargetGroupModalComponent, {
      width: '1024px',
      data:{
        title: 'Grupos Objetivos',
        announcement: data.id,
        company_pk: data.company_pk,
        description:data.description,
        date_end: data.data_end,
        date_start: data.date_start
      }
    }).componentInstance;
   dialogRef.afterSelected.subscribe(result => {
   });
  }
  /*onSelect(id:any):void{

  if(id==1){
    const list=this.listArea;
    const habilitit=list.filter(item=>{
      if(item.is_active===true){
        return true;
      }
      else{
        return false;
      }
    })
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  if(id==2){
    const list=this.listArea;
    const habilitit=list.filter(item=>{
      if(item.is_active===false){
        return true;
      }
      else{
        return false;
      }
    })
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  if(id==3){
    const list=this.listArea;
    const habilitit=list;
    this.dataSource = new MatTableDataSource(habilitit);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
       relativeTo: this.route
    });
 }*/

  /*add() {
    this.areaServices.areaControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }*/
 
  applyFilter(key: any) {
          var keycode = key.keyCode || key.which;
      if (keycode == 13) {
        this.search = (key.target as HTMLInputElement).value;
        this.getData();
        this.paginator.firstPage();
      }
    }
  

}
