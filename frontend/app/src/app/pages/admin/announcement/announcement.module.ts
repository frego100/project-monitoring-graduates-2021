import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AnnouncementListComponent} from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from '../../../shared/guards/check-permission-guard';
import {AnnoucementFormComponent} from "@pages/admin/announcement/form/form.component";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import { TargetGroupFormComponent } from '@app/shared/components/target-group-announcement/form/form.component';
import { TargetGroupModalComponent } from '@app/shared/components/target-group-announcement/target-group-announcement.component';
export const AnnoucementRoutes: Routes = [
    {
       path: 'convocatorias',
       children: [
          {
             path: '',
             component: AnnouncementListComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['view_announcement']},
          },
          {
             path: 'nuevo',
             component: AnnoucementFormComponent,
              canActivate: [CanActivateAuthGuard],
              data: {permission: ['add_announcement']},
          },
          {
             path: ':convocatorias',
             component: AnnoucementFormComponent,
             canActivate: [CanActivateAuthGuard],
             data: {permission: ['change_announcement']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [AnnouncementListComponent, AnnoucementFormComponent, TargetGroupFormComponent,
      TargetGroupModalComponent],
   imports: [
     RouterModule,
     AppMaterialModule,
     ReactiveFormsModule,
     FormsModule,
     CommonModule,
     AppCommonModule,
     BrowserAnimationsModule,
     PipesModule,
     CKEditorModule
   ],
    entryComponents: [],
 })
 export class AnnouncementModule {}
