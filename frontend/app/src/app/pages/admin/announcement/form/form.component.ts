import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import {AnnouncementService, CompanyService} from "@services";
import {Company} from "@models/company";

@Component({
  selector: 'app-announcement-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class AnnoucementFormComponent implements OnInit {
  annoucementForm: FormGroup;
  editor = ClassicEditorBuild;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  listCompanies: any [] = []
  announcementSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  annoucement: any;
  showBeforeImage: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private companiesService: CompanyService,
    private annoucementService: AnnouncementService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService
  ) {
    this.annoucementForm = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      description: ['', Validators.required],
      date_start: ['', Validators.required],
      date_end: ['', Validators.required],
      extra_file: [''],
      status: [true],
      is_validated: [false],
      company_pk: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getAllCompanies()
    this.route.params.subscribe((params) => {
      if (params.convocatorias && params.area !== 'nuevo') {
        this.getAnnoucementById();
        this.getAnnoucementByPathId(params.convocatorias);
      }
    });
  }
  private getAllCompanies(){
    this.companiesService.getAll().subscribe((response: any) => {
      this.listCompanies = response.results
      console.log(this.listCompanies)
    })
  }
  private getAnnoucementById() {
    this.annoucementService.annoucementControl.subscribe((res: any) => {
      this.announcementSubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.annoucementForm.reset()
      this.announcementSubscription = this.getSelectedItem(res).subscribe(
        (conv: any) => {
          if (conv.date_start || conv.date_end) {
            conv.date_start = new Date(conv.date_start)
              .toISOString()
              .substring(0, 19);
            conv.date_end = new Date(conv.date_end)
              .toISOString()
              .substring(0, 19);
          }
          conv.company_pk = conv.company_pk.id
          console.log(conv)
          this.annoucementForm.patchValue(conv);
          console.log(this.annoucementForm)
          this.annoucementService.formControl.next({
            data: conv,
            event: 'select',
          });
        }
      );
    });
  }

  private getAnnoucementByPathId(id) {
    this.annoucementService.getById(id).subscribe((res) => {
      this.annoucement = Object.assign({}, this.annoucement, res);
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.annoucementService
            .getById(params.convocatorias)
            .subscribe((res) => observer.next(res));
        }
      });
    });
  }

  cancel() {
    history.back();
  }

  update(_id: any, area: any) {
    return new Promise((resolve, reject) => {
      this.annoucementService.update(_id, area).subscribe(
        (res) => {
          resolve({
            data: area,
            event: 'update',
          });
          this.router.navigate(['/admin/convocatorias']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(puesto: any) {
    return new Promise((resolve, reject) => {
      this.annoucementService.createAnnoucement(puesto).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.router.navigate(['/admin/convocatorias']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const puesto = this.annoucementForm.value;
    const send = puesto.id
      ? this.update(puesto.id, puesto)
      : this.create(puesto);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });

        this.annoucementService.formControl.next(res);

        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.allErrors(err)
      });
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.annoucementForm.patchValue({
        extra_file: file,
      });
      this.annoucementForm.get('extra_file').updateValueAndValidity();
    }
  }

  remove() { }

  removeAction() {
    const puesto = this.annoucementForm.value;

    this.annoucementService.remove(puesto.id).subscribe(
      () => {
        this.annoucementService.annoucementControl = new BehaviorSubject(null);

        this.annoucementService.formControl.next({
          data: puesto.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['/admin/convocatorias']);
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }
}
