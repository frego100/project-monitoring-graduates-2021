import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MyAnnouncementListComponent} from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CanActivateAuthGuard} from '../../../shared/guards/check-permission-guard';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {MyAnnoucementFormComponent} from "@pages/admin/my-announcements/form/form.component";
import {UsersByAnnoucementsModalComponent} from "@shared/components/users-by-annoucements/users-by-annoucements.component";
export const MyAnnoucementRoutes: Routes = [
    {
       path: 'mis-convocatorias',
       children: [
          {
             path: '',
             component: MyAnnouncementListComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['view_annoucement']},
          },
          {
             path: 'nuevo',
             component: MyAnnoucementFormComponent,
              //canActivate: [CanActivateAuthGuard],
              //data: {permission: ['add_annoucement']},
          },
          {
             path: ':mis-convocatorias',
             component: MyAnnoucementFormComponent,
             //canActivate: [CanActivateAuthGuard],
             //data: {permission: ['change_annoucement']},
          }
       ]
    }

 ];

 @NgModule({
    declarations: [MyAnnouncementListComponent, MyAnnoucementFormComponent, UsersByAnnoucementsModalComponent],
   imports: [
     RouterModule,
     AppMaterialModule,
     ReactiveFormsModule,
     FormsModule,
     CommonModule,
     AppCommonModule,
     BrowserAnimationsModule,
     PipesModule,
     CKEditorModule
   ],
    entryComponents: [],
 })
 export class MyAnnouncementModule {}
