import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Enabled } from '@app/models/enabled';
import {Announcement} from "@models/announcement";
import {AnnouncementService, AuthUserService} from "@services";
import {MatDialog} from "@angular/material/dialog";
import {UsersByAnnoucementsModalComponent} from "@shared/components/users-by-annoucements/users-by-annoucements.component";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class MyAnnouncementListComponent implements OnInit {



  selectedItem: any;
   searchCtrl: any;
   public selectHabilitado:Enabled={id:0,name:""};
   public habilitado:Enabled[]=[
    {
      id:1,
      name: "Habilitados"
    },
    {
      id:2,
      name: "Deshabilitados"
    },
    {
      id:3,
      name: "Todos"
    }
  ]

  public listAnnouncement:any[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      //'nro',
      'name','date_start','date_end','created_by','company_pk','actions'];
   dataSource !:MatTableDataSource<Announcement>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private announcementService: AnnouncementService,
    public authUserService: AuthUserService,
    private dialog: MatDialog,

  ) {}

  ngOnInit(): void {
    this.getAllAnnoucements();
   // this.dataSource.filterPredicate = this.getFilterPredicate();
  }

  check(){
    return this.authUserService.checkPermissions("view_area");
  }
  getAllAnnoucements(){
    this.announcementService.getMyAnnouncements().subscribe((response:any) => {
      this.listAnnouncement = response.results;
      this.dataSource = new MatTableDataSource(this.listAnnouncement);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  edit(manual: any) {
    this.router.navigate([manual.id], {
      relativeTo: this.route
    });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  viewUsers(element){
    const dialogRef = this.dialog.open(UsersByAnnoucementsModalComponent, {
      width: '900px',
      data: {
        annoucement: element.id
      }
    });
  }
}
