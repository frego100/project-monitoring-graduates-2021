import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserCreationRequestListComponent } from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
export const UserCreationRequestRoutes: Routes = [
  {
    path: 'solicitudes-usuarios',
    children: [
      {
        path: '',
        component: UserCreationRequestListComponent,
        canActivate: [CanActivateAuthGuard],
        data: {permission: ['view_preregistration']},
      }
    ],
  },
];

@NgModule({
  declarations: [UserCreationRequestListComponent],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule,
  ],
  entryComponents: [],
})
export class UserModule {}
