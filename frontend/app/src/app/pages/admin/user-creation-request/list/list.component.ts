import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCreationRequest } from '@app/models/user-creation-request';
import { Roles } from '@app/models/rol';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { RolesService, UserService } from '@services';
import {ValidateUser} from "@shared/components/validate-user/validate-user";

@Component({
  selector: 'app-user-creation-request-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class UserCreationRequestListComponent implements OnInit {
  selectedItem: any;
  searchCtrl: any;
  all:any;
  public listRequest: UserCreationRequest[] = [];
  public listRoles: Roles[]=[];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    //'nro',
    'name',
    'active',
    'actions',
  ];
  dataSource!: MatTableDataSource<UserCreationRequest>;
  areas: Array<any>;
  selectArea = '';

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private dialog: MatDialog,
    private rolesService:RolesService,
    private userService: UserService,
    public authUserService: AuthUserService
  ) {}

  ngOnInit(): void {
    this.getAllRequests();
    this.getAllRoles();
  }

  check() {
    return this.authUserService.checkPermissions('view_preregistration');
  }
  getAllRequests() {
    this.userService.getUserCreationRequest().subscribe((res: any) => {
      this.listRequest = res.results;
      this.dataSource = new MatTableDataSource(this.listRequest);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  openDialog(valUser: any): void {
    let aux_date = new Date(valUser.user.date_joined).toISOString();
   const aux= valUser.user;
    const dialogRef = this.dialog.open(ValidateUser, {
        data: {
         id: valUser.id,
         aux:valUser.user,
         date_joined:aux_date
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      this.getAllRequests()
    })
  }


  getAllRoles(){
    this.rolesService.getAll().subscribe((roles) => {
      this.listRoles = roles.results;
    })
  }
  onSelect(id:any):void{
      if(id==this.all){
       this.getAllRequests();
      }else {
      const list=this.listRequest;

      const enabled=list.filter(item=>{
        if(item.user.groups[0].id===id){
          return true;
        }
        else{
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(enabled);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }
 

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
