import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "@services";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class MasiveUsersComponent implements OnInit {

  masiveUsersForm: FormGroup;
  selectedItem: any;
  public listErrors: any [] = [];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
      //'nro',
      'errors.full_name','errors.description'];
   dataSource !:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userServive: UserService,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService,
    private formBuilder: FormBuilder,

  ) {
    this.masiveUsersForm = this.formBuilder.group({
      file: ['', Validators.required],
    });
  }

  ngOnInit(): void {

  }

  check(){
    return this.authUserService.checkPermissions("view_area");
  }
  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.masiveUsersForm.patchValue({
        file: file,
      });
      this.masiveUsersForm.get('file').updateValueAndValidity();
    }
  }
  saveMasiveUsers() {
    const file = this.masiveUsersForm.value
    this.userServive.uploadExcel(file).subscribe( (res:any) => {
      if(res.errors.length ===  0){
        this.snackBar.open("Subida de Archivos exitosa", 'Cerrar', {
          duration: 3000,
        });
      }
      this.listErrors = res.errors
      this.dataSource = new MatTableDataSource(this.listErrors);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (error: any) => {
      this.snackBar.open("Verifique el tipo de archivo", 'Cerrar', {
        duration: 3000,
      });
    })
  }
}
