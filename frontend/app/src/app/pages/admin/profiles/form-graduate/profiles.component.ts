import { Identifier } from '@app/models/identifier';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Roles } from '@app/models/rol';
import { IdentifierService, UserService, ProfileService, AuthUserService } from '@services';
import { Company } from '@app/models/company';
import { Gender } from '@app/models/gender';
import {ObjectiveGroupModalComponent} from "@shared/components/objective-group/objective-group.component";
import {MatDialog} from "@angular/material/dialog";
import {DisabledAccount} from "@shared/components/disabled-account/disabled-account";

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfileGraduateComponent implements OnInit {
  isLinear = false;
  isUpdate = true;
  logo: any;
  picture: any;
  isLogo = false;
  isPicture = false;
  isDCP:number;
  userGraduateForm: FormGroup;
  companyForm: FormGroup;
  registerPreUser: FormGroup;
  company: Company;
  public listIdentifier: Identifier[] = [];
  idUser: number;
  idIdent: number;
  idGender: number;
  genderForm: boolean;
  public dcpList:any[]=[
    {
      id:1,
      name: "Si",
      value:true,
      checked:false,
    },
    {
      id:0,
      name: "No",
      value:false,
      checked:false,
    }
  ]
  public gender: Gender[] = [
    {
      id: 1,
      name: "Femenino",
      value: true
    },
    {
      id: 0,
      name: "Masculino",
      value: false
    }
  ]
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private authService: AuthUserService,
    private identService: IdentifierService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {

    this.userGraduateForm = this.formBuilder.group({
      user: [''],
      first_name:['',Validators.required],
      last_name:['',Validators.required],
      identificator: ['', Validators.required],
      document: ['', Validators.required],
      is_dcp:[''],
      cell_phone: ['', Validators.required],
      phone: ['', Validators.required],
      is_active: true,
      username:[''],
      description_dcp:[''],
      email:[''],
      gender: [''],
      cui: ['', Validators.required],
      date_birthday: ['', Validators.required],
      web: [''],
      address: ['']
    });

  }

  ngOnInit(): void {
    this.getAllIdent();
    this.getUser();

  }
  radioButton(event: any) {

    if(event.value===1){
      this.isDCP=1;
      this.userGraduateForm.controls['description_dcp'].enable();
      this.userGraduateForm.patchValue({
        is_dcp:true,
       });

    }else if(event.value===0){
      this.isDCP=0;
      this.userGraduateForm.patchValue({
        description_dcp:null,
       });

       this.userGraduateForm.controls['description_dcp'].disable();

    }else{
      this.userGraduateForm.patchValue({
        description_dcp:null,
       });
       this.userGraduateForm.controls['description_dcp'].disable();

    }

}
  getUser() {
    this.authService.getUserCompany().subscribe((res: any) => {
      if (res.user_profile.gender === true) {
        this.idGender = 1;
      }
      else {
        this.idGender = 0;
      }
      if (res.user_profile.is_dcp === true) {
        this.isDCP = 1;

      }
      else {
        this.isDCP = 0;
      }
      this.userGraduateForm.patchValue({
        user: res.user_profile.user.id,
        username:res.username,
        first_name:res.first_name,
        last_name:res.last_name,
        document: res.user_profile.document,
        email:res.user_profile.user.email,
        cell_phone: res.user_profile.cell_phone,
        phone: res.user_profile.phone,
        is_dcp:this.isDCP,
        description_dcp:res.user_profile.description_dcp,
        identificator: res.user_profile.identificator.id,
        gender: this.idGender,
        address: res.user_profile.address,
        cui: res.user_profile.cui,
        web: res.user_profile.web,
        date_birthday: res.user_profile.date_birthday
      });
      /*this.userService.getAll().subscribe((users) => {
        const aux = users.results;
        const idU = aux.filter(item => {
          if (item.username === res.username) {
            return true;
          }
          else {
            return false;
          }
        })
        this.idUser = idU[0].id;
        if (res.user_profile.gender === true) {
          this.idGender = 1;

        }
        else {
          this.idGender = 0;
        }
        if (res.user_profile.is_dcp === true) {
          this.isDCP = 1;

        }
        else {
          this.isDCP = 0;
        }

        this.userGraduateForm.patchValue({
          user: this.idUser,
          username:res.username,
          first_name:res.first_name,
          last_name:res.last_name,
          document: res.user_profile.document,
          email:res.user_profile.email,
          cell_phone: res.user_profile.cell_phone,
          phone: res.user_profile.phone,
          is_dcp:this.isDCP,
          description_dcp:res.user_profile.description_dcp,
          identificator: res.user_profile.identificator.id,
          gender: this.idGender,
          address: res.user_profile.address,
          cui: "1111",
          web: res.user_profile.web,
          date_birthday: res.user_profile.date_birthday
        });
        if(res.user_profile.is_dcp==false){

           this.dcpList[0].checked=false;
           this.dcpList[1].checked=true;
           this.userGraduateForm.controls['description_dcp'].disable();
          this.isDCP=0;
        }else{
          this.isDCP=1;
          this.dcpList[0].checked=true;
          this.dcpList[1].checked=false;
        }
      });*/

    });
  }

  onFileChangePicture(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.picture = file;
      this.isPicture = true;
    }
  }
  savePicture() {
    this.profileService.updatePicture(this.picture).subscribe(
      (dat) => {
        dat.data = this.picture;
      });
  }

  save() {

    if (this.isPicture) {
      this.savePicture();
    }
    const genderValue = this.gender.filter(item => {

      if (item.id === this.idGender) {
        return true;
      }
      else {
        return false;
      }
    });
    if(this.isDCP==0){

      this.userGraduateForm.controls['description_dcp'].enable();
      this.userGraduateForm.patchValue({
        description_dcp:null,
       });
    }
    const dcpValue = this.dcpList.filter(item => {

      if (item.id === this.isDCP) {
        return true;
      }
      else {
        return false;
      }
    });
    this.userGraduateForm.patchValue({
      gender: genderValue[0].value,
      is_dcp:dcpValue[0].value
    });
    const formValue = this.userGraduateForm.value;
    const send = this.update(formValue);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000
        });
        let currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);

      })
      .catch((err) => {
        this.snackBar.open('Error! Su usuario no esta asignado a ningún perfil', 'Cerrar', {
          duration: 3000
        });
      });
    this.update(formValue);
  }
  update(profileS: any) {
    return new Promise((resolve, reject) => {
      this.profileService.patchProfileSelf(profileS).subscribe(
        (res) => {

          resolve({
            data: profileS,
            event: 'create'
          });
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
          //this.router.navigate(['/admin/perfil/egresado']);
        },
        (err) => {
          reject(err);
        }

      );;
    });
  }

  error() {
    this.snackBar.open('Usuario o Contraseña no válidos', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  onSelectGender(id: any): void {
    this.idGender = id;
  }

  onSelectIdent(id: any): void {
    this.idIdent = id;
  }
  onSelect(id: any): void {
  }

  getAllIdent() {
    this.identService.getAll().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const habilitit = list.filter((item) => {
        if (item.is_active === true) {
          return true;
        } else {
          return false;
        }
      });
      this.listIdentifier = habilitit;
    });
  }
  disabledAccount( openDialog = true) {
    const dialogRef = this.dialog.open(DisabledAccount, {
      width: '500px',
      data:{
      }
    }).componentInstance;
  }
}
