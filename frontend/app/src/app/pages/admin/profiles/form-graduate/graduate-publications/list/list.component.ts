import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import {GraduatePublications} from "@models/graduate";
import {GraduatePublicationsService} from "@app/services/graduate/graduate-publications.service";

import {IsNullUndefinedBlank} from "@shared/helpers/general-utils";
import {AuthUserService} from "@services";

@Component({
  selector: 'list-graduate-publications',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class GraduatePublicationsListComponent implements OnInit {

  public listPublicationsProf: Array<any> = [];
  public listPublicationsAdmin: Array<any> = [];

  @Input() auth;
  @Input() graduateProfilePk;

  valueEvent: string;

  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'title', 'description','type_publication','function_type', 'actions'];
  dataSource !: MatTableDataSource<GraduatePublications>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private graduatePublicationsService: GraduatePublicationsService,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService
  ) { }

  ngOnInit(): void {
    this.graduatePublicationsService.set(this.auth);
    if(this.auth){
      this.getAllAuthPublications()
    }
    else{
      this.getAllPublications()
    }
  }
  @Output()
  send: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  data: EventEmitter<any> = new EventEmitter<any>();

  addClick() {
    this.valueEvent = "add";
    this.send.emit(this.valueEvent);
  }
  edit(data: any) {
    this.data.emit(data);
  }
  delete(data: any) {
    if (!IsNullUndefinedBlank(data)) {
      this.graduatePublicationsService.delete(data.id).subscribe(() => {
        this.snackBar.open('Eliminado correctamente!', 'Cerrar', { duration: 3000 });
        this.getAllPublications();
      }, () => {
        this.snackBar.open('Error en la eliminación.. !', 'Cerrar', { duration: 3000 });
      })
    }
  }
  getAllPublications(){
    this.graduatePublicationsService.getAll(this.graduateProfilePk).subscribe((response)=>{
      this.listPublicationsAdmin = response.results
      
      this.dataSource = new MatTableDataSource(this.listPublicationsAdmin);
    })
  }
  getAllAuthPublications(){
    this.graduatePublicationsService.getAllAuth().subscribe((response)=>{
      this.listPublicationsProf = response.results
      this.dataSource = new MatTableDataSource(this.listPublicationsProf);
    })
  }
}
