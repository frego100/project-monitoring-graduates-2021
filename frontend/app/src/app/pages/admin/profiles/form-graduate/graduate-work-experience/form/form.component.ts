import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {CompanyService} from '@app/services';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { Subscription } from 'rxjs';
import {GraduateWorkExperienceService} from "@app/services/graduate/graduate-work-experience.service";
import {isObject} from "@shared/helpers/general-utils";

@Component({
  selector: 'form-graduate-work-experience',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class WorkExperienceFormComponent implements OnInit {

  @Input() auth;
  @Input() graduateProfilePk;

  workExperienceForm: FormGroup;
  editor = ClassicEditorBuild;

  routeParamsSubscription: Subscription = new Subscription();
  @Input()
  dataGenerate: any;
  public listInstitution: any[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private institutionService: CompanyService,
    private workExperienceService: GraduateWorkExperienceService,
    private snackBar: MatSnackBar,
  ) {
    this.workExperienceForm = this.formBuilder.group({
      id: [null],
      charge: ['', Validators.required],
      date_start: ['', Validators.required],
      date_end: ['', Validators.required],
      institution: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getAllInstitution()
    if (this.dataGenerate != null) {
      this.workExperienceService.getById(this.dataGenerate.id).subscribe((res: any) => {
        let data = Object.assign({},res,res)
        data.institution = data.institution.id
        this.workExperienceForm.patchValue(data)
      });
    }
  }

  @Output()
  cancel: EventEmitter<string> = new EventEmitter<string>();
  cancelButton() {
    this.cancel.emit("cancel");
  }
  getAllInstitution() {
    this.institutionService.getActive().subscribe((res: any) =>{
      this.listInstitution = res.results
    })
  }
  update(_id: any, workExp: any) {
    if (isObject(workExp.institution)) {
      workExp.institution = workExp.institution.id;
    }
    return new Promise((resolve, reject) => {
      this.workExperienceService.update(_id, workExp).subscribe(
        () => {
          resolve({
            data: workExp,
            event: 'update',
          });
          this.cancelButton();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(language: any) {
    return new Promise((resolve, reject) => {
      this.workExperienceService.create(language).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.cancelButton();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  save() {
    const workExperienceValue = this.workExperienceForm.value;
    const workExperience = Object.assign(workExperienceValue, {
      graduate_profile_pk: this.graduateProfilePk
    });
    const send = this.dataGenerate
      ? this.update(this.dataGenerate.id, workExperience)
      : this.create(workExperience);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.workExperienceService.formControl.next(res);
      })
      .catch(() => {
        this.snackBar.open('Ops...algo no esta funcionando!', 'Cerrar', {
          duration: 3000,
        });
      });
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }

  remove() { }

  removeAction() {

    this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
      duration: 3000,
    });
  }
}
