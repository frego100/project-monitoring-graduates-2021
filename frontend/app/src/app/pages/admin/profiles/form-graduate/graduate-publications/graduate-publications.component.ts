import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-graduate-publications',
  templateUrl: './graduate-publications.component.html',
  styleUrls: ['./graduate-publications.component.scss']
})
export class GraduatePublicationsComponent implements OnInit {

  @Input() auth;
  @Input() graduateProfilePk;

  valueList:boolean;
  valueForm:boolean;
  //para ver si se quiere editar o agregar
  action:string;
  //el id en caso se quiera editar
  editId:number;
  data:any;

  constructor() {
    this.valueList=true;
    this.valueForm=false;
    this.action="add"
  }

  ngOnInit(): void {
  }
  changeValue(mensaje: string){
    this.action=mensaje;
    if(this.action=="add"){
      this.valueList=false;
      this.valueForm=true;
    }

  }
  changeData(data:any){
    this.action="edit";
    this.valueList=false;
    this.valueForm=true;
    this.data=data;
  }
  changeValueCancel(mensaje:string){
    this.data=null;
    this.valueList=true;
    this.valueForm=false;
  }
}
