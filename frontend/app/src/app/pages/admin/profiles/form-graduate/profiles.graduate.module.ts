import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ProfileGraduateComponent } from './profiles.component';
import { LanguageFormComponent } from './graduate-language/form/form.component';
import { WorkExperienceFormComponent } from './graduate-work-experience/form/form.component';
import { LanguageListComponent } from './graduate-language/list/list.component';
import { WorkExperienceListComponent } from './graduate-work-experience/list/list.component';
import { GraduateLanguageComponent } from './graduate-language/graduate-language.component';
import { GraduateAcademicsComponent } from './graduate-data-academic/graduate-academics.component';
import { GraduateDistinctionsComponent } from './graduate-distinctions/graduate-distinctions.component';
import { GraduateProjectsComponent } from './graduate-projects/graduate-projects.component';
import { GraduateWorkExperienceComponent } from './graduate-work-experience/graduate-work-experience.component';
import { GraduatePublicationsComponent } from './graduate-publications/graduate-publications.component';
import { TableViewSimpleComponent } from '@app/shared/components/table-view-simple/table-view-simple';
import { GraduateAchievementsComponent } from './graduate-achievements/graduate-achievements.component';
import {GraduatePublicationsFormComponent} from "@pages/admin/profiles/form-graduate/graduate-publications/form/form.component";
import {GraduatePublicationsListComponent} from "@pages/admin/profiles/form-graduate/graduate-publications/list/list.component";
import {DisabledAccount} from "@shared/components/disabled-account/disabled-account";

export const ProfileGraduateRoutes: Routes = [];

@NgModule({
  declarations: [
    ProfileGraduateComponent,
    LanguageFormComponent,
    LanguageListComponent,
    GraduateAchievementsComponent,
    WorkExperienceFormComponent,
    WorkExperienceListComponent,
    TableViewSimpleComponent,
    GraduateLanguageComponent,
    GraduateAcademicsComponent,
    GraduateDistinctionsComponent,
    GraduateProjectsComponent,
    GraduateWorkExperienceComponent,
    GraduatePublicationsComponent,
    GraduateAchievementsComponent,
    GraduatePublicationsFormComponent,
    GraduatePublicationsListComponent,
    DisabledAccount
  ],

  exports: [
    ProfileGraduateComponent,
    LanguageFormComponent,
    LanguageListComponent,
    WorkExperienceFormComponent,
    WorkExperienceListComponent,
    GraduatePublicationsFormComponent,
    GraduatePublicationsListComponent,
    TableViewSimpleComponent,
    GraduateLanguageComponent,
    GraduateAcademicsComponent,
    GraduateDistinctionsComponent,
    GraduateProjectsComponent,
    GraduateWorkExperienceComponent,
    GraduatePublicationsComponent,
    GraduateAchievementsComponent,
    DisabledAccount
  ],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    CKEditorModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule,
  ],
  entryComponents: [],
})
export class ProfileModule {}
