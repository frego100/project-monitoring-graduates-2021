import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { GraduateLanguage } from '@app/models/graduate';
import { Language } from '@app/models/language';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { GraduateLanguageService } from '@app/services/graduate/graduate-language.service';
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

@Component({
  selector: 'list-graduate-language',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class LanguageListComponent implements OnInit {

  @Input() auth;
  @Input() graduateProfilePk;
  value: number;
  selectedItem: any;
  searchCtrl: any;
  public level_ty: any[] = [
    {
      id: 1,
      name: "Básico"
    },
    {
      id: 2,
      name: "Intermedio"
    },
    {
      id: 3,
      name: "Avanzado"
    }
  ]
  valueEvent: string;
  public listLanguageProf: Array<any> = [];
  public listLanguages: Array<Language> = [];
  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'language', 'level_typ', 'actions'];
  dataSource !: MatTableDataSource<GraduateLanguage>;
  dataLanguage: GraduateLanguage[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private languageService: GraduateLanguageService,
    public authUserService: AuthUserService
  ) {
    this.valueEvent = "edit";
    this.value = 1;
  }

  ngOnInit(): void {
    this.languageService.set(this.auth);
    this.getAllLanguages();
    if(this.auth){
      this.getAllAuth();
    }
    else{
      this.getGraduateLanguages();
    }
  }


  @Output()
  send: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  data: EventEmitter<any> = new EventEmitter<any>();

  getGraduateLanguages() {
    this.loading = true;
    this.languageService.getList(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.graduateProfilePk,
      null,
      null,
    ).subscribe((response) => {
      const res = response.results;
      for (let i = 0; i < res.length; i++) {
        const levelL = this.level_ty;
        const level = levelL.filter(item => {
          if (item.id === res[i].level_type) {
            return true;
          }
          else {
            return false;
          }
        });
        let aux = new GraduateLanguage();
        aux.id = res[i].id;
        aux.level_type = level[0].name;
        aux.language = res[i].language;
        this.listLanguageProf.push(aux);
      }
      this.dataSource = new MatTableDataSource(this.listLanguageProf);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  addClick() {
    this.valueEvent = "add";
    this.send.emit(this.valueEvent);
  }

  edit(data: any) {
    this.data.emit(data);
  }

  delete(data: any) {
    if (!IsNullUndefinedBlank(data)) {
      this.languageService.delete(data.id).subscribe(() => {
        this.snackBar.open('Eliminado correctamente!', 'Cerrar', { duration: 3000 });
        this.getGraduateLanguages();
      }, () => {
        this.snackBar.open('Error en la eliminación.. !', 'Cerrar', { duration: 3000 });
      })
    }
  }
  getAllLanguages() {
    this.languageService.allLenguages().subscribe((res: any) => {
      this.listLanguages = res;
    });
  }

  getAllAuth() {
    this.languageService.getAllAuth().subscribe((response: any) => {
      let res = response.results;
      for (let i = 0; i < res.length; i++) {
        const list = this.listLanguages;
        list.filter(item => {
          return item.code === res[i].language;
        });
        const levelL = this.level_ty;
        const level = levelL.filter(item => {
          return item.id === res[i].level_type;
        });
        let aux = new GraduateLanguage();
        aux.id = res[i].id;
        aux.level_type = level[0].name;
        aux.language = res[i].language;
        this.listLanguageProf.push(aux);
      }
      this.dataSource = new MatTableDataSource(this.listLanguageProf);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  add() {
    this.languageService.jobsControl.next(null);
    this.router.navigate([`nuevo`], { relativeTo: this.route });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
