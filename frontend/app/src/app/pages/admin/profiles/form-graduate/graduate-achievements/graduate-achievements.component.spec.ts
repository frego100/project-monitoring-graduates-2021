import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduateDistinctionsComponent } from './graduate-distinctions.component';

describe('GraduateDistinctionsComponent', () => {
  let component: GraduateDistinctionsComponent;
  let fixture: ComponentFixture<GraduateDistinctionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraduateDistinctionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduateDistinctionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
