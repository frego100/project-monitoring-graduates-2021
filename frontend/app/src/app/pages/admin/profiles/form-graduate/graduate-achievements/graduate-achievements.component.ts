import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Grade, GraduateAchievement } from '@app/models/graduate';
import { CountryService } from '@app/services/country/country.service';
import { GraduateAchievementService } from '@app/services/graduate/graduate-achievements.service';
import { formateMyDate, IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { DateAdapter } from '@angular/material/core';

const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
@Component({
  selector: 'app-graduate-achievements',
  templateUrl: './graduate-achievements.component.html',
  styleUrls: ['./graduate-achievements.component.scss']
})
export class GraduateAchievementsComponent implements OnInit {

  @Input() auth;
  @Input() graduateProfilePk;
  showView: boolean = true;
  modoEdit: boolean = false;
  loading: boolean = false;
  currentPage: number = 0;
  pageSize: number = 10;
  totalSize: number = 0;
  form: FormGroup;
  data: GraduateAchievement[];
  current: any;
  countries: any[];
  grades: Grade[]
  columns = [
    { columnDef: 'title', header: 'Título', cell: (element: any) => `${element.title}` },
    { columnDef: 'grade', header: 'Grado', cell: (element: any) => `${element.grade.name}` },
    { columnDef: 'country', header: 'País', cell: (element: any) => `${element.country.name}` },
    { columnDef: 'actions', header: 'Acciones', cell: (element: any) => `` }
  ];
  displayedColumns = this.columns.map(c => c.columnDef);

  constructor(
    private formBuilder: FormBuilder,
    private graduateAchievementService: GraduateAchievementService,
    private countryService: CountryService,
    private dateAdapter: DateAdapter<Date>,
    private snackBar: MatSnackBar
  ) {
    this.dateAdapter.setLocale('en-GB');
    this.form = this.formBuilder.group({
      id: [null],
      grade: ['', Validators.required],
      title: ['', Validators.required],
      country: ['', Validators.required],
    });
  }
  ngOnInit(): void {
    this.graduateAchievementService.set(this.auth);
    this.getData();
    this.getCountries();
    this.getGrades();
  }

  getData() {
    this.loading = true;
    this.graduateAchievementService.getList(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.graduateProfilePk,
      null
    ).subscribe((response) => {
      this.data = response.results;
      this.totalSize = response.count;
      this.loading = false;
    });
  }

  getCountries() {
    this.countryService.getAll().subscribe((response) => {
      this.countries = response;
    });
  }

  getGrades() {
    this.graduateAchievementService.getGrades().subscribe((response) => {
      this.grades = response;
    });
  }

  changePagination(value: any) {    
    this.currentPage = value.pageIndex;
    this.pageSize = value.pageSize;
    this.getData();
  }
  
  save() {
    if (this.form.valid) {
      var dataForm = this.form.value;
      dataForm.graduate_profile_pk = this.graduateProfilePk;
      if (this.modoEdit) {
        this.update(dataForm.id, dataForm);
        return;
      }
      this.create(dataForm);
    }
  }

  update(id: number, data: any) {
    delete data.graduate_profile_pk;
    delete data.id;
    this.graduateAchievementService.patch(id, data).subscribe(() => {
      this.cancel();
      this.snackBar.open('Guardado correctamente!', 'Cerrar', { duration: 3000 });
      this.getData();
    }, () => {
      this.snackBar.open('Error al agregar.. !', 'Cerrar', { duration: 3000 });
    })
  }

  create(data: any) {
    this.graduateAchievementService.post(data).subscribe(() => {
      this.cancel();
      this.snackBar.open('Guardado correctamente!', 'Cerrar', { duration: 3000 });
      this.getData();
    }, () => {
      this.snackBar.open('Error en la edición.. !', 'Cerrar', { duration: 3000 });
    });
  }

  cancel(): void {
    this.form.reset();
    this.showView = true;
  }

  showFormItem(value: boolean) {
    this.showView = false;
    this.modoEdit = value;
  }

  getDataItem(value: any) {
    var data = Object.assign({}, this.current, value);
    data.country = data.country.code;
    data.grade = data.grade.id;
    this.form.patchValue(data)
  }

  deleteItem(value: any) {
    if (!IsNullUndefinedBlank(value)) {
      this.graduateAchievementService.delete(value.id).subscribe(() => {
        this.snackBar.open('Eliminado correctamente!', 'Cerrar', { duration: 3000 });
        this.getData();
      }, () => {
        this.snackBar.open('Error en la eliminación.. !', 'Cerrar', { duration: 3000 });
      })
    }
  }
}
