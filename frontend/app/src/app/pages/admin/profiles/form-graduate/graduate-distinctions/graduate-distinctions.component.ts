import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GraduateDistinction } from '@app/models/graduate';
import { CountryService } from '@app/services/country/country.service';
import { GraduateDistinctionService } from '@app/services/graduate/graduate-distinctions.service';
import { formateMyDate, IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { DateAdapter } from '@angular/material/core';

const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

@Component({
  selector: 'app-graduate-distinctions',
  templateUrl: './graduate-distinctions.component.html',
  styleUrls: ['./graduate-distinctions.component.scss']
})
export class GraduateDistinctionsComponent implements OnInit {

  @Input() auth;
  @Input() graduateProfilePk;
  showView: boolean = true;
  modoEdit: boolean = false;
  loading: boolean = false;
  currentPage: number = 0;
  pageSize: number = 10;
  totalSize: number = 0;
  form: FormGroup;
  data: GraduateDistinction[];
  current: any;
  countries: any[]
  columns = [
    { columnDef: 'name', header: 'Nombre', cell: (element: any) => `${element.name}` },
    { columnDef: 'date', header: 'Fecha', cell: (element: any) => `${element.date}` },
    { columnDef: 'country', header: 'País', cell: (element: any) => `${element.country.name}` },
    { columnDef: 'link', header: 'Enlace', cell: (element: any) => `${element.reference_url}` },
    { columnDef: 'actions', header: 'Acciones', cell: (element: any) => `` },
  ];
  displayedColumns = this.columns.map(c => c.columnDef);

  constructor(
    private formBuilder: FormBuilder,
    private graduateDistinctionService: GraduateDistinctionService,
    private countryService: CountryService,
    private dateAdapter: DateAdapter<Date>,
    private snackBar: MatSnackBar
  ) {
    this.dateAdapter.setLocale('en-GB');
    this.form = this.formBuilder.group({
      id: [null],
      name: ['', Validators.required],
      date: ['', Validators.required],
      reference_url: ['', [Validators.required, Validators.pattern(urlRegex)]],
      description: ['', Validators.required],
      country: ['', Validators.required],
      graduate_profile_pk: [null],
    });
  }
  ngOnInit(): void {
    this.graduateDistinctionService.set(this.auth);
    this.getData();
    this.getCountries();
  }
  getData() {
    this.loading = true;
    this.graduateDistinctionService.getList(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.graduateProfilePk,
      null
    ).subscribe((response) => {
      this.data = response.results;
      this.totalSize = response.count;
      this.loading = false;
    });
  }
  getCountries() {
    this.countryService.getAll().subscribe((response) => {
      this.countries = response
    });
  }
  changePagination(value: any) {
    this.currentPage = value.pageIndex;
    this.pageSize = value.pageSize;
    this.getData();
  }
  save() {
    if (this.form.valid) {
      let dataForm = this.form.value;
      dataForm.graduate_profile_pk = this.graduateProfilePk;
      dataForm.date = formateMyDate(dataForm.date)
      if (this.modoEdit) {
        this.update(dataForm.id, dataForm);
        return;
      }
      this.create(dataForm);
    }
  }
  update(id: number, data: any) {
    delete data.graduate_profile_pk;
    delete data.id;
    this.graduateDistinctionService.patch(id, data).subscribe(() => {
      this.cancel();
      this.snackBar.open('Guardado correctamente!', 'Cerrar', { duration: 3000 });
      this.getData();
    }, () => {
      this.snackBar.open('Error al agregar.. !', 'Cerrar', { duration: 3000 });
    })
  }
  create(data: any) {
    this.graduateDistinctionService.post(data).subscribe(() => {
      this.cancel();
      this.snackBar.open('Guardado correctamente!', 'Cerrar', { duration: 3000 });
      this.getData();
    }, () => {
      this.snackBar.open('Error en la edición.. !', 'Cerrar', { duration: 3000 });
    });
  }
  cancel(): void {
    this.form.reset();
    this.showView = true;
  }
  showFormItem(value: boolean) {
    this.showView = false;
    this.modoEdit = value;
  }
  getDataItem(value: any) {
    let data = Object.assign({}, this.current, value);
    data.country = data.country.code;
    data.date = new Date(data.date + ' 0:00:00');
    this.form.patchValue(data)
  }
  deleteItem(value: any) {
    if (!IsNullUndefinedBlank(value)) {
      this.graduateDistinctionService.delete(value.id).subscribe(() => {
        this.snackBar.open('Eliminado correctamente!', 'Cerrar', { duration: 3000 });
        this.getData();
      }, () => {
        this.snackBar.open('Error en la eliminación.. !', 'Cerrar', { duration: 3000 });
      })
    }
  }
}
