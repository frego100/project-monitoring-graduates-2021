import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import {GraduateWorkExperience} from "@models/graduate";
import {GraduateWorkExperienceService} from "@app/services/graduate/graduate-work-experience.service";

import {IsNullUndefinedBlank} from "@shared/helpers/general-utils";
import {AuthUserService} from "@services";

@Component({
  selector: 'list-graduate-work-experience',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class WorkExperienceListComponent implements OnInit {

  public listWorkExperienceProf: Array<any> = [];
  public listWorkExperienceAdmin: Array<any> = [];

  @Input() auth;
  @Input() graduateProfilePk;

  valueEvent: string;

  pageSize = 10;
  loading: boolean = false;
  currentPage = 0;
  totalSize = 0;
  displayedColumns: string[] = [
    'charge', 'date_start', 'date_end', 'actions'
  ];
  dataSource !: MatTableDataSource<GraduateWorkExperience>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(
    private graduateWorkExperienceService: GraduateWorkExperienceService,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService
  ) { }

  ngOnInit(): void {
    this.graduateWorkExperienceService.set(this.auth);
    if(this.auth){
      this.getAllAuthWorkExperience()
    }
    else{
      this.getAllWorkExperience()
    }
  }
  @Output()
  send: EventEmitter<string> = new EventEmitter<string>();
  @Output()
  data: EventEmitter<any> = new EventEmitter<any>();

  addClick() {
    this.valueEvent = "add";
    this.send.emit(this.valueEvent);
  }
  edit(data: any) {
    this.data.emit(data);
  }
  delete(data: any) {
    if (!IsNullUndefinedBlank(data)) {
      this.graduateWorkExperienceService.delete(data.id).subscribe(() => {
        this.snackBar.open('Eliminado correctamente!', 'Cerrar', { duration: 3000 });
        this.getAllWorkExperience();
      }, () => {
        this.snackBar.open('Error en la eliminación.. !', 'Cerrar', { duration: 3000 });
      })
    }
  }
  getAllWorkExperience(){
    this.graduateWorkExperienceService.getList(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.graduateProfilePk,
      null
    ).subscribe((response)=>{
      this.listWorkExperienceAdmin = response.results
      this.dataSource = new MatTableDataSource(this.listWorkExperienceAdmin);
    })
  }

  getAllAuthWorkExperience(){
    this.graduateWorkExperienceService.getAllAuth().subscribe((response)=>{
      this.listWorkExperienceProf = response.results
      this.dataSource = new MatTableDataSource(this.listWorkExperienceProf);
    })
  }

}
