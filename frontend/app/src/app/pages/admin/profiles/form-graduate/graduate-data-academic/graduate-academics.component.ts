import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GraduateDataAcademic } from '@app/models/graduate';
import { Program } from '@app/models/program';
import { ProgramService} from '@app/services';
import { GraduateDataAcademicService } from '@app/services/graduate/graduate-data-academic.service';
import { getYears,getYearofUniversity, IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';
import { SedeService } from "@app/services/sede/sede.service";
import {QuestionService} from "@app/services/question/question.service";

@Component({
  selector: 'app-graduate-academics',
  templateUrl: './graduate-academics.component.html',
  styleUrls: ['./graduate-academics.component.scss']
})
export class GraduateAcademicsComponent implements OnInit {

  @Input() auth;
  @Input() graduateProfilePk;
  showView: boolean = true;
  modoEdit: boolean = false;
  loading: boolean = false;
  currentPage: number = 0;
  pageSize: number = 10;
  totalSize: number = 0;
  form: FormGroup;
  data: GraduateDataAcademic[];
  current: any;
  years: [];
  yearsUniversity: [];
  public programList: Program[] = [];
  public campusList = [];
  public graduatedQuestionList: any [] = []
  columns = [
    { columnDef: 'type', header: 'Tipo', cell: (element: any) =>
      {
        if (element.is_student) {
          return 'Estudiante'
        } else {
          return 'Egresado'
        }
      }
    },
    { columnDef: 'campus', header: 'Sede', cell: (element: any) => {
        if (element.campus!==null && element.campus!==undefined) {
          return `${element.campus.name}`
        } else {
          return `-`
        }
      }
    },
    { columnDef: 'profesional_program', header: 'Escuela Profesional', cell: (element: any) =>
      `${element.profesional_program.name}`
    },
    { columnDef: 'year_graduate', header: 'Año de egreso', cell: (element: any) =>
      {
        if (element.is_student) {
          return `-`
        } else {
          return `${element.year_graduate}`
        }
      }
    },
    { columnDef: 'period_graduate', header: 'Periodo de egreso', cell: (element: any) =>
      {
        if (element.is_student) {
          return `-`
        } else {
          return `${element.period_graduate}`
        }
      }
    },
    { columnDef: 'year_of_university_studies', header: 'Año en universidad', cell: (element: any) =>
      {
        if (element.is_student) {
          return `${element.year_of_university_studies} `
        } else {
          return `-`
        }
      }
    },
    { columnDef: 'actions', header: 'Acciones', cell: (element: any) => `` },
  ];
  displayedColumns = this.columns.map(c => c.columnDef);

  constructor(
    private formBuilder: FormBuilder,
    private graduateDataAcademicService: GraduateDataAcademicService,
    private graduateQuestionService: QuestionService,
    private snackBar: MatSnackBar,
    private programService: ProgramService,
    private sedeService: SedeService
  ) {
    this.form = this.formBuilder.group({
      id: [null],
      type: ['is_graduate', Validators.required],
      period_graduate: [null],
      profesional_program: [null],
      year_of_university_studies:[null],
      profesional_program_pk: [null, Validators.required],
      year_graduate: [null],
      graduate_profile_pk: [null],
      campus: [null],
      campus_pk: ['', Validators.required],
      graduate_questions: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.graduateDataAcademicService.set(this.auth);
    this.getData();
    this.getAllGraduate();
    this.getDataProgram();
    this.getDataCampus();
    this.years = getYears();
    this.yearsUniversity = getYearofUniversity();
  }
  getData() {
    this.loading = true;
    this.graduateDataAcademicService.getList(
      this.pageSize,
      this.currentPage * this.pageSize,
      this.graduateProfilePk,
      null,
      null,
    ).subscribe((response) => {
      this.data = response.results;
      this.totalSize = response.count;
      this.loading = false;
    });
  }
  getAllGraduate() {
    this.graduateQuestionService.getAll().subscribe((response) => {
      this.graduatedQuestionList = response.results
    });
  }
  getDataProgram() {
    this.programService.getWitOutPagination().subscribe((response) => {
      this.programList = response.results;
    });
  }
  getDataCampus() {
    this.sedeService.getAll().subscribe((data) => {
      this.campusList = data.results;
    });
  }
  changePagination(value: any) {
    this.currentPage = value.pageIndex;
    this.pageSize = value.pageSize;
    this.getData();
  }
  save() {
    if(!this.form.valid) {
      this.snackBar.open('Faltan completar correctamente algunos datos (revise los puntos en rojo)', 'Cerrar', {
        duration: 3000,
      });
      return;
    }
      let dataForm: GraduateDataAcademic = this.form.value;
      dataForm.graduate_profile_pk = this.graduateProfilePk;
      if (this.form.value.type === 'is_student') {
        dataForm.is_student = true
        dataForm.is_graduate = false
      } else if (this.form.value.type === 'is_graduate') {
        dataForm.is_student = false
        dataForm.is_graduate = true
      }
      if (this.modoEdit) {
        this.update(dataForm.id, dataForm);
        return;
      }
      this.create(dataForm);
  }
  update(id: number, data: any) {
    delete data.graduate_profile_pk;
    delete data.id;
    this.graduateDataAcademicService.patch(id, data).subscribe(() => {
      this.cancel();
      this.snackBar.open('Guardado correctamente!', 'Cerrar', { duration: 3000 });
      this.getData();
    }, () => {
      this.snackBar.open('Error al agregar.. !', 'Cerrar', { duration: 3000 });
    })
  }
  create(data: GraduateDataAcademic) {
    this.graduateDataAcademicService.post(data).subscribe(() => {
      this.cancel();
      this.snackBar.open('Guardado correctamente!', 'Cerrar', { duration: 3000 });
      this.getData();
    }, () => {
      this.snackBar.open('Error en la edición.. !', 'Cerrar', { duration: 3000 });
    });
  }
  cancel(): void {
    this.form.reset();
    this.showView = true;
  }
  showFormItem(value: boolean) {
    this.showView = false;
    this.modoEdit = value;
  }
  getDataItem(value: any) {
    var data = Object.assign({}, this.current, value);
    if (data.is_graduate) {
      data.type = 'is_graduate'
    } else if (data.is_student) {
      data.type = 'is_student'
    }
    let graduateIdQuestions = []
    for(let i = 0; i < data.graduate_questions.length; i++){
      graduateIdQuestions.push(this.graduatedQuestionList[i].id)
    }
    data.graduate_questions = graduateIdQuestions
    this.form.patchValue(data)
  }
  deleteItem(value: any) {
    if (!IsNullUndefinedBlank(value)) {
      this.graduateDataAcademicService.delete(value.id).subscribe(() => {
        this.snackBar.open('Eliminado correctamente!', 'Cerrar', { duration: 3000 });
        this.getData();
      }, () => {
        this.snackBar.open('Error en la eliminación.. !', 'Cerrar', { duration: 3000 });
      })
    }
  }

}
