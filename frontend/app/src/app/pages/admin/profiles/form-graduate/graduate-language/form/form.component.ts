import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Faculty } from '@app/models/faculty';
import { Language } from '@app/models/language';
import { FacultyService } from '@app/services';
import { GraduateLanguageService } from '@app/services/graduate/graduate-language.service';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'form-graduate-language',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class LanguageFormComponent implements OnInit {
  @Input() auth;
  @Input() graduateProfilePk;
  languageForm: FormGroup;
  editor = ClassicEditorBuild;
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  languageTrSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  languageTr: any;
  editLanguage: boolean;
  idLenguage: string;
  idLevel: number;
  language: string;
  showBeforeManual: boolean = false;
  @Input()
  dataGenerate: any;
  idTemp: string;
  public listFaculty: Faculty[] = [];
  public listLanguages: Array<Language> = [];
  public level_type: any[] = [
    {
      id: 1,
      name: 'Básico',
    },
    {
      id: 2,
      name: 'Intermedio',
    },
    {
      id: 3,
      name: 'Avanzado',
    },
  ];
  constructor(
    private formBuilder: FormBuilder,
    private languageService: GraduateLanguageService,
    private facultadService: FacultyService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.languageForm = this.formBuilder.group({
      level_type: ['', Validators.required],
      language: ['', Validators.required],
    });
    this.editLanguage = false;
  }

  ngOnInit(): void {
    this.getAllLanguages();
    if (this.dataGenerate != null) {
      this.languageService
        .getById(this.dataGenerate.id)
        .subscribe((res: any) => {
          this.languageForm.patchValue({
            id: res.id,
            level_type: res.level_type,
            language: res.language,
          });
        });
      this.editLanguage = true;
    }
    this.language = this.dataGenerate.language.name;
  }

  @Output()
  cancel: EventEmitter<string> = new EventEmitter<string>();
  //Este metodo retorna al padre funciona en cancelar como tal
  //Funciona como redireccion cuando se crea o edita algo
  
  cancelButton() {
    this.cancel.emit('cancel');
  }
  
  getAllLanguages() {
    this.languageService.allLenguages().subscribe((res) => {
      this.listLanguages = res;
    });
  }
  
  onSelectLanguage(id: any) {
    this.idLenguage = id;
  }
  
  onSelectLevel(id: any) {
    this.idLevel = id;
  }
  update(_id: any, languageTr: any) {
    return new Promise((resolve, reject) => {
      this.languageService.update(_id, languageTr).subscribe(
        (res) => {
          resolve({
            data: languageTr,
            event: 'update',
          });
          this.cancelButton();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(language: any) {
    return new Promise((resolve, reject) => {
      this.languageService.create(language).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.cancelButton();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  save() {
    const languageValue = this.languageForm.value;
    const language = Object.assign(languageValue, {
      graduate_profile_pk: this.graduateProfilePk, 
      language: languageValue.language.code
    });

    const send = this.dataGenerate
      ? this.update(this.dataGenerate.id, language)
      : this.create(language);

    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.languageService.formControl.next(res);
        if (res.event === 'create') {
          // this.router.navigate(['/admin/manuales']);
        }
      })
      .catch((err) => {
        this.snackBar.open('Ops...algo no esta funcionando!', 'Cerrar', {
          duration: 3000,
        });
      });
  }

  remove() {}

  removeAction() {
    this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
      duration: 3000,
    });
  }
  onSelect(id: any): void {
    //this.programForm=program;

    this.idTemp = id;
  }
  getAllFaculty() {
    this.facultadService.getAll().subscribe((faculty: any) => {
      this.listFaculty = faculty.results;
      const list = this.listFaculty;
      const enabled = list.filter((item) => {
        if (item.is_active === true) {
          return true;
        } else {
          return false;
        }
      });
      this.listFaculty = enabled;
    });
  }
}
