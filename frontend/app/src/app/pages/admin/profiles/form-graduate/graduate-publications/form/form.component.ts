import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as ClassicEditorBuild from '@ckeditor/ckeditor5-build-classic';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { typePublication } from '@models/typePublication';
import { BaseModel } from '@core';
import { GraduatePublicationsService } from '@app/services/graduate/graduate-publications.service';

@Component({
  selector: 'form-graduate-publications',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class GraduatePublicationsFormComponent implements OnInit {
  @Input() auth;
  @Input() graduateProfilePk;

  publicationsForm: FormGroup;
  editor = ClassicEditorBuild;

  routeParamsSubscription: Subscription = new Subscription();
  @Input()
  dataGenerate: any;
  idTemp: string;
  public listTypePublication: typePublication[] = [];
  public listFunctionType: BaseModel[] = [];

  public listTypePub: typePublication[] = [];
  public listFuncType: BaseModel[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private publicationsService: GraduatePublicationsService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.publicationsForm = this.formBuilder.group({
      type_publication: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      function_type: ['', Validators.required],
      DOI: ['', Validators.required],
      reference_url: ['', Validators.required],
      graduate_profile_pk: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getAllTypePublications();
    this.getAllFunctionType();
    if (this.dataGenerate != null) {
      this.publicationsService
        .getById(this.dataGenerate.id)
        .subscribe((res: any) => {
          this.publicationsForm.patchValue(res);
        });
    }
  }

  @Output()
  cancel: EventEmitter<string> = new EventEmitter<string>();
  cancelButton() {
    this.cancel.emit('cancel');
  }
  getAllTypePublications() {
    this.publicationsService.getAllTypePublications().subscribe((res: any) => {
      this.listTypePub = res;
    });
  }
  getAllFunctionType() {
    this.publicationsService.getAllFunctionType().subscribe((res: any) => {
      this.listFuncType = res;
    });
  }
  update(_id: any, publications: any) {
    let pub = null;
    pub = Object.assign({}, publications, {
      function_type: publications.function_type.id,
      type_publication: publications.type_publication.id,
    });
    if (
      typeof publications.function_type === 'number' ||
      typeof publications.type_publication === 'number'
    ) {
      if (typeof publications.function_type === 'number') {
        pub = Object.assign({}, publications, {
          type_publication: publications.type_publication.id,
        });
      }
      if (typeof publications.type_publication === 'number') {
        pub = Object.assign({}, publications, {
          function_type: publications.function_type.id,
        });
      }
    } else {
      pub = Object.assign({}, publications, {
        function_type: publications.function_type.id,
        type_publication: publications.type_publication.id,
      });
    }
    return new Promise((resolve, reject) => {
      this.publicationsService.update(_id, pub).subscribe(
        (res) => {
          resolve({
            data: pub,
            event: 'update',
          });
          this.cancelButton();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  create(publication: any) {
    return new Promise((resolve, reject) => {
      this.publicationsService.create(publication).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.cancelButton();
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  allErrors(err: any) {
    let keys = Object.keys(err.error);
    let values = Object.values(err.error);
    let allErrors = '';
    for (let i = 0; i < keys.length; i++) {
      if (keys[i] === 'type_publication') keys[i] = 'Nombre';
      if (keys[i] === 'title') keys[i] = 'Título';
      if (keys[i] === 'description') keys[i] = 'Descripción';
      if (keys[i] === 'function_type') keys[i] = 'Rol del Investigador';
      if (keys[i] === 'reference_url') keys[i] = 'URL de Referencia';
      allErrors += keys[i] + ': ' + values[i] + '\n';
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    const publicationValue = this.publicationsForm.value;
    const publication = Object.assign(publicationValue, {
      graduate_profile_pk: this.graduateProfilePk
    });
    const send = this.dataGenerate
      ? this.update(this.dataGenerate.id, publication)
      : this.create(publication);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.publicationsService.formControl.next(res);
        if (res.event === 'create') {
        }
      })
      .catch((err) => {
        this.allErrors(err);
      });
  }
  compareSelectedValue(oldBlock, newBlock): boolean {
    return oldBlock && newBlock && oldBlock._id === newBlock._id;
  }

  remove() {}

  removeAction() {
    this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
      duration: 3000,
    });
  }
}
