import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GraduateProject } from '@app/models/graduate';
import {
  formateMyDate,
  IsNullUndefinedBlank,
} from '@app/shared/helpers/general-utils';
import { DateAdapter } from '@angular/material/core';
import { GraduateProjectService } from '@app/services/graduate/graduate-project.service';
import { GraduateExtrasService } from '@app/services/graduate/graduate-extras.service';

const urlRegex =
  /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;

@Component({
  selector: 'app-graduate-projects',
  templateUrl: './graduate-projects.component.html',
  styleUrls: ['./graduate-projects.component.scss'],
})
export class GraduateProjectsComponent implements OnInit {
  @Input() auth;
  @Input() graduateProfilePk;
  showView: boolean = true;
  modoEdit: boolean = false;
  loading: boolean = false;
  currentPage: number = 0;
  pageSize: number = 10;
  totalSize: number = 0;
  form: FormGroup;
  data: GraduateProject[];
  current: any;
  listTypeProject: any[];
  listTypeRole: any[];
  columns = [
    {
      columnDef: 'title',
      header: 'Título',
      cell: (element: any) => `${element.title}`,
    },
    {
      columnDef: 'code',
      header: 'Código',
      cell: (element: any) => `${element.code}`,
    },
    {
      columnDef: 'type_project',
      header: 'Tipo de Proyecto',
      cell: (element: any) => {
        return this.graduateExtrasService.getItemTypeProjectbyId(
          element.type_project
        );
      },
    },
    {
      columnDef: 'type_role',
      header: 'Rol desempeñado',
      cell: (element: any) => {
        return this.graduateExtrasService.getItemTypeRolebyId(
          element.type_role
        );
      },
    },
    {
      columnDef: 'date_start',
      header: 'Fecha inicio',
      cell: (element: any) => `${element.date_start}`,
    },
    {
      columnDef: 'date_end',
      header: 'Ficha fin',
      cell: (element: any) => `${element.date_end}`,
    },
    // { columnDef: 'description', header: 'Descripción', cell: (element: any) => `${element.description}` },
    {
      columnDef: 'link',
      header: 'Enlace',
      cell: (element: any) => `${element.reference_url}`,
    },
    { columnDef: 'actions', header: 'Acciones', cell: (element: any) => `` },
  ];
  displayedColumns = this.columns.map((c) => c.columnDef);

  constructor(
    private formBuilder: FormBuilder,
    private graduateProjectService: GraduateProjectService,
    private dateAdapter: DateAdapter<Date>,
    private snackBar: MatSnackBar,
    private graduateExtrasService: GraduateExtrasService
  ) {
    this.dateAdapter.setLocale('en-GB');
    this.form = this.formBuilder.group({
      id: [null],
      title: ['', Validators.required],
      code: ['', Validators.required],
      reference_url: ['', [Validators.required, Validators.pattern(urlRegex)]],
      description: ['', Validators.required],
      type_project: ['', Validators.required],
      type_role: ['', Validators.required],
      date_start: ['', Validators.required],
      date_end: ['', Validators.required],
      graduate_profile_pk: [null],
    });
    this.listTypeProject = this.graduateExtrasService.getTypesProject();
    this.listTypeRole = this.graduateExtrasService.getTypesRole();
  }

  ngOnInit(): void {
    this.graduateProjectService.set(this.auth);
    this.getData();
  }
  getData() {
    this.loading = true;
    this.graduateProjectService
      .getList(
        this.pageSize,
        this.currentPage * this.pageSize,
        this.graduateProfilePk
      )
      .subscribe((response) => {
        this.data = response.results;
        this.totalSize = response.count;
        this.loading = false;
      });
  }
  changePagination(value: any) {
    this.currentPage = value.pageIndex;
    this.pageSize = value.pageSize;
    this.getData();
  }
  save() {
    if (this.form.valid) {
      var dataForm: GraduateProject = this.form.value;
      dataForm.graduate_profile_pk = this.graduateProfilePk;
      dataForm.date_end = formateMyDate(dataForm.date_end);
      dataForm.date_start = formateMyDate(dataForm.date_start);
      if (this.modoEdit) {
        this.update(dataForm.id, dataForm);
        return;
      }
      this.create(dataForm);
    }
  }
  update(id: number, data: any) {
    delete data.graduate_profile_pk;
    delete data.id;
    this.graduateProjectService.patch(id, data).subscribe(
      () => {
        this.cancel();
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.getData();
      },
      (res) => {
        if (res.error.code) {
          this.snackBar.open('Asegúrese de que el código no tenga más de 10 caracteres', 'Cerrar', {
            duration: 3000,
          });
        }
        else if (res.error.reference_url){
          this.snackBar.open(res.error.reference_url, 'Cerrar', {
            duration: 3000,
          });
        }
        else{
          this.snackBar.open('Error en la edición.. !', 'Cerrar', {
            duration: 3000,
          });
        }
      }
    );
  }

  create(data: GraduateProject) {
    this.graduateProjectService.post(data).subscribe(
      () => {
        this.cancel();
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000,
        });
        this.getData();
      },
      (res) => {
        if (res.error.code) {
          this.snackBar.open('Asegúrese de que el código no tenga más de 10 caracteres', 'Cerrar', {
            duration: 3000,
          });
        }
        else if (res.error.reference_url){
          this.snackBar.open(res.error.reference_url, 'Cerrar', {
            duration: 3000,
          });
        }
        else{
          this.snackBar.open('Error en la creación.. !', 'Cerrar', {
            duration: 3000,
          });
        }
      }
    );
  }

  cancel(): void {
    this.form.reset();
    this.showView = true;
  }

  showFormItem(value: boolean) {
    this.showView = false;
    this.modoEdit = value;
  }
  getDataItem(value: any) {
    var data = Object.assign({}, this.current, value);
    let fechaDate_end = new Date(data.date_end + ' 0:00:00');
    data.date_end = fechaDate_end;
    let fechaDate_start = new Date(data.date_start + ' 0:00:00');
    data.date_start = fechaDate_start;
    this.form.patchValue(data);
  }
  deleteItem(value: any) {
    if (!IsNullUndefinedBlank(value)) {
      this.graduateProjectService.delete(value.id).subscribe(
        () => {
          this.snackBar.open('Eliminado correctamente!', 'Cerrar', {
            duration: 3000,
          });
          this.getData();
        },
        () => {
          this.snackBar.open('Error en la eliminación.. !', 'Cerrar', {
            duration: 3000,
          });
        }
      );
    }
  }
}
