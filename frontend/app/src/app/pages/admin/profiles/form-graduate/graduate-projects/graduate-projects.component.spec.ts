import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduateProjectsComponent } from './graduate-projects.component';

describe('GraduateProjectsComponent', () => {
  let component: GraduateProjectsComponent;
  let fixture: ComponentFixture<GraduateProjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraduateProjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduateProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
