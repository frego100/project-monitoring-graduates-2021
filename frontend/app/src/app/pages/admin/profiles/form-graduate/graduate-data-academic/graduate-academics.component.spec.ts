import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduateAcademicsComponent } from './graduate-academics.component';

describe('GraduateAcademicsComponent', () => {
  let component: GraduateAcademicsComponent;
  let fixture: ComponentFixture<GraduateAcademicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraduateAcademicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduateAcademicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
