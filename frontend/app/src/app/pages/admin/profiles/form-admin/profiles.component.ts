import { Identifier } from '@app/models/identifier';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '@app/models/employee';
import { Jobs } from '@app/models/job';
import { Program } from '@app/models/program';
import { Roles } from '@app/models/rol';
import { IdentifierService, TypeEmployeeService, JobsService, ProgramService, ProfileService, AuthUserService } from '@services';
import { Subscription } from 'rxjs';
import {DisabledAccount} from "@shared/components/disabled-account/disabled-account";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfileAdminComponent implements OnInit {

  idTempIdenti: number;
  idTempJobs: number;
  idTempEmpl: number;
  idTempProgr: number;
  public listIdentifi: Identifier[] = [];
  public listJobs: Jobs[] = [];
  public listEmployees: Employee[] = [];
  public listPrograms: Program[] = [];
  picture: any;
  isPicture = false;
  public listRoles: Roles[] = [];
  adminProfile: any;
  profileSelfForm: FormGroup;
  user: any;
  profileSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private identiService: IdentifierService,
    private jobsService: JobsService,
    private programService: ProgramService,
    private employService: TypeEmployeeService,
    private router: Router,
    private authService: AuthUserService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    this.profileSelfForm = this.formBuilder.group({
      first_name:['', Validators.required],
      last_name:['', Validators.required],
      document: [{value:'',disabled: true}, Validators.required],
      cell_phone: ['', Validators.required],
      identificator: [{value:'',disabled: true}],
      job_position: [null],
      username:[''],
      type_employee: [''],
      instance: [''],
      is_active:true,
    });

  }

  ngOnInit(): void {
    this.getAllIdent();
    this.getAllJobs();
    this.getAllEmpl();
    this.getAllProgr();
    this.getUser();

  }
  getAllIdent() {
    this.identiService.getAll().subscribe((ident) => {
      this.listIdentifi = ident.results;
      const list = this.listIdentifi;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listIdentifi = habilitit;
    });
  }

  getUser() {
    this.authService.getUserAdmin().subscribe((res: any) => {
      this.profileSelfForm.patchValue({
        first_name:res.first_name,
        last_name:res.last_name,
        username:res.username,
        document: res.user_profile.document,
        cell_phone: res.user_profile.cell_phone,
        identificator: res.user_profile.identificator,
        job_position: res.user_profile.job_position,
        type_employee: res.user_profile.type_employee,
        instance: res.user_profile.instance
      });
    });

  }
  onFileChangePicture(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.picture = file;
      this.isPicture = true;
    }
  }
  getAllJobs() {
    this.jobsService.getAll().subscribe((ident) => {
      this.listJobs = ident.results;

      const list = this.listJobs;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listJobs = habilitit;
    });
  }
  getAllEmpl() {
    this.employService.getAll().subscribe((ident) => {
      this.listEmployees = ident.results;
      const list = this.listEmployees;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listEmployees = habilitit;
    });

  }
  getAllProgr() {
    this.programService.getAll().subscribe((ident) => {
      this.listPrograms = ident.results;
      const list = this.listPrograms;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listPrograms = habilitit;
    });
  }

  cancel() {
    history.back();
  }

  edit(profileS: any) {
    return new Promise((resolve, reject) => {
      this.profileService.patchProfileSelf(profileS).subscribe(
        (res) => {
          this.authService.updateUserAuth(profileS).subscribe((res)=>{
          });
          resolve({
            data: profileS,
            event: 'create'
          });
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
        },
        (err) => {
          reject(err);
        }

      );;
    });
  }
  savePicture() {
    this.profileService.updatePicture(this.picture).subscribe(
      (dat) => {
        dat.data = this.picture;
      });
  }
  save() {
    if (this.isPicture) {
      this.savePicture();
    }

    const user = this.profileSelfForm.value;

    const send = this.edit(user);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000
        });

        let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
      })
      .catch((err) => {
        this.snackBar.open('Error! Su usuario no esta asignado a ningún perfil', 'Cerrar', {
          duration: 3000
        });
      });
  }

  onSelectIdent(id: any): void {
    this.idTempIdenti = id;
    this.profileSelfForm.patchValue({
      identificator: this.idTempIdenti,
    });
  }

  onSelectEmpl(id: any): void {
    this.idTempEmpl = id;
    this.profileSelfForm.patchValue({
      type_employee: this.idTempEmpl
    });
  }

  onSelectJobs(id: any): void {
    this.idTempJobs = id;
    this.profileSelfForm.patchValue({
      job_position: this.idTempJobs
    });
  }

  onSelectProg(id: any): void {
    this.profileSelfForm.patchValue({
      instance: this.idTempProgr
    });
    this.idTempProgr = id;
  }
  disabledAccount( openDialog = true) {
    const dialogRef = this.dialog.open(DisabledAccount, {
      width: '500px',
      data:{
      }
    }).componentInstance;
  }


}
