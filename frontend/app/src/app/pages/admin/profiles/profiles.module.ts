import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileAdminComponent } from './form-admin/profiles.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CheckProfileGuard } from '@app/shared/guards/check-profile.guard';
import { ProfileCompanyComponent } from './form-company/profiles.component';
import { ProfileGraduateComponent } from './form-graduate/profiles.component';
import { LanguageListComponent } from './form-graduate/graduate-language/list/list.component';
import { ProfileGraduateRoutes } from './form-graduate/profiles.graduate.module';
import { ProfileSuperAdminComponent } from './form-superadmin/profiles.component';


export const ProfileRoutes: Routes = [
   {
      path: 'perfil',
      children: [
         {
            path: 'admin',
            component: ProfileAdminComponent,
            canActivate: [CheckProfileGuard],
            data: { profile: 'admin' },

         },
         {
            path: 'empresa',
            component: ProfileCompanyComponent,
            canActivate: [CheckProfileGuard],
            data: { profile: 'empresa' },

         },
         {
            path: 'superadmin',
            component: ProfileSuperAdminComponent,
            //    canActivate: [CheckProfileGuard],
            //    data: { profile: 'empresa' },

         },
         {
            path: 'egresado',
            component: ProfileGraduateComponent,
            children: ProfileGraduateRoutes,
          
        
         },
      ]
   }
];

@NgModule({
   declarations: [ProfileAdminComponent, ProfileCompanyComponent, 
                  ProfileSuperAdminComponent ],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      CKEditorModule,
      AppCommonModule,
      BrowserAnimationsModule,
      PipesModule
   ],
   entryComponents: []
})
export class UserModule { }
