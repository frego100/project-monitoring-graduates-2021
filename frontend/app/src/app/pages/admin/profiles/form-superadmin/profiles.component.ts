import { Identifier } from '@app/models/identifier';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '@app/models/employee';
import { Jobs } from '@app/models/job';
import { Program } from '@app/models/program';
import { Roles } from '@app/models/rol';
import { IdentifierService, UserService, TypeEmployeeService, JobsService, ProfileService, AuthUserService } from '@services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfileSuperAdminComponent implements OnInit {

  idTempIdenti: number;
  idTempJobs: number;
  idTempEmpl: number;
  idTempProgr: number;
  public listIdentifi: Identifier[] = [];
  public listJobs: Jobs[] = [];
  public listEmployees: Employee[] = [];
  public listPrograms: Program[] = [];
  picture: any;
  idUser: number;
  isPicture = false;
  public listRoles: Roles[] = [];
  adminProfile: any;
  profileSelfForm: FormGroup;
  user: any;
  profileSubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private identiService: IdentifierService,
    private jobsService: JobsService,
    private employService: TypeEmployeeService,
    private router: Router,
    private authService: AuthUserService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
    this.profileSelfForm = this.formBuilder.group({
      user: [],
      first_name:['',Validators.required],
      last_name:['',Validators.required],
      document: ['', Validators.required],
      cell_phone: ['', Validators.required],
      phone: ['', Validators.required],
      identificator: [null],
      job_position: [null],
      type_employee: [''],
      username:[''],
      is_active: true,
      
    });

  }

  ngOnInit(): void {
    this.getAllIdent();
    this.getAllJobs();
    this.getAllEmpl();
    this.getUser();

  }
  getAllIdent() {
    this.identiService.getAll().subscribe((ident) => {
      this.listIdentifi = ident.results;
      const list = this.listIdentifi;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listIdentifi = habilitit;
    });
  }

  getUser() {

    this.authService.getUserGeneral().subscribe((res: any) => {
      this.userService.getAll().subscribe((users) => {
        const aux = users.results;
        const idU = aux.filter(item => {
          if (item.username === res.username) {
            return true;
          }
          else {
            return false;
          }
        });
        this.idUser = idU[0].id;
        this.profileSelfForm.patchValue({

          user: this.idUser,
          username:res.username,
          first_name:res.first_name,
          last_name:res.last_name,
          document: res.user_profile.document,
          cell_phone: res.user_profile.cell_phone,
          identificator: res.user_profile.identificator,
          job_position: res.user_profile.job_position,
          type_employee: res.user_profile.type_employee,
          phone: res.user_profile.phone
        });

      });
    });
  }
  onFileChangePicture(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.picture = file;
      this.isPicture = true;
    }
  }
  getAllJobs() {
    this.jobsService.getAll().subscribe((ident) => {
      this.listJobs = ident.results;

      const list = this.listJobs;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listJobs = habilitit;
    });
  }
  getAllEmpl() {
    this.employService.getAll().subscribe((ident) => {
      this.listEmployees = ident.results;
      const list = this.listEmployees;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listEmployees = habilitit;
    });

  }

  cancel() {
    history.back();
  }

  edit(profileS: any) {
   
    return new Promise((resolve, reject) => {
      this.profileService.patchProfileSelf(profileS).subscribe( (res) => {
          this.authService.updateUserAuth(profileS).subscribe((res)=>{
          });
          resolve({
            data: profileS,
            event: 'create'
          });
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
        },
        (err) => {
          reject(err);
        }

      );;
    });
  }
  savePicture() {
    this.profileService.updatePicture(this.picture).subscribe(
      (dat) => {
        dat.data = this.picture;
      });
  }
  save() {
    if (this.isPicture) {
      this.savePicture();
    }

    const user = this.profileSelfForm.value;

    const send = this.edit(user);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000
        });

        let currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);

      })
      .catch((err) => {
        this.snackBar.open('Error! Su usuario no esta asignado a ningún perfil', 'Cerrar', {
          duration: 3000
        });
      });
  }

  onSelectIdent(id: any): void {
    this.idTempIdenti = id;
    this.profileSelfForm.patchValue({
      identificator: this.idTempIdenti,
    });
  }

  onSelectEmpl(id: any): void {
    this.idTempEmpl = id;
    this.profileSelfForm.patchValue({
      type_employee: this.idTempEmpl
    });
  }

  onSelectJobs(id: any): void {
    this.idTempJobs = id;
    this.profileSelfForm.patchValue({
      job_position: this.idTempJobs
    });
  }

}
