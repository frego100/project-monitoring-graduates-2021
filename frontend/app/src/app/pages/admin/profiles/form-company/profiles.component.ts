import { Identifier } from '@app/models/identifier';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Roles } from '@app/models/rol';
import { IdentifierService, UserService, ProfileService, AuthUserService } from '@services';
import { Company } from '@app/models/company';
import {DisabledAccount} from "@shared/components/disabled-account/disabled-account";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfileCompanyComponent implements OnInit {
  isLinear = false;
  isUpdate = true;
  logo: any;
  picture: any;
  isLogo = false;
  isPicture = false;
  userCompanyForm: FormGroup;
  companyForm: FormGroup;
  registerPreUser: FormGroup;
  company: Company;
  public listRoles: Roles[] = [];
  public listIdentifier: Identifier[] = [];
  idUser: number;
  idIdent: number;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private authService: AuthUserService,
    private identService: IdentifierService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    this.companyForm = this.formBuilder.group({
      identificator: [{value:'',disabled: true}, Validators.required],
      company_name: ['', Validators.required],
      social_reason: ['', Validators.required],
      ruc: [{value:'',disabled: true}, Validators.required],
      company_activity: ['', Validators.required],

    });
    this.userCompanyForm = this.formBuilder.group({
      user: [''],
      first_name:['',Validators.required],
      last_name:['',Validators.required],
      username:[''],
      identificator: ['', Validators.required],
      document: ['', Validators.required],
      cell_phone: ['', Validators.required],
      phone: ['', Validators.required],
      is_active: true,
      company: [''],
    });
  }

  ngOnInit(): void {
    this.getAllIdentifiers();
    this.getUser();
  }
  getUser() {
    this.authService.getUserCompany().subscribe((res: any) => {
      this.userService.getAll().subscribe((users) => {
        const aux = users.results;
        const idU = aux.filter(item => {
          if (item.username === res.username) {
            return true;
          }
          else {
            return false;
          }
        })
        this.idUser = idU[0].id;

      this.userCompanyForm.patchValue({
        user: idU[0].id,
        username:res.username,
        first_name:res.first_name,
        last_name:res.last_name,
        document: res.user_profile.document,
        cell_phone: res.user_profile.cell_phone,
        phone: res.user_profile.phone,
        identificator: res.user_profile.identificator.id,
        job_position: res.user_profile.job_position,
        type_employee: res.user_profile.type_employee,
        instance: res.user_profile.instance
      });
    });
      this.companyForm.patchValue({
        identificator: res.user_profile.company.identificator.id,
        company_name: res.user_profile.company.company_name,
        social_reason: res.user_profile.company.social_reason,
        ruc: res.user_profile.company.ruc,
        company_activity: res.user_profile.company.company_activity,
      });
    });
  }
  onFileChangeLogo(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.logo = file;
      this.isLogo = true;
    }
  }
  onFileChangePicture(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.picture = file;
      this.isPicture = true;
    }
  }
  savePicture() {
    this.profileService.updatePicture(this.picture).subscribe(
      (dat) => {
        dat.data = this.picture;
      });

  }
  saveLogo() {
    this.profileService.updateLogo(this.logo).subscribe(
      (dat) => {
        dat.data = this.logo;
      });

  }
  guardar() {
    if (this.isPicture) {
      this.savePicture();
    }
    if (this.isLogo) {
      this.saveLogo();
    }
    const companyVal = this.companyForm.value;
    this.company = companyVal;

    this.userCompanyForm.patchValue({
      company: this.companyForm.value,
    });

    const formValue = this.userCompanyForm.value;
    const send = this.update(formValue);
    send
      .then((res: any) => {
        this.snackBar.open('Guardado correctamente!', 'Cerrar', {
          duration: 3000
        });

        let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);

      })
      .catch((err) => {
        this.snackBar.open('Error! Su usuario no esta asignado a ningún perfil', 'Cerrar', {
          duration: 3000
        });
      });
    this.update(formValue);
  }
  update(profileS: any) {
    return new Promise((resolve, reject) => {
      this.profileService.patchProfileSelf(profileS).subscribe(
        (res) => {
          this.authService.updateUserAuth(profileS).subscribe((res)=>{
          });
          resolve({
            data: profileS,
            event: 'create'
          });
          let currentUrl = this.router.url;
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate([currentUrl]);
        },
        (err) => {
          reject(err);
        }

      );;
    });
  }

  error() {
    this.snackBar.open('Usuario o Contraseña no válidos', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }

  onSelectIdent(id: any): void {
    this.idIdent = id;
  }
  getAllIdentifiers() {
    this.identService.getAll().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const habilitit = list.filter((item) => {
        if (item.is_active === true) {
          return true;
        } else {
          return false;
        }
      });
      this.listIdentifier = habilitit;
    });
  }
  disabledAccount( openDialog = true) {
    const dialogRef = this.dialog.open(DisabledAccount, {
      width: '500px',
      data:{
      }
    }).componentInstance;
  }
}
