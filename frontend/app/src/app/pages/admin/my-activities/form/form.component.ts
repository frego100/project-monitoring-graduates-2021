import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ActivityService, UserService } from '@services';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Subscription, of, Observable, BehaviorSubject } from 'rxjs';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { TypeActivity } from '@app/models/activity';
@Component({
  selector: 'app-activities-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class MyActivityFormComponent implements OnInit {
  @ViewChild('selectTarget', { static: false }) selectTarget: any;
  imageSrc: string = '';
  activityForm: FormGroup;
  typeActivity: TypeActivity;
  editor = ClassicEditor;
  mediaType = 'image';
  editorConfig = {
    toolbar: [
      'bold',
      'italic',
      '|',
      'bulletedList',
      'numberedList',
      '|',
      'link',
    ],
  };
  activitySubscription: Subscription = new Subscription();
  routeParamsSubscription: Subscription = new Subscription();
  activity: any;
  searchCtrl: any;
  idTemp:number;
  selectedUsers = [];
  showBeforeImage: boolean = false;
  public activity_types: TypeActivity [] = [];

  constructor(
    private formBuilder: FormBuilder,
    private activityService: ActivityService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private userService: UserService,
    public authUserService: AuthUserService
  ) {
    this.activityForm = this.formBuilder.group({
      id: [null],
      title: ['', Validators.required],
      description: [''],
      start_date: [null],
      end_date: [null],
      link: [''],
      image: [null],
      stream_link: [''],
      meet_link:[''],
      can_register:[''],
      is_validated: [false],
      status: [true],
      activity_type: [null],
    });

  }

  ngOnInit(): void {
    this.getAllKindActivity();
    this.route.params.subscribe((params) => {
      if (params.activity && params.activity !== 'nuevo') {
        this.getActivityById();
        this.getActivityByPathId(params.activity);

      } else {
        this.showBeforeImage = false;
      }
    });
  }
  private getAllKindActivity(){
    this.activityService.getAllKindActivity().subscribe((res:any) => {
      this.activity_types = res
    });
  }

  private getActivityById() {
    this.activityService.activityControl.subscribe((res: any) => {
      this.activitySubscription.unsubscribe();
      this.routeParamsSubscription.unsubscribe();
      this.activityForm.reset();

      this.activitySubscription = this.getSelectedItem(res).subscribe(
        (activity: any) => {
          if (activity.start_date || activity.end_date) {
            activity.start_date = new Date(activity.start_date)
              .toISOString()
              .substring(0, 19);

            activity.end_date = new Date(activity.end_date)
              .toISOString()
              .substring(0, 19);
          }
          activity.activity_type = activity.activity_type.id;
          this.activityForm.patchValue(activity);
          this.activityService.formControl.next({
            data: activity,
            event: 'select',
          });

        }
      );
    });
  }

  private getActivityByPathId(id) {
    this.activityService.getById(id).subscribe((res) => {
    
    //  let aux= res.activity_type;
     
    //   this.activityForm.patchValue({
    //       activity_type:aux.id
    //   });
      this.activity = Object.assign({}, this.activity, res);
      if (!IsNullUndefined(this.activity.image)) {
        this.showBeforeImage = true;
      } else {
        this.showBeforeImage = false;
      }
    });
  }

  private getSelectedItem(res: any) {
    if (res) {
      return of(res);
    }

    return new Observable((observer) => {
      this.routeParamsSubscription = this.route.params.subscribe((params) => {
        
        if (Object.keys(params).length === 0) {
          observer.next({});
        } else {
          this.activityService
            .getById(params.activity)
            .subscribe((res) => observer.next(res));
           
        }
      });
    });
  }

  cancel() {
    history.back();
  }
  onSelect(id: any): void {

    //this.programForm=program;

    this.idTemp = id;
  }
  update(_id: any, activity: any) {
   
    return new Promise((resolve, reject) => {
      this.activityService.updateMyActivity(_id, activity).subscribe(
        (res) => {
      
          resolve({
            data: res,
            event: 'update',
          });
        },
        (err) => {
          this.allErrors(err)
          reject(err);
        }
      );
    });
  }

  create(activity: any) {
    
    return new Promise((resolve, reject) => {
      this.activityService.create(activity).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
        },
        (err) => {
          this.allErrors(err)
          reject(err);
        }
      );
    });
  }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

  save() {
    
    const activity = this.activityForm.value;
    const send = activity.id
      ? this.update(activity.id, activity)
      : this.create(activity);

    send
      .then((res: any) => {
      this.snackBar.open('Guardado correctamente!', 'Cerrar', {
        duration: 3000,
      });

      this.activityService.formControl.next(res);

      this.router.navigate(['..'], {
        relativeTo: this.route,
      });

      if (this.selectedUsers.length > 0) {
        this.notify({
          emails: this.selectedUsers,
          activity: res.data.id,
        }).subscribe();
      }
    })
    .catch((err) => {
      this.allErrors(err)
    });

  }

  notify(activity) {
    return this.activityService.notify(activity);
  }

  remove() {}

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = (event.target as HTMLInputElement).files[0];
      this.activityForm.patchValue({
        image: file,
      });
      this.activityForm.get('image').updateValueAndValidity();
    }
  }

  removeAction() {
    const activity = this.activityForm.value;

    this.activityService.remove(activity.id).subscribe(
      () => {
        this.activityService.activityControl = new BehaviorSubject(null);

        this.activityService.formControl.next({
          data: activity.id,
          event: 'remove',
        });

        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });

        this.router.navigate(['..'], { relativeTo: this.route });
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }

  select(openDialog = true) {
    const columns = [
      { name: 'pk', title: 'id', show: false },
      { name: 'first_name', title: 'Nombre', show: true },
      { name: 'email', title: 'Correo', show: true },
    ];

    this.selectTarget
      .config(
        {
          title: 'Seleccione Destinatarios',
          textSearch: this.searchCtrl,
          checkbox: true,
          sticky: true,
          columns,
          selected: {
            users: this.selectedUsers,
          },
          dataSource: this.userService.getAll(),
        },
        openDialog
      )
      .afterSelected((items: any) => {
        if (!items || items.length === 0) {
          return;
        }

        this.selectedUsers = items.users.map((a) => a.email);
        this.selectTarget.dismissModal();
      });
  }
}
