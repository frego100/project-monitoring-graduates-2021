import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Activity } from '@app/models/activity';
import { ActivityService } from '@services';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Enabled } from '@app/models/enabled';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ActivityListComponent implements OnInit {
  selectedItem: any;
  searchCtrl: any;
  loading: boolean = false;
  public selectHabilitado: Enabled = { id: 0, name: "" };
  public habilitado: Enabled[] = [
    {
      id: 1,
      name: "Habilitados"
    },
    {
      id: 2,
      name: "Deshabilitados"
    },
    {
      id: 3,
      name: "Todos"
    }
  ]
  public listActivitys: Activity[] = [];
  displayedColumns: string[] = [
    //  'nro',
    'title',
    'startDate',
    'endDate',
    'validated',
    'actions',
  ];
  dataSource!: MatTableDataSource<Activity>;
  search = '';
  pageSize = 10;
  currentPage = 0;
  totalSize = 0;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activityService: ActivityService,
    private snackBar: MatSnackBar,
    public authUserService: AuthUserService,
  ) { }


  /**
  * This event happens when the user changes the page or changes the page size,
  * so we update the values of the current page and the page size.
  * Finally we ask the server for the data again
  *
  * @method handlePage
  * @param e its a event
  */
   handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getAllActivitys();
  }

  ngOnInit(): void {
    this.getAllActivitys();
  }

  onSelect(id: any): void {
    if (id == 1) {
      const list = this.listActivitys;
      const habilitit = list.filter(item => {
        if (item.is_validated === true) {
          return true;
        }
        else {
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if (id == 2) {
      const list = this.listActivitys;
      const habilitit = list.filter(item => {
        if (item.is_validated === false) {
          return true;
        }
        else {
          return false;
        }
      })
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    if (id == 3) {
      const list = this.listActivitys;
      const habilitit = list;
      this.dataSource = new MatTableDataSource(habilitit);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  }

  ngAfterViewInit() {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  ngOnDestroy(): void {
    this.activityService.activityControl = new BehaviorSubject(null);
  }

  getAllActivitys() {
    this.loading = true;
    this.activityService.getMyActivities(
      this.pageSize,
      this.currentPage * this.pageSize,
      true,
      this.search
    ).subscribe((activities: any) => {
      this.listActivitys = activities.results;
      this.dataSource = new MatTableDataSource(activities.results);
      this.totalSize = activities.count;
      this.loading = false;
    });
  }

  edit(activity: any) {
    this.router.navigate(['../mis-actividades',activity.id], {
      relativeTo: this.route
    });
  }

  applyFilter(key: any) {
    var keycode = key.keyCode || key.which;
    if (keycode == 13) {
      this.search = (key.target as HTMLInputElement).value;
      this.getAllActivitys();
      this.paginator.firstPage();
    }
    // const filterValue = (event.target as HTMLInputElement).value;
    // this.datasoruce.filter = filterValue.trim().toLowerCase();
  }
}
