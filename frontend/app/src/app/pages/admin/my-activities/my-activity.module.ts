import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityListComponent } from './list/list.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SelectTargetModule } from '@app/shared/components/select-target/module';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
import { MyActivityFormComponent } from './form/form.component';


export const MyActivityRoutes: Routes = [
   {
      path: 'mis-actividades',
      children: [
         {
            path: '',
            component: ActivityListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_activity']},

         },
         {
            path: 'nuevo',
            component: MyActivityFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['add_activity']},
         },
         {
            path: ':activity',
            component: MyActivityFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_activity']},   
         }
      ]
   }
];

@NgModule({
   declarations: [ ActivityListComponent,MyActivityFormComponent],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      PipesModule,
      CKEditorModule,
      SelectTargetModule
   ],
   entryComponents: []
})
export class MyActivityModule {}
