import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {BehaviorSubject, Subscription} from 'rxjs';
import { Choice } from '@app/models/item-choice';
import { Identifier } from '@app/models/identifier';
import { IdentifierService } from '@app/services/identifier/identifier.service';
import { IsNullUndefined } from '@app/shared/helpers/general-utils';

@Component({
   selector: 'app-users-form',
   templateUrl: './form.component.html',
   styleUrls: ['./form.component.scss']
})

export class IdentifierFormComponent implements OnInit {

   listAlphabet: Choice[] = [
      { value: 'NUM', text: 'NUMÉRICO' },
      { value: 'ALN', text: 'ALFANUMÉRICO' },
   ];
   modeEdit: boolean = false;
   selectedValue: string;
   currentIdentifier: Identifier;
   identifierForm: FormGroup;
   userSubscription: Subscription = new Subscription();
   routeParamsSubscription: Subscription = new Subscription();

   constructor(
      private formBuilder: FormBuilder,
      private router: Router,
      private identifierService: IdentifierService,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar,
   ) {
      this.identifierForm = this.formBuilder.group({
         id: [null],
         code: ['', Validators.required],
         max_length: ['', Validators.required],
         name: ['', Validators.required],
         alphabet: ['NUM', Validators.required],
         is_active: [true]
      });
   }

   ngOnInit(): void {
      this.modeEdit = false;
      this.getIdentifierByRoute();
   }

   private getIdentifierByRoute () {
      this.route.params.subscribe((params) => {
         if (!IsNullUndefined(params.id)) {
            this.getIdentifierByService(params.id)
            this.modeEdit = true;
         }
      });
   }

   private getIdentifierByService (id :number) {
      this.identifierService.get(id).subscribe((res) => {
         this.identifierForm.patchValue(Object.assign({},this.currentIdentifier, res))
      },()=>{
         this.cancel();
      });
   }

   cancel() {
      history.back();
   }

   private update() {
      this.identifierService.put(this.currentIdentifier.id, this.currentIdentifier).subscribe(() => {
         this.showSnackBar('Editado correctamente!');
         this.router.navigate(['..'], { relativeTo: this.route });
      }, (err) => {
         this.allErrors(err)
      });
   }

   private create() {
      this.identifierService.post(this.currentIdentifier).subscribe(() => {
         this.showSnackBar('Guardado correctamente!');
         this.router.navigate(['..'], { relativeTo: this.route });
      }, (err) => {
         this.allErrors(err)
      });
   }
  allErrors(err: any){
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for(let i = 0; i < keys.length; i++){
      if(keys[i] === "name")
        keys[i] = "Nombre"
      if(keys[i] === "code")
        keys[i] = "Código"
      if(keys[i] === "max_length")
        keys[i] = "Longitud Máxima"
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
    });
  }

   save() {
      this.currentIdentifier = this.identifierForm.value;
      this.currentIdentifier.id ? this.update() : this.create();
   }

   private showSnackBar(text) {
      this.snackBar.open(text, 'Cerrar', { duration: 3000 });
   }

  removeAction() {
    const manual = this.identifierForm.value;
    this.identifierService.delete(manual.id).subscribe(
      () => {
        this.snackBar.open('Eliminado con éxito!', 'Cerrar', {
          duration: 3000,
        });
        this.router.navigate(['..'], { relativeTo: this.route });
      },
      (err) => {
        this.snackBar.open('Ops...algo no salió bien!', 'Cerrar', {
          duration: 3000,
        });
      }
    );
  }

}
