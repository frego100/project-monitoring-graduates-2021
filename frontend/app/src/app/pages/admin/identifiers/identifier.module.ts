import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdentifierFormComponent } from './form/form.component';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { SelectTargetModule } from '@app/shared/components/select-target/module';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
import { IdentifierListComponent } from './list/list.component';


export const IdentifierRoutes: Routes = [
   {
      path: 'identificadores',
      children: [
         {
            path: '',
            component: IdentifierListComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['view_identificator']},

         },
         {
            path: 'nuevo',
            component: IdentifierFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['add_identificator']},
         },
         {
            path: ':id',
            component: IdentifierFormComponent,
            canActivate: [CanActivateAuthGuard],
            data: {permission: ['change_identificator']},   
         }
      ]
   }
];

@NgModule({
   declarations: [ IdentifierListComponent, IdentifierFormComponent ],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      PipesModule,
      SelectTargetModule
   ],
   entryComponents: []
})
export class IdentifierModule {}
