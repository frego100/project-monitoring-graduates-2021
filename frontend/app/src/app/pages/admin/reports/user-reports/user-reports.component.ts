import { Component, OnInit } from '@angular/core';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { FormControl } from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import { DateAdapter } from '@angular/material/core';
import {formateMyDate} from "@shared/helpers/general-utils";
import {ProgramService, ReportService} from "@services";
import { IsNullUndefinedBlank } from '@app/shared/helpers/general-utils';

@Component({
  selector: 'app-user-reports',
  templateUrl: './user-reports.component.html',
  styleUrls: ['./user-reports.component.scss'],
})
export class UserReportsComponent implements OnInit {
  columnsGraduate: any = {
    name: 'Seleccionar todos',
    completed: false,
    color: 'primary',
    subField: [
      {name: 'Fecha de Registro', value: 'F. Creación', completed: false, color: 'primary'},
      {name: 'Apellidos', value: 'Apellidos',  completed: false, color: 'primary'},
      {name: 'Nombres', value: 'Nombres',  completed: false, color: 'primary'},
      {name: 'DNI', value: 'DNI',  completed: false, color: 'primary'},
      {name: 'CUI', value: 'CUI',  completed: false, color: 'primary'},
      {name: 'Género', value: 'Género',  completed: false, color: 'primary'},
      {name: 'Correo universitario', value: 'C.institucional',  completed: false, color: 'primary'},
      {name: 'Correo alternativo', value: 'C.personal/alternativo',  completed: false, color: 'primary'},
      {name: 'Fecha de Nacimiento', value: 'F.Nacimiento',  completed: false, color: 'primary'},
      {name: 'Celular', value: 'Celular',  completed: false, color: 'primary'},
      {name: 'Dirección', value: 'Dirección',  completed: false, color: 'primary'},
      {name: 'Condición', value: 'Condición',  completed: false, color: 'primary'},
      {name: 'Sede', value: 'Sede',  completed: false, color: 'primary'},
      {name: 'Area', value: 'Area',  completed: false, color: 'primary'},
      {name: 'Facultad', value: 'Facultad',  completed: false, color: 'primary'},
      {name: 'Periodo', value: 'Periodo',  completed: false, color: 'primary'},
      {name: 'Experiencia Laboral', value: 'Experiencia L.',  completed: false, color: 'primary'},
      {name: 'Condición de Trabajo', value: 'Condición de Trabajo',  completed: false, color: 'primary'},
    ],
  };

  columnsCompany: any = {
    name: 'Seleccionar todos',
    completed: false,
    color: 'primary',
    subField: [
      {name: 'Fecha de Registro', value: 'date_joined', completed: false, color: 'primary'},
      {name: 'Nombre de la empresa', value: 'company_name',  completed: false, color: 'primary'},
      {name: 'Razon Social', value: 'business_name',  completed: false, color: 'primary'},
      {name: 'Rubro', value: 'activity',  completed: false, color: 'primary'},
      {name: 'R.U.C.', value: 'ruc',  completed: false, color: 'primary'},
      {name: 'Nombre de encargado', value: 'user_name',  completed: false, color: 'primary'},
      {name: 'Correo del encargado', value: 'user_email',  completed: false, color: 'primary'},
      {name: 'Telefono de contacto', value: 'user_phone',  completed: false, color: 'primary'},
      {name: 'Documento Identificación del Encargado', value: 'user_document',  completed: false, color: 'primary'},
    ],
  };

  columnsAdmin: any = {
    name: 'Seleccionar todos',
    completed: false,
    color: 'primary',
    subField: [
      {name: 'Fecha de Registro', value: 'F. Creación', completed: false, color: 'primary'},
      {name: 'Area', value: 'Area', completed: false, color: 'primary'},
      {name: 'Facultad', value: 'Facultad', completed: false, color: 'primary'},
      {name: 'Escuela Profesional', value: 'Escuela Profesional', completed: false, color: 'primary'},
      {name: 'Cargo/Puesto', value: 'Cargo/Puesto', completed: false, color: 'primary'},
      {name: 'Docente o Administrativo', value: 'Docente o Administrativo', completed: false, color: 'primary'},
      {name: 'Apellidos y nombres', value: 'Apellidos y nombres', completed: false, color: 'primary'},
      {name: 'DNI', value: 'DNI', completed: false, color: 'primary'},
      {name: 'Email', value: 'Email', completed: false, color: 'primary'},
      {name: 'Celular', value: 'Celular', completed: false, color: 'primary'},
      {name: 'Nº de usuarios egresados', value: 'Nº de usuarios egresados', completed: false, color: 'primary'},
      {name: 'Nº de usuarios alumnos', value: 'Nº de usuarios alumnos', completed: false, color: 'primary'},
      {name: 'Nº de convocatorias', value: 'Nº de convocatorias', completed: false, color: 'primary'},
      {name: 'Nº de actividades', value: 'Nº de actividades', completed: false, color: 'primary'},
    ],
  };

  columnsSuperAdmin: any = {
    name: 'Seleccionar todos',
    completed: false,
    color: 'primary',
    subField: [
      {name: 'Fecha de Registro', value: 'F. Creación', completed: false, color: 'primary'},
      {name: 'Oficina/Programa', value: 'Oficina/Programa', completed: false, color: 'primary'},
      {name: 'Cargo/Puesto', value: 'Cargo/Puesto', completed: false, color: 'primary'},
      {name: 'Apellidos y nombres', value: 'Apellidos y nombres', completed: false, color: 'primary'},
      {name: 'DNI', value: 'DNI', completed: false, color: 'primary'},
      {name: 'Email', value: 'Email', completed: false, color: 'primary'},
      {name: 'Celular', value: 'Celular', completed: false, color: 'primary'},
      {name: 'Nº de usuarios egresados', value: 'Nº de usuarios egresados', completed: false, color: 'primary'},
      {name: 'Nº de usuarios alumnos', value: 'Nº de usuarios alumnos', completed: false, color: 'primary'},
      {name: 'Nº de convocatorias', value: 'Nº de convocatorias', completed: false, color: 'primary'},
      {name: 'Nº de actividades', value: 'Nº de actividades', completed: false, color: 'primary'},
      {name: 'Nº de usuarios empresas', value: 'Nº de usuarios empresas', completed: false, color: 'primary'},
    ],
  };


  dateGraduateStart = new FormControl();
  dateGraduateEnd = new FormControl();
  dateCompanyStart = new FormControl();
  dateCompanyEnd = new FormControl();
  dateAdminStart = new FormControl();
  dateAdminEnd = new FormControl();
  dateSuperAdminStart = new FormControl();
  dateSuperAdminEnd = new FormControl();
  loading: boolean = false;
  allCompleteGraduate: boolean = false;
  allCompleteCompany: boolean = false;
  allCompleteAdmin: boolean = false;
  allCompleteSuperAdmin: boolean = false;
  instances = []
  selectInstances = new FormControl();

  constructor(
    public authUserService: AuthUserService,
    private snackBar: MatSnackBar,
    private dateAdapter: DateAdapter<Date>,
    private reportService: ReportService,
    private programService: ProgramService
  ) {
    this.dateAdapter.setLocale('en-GB');
  }

  ngOnInit(): void {
    this.programService.getLite(null,null).subscribe((data)=>{
      this.instances = data;
    })
  }

  updateAllCompleteCompany() {
    this.allCompleteCompany = this.columnsCompany.subField != null && this.columnsCompany.subField.every(t => t.completed);
  }

  someCompleteCompany(): boolean {
    if (this.columnsCompany.subField == null) {
      return false;
    }
    return this.columnsCompany.subField.filter(t => t.completed).length > 0 && !this.allCompleteCompany;
  }

  setAllCompany(completed: boolean) {
    this.allCompleteCompany = completed;
    this.columnsCompany.subField.forEach(t => (t.completed = completed));
  }

  generateReportCompany(): void{
    if(!this.validateDates(this.dateCompanyStart.value,this.dateCompanyEnd.value)){
      return
    }
    let columns = this.validGenerateColumns(this.columnsCompany);
    if (columns === null) {
      return;
    }
    let dataRequest = {
      "start_date": formateMyDate(this.dateCompanyStart.value)  + " 00:00:00",
      "start_end": formateMyDate(this.dateCompanyEnd.value)  + " 23:59:59",
      "columns": columns
    }
    this.loading = true;
    this.reportService.getReportCompany(dataRequest).subscribe((data) => {
      this.generateFile(data, "ReporteEmpresas");
      this.loading = false;
    },() => {
      this.snackBar.open('Encontramos un error al descargar el archivo', 'Cerrar', {
        duration: 3000,
      });
      this.loading = false;
    });
  }

  updateAllCompleteGraduate() {
    this.allCompleteGraduate = this.columnsGraduate.subField != null && this.columnsGraduate.subField.every(t => t.completed);
  }

  someCompleteGraduate(): boolean {
    if (this.columnsGraduate.subField == null) {
      return false;
    }
    return this.columnsGraduate.subField.filter(t => t.completed).length > 0 && !this.allCompleteGraduate;
  }

  setAllGraduate(completed: boolean) {
    this.allCompleteGraduate = completed;
    this.columnsGraduate.subField.forEach(t => (t.completed = completed));
  }

  generateReportGraduate(): void{
    if(IsNullUndefinedBlank(this.selectInstances.value)){
      this.snackBar.open('Seleccione al menos una escuela profesional', 'Cerrar', {
        duration: 3000,
      });
      return
    }
    if(!this.validateDates(this.dateGraduateStart.value,this.dateGraduateEnd.value)){
      return
    }
    let columns = this.validGenerateColumns(this.columnsGraduate);
    if (columns === null) {
      return;
    }
    let dataRequest = {
      "start_date": formateMyDate(this.dateGraduateStart.value),
      "start_end": formateMyDate(this.dateGraduateEnd.value),
      "columns": columns,
      "professional_program":this.selectInstances.value,
    }
    this.loading = true;
    this.reportService.getReportGraduate(dataRequest).subscribe((data) => {
      this.generateFile(data, "ReporteEgresados");
      this.loading = false;
    },() => {
      this.snackBar.open('Encontramos un error al descargar el archivo', 'Cerrar', {
        duration: 3000,
      });
      this.loading = false;
    });
  }

  updateAllCompleteAdmin() {
    this.allCompleteAdmin = this.columnsAdmin.subField != null && this.columnsAdmin.subField.every(t => t.completed);
  }

  someCompleteAdmin(): boolean {
    if (this.columnsAdmin.subField == null) {
      return false;
    }
    return this.columnsAdmin.subField.filter(t => t.completed).length > 0 && !this.allCompleteAdmin;
  }

  setAllAdmin(completed: boolean) {
    this.allCompleteAdmin = completed;
    this.columnsAdmin.subField.forEach(t => (t.completed = completed));
  }

  generateReportAdmin(): void{
    if(!this.validateDates(this.dateAdminStart.value,this.dateAdminEnd.value)){
      return
    }
    let columns = this.validGenerateColumns(this.columnsAdmin);
    if(columns === null){
      return;
    }
    let dataRequest = {
      "start_date": formateMyDate(this.dateGraduateStart.value),
      "start_end": formateMyDate(this.dateGraduateEnd.value),
      "columns": columns,
    }
    this.loading = true;
    this.reportService.getReportAdmin(dataRequest).subscribe((data) => {
      this.generateFile(data,"ReporteAdministradores");
      this.loading = false;
    },() => {
      this.snackBar.open('Encontramos un error al descargar el archivo', 'Cerrar', {
        duration: 3000,
      });
      this.loading = false;
    });
  }

  updateAllCompleteSuperAdmin() {
    this.allCompleteSuperAdmin = this.columnsSuperAdmin.subField != null && this.columnsSuperAdmin.subField.every(t => t.completed);
  }

  someCompleteSuperAdmin(): boolean {
    if (this.columnsSuperAdmin.subField == null) {
      return false;
    }
    return this.columnsSuperAdmin.subField.filter(t => t.completed).length > 0 && !this.allCompleteSuperAdmin;
  }

  setAllSuperAdmin(completed: boolean) {
    this.allCompleteSuperAdmin = completed;
    this.columnsSuperAdmin.subField.forEach(t => (t.completed = completed));
  }

  generateReportSuperAdmin(): void{
    if(!this.validateDates(this.dateSuperAdminStart.value,this.dateSuperAdminEnd.value)){
      return
    }
    let columns = this.validGenerateColumns(this.columnsSuperAdmin);
    if (columns === null){
      return;
    }
    let dataRequest = {
      "start_date": formateMyDate(this.dateSuperAdminStart.value),
      "start_end": formateMyDate(this.dateSuperAdminEnd.value),
      "columns": columns,
    }
    this.loading = true;
    this.reportService.getReportSuperAdmin(dataRequest).subscribe((data) => {
      this.generateFile(data,"ReporteSuperAdministradores");
      this.loading = false;
    },() => {
      this.snackBar.open('Encontramos un error al descargar el archivo', 'Cerrar', {
        duration: 3000,
      });
      this.loading = false;
    });
  }

  validateDates(date_start, date_end){
    if(IsNullUndefinedBlank(date_start)){
      this.snackBar.open('Ingrese fecha de inicio', 'Cerrar', {
        duration: 3000,
      });
      return false
    }
    if(IsNullUndefinedBlank(date_end)){
      this.snackBar.open('Ingrese fecha de fin', 'Cerrar', {
        duration: 3000,
      });
      return false
    }
    if(date_end < date_start){
      this.snackBar.open('La fecha de fin debe ser mayor a la fecha de inicio', 'Cerrar', {
        duration: 3000,
      });
      return false
    }
    return true
  }

  generateFile(data, title){
    const downloadUrl = window.URL.createObjectURL(new Blob([data]));
    const link = document.createElement("a");
    link.href = downloadUrl;
    let now = new Date();
    let dateFile = formateMyDate(now);
    let timeFile = now.getHours() + now.getMinutes();
    link.setAttribute("download", `${title}_${dateFile}${timeFile}.xlsx`);
    document.body.appendChild(link);
    link.click();
    link.remove();
    this.snackBar.open('El reporte se  genero exitosamente... se descargara inmediatamente', 'Cerrar', {
      duration: 3000,
    });
  }

  validGenerateColumns(columnsView){
    let columns = []
    for (let i = 0; i<columnsView.subField.length;i++){
      if (columnsView.subField[i].completed){
        columns.push(columnsView.subField[i].value)
      }
    }
    if (columns.length==0){
      this.snackBar.open('Se debe seleccionar al menos una columna', 'Cerrar', {
        duration: 3000,
      });
      return null;
    }
    return columns;
  }
}
