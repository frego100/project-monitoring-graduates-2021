import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CanActivateAuthGuard } from '../../../shared/guards/check-permission-guard';
import {UserReportsComponent} from "@pages/admin/reports/user-reports/user-reports.component";

export const ReportRoutes: Routes = [
  {
    path: 'reportes',
    children: [
      {
        path: '',
        component: UserReportsComponent,
        canActivate: [CanActivateAuthGuard],
        data: {permission: ['view_user']},
      },
    ]
  }

];

@NgModule({
  declarations: [ UserReportsComponent ],
  imports: [
    RouterModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AppCommonModule,
    BrowserAnimationsModule,
    PipesModule
  ],
  entryComponents: [],
})
export class ReportsModule {}
