import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '@app/services/auth/auth.service';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import {ClientService} from "@app/services/client/client.service";

@Component({
  selector: 'app-login',
  templateUrl: './forgot_password.component.html',
  styleUrls: ['./forgot_password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  loading:boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authSvc: AuthService,
    private snackBar: MatSnackBar,
    private clientService: ClientService) {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['',Validators.required],
      redirect_to: [''],
    });
  }

  ngOnInit(): void {
  }
  back(){
    this.router.navigate(['/login']);
 }
  forgotPassword() {
    this.loading = true;
    const formValue = this.forgotPasswordForm.value;
    formValue.redirect_to = window.location.origin + "/reset_password/"
    this.clientService.forgotPassword(formValue).subscribe(
      (res) => {
      this.loading = false;
      this.snackBar.open("Se envió a tu correo electrónico un correo de confirmación", 'cerrar', {
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
      this.loading = false;
    }, (error) => {
      this.snackBar.open("No se realizar la solicitud de cambio de contraseña", 'cerrar', {
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
      this.loading = false;
    })

  }
}
