import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMaterialModule } from '@app/app.material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { AppCommonModule } from '@app/app-common.module';
import {LoginComponent} from "@pages/client/login/login.component";
import {HomeComponent} from "@pages/client/home/home.component";
import {ConvocatoriesComponent} from "@pages/client/convocatories/convocatories.component";
export const LoginRoutes: Routes = [
   {
      path: 'login',
      component: LoginComponent,
      children: [
         {
            path: 'nuevo',
            component: LoginComponent
         }

      ]
   }
];

@NgModule({
   declarations: [ LoginComponent, HomeComponent, ConvocatoriesComponent],
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      AppCommonModule,
      PipesModule
   ],
   entryComponents: []
})
export class LoginModule {}

