import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '@app/services/auth/auth.service';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading:boolean = false;

  constructor(
    private router: Router,
    private authSvc: AuthService,
    private snackBar: MatSnackBar,
    private authUserSvc: AuthUserService) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
  }
  back(){
    this.router.navigate(['/home']);
 }
  onLogin() {
    this.loading = true;
    const formValue = this.loginForm.value;
    this.authSvc.login(formValue).subscribe(
      (res) => {
      if (res) {
        this.authUserSvc.getUserVerify().subscribe( (response)=>{
          this.authUserSvc.setUser(response);
          this.router.navigate(['/admin/home']);
        })
      }
      this.loading = false;
    }, (error) => {
      this.snackBar.open("No se puede iniciar sesión con las credenciales proporcionadas", 'cerrar', {
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
      this.loading = false;
    })

  }
}
