import { Component, OnInit } from '@angular/core';
import {ActivityClientService, ActivityService, AuthService, AuthUserService, PageConfigService} from '@services';
import { Activity } from '@app/models/activity';
import { MatSnackBar } from '@angular/material/snack-bar';
import {PageConfig} from "@models/page-config";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listActivity:Activity[] = [];
  configPage: PageConfig;

  constructor(
    private activityClientService: ActivityClientService,
    public authService: AuthService,
    public authUserService: AuthUserService,
    private activityService: ActivityService,
    private snackBar: MatSnackBar,
    public pageConfigService: PageConfigService
  ) {}

  ngOnInit(): void {
    this.getConfig();
    this.getAllActivitys();
  }

  getConfig(){
    this.pageConfigService.getPublic().subscribe((response)=>{
      this.configPage = response
    })
  }

  getAllActivitys() {
    this.activityClientService.getActivities().subscribe((activities: any) => {
      const array = activities.results;
      array.sort((val1, val2)=> {
        return new Date(val1.start_date).getDay() - new Date(val2.start_date).getDay()
      })
      this.listActivity=array;
    });
  }

  enroll(activity: any): void {
    this.activityService.enroll(activity.id).subscribe(
      () => {
        this.snackBar.open('Inscrito satisfactoriamente!', 'Cerrar', {
          duration: 3000,
        });
      },
      (err) => {
        if (err.error.message) {
          this.snackBar.open(err.error.message, 'Cerrar', {
            duration: 3000,
          });
        } else {
          this.snackBar.open('No fue posible su inscripción!', 'Cerrar', {
            duration: 3000,
          });
        }
      }
    );
  }

}
