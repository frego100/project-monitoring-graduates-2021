import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '@app/services/auth/auth.service';
import {ClientService} from "@app/services/client/client.service";

@Component({
  selector: 'app-login',
  templateUrl: './reset_password.component.html',
  styleUrls: ['./reset_password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  loading:boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authSvc: AuthService,
    private snackBar: MatSnackBar,
    private clientService: ClientService) {
    this.resetPasswordForm = this.formBuilder.group({
      password: ['',Validators.required],
    });
  }

  ngOnInit(): void {
  }
  back(){
    this.router.navigate(['/login']);
  }
  resetPassword() {
    this.loading = true;
    const formValue = this.resetPasswordForm.value;
    let url = window.location.pathname
    let uuid = url.substr(16)

    this.clientService.resetPassword(formValue, uuid).subscribe(
      (res) => {
        this.snackBar.open("Contraseña cambiada correctamente", 'cerrar', {
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'bottom'
        });
        this.loading = false;
        this.router.navigate(['/login']);
      }, (error) => {
        this.snackBar.open("No se pudo cambiar la contraseña", 'cerrar', {
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'bottom',
        });
        this.loading = false;
      })
  }
}
