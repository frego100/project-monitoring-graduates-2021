import { Component, OnInit } from '@angular/core';
import { ManualsService } from '@services';
import { MatDialog } from '@angular/material/dialog';
import { DetailManual } from '@shared/components/detail-manual/detail-manual';

@Component({
  selector: 'app-activities',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
})
export class HelpComponent implements OnInit {
  gridColumns = 3;
  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }
  public displayManuals: Array<any> = [];

  constructor(
    private manualService: ManualsService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getAllManual();
  }

  getAllManual() {
    this.manualService.getPublics().subscribe(
      (res: any) => {
        let activos = res.results.filter((x) => x.is_active === true);
        this.displayManuals = activos;
      },
      (err) => {}
    );
  }

  openDialog(man: any): void {
    const dialogRef = this.dialog.open(DetailManual, {
      width: '60%',
      height: 'auto',
      data: {
        name: man.name,
        description: man.description,
        link: man.link,
        file: man.file,
        is_active: man.is_active,
      },
    });
  }
}
