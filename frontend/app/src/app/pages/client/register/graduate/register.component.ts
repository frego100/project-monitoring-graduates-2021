import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { IdentifierService, UserService, JobsService, TypeEmployeeService } from '@services';
import { Program } from '@app/models/program';
import { Jobs } from '@app/models/job';
import { Employee } from '@app/models/employee';
import { Identifier } from '@app/models/identifier';
import { ProfileService } from '@services';
import { ClientService } from '@app/services/client/client.service';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterGraduateComponent {
  // @Input(‘childToMaster’) userRegister: string;
  registerGraduateForm: FormGroup;
  registerPreUser: FormGroup;
  public listProgram: Program[] = [];
  public listJobs: Jobs[] = [];
  public listEmployee: Employee[] = [];
  public listIdentifier: Identifier[] = [];
  public dcpList: any[] = [
    {
      id: 1,
      name: "Si",
      value: true,
      checked: false,
    },
    {
      id: 0,
      name: "No",
      value: false,
      checked: false,
    }
  ]
  idIdent: number;
  idJob: number;
  idEmpl: number;
  idProgr: number;
  idUser: number;
  isDCP: number;
  limitDoc: number;
  typeDoc: string;
  typeDocTemp: string;
  errorDocument: boolean;
  limitDocument: number;
  limitDocumentCompany: number;
  documentError: string;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private clientService: ClientService,
    private snackBar: MatSnackBar) {
    this.registerGraduateForm = this.formBuilder.group({
      user: [''],
      identificator: [''],
      document: ['', Validators.required],
      cell_phone: ['', Validators.compose([Validators.min(111111111), Validators.max(999999999), Validators.required])],
      cui: ['', Validators.compose([Validators.min(11111111), Validators.max(99999999), Validators.required])],
      date_birthday: ['', Validators.required],
      is_dcp: [''],
      description_dcp: [''],
    });
  }

  ngOnInit(): void {
    this.getAllComplements();
    this.registerPreUser = this.clientService.getUserPreRegister();
    this.documentErr();
    if (this.isDCP == 0) {

      this.registerGraduateForm.controls['description_dcp'].disable();
      this.registerGraduateForm.patchValue({
        description_dcp: null,
      });
    }
  }
  register() {
    
    this.validacion();
  }
  validacion() {
    var valid = false;
    if (this.isDCP == 0) {

      this.registerGraduateForm.controls['description_dcp'].enable();
      this.registerGraduateForm.patchValue({
        description_dcp: null,
      });
    }
    const dcpValue = this.dcpList.filter(item => {

      if (item.id === this.isDCP) {
        return true;
      }
      else {
        return false;
      }
    });

    this.registerGraduateForm.patchValue({
      is_dcp: dcpValue[0].value
    });
    let data = {
      type: "graduateprofile",
      data: this.registerGraduateForm.value
    };
    
    this.clientService.validateData(data).subscribe(res => {
      if (res.message == "Datos Correctos") {
           this.addPreUser();
      } else {
         valid = false;
      }
    },
      (err) => {
        this.allErrors(err);
      });

    return valid;
  }

  radioButton(event: any) {
    if (event.value === 1) {
      this.isDCP = 1;
      this.registerGraduateForm.controls['description_dcp'].enable();
      this.registerGraduateForm.patchValue({
        is_dcp: true,
      });

    } else if (event.value === 0) {
      this.isDCP = 0;
      this.registerGraduateForm.patchValue({
        description_dcp: null,
      });
      this.registerGraduateForm.controls['description_dcp'].disable();

    } else {
      this.registerGraduateForm.patchValue({
        description_dcp: null,
      });
      this.registerGraduateForm.controls['description_dcp'].disable();

    }
  }
  documentErr() {
    //this.errorDocument=true;
    this.registerGraduateForm.valueChanges.subscribe(value => {
      if (value.document.length == this.limitDocument) {
        this.errorDocument = false;
        //this.documentError="El limite es "+this.limitDocument;
      }
      else {
        this.errorDocument = true;
        this.documentError = "El limite debe ser de " + this.limitDocument + " caracteres";
      }

    });
  }
  addPreUser() {

    this.clientService.preRegisterUser(this.registerPreUser.value).subscribe(res => {
      this.idUser = res.id;
      this.registerGraduateForm.patchValue({
        user: this.idUser,
        identificator: this.idIdent,
      });

      const formValue = this.registerGraduateForm.value;

      this.create(formValue);
    },
      (err) => {

        this.allErrors(err)
      }
    );

  }
  back() {
    this.clientService.setUserPreRegisterBack(this.registerPreUser);
    this.router.navigate(['/registrate']);
  }
  allErrors(err: any) {
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for (let i = 0; i < keys.length; i++) {
      if (keys[i] === "username")
        keys[i] = "Error username"
      if (keys[i] === "cui")
        keys[i] = "Error CUI"
      if (keys[i] === "cell_phone")
        keys[i] = "Error celular: "
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
      
    });


  }
  create(graduate: any) {

    return new Promise((resolve, reject) => {
      this.clientService.postGraduate(graduate).subscribe(

        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.snackBar.open('Registrado correctamente!', 'Cerrar', {
            duration: 3000
          });
          this.router.navigate(['/home']);
        },
        (err) => {
          this.allErrors(err)
        }
      );
    });
  }

  onSelectIdent(id: any): void {
    this.idIdent = id;
    this.clientService.getIdentificator().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const aux = list.filter(item => {
        if (item.id == id) {
          return true;
        }
        else {
          return false;
        }
      });

      this.limitDocument = aux[0].max_length;
      this.typeDocTemp = aux[0].alphabet;

      if (this.typeDocTemp === "NUM") {
        this.typeDoc = "number";
      } else if (this.typeDocTemp === "ALN") {
        this.typeDoc = "text";
      }

    });
  }

  getAllComplements() {

    this.clientService.getIdentificator().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listIdentifier = habilitit;
    });

  }
}
