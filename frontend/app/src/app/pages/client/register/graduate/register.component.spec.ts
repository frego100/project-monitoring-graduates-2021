import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterGraduateComponent } from './register.component';

describe('LoginComponent', () => {
  let component: RegisterGraduateComponent;
  let fixture: ComponentFixture<RegisterGraduateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterGraduateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterGraduateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
