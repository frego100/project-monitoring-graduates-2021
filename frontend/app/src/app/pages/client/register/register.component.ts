import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClientService } from '@app/services/client/client.service';
import { UserService } from '@app/services';
import { Roles } from "@app/models/rol";

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  public listRoles: Roles[] = [];
  profile: any;
  idUser: number;
  idTemp: number;
  auxEmail: any;
  email: any;
  isGraduate: boolean;
  errorUser: boolean;
  userError: string;
  valueGraduate: boolean;
  disable: boolean;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private clientService: ClientService,
    private rolesService: ClientService,
    private snackBar: MatSnackBar
  ) {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      username: ['', Validators.required],
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      password: ['', Validators.required],
      group_id: ['']
    });
    this.isGraduate = false;
    this.valueGraduate = false;

  }

  ngOnInit(): void {
    this.returnActive();
    this.getAllRoles();

  }
  back(){
     this.router.navigate(['/login']);
  }
  returnActive() {
    if (typeof this.clientService.getUserPreRegisterBack() == "undefined") {

    }
    else {

      this.registerForm.patchValue({
        email: this.clientService.getUserPreRegisterBack().value.email,
        username: this.clientService.getUserPreRegisterBack().value.username,
        first_name: this.clientService.getUserPreRegisterBack().value.first_name,
        last_name: this.clientService.getUserPreRegisterBack().value.last_name,
        password: this.clientService.getUserPreRegisterBack().value.password,
        group_id: this.clientService.getUserPreRegisterBack().value.group_id
      });
      this.idTemp = this.clientService.getUserPreRegisterBack().value.group_id;

    }
  }
  register() {
    this.clientService.setUserPreRegister(this.registerForm);
    this.auxEmail = this.registerForm.value;
    this.email = this.auxEmail.email.split("@");
    this.registerForm.patchValue({
      group_id: this.idTemp
    });

    if (this.idTemp == null && typeof this.clientService.getUserPreRegisterBack() != "object") {
            this.clientService.preRegisterUser(this.registerForm.value).subscribe((dat) => {
              dat.data = this.registerForm.value;
            });
      this.snackBar.open('Registrado correctamente!', 'Cerrar', {
        duration: 3000
      });
      this.router.navigate(['/home']);
    }
    else {
      this.getProfile(this.idTemp);
    }
  }

  onSelect(id: any): void {
    this.idTemp = id;
    this.rolesService.getRoles().subscribe((roles) => {
      const list = roles.results;
      const rolSelect = list.filter(item => {
        if (item.id === this.idTemp) {
          return true;
        }
        else {
          return false;
        }
      });

      if (rolSelect[0].profile_type.model == "graduateprofile") {
        this.isGraduate = true;
        this.auxEmail = this.registerForm.value;
        this.email = this.auxEmail.email.split("@");
        if (this.email[1] != "unsa.edu.pe" && this.isGraduate) {
          this.valueGraduate = true;
        }
        else {
          this.valueGraduate = false;
        }
      }
      else {
        this.isGraduate = false;
      }
    });
  }

  error() {
    this.snackBar.open('Usuario o Contraseña no válidos', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
  }

  getAllRoles() {
    this.rolesService.getRoles().subscribe((roles) => {
      this.listRoles = roles.results;
      const list = this.listRoles;
      const habilitit = list.filter(item => {
        if (item.state === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listRoles = habilitit;
    });
  }
  getProfile(id: any) {
    this.rolesService.getRoles().subscribe((roles) => {
      const aux = roles.results;

      const list = aux;
      const profile_Select = list.filter(item => {
        if (item.id === id) {

          return true;
        }
        else {
          return false;
        }
      });

      if (profile_Select[0].profile_type == null) {
        /*
        this.userService.preRegisterUser(this.registerForm.value).subscribe((dat) => {
          dat.data = this.registerForm.value;
        });
        this.snackBar.open('Registrado correctamente!', 'Cerrar', {
          duration: 3000
        });*/
        this.router.navigate(['/home']);
      }
      else if (profile_Select[0].profile_type.model == "adminprofile") {
        this.router.navigate(['/registrate/admin']);
      } else if (profile_Select[0].profile_type.model == "companyprofile") {
        this.router.navigate(['/registrate/empresa']);
      }
      else if (profile_Select[0].profile_type.model == "graduateprofile") {
        if (this.email[1] != "unsa.edu.pe") {
          this.snackBar.open('El correo debe ser "@unsa.edu.pe"  !', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.router.navigate(['/registrate/egresado']);
        }
      }

    });
  }
}
