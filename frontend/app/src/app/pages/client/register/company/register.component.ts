import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Roles } from '@app/models/rol';
import { IdentifierService, UserService, ProfileService } from '@services';
import { Identifier } from '@app/models/identifier';
import { Company } from '@app/models/company';
import { ClientService } from '@app/services/client/client.service';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})

export class RegisterCompanyComponent implements OnInit {
  isLinear = false;
  isUpdate = true;
  registerUserCompanyForm: FormGroup;
  registerCompanyForm: FormGroup;
  registerPreUser: FormGroup;
  company: Company;
  public listRoles: Roles[] = [];
  public listIdentifier: Identifier[] = [];
  idUser: number;
  idIdent: number;
  idIdentCompany: number;
  typeDoc:string;
  typeDocTemp:string;
  errorDocument:boolean;
  limitDocument:number;
  limitDocumentCompany:number;
  documentError:string;
  errorDocumentCompany:boolean;
  documentErrorCompany:string;
  errorCellphone:boolean;
  cellphoneError:string;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private clientService: ClientService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar
  ) {
    this.registerCompanyForm = this.formBuilder.group({
      identificator: ['', Validators.required],
      company_name: ['', Validators.required],
      social_reason: ['', Validators.required],
      ruc: ['', Validators.required],
      company_reason: ['', Validators.required],
      company_activity: ['', Validators.required],
      is_active: true,
      logo: [null],
    });
    this.registerUserCompanyForm = this.formBuilder.group({
      user: [''],
      identificator: ['', Validators.required],
      document: ['', Validators.required],
      cell_phone: ['', Validators.required],
      is_active: false,
      company: [''],
    });
  }

  ngOnInit(): void {
    this.registerPreUser = this.clientService.getUserPreRegister();
    this.getAllIdentifiers();
    this.documentErr();
    this.documentErrCompany();
  }

  register() {
    this.registerCompanyForm.patchValue({
      identificator: this.idIdentCompany,
    });
  
    this.registerUserCompanyForm.patchValue({
      user: this.idUser,
      identificator: this.idIdent,
      company: this.registerCompanyForm.value,
    });
    let data = {
      type: "companyprofile",
      data: this.registerUserCompanyForm.value
    };
    this.clientService.validateData(data).subscribe(res => {
      if (res.message == "Datos Correctos") {
            this.addPreUser();
      } else {
        this.snackBar.open('Datos incorrectos!', 'Cerrar', {
          duration: 3000
        });
      }
    },
      (err) => {
        this.allErrors(err);
        });
  
  }
  allErrors(err: any) {
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for (let i = 0; i < keys.length; i++) {
      if (keys[i] === "username")
        keys[i] = "Error username"
      if (keys[i] === "phone")
        keys[i] = "Error teléfono"
      if (keys[i] === "cell_phone")
        keys[i] = "Error celular: "
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
      
    });


  }
  addPreUser(){
   
    const companyVal = this.registerCompanyForm.value;
    this.company = companyVal;
    this.userService
      .preRegisterUser(this.registerPreUser.value)
      .subscribe((res) => {
        this.idUser = res.id;
        this.registerUserCompanyForm.patchValue({
          user: this.idUser,
          identificator: this.idIdent,
          company: this.registerCompanyForm.value,
        });

        const formValue = this.registerUserCompanyForm.value;
        this.create(formValue);
      });
  }
  create(company: any) {
    return new Promise((resolve, reject) => {
      this.clientService.postCompany(company).subscribe(
        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.snackBar.open('Registrado correctamente!', 'Cerrar', {
            duration: 3000
          });
          this.router.navigate(['/home']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
  error() {
    this.snackBar.open('Usuario o Contraseña no válidos', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
  documentErr(){
    //this.errorDocument=true;
    this.registerUserCompanyForm.valueChanges.subscribe(value=>{
      
      if(value.document.length==this.limitDocument){
        this.errorDocument=false;
        //this.documentError="El limite es "+this.limitDocument;
      }    
      else{
       this.errorDocument=true;
        this.documentError="El limite debe ser de "+this.limitDocument+" caracteres";
      }
      if(value.cell_phone>999999999){
        this.errorCellphone=true;
        this.cellphoneError="El celular no debe exceder los 9 dígitos";
      }    
      else if(value.cell_phone<900000000){
        this.errorCellphone=true;
        this.cellphoneError="El celular no debe ser menor a los 9 dígitos y debe iniciar con el digito 9";
      }else{
        this.errorCellphone=false;
      }
    });
  }
  back(){
    this.clientService.setUserPreRegisterBack(this.registerPreUser);
    this.router.navigate(['/registrate']);
  }
  documentErrCompany(){
    //OJO EN IDENTIFICADOR NO ESTA COMO "DOCUMENT" ESTA COMO "ruc"
    this.errorDocumentCompany=false;
    this.registerCompanyForm.valueChanges.subscribe(value=>{
       if(value.ruc.length==this.limitDocumentCompany){
        this.errorDocumentCompany=false;
        //this.documentError="El limite es "+this.limitDocument;
      }    
      else{
       this.errorDocumentCompany=true;
        this.documentErrorCompany="El limite debe ser de "+this.limitDocumentCompany+" caracteres";
      }
    });
  }

  onSelectIdent(id: any): void {
    this.idIdent = id;
    this.clientService.getIdentificator().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const aux = list.filter(item => {
        if (item.id == id) {
          return true;
        }
        else {
          return false;
        }
      });
     
      this.limitDocument=aux[0].max_length;
      this.typeDocTemp=aux[0].alphabet;

      if(this.typeDocTemp==="NUM"){
            this.typeDoc="number";
      }else  if(this.typeDocTemp==="ALN"){
        this.typeDoc="text";
      }
   
    });
  
  }
  onSelectIdentCompany(id: any): void {
    this.idIdentCompany = id;
    this.clientService.getIdentificator().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const aux = list.filter(item => {
        if (item.id == id) {
          return true;
        }
        else {
          return false;
        }
      });
     
      this.limitDocumentCompany=aux[0].max_length;
      this.typeDocTemp=aux[0].alphabet;

      if(this.typeDocTemp==="NUM"){
            this.typeDoc="number";
      }else  if(this.typeDocTemp==="ALN"){
        this.typeDoc="text";
      }
   
    });
  
  }
  getAllIdentifiers() {
    this.clientService.getIdentificator().subscribe((val) => {
    
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const habilitit = list.filter((item) => {
        if (item.is_active === true) {
          return true;
        } else {
          return false;
        }
      });
      this.listIdentifier = habilitit;
    });
  }
}
