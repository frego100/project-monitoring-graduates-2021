import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, NumberValueAccessor } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthUserService } from '@app/services/auth-user/auth-user.service';
import { IdentifierService, UserService, JobsService, TypeEmployeeService, ProgramService } from '@services';
import { Program } from '@app/models/program';
import { Jobs } from '@app/models/job';
import { Employee } from '@app/models/employee';
import { Identifier } from '@app/models/identifier';
import { ProfileService } from '@app/services/profile/profile.service';
import { ClientService } from '@app/services/client/client.service';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterAdminComponent {
  // @Input(‘childToMaster’) userRegister: string;
  registerAdminForm: FormGroup;
  registerPreUser: FormGroup;
  public listProgram: Program[] = [];
  public listJobs: Jobs[] = [];
  public listEmployee: Employee[] = [];
  public listIdentifier: Identifier[] = [];
  idIdent: number;
  idJob: number;
  idEmpl: number;
  idProgr: number;
  typeDoc:string;
  typeDocTemp:string;
  idUser: number;
  limitDocument: number;
  documentError: string;
  errorDocument: boolean;
  limitNumber: string;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private clientService: ClientService,
    private profileService: ProfileService,
    private snackBar: MatSnackBar,
    private authUserSvc: AuthUserService) {
      
    this.registerAdminForm = this.formBuilder.group({
      user: [''],
      identificator: [''],
      job_position: [''],
      type_employee: [''],
      //document: ['',ators.min(Number(this.limitNumber)),Validators.max(Number(this.limitNumber)), Validators.required])],
      document: ['', Validators.required],
      cell_phone: ['', Validators.compose([Validators.min(111111111),Validators.max(999999999), Validators.required])],
      phone: ['', Validators.compose([Validators.min(111111),Validators.max(999999), Validators.required])],
      is_active: true,
      instance: [''],
      validated: false
    });
    
  }

  ngOnInit(): void {
    this.getAllComplements();
    this.registerPreUser = this.clientService.getUserPreRegister();
    this.documentErr();
  }
  register() {
    let data = {
      type: "adminprofile",
      data: this.registerAdminForm.value
    };
    this.clientService.validateData(data).subscribe(res => {
      if (res.message == "Datos Correctos") {
            this.addPreUser();
      } else {
        this.snackBar.open('Datos incorrectos!', 'Cerrar', {
          duration: 3000
        });
      }
    },
      (err) => {
        this.allErrors(err);
        });
  
    }
  allErrors(err: any) {
    let keys = Object.keys(err.error)
    let values = Object.values(err.error)
    let allErrors = ""
    for (let i = 0; i < keys.length; i++) {
      if (keys[i] === "username")
        keys[i] = "Error username"
      if (keys[i] === "cui")
        keys[i] = "Error CUI"
      if (keys[i] === "cell_phone")
        keys[i] = "Error celular: "
      allErrors += keys[i] + ": " + values[i] + "\n"
    }
    this.snackBar.open(allErrors, 'Cerrar', {
      duration: 3000,
      
    });


  }
  documentErr(){
    this.errorDocument=true;
    this.registerAdminForm.valueChanges.subscribe(value=>{
      if(value.document.length==this.limitDocument){
        this.errorDocument=false;
        //this.documentError="El limite es "+this.limitDocument;
      }    
      else{
        this.errorDocument=true;
        this.documentError="El limite debe ser de "+this.limitDocument+" caracteres";
      }
    })
  }
  back(){
    this.clientService.setUserPreRegisterBack(this.registerPreUser);
    this.router.navigate(['/registrate']);
  }
  addPreUser() {

    this.clientService.preRegisterUser(this.registerPreUser.value).subscribe(res => {
      this.idUser = res.id;
      this.registerAdminForm.patchValue({
        user: this.idUser,
        identificator: this.idIdent,
        job_position: this.idJob,
        instance: this.idProgr,
        type_employee: this.idEmpl,
      });

      const formValue = this.registerAdminForm.value;
      this.create(formValue);
    });

  }
  create(admin: any) {

    return new Promise((resolve, reject) => {
      this.clientService.postAdmin(admin).subscribe(

        (res) => {
          resolve({
            data: res,
            event: 'create',
          });
          this.snackBar.open('Registrado correctamente!', 'Cerrar', {
            duration: 3000
          });
          this.router.navigate(['/home']);
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  error() {
    this.snackBar.open('Usuario o Contraseña no válidos', '', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
  }

  onSelectIdent(id: any): void {
    this.idIdent = id;
    this.clientService.getIdentificator().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const aux = list.filter(item => {
        if (item.id == id) {
          return true;
        }
        else {
          return false;
        }
      });
      this.limitDocument=aux[0].max_length;
     
      this.typeDocTemp=aux[0].alphabet;

      if(this.typeDocTemp==="NUM"){
            this.typeDoc="number";
      }else  if(this.typeDocTemp==="ALN"){
        this.typeDoc="text";
      }
    });
 
  
  }

  onSelectJob(id: any): void {
    this.idJob = id;
  }

  onSelectEmpl(id: any): void {
    this.idEmpl = id;
  }

  onSelectProg(id: any): void {
    this.idProgr = id;
  }

  getAllComplements() {
    this.clientService.getJobs().subscribe((val) => {
      this.listJobs = val.results;
      const list = this.listJobs;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listJobs = habilitit;
    });
    this.clientService.getPrograms().subscribe((val) => {
      this.listProgram = val.results;
      const list = this.listProgram;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listProgram = habilitit;
    });
    this.clientService.getIdentificator().subscribe((val) => {
      this.listIdentifier = val.results;
      const list = this.listIdentifier;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listIdentifier = habilitit;
    });
    this.clientService.getTypeEmployee().subscribe((val) => {
      this.listEmployee = val.results;
      const list = this.listEmployee;
      const habilitit = list.filter(item => {
        if (item.is_active === true) {
          return true;
        }
        else {
          return false;
        }
      });
      this.listEmployee = habilitit;
    });
  }
}
