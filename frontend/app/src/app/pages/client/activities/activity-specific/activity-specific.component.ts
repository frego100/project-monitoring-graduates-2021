import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivityService, UserService, AuthUserService, ActivityClientService, AnnouncementService, AuthService } from '@services';

import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-activity-specific',
  templateUrl: './activity-specific.component.html',
  styleUrls: ['./activity-specific.component.scss']
})
export class ActivitySpecificComponent implements OnInit {
  idActivity: any;
  activity: any;
  image:any;
  constructor(private routeActive: ActivatedRoute,
              private activityServices: ActivityService,
              private snackBar: MatSnackBar,
              private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.idActivity=this.routeActive.snapshot.params.especifica;
    
    this.getActivityByPathId(this.idActivity);
  }
  private getActivityByPathId(id) {
    this.activityServices.getById(id).subscribe((res) => {
    
      this.activity = res;
      this.image=res.image;
  },
  (err) => {
    this.snackBar.open("Error de muestra, por favor inicie sesion para ver la actividad", 'Cerrar', {
      duration: 5000,
      
    });
    });
      

  }
  registerA(id):void{
    if(this.authService.getToken()==null){
      this.snackBar.open('Debe iniciar sesión para poder Inscribirse!', 'Cerrar', {
        duration: 4000,
      });
    }else{
      let body={
        announcement:id
      };
        this.activityServices.enroll(this.idActivity).subscribe(()=> {
          this.snackBar.open('Inscripción realizada correctamente!', 'Cerrar', {
            duration: 4000,
          });
          
       }, (err) => {
           this.snackBar.open('Usted ya se inscribió a esta actividad anteriormente!', 'Cerrar', {
            duration: 4000,
          });
       });
    }
  }
 
}
