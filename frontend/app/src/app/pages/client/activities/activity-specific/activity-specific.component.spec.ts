import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitySpecificComponent } from './activity-specific.component';

describe('ActivitySpecificComponent', () => {
  let component: ActivitySpecificComponent;
  let fixture: ComponentFixture<ActivitySpecificComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivitySpecificComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitySpecificComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
