import { Component, OnInit } from '@angular/core';
import {ActivityClientService} from '@app/services/activity-client/activity-client.service';
import {MatDialog} from "@angular/material/dialog";
import {DetailActivity} from "@shared/components/detail-activity/detail-activity";

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {
  gridColumns = 3;
  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }
  public displayActivities: Array<any> = []


  constructor(
    private activityService: ActivityClientService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getActivities();
  }
  getActivities(){
    let activity;
    this.activityService.getActivities().subscribe(
      (res: any) => {
        //res.results = this.displayActivites;
        this.displayActivities = res.results;
      },
      err => {

      }
    );
  }

  getElement(act: any) {
  }
  openDialog(act: any): void {

    const dialogRef = this.dialog.open(DetailActivity, {
      width: '50%',
      data: {
        id:act.id,
        title: act.title,
        description: act.description,
        owner:act.owner,
        start_date: act.start_date,
        end_date: act.end_date,
        link: act.link,
        stream_link: act.stream_link,
        image: act.image,
        can_register: act.can_register
      }
    });
    dialogRef.afterClosed().subscribe(res => {
    })
  }
}
