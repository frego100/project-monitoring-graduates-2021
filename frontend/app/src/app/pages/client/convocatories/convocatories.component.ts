import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AnnouncementClientService } from '@app/services/announcement-client/annoucement-client.service';
import { AnnouncementDetail } from '@app/shared/components/detail-announcement/detail-announcement';

@Component({
  selector: 'app-convocatories',
  templateUrl: './convocatories.component.html',
  styleUrls: ['./convocatories.component.scss']
})
export class ConvocatoriesComponent implements OnInit {

  gridColumns = 3;
  toggleGridColumns() {
    this.gridColumns = this.gridColumns === 3 ? 4 : 3;
  }
  public displayActivities: Array<any> = []


  constructor(
    private announcementService: AnnouncementClientService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getAnnouncement();
   }
  getAnnouncement(){
    let activity;
    this.announcementService.getAnnouncementOrder().subscribe(
      (res: any) => {

        //res.results = this.displayActivites;
        this.displayActivities = res.results;
      },
      err => {

      }
    );
  }

  getElement(act: any) {
  }
  openDialog(act: any): void {

    const dialogRef = this.dialog.open(AnnouncementDetail, {
      width: '50%',
      data: {
        id:act.id,
        name: act.name,
        description: act.description,
        date_start: act.date_start,
        date_end: act.date_end,
        created_by:act.created_by,
        link: act.link,
        company_pk: act.company_pk,
        extra_file: act.extra_file,
        can_register: act.can_register
      }
    });
    dialogRef.afterClosed().subscribe(res => {
    })
  }
}
