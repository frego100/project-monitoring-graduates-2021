import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvocatoriesComponent } from './convocatories.component';

describe('ConvocatoriesComponent', () => {
  let component: ConvocatoriesComponent;
  let fixture: ComponentFixture<ConvocatoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConvocatoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvocatoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
