import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMaterialModule } from '@app/app.material.module';
import { PipesModule } from '@app/shared/pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/app-common.module';
import { PrincipalComponent } from './admin/principal/principal.component';
import { RegisterComponent } from "@pages/client/register/register.component";
import { HomeComponent } from './client/home/home.component';
import { ActivitiesComponent } from './client/activities/activities.component';
import { ConvocatoriesComponent } from './client/convocatories/convocatories.component';
import {HelpComponent} from "@pages/client/help/help.component";
import { RegisterAdminComponent } from './client/register/admin/register.component';
import { RegisterCompanyComponent } from './client/register/company/register.component';
import { RegisterGraduateComponent } from './client/register/graduate/register.component';
import { ActivitySpecificComponent } from './client/activities/activity-specific/activity-specific.component';
import {LoginComponent} from "@pages/client/login/login.component";
import {ForgotPasswordComponent} from "@pages/client/forgot_password/forgot_password.component";
import {ResetPasswordComponent} from "@pages/client/reset_password/reset_password.component";




export const PagesRoutes: Routes = [
   {
     path: 'home',
      component: HomeComponent,

   },
   {
      path: 'actividades',
       component: ActivitiesComponent,
       children: [
         {
           // path: 'especifica/:especifica',
           path: 'especifica',
            component: ActivitySpecificComponent
         }
       ],
    },
    {
      path: 'actividad/:especifica',
      component: ActivitySpecificComponent

    }
    ,
    {
      path: 'convocatorias',
       component: ConvocatoriesComponent,

    },
    {
      path: 'ayuda',
      component: HelpComponent,

    },
];

@NgModule({
   imports: [
      RouterModule,
      AppMaterialModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      PipesModule,
      AppCommonModule
   ],
   declarations: [
     PrincipalComponent,
     LoginComponent,
     ForgotPasswordComponent,
     ResetPasswordComponent,
     RegisterComponent,
     RegisterAdminComponent,
     RegisterGraduateComponent,
     RegisterCompanyComponent,
     HomeComponent,
     ConvocatoriesComponent,
     ActivitySpecificComponent,
     ActivitiesComponent,
     HelpComponent
      ],
})
export class PagesModule {}
