import { Routes } from "@angular/router";
import { RegisterAdminComponent } from "@app/pages/client/register/admin/register.component";
import { RegisterCompanyComponent } from "@app/pages/client/register/company/register.component";
import { RegisterGraduateComponent } from "@app/pages/client/register/graduate/register.component";
import { RegisterComponent } from "@app/pages/client/register/register.component";
import { CheckLoginGuard } from "@app/shared/guards/check-login.guard";
import { AdminNavigationComponent } from "./admin/admin.navigation.component";
import { AdminNavigationRoutes } from "./admin/admin.navigation.module";
import { ClientNavigationComponent } from "./client-navigation/client-navigation.component";
import { ClientNavigationRoute } from "./client-navigation/client-navigation.module";
import {LoginComponent} from "@pages/client/login/login.component";
import {ForgotPasswordComponent} from "@pages/client/forgot_password/forgot_password.component";
import {ResetPasswordComponent} from "@pages/client/reset_password/reset_password.component";

export const NavigationRoutes: Routes = [
    {
       path: '',
       component: ClientNavigationComponent,
       children: ClientNavigationRoute
    },
    // {
    //    path: 'forgot_password',
    //    component: LoginComponent
    // },
    {
        path: 'admin',
        component: AdminNavigationComponent,
        children: AdminNavigationRoutes,
        //canActivate:[CheckLoginGuard]
    },
     {
         path: 'cliente',
         component: ClientNavigationComponent,
         children: ClientNavigationRoute
  },
  {
    path: 'login',
    component: LoginComponent,

  },
  {
    path: 'forgot_password',
    component: ForgotPasswordComponent,
  },
  {
    path: 'reset_password',
    children: [
      {
        path: ':uuid',
        component: ResetPasswordComponent
      }
    ]
  },
  {
    path: 'registrate',
    children: [
      {
         path: '',
         component: RegisterComponent,
      },
      {
         path: 'admin',
         component: RegisterAdminComponent,
      }
      ,
      {
         path: 'empresa',
         component: RegisterCompanyComponent,
      },
      {
         path: 'egresado',
         component: RegisterGraduateComponent,
      }
    //  {
    //   path: 'empresa',
    //   component: EnterpriseNavigationComponent,
    //     children: EnterpriseNavigationRoutes
    // }
    ]}
  ];
