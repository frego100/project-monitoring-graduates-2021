export const SUGGESTION_MENU = [
  {
    link: ['sugerencias'],
    icon: 'work',
    text: 'Sugerencias',
  },
  {
    link: ['mis_sugerencias'],
    icon: 'work',
    text: 'Mis Sugerencias',
  },
];

export const CONFIG_MENU = [
  {
    link: ['identificadores'],
    icon: 'list',
    text: 'Identificadores',
  },
  {
    link: ['page-config'],
    icon: 'style',
    text: 'Página',
  },
];
export const INSTANCES_MENU = [
  {
    link: ['sedes'],
    icon: 'location_on',
    text: 'Sedes',
  },
  {
    link: ['areas'],
    icon: 'apartment',
    text: 'Areas',
  },
  {
    link: ['facultades'],
    icon: 'corporate_fare',
    text: 'Facultad',
  },
  {
    link: ['programas'],
    icon: 'holiday_village',
    text: 'Programa Profesional',
  }
];

export const ACTIVITY_MENU = [
  {
    link: ['actividades'],
    icon: 'task',
    text: 'Actividades',
  },
  {
    link: ['mis-actividades'],
    icon: 'task',
    text: 'Mis Actividades',
  }
];

export const REQUEST_MENU = [
  {
    link: ['solicitudes-usuarios'],
    icon: 'manage_accounts',
    text: 'Usuarios',
  },
  {
    link: ['solicitudes-actividades'],
    icon: 'task',
    text: 'Actividades',
  },
  {
    link: ['solicitudes-convocatorias'],
    icon: 'announcement',
    text: 'Convocatorias',
  }
];

export const ANNOUNCEMENT_MENU = [
  {
    link: ['convocatorias'],
    icon: 'announcement',
    text: 'Convocatorias',
  },
  {
    link: ['mis_convocatorias'],
    icon: 'pending',
    text: 'Mis Convocatorias',
  },
  {
    link: ['mis-postulaciones'],
    icon: 'done_all',
    text: 'Me interesa',
  }
];
