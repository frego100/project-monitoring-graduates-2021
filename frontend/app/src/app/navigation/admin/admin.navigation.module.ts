import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppMaterialModule } from "@app/app.material.module";
import { UserListComponent } from "@app/pages/admin/users/list/list.component";
import { UserRoutes } from "@app/pages/admin/users/user.module";
import { AdminNavigationComponent } from "./admin.navigation.component";
import {RolesRoutes} from "@pages/admin/roles/roles.module";
import { ActivityRoutes } from "@app/pages/admin/activities/activity.module";
import { ManualsRoutes } from "@app/pages/admin/manuals/manuals.module";
import { TrackingUserRoutes } from "@app/pages/admin/tracking-history-user/tracking-history-user.module";
import { IdentifierRoutes } from "@app/pages/admin/identifiers/identifier.module";
import { EmployeeRoutes } from "@app/pages/admin/employee/employee.module";
import { JobsRoutes } from "@app/pages/admin/jobs/jobs.module";
import { ProfileRoutes } from "@app/pages/admin/profiles/profiles.module";
import { AreaRoutes } from "@app/pages/admin/area/area.module";
import { FacultyRoutes } from "@app/pages/admin/faculty/faculty.module";
import { ProgramRoutes } from "@app/pages/admin/program/program.module";
import { UserCreationRequestRoutes } from "@app/pages/admin/user-creation-request/user-creation-request.module";
import {CompaniesRoutes} from "@pages/admin/companies/companies.module";
import {InstitutionRoutes} from "@pages/admin/institutions/companies.module";
import { MyActivityRoutes } from "@app/pages/admin/my-activities/my-activity.module";
import { ActivityCreationRequestRoutes } from "@app/pages/admin/activity-creation-request/activity-creation-request.module";
import { TargetGroupAnnouncementRoutes } from "@app/pages/admin/target-group-announcement/target-group-announcement";
import { MyApplicationRoutes } from "@app/pages/admin/my-applications/my-application.module";
import {AnnoucementRoutes} from "@pages/admin/announcement/announcement.module";
import {MyAnnoucementRoutes} from "@pages/admin/my-announcements/my-announcement.module";
import {AnnoucementCreationRequestRoutes} from "@pages/admin/annoucement-creation-request/annoucement-creation-request.module";
import { HomeAdminRoute } from "@app/pages/admin/home-admin/home-admin.module";
import {ReportRoutes} from "@pages/admin/reports/reports.module";
import {PollRoutes} from "@pages/admin/poll/poll.module";
import {MasiveUsersRoutes} from "@pages/admin/masive-users/masive-users.module";
import {SedesRoutes} from "@pages/admin/sedes/sedes.module";
import {QuestionRoutes} from "@pages/admin/questions/questions.module";
import {PageAdminRoute} from "@pages/admin/page-config/page-config.module";
import {SuggestionRoutes} from "@pages/admin/suggestion-box/suggestion.module";
import {MySuggestionRoutes} from "@pages/admin/my-suggestion-box/suggestion.module";
import {GraduateQuestionRoutes} from "@pages/admin/graduate_questions/graduate.module";

export const AdminNavigationRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  ...UserRoutes,
  ...RolesRoutes,
  ...ActivityRoutes,
  ...ManualsRoutes,
  ...TrackingUserRoutes,
  ...ReportRoutes,
  ...IdentifierRoutes,
  ...EmployeeRoutes,
  ...JobsRoutes,
  ...ProfileRoutes,
  ...AreaRoutes,
  ...FacultyRoutes,
  ...ProgramRoutes,
  ...HomeAdminRoute,
  ...CompaniesRoutes,
  ...InstitutionRoutes,
  ...UserCreationRequestRoutes,
  ...TargetGroupAnnouncementRoutes,
  ...ActivityCreationRequestRoutes,
  ...AnnoucementCreationRequestRoutes,
  ...MyActivityRoutes,
  ...MyApplicationRoutes,
  ...AnnoucementRoutes,
  ...MyAnnoucementRoutes,
  ...MasiveUsersRoutes,
  ...SedesRoutes,
  ...PollRoutes,
  ...QuestionRoutes,
  ...GraduateQuestionRoutes,
  ...PageAdminRoute,
  ...SuggestionRoutes,
  ...MySuggestionRoutes,
];

@NgModule({
  declarations: [AdminNavigationComponent],
  imports: [
    RouterModule,
    CommonModule,
    AppMaterialModule
  ],
  exports: [AdminNavigationComponent]
})
export class AdminNavigationModule {}
