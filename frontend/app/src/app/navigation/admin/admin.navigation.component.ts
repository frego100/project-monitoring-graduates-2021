import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { BreakpointObserver } from '@angular/cdk/layout';
import { AuthService, AuthUserService } from '@services';
import { Router } from '@angular/router';
import {
  ACTIVITY_MENU,
  CONFIG_MENU,
  SUGGESTION_MENU,
} from './admin.navigation.menu';

@Component({
  selector: 'app-admin-navigation',
  templateUrl: './admin.navigation.component.html',
  styleUrls: ['./admin.navigation.component.scss'],
})
export class AdminNavigationComponent implements OnInit {
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  navigators = ['hidden', 'hidden', 'hidden', 'hidden', 'hidden', 'hidden', 'hidden', 'hidden'];
  mainMenu: any;
  image: string;
  username: string;
  rol: string;
  name: string;
  userMenu: any;
  instancesMenu: any;
  activityMenu: any;
  suggestionMenu: any;
  requestMenu: any;
  configMenu: any;
  questionsMenu: any;
  announcementMenu: any;
  show: Boolean = true;


  constructor(
    private observer: BreakpointObserver,
    private authSvc: AuthService,
    private userProfileService: AuthUserService,
    private routes: Router,
  ) {
    this.mainMenu = [
      {
        link: ['home'],
        icon: 'home',
        text: 'Principal',
        show: true,
      },
      {
        drilldown: 2,
        link: ['actividades'],
        icon: 'task',
        text: 'Actividades',
        show: this.userProfileService.checkPermissions('view_activity'),
      },
      {
        link: ['manuales'],
        icon: 'menu_book',
        text: 'Manuales',
        show:  this.userProfileService.checkPermissions('view_manual'),
      },
      {
        link: ['empresas'],
        icon: 'apartment',
        text: 'Empresas/Instituciones',
        show: this.userProfileService.checkPermissions('view_company'),
      },
      /* {
        link: ['instituciones'],
        icon: 'work',
        text: 'Instituciones',
        show: this.userProfileService.checkPermissions('view_institution'),
      }, */
      {
        drilldown: 0,
        link: [],
        icon: 'manage_accounts',
        text: 'Usuarios',
        show: this.userProfileService.checkPermissions('view_user') || this.userProfileService.checkPermissions('view_usergroup') || this.userProfileService.checkPermissions('view_historyuser'),
      },
      {
        drilldown: 4,
        link: [],
        icon: 'villa',
        text: 'Instancias',
        show: this.userProfileService.checkPermissions('view_campus') || this.userProfileService.checkPermissions('view_area') || this.userProfileService.checkPermissions('view_faculty') || this.userProfileService.checkPermissions('view_professionalprogram'),
      },
      {
        link: ['puestos-trabajo'],
        icon: 'fact_check',
        text: 'Puestos de Trabajo',
        show: this.userProfileService.checkPermissions('view_jobposition'),
      },
      {
        link: ['empleados'],
        icon: 'maps_home_work',
        text: 'Tipos de Empleado',
        show: this.userProfileService.checkPermissions('view_typeemployee'),
      },
      {
        drilldown: 3,
        link: [],
        icon: 'pending',
        text: 'Solicitudes',
        show: this.userProfileService.checkPermissions('view_preregistration') || this.userProfileService.checkPermissions('validate_activity') || this.userProfileService.checkPermissions('view_targetgroupannouncement'),
      },
      {
        drilldown: 5,
        link: [],
        icon: 'announcement',
        text: 'Convocatorias',
        show: true
      },
      {
        drilldown: 6,
        link: [],
        icon: 'work',
        text: 'Sugerencias',
        show: true,
      },
      {
        link: ['encuestas'],
        icon: 'file_copy',
        text: 'Encuestas',
        show: this.userProfileService.checkPermissions('view_poll'),
      },
      {
        link: ['preguntas'],
        icon: 'live_help',
        text: 'Preguntas',
        show: this.userProfileService.checkPermissions('view_question'),
      },
      {
        drilldown: 1,
        link: [],
        icon: 'build',
        text: 'Configuraciones',
        show:  this.userProfileService.checkPermissions('view_identificator'),
      },
      {
        link: ['ayuda'],
        icon: 'help',
        text: 'Ayuda',
        show: true,
      },
    ]
    this.mainMenu = this.mainMenu.filter((item) => {
      return item.show;
    })
    this.suggestionMenu  = SUGGESTION_MENU
    this.activityMenu = ACTIVITY_MENU
    this.userMenu = [
      {
        link: ['usuarios'],
        icon: 'manage_accounts',
        text: 'Cuentas de Usuario',
        show: this.userProfileService.checkPermissions('view_user'),
      },
      {
        link: ['usuarios_masivos'],
        icon: 'upload',
        text: 'Carga Masiva de Usuarios',
        show: this.userProfileService.checkPermissions('view_user'),
      },
      {
        link: ['roles'],
        icon: 'playlist_add_check',
        text: 'Roles',
        show: this.userProfileService.checkPermissions('view_usergroup'),
      },
      {
        link: ['historial'],
        icon: 'task',
        text: 'Historial',
        show: this.userProfileService.checkPermissions('view_historyuser'),
      },
      {
        link: ['reportes'],
        icon: 'description',
        text: 'Reportes',
        show: this.userProfileService.checkPermissions('view_user'),
      }
    ];
    this.userMenu = this.userMenu.filter((item) => {
      return item.show;
    })
    this.configMenu = CONFIG_MENU
    this.instancesMenu =  [
      {
        link: ['sedes'],
        icon: 'location_on',
        text: 'Sedes',
        show:  this.userProfileService.checkPermissions('view_campus'),
      },
      {
        link: ['areas'],
        icon: 'apartment',
        text: 'Areas',
        show:  this.userProfileService.checkPermissions('view_area'),
      },
      {
        link: ['facultades'],
        icon: 'corporate_fare',
        text: 'Facultad',
        show:  this.userProfileService.checkPermissions('view_faculty'),
      },
      {
        link: ['programas'],
        icon: 'holiday_village',
        text: 'Programa Profesional',
        show:  this.userProfileService.checkPermissions('view_professionalprogram'),
      }
    ];

    this.instancesMenu = this.instancesMenu.filter((item) => {
      return item.show;
    })
    this.requestMenu = [
      {
        link: ['solicitudes-usuarios'],
        icon: 'manage_accounts',
        text: 'Usuarios',
        show:  this.userProfileService.checkPermissions('view_preregistration'),
      },
      {
        link: ['solicitudes-actividades'],
        icon: 'task',
        text: 'Actividades',
        show:  this.userProfileService.checkPermissions('validate_activity'),
      },
      {
        link: ['solicitudes-convocatorias'],
        icon: 'announcement',
        text: 'Convocatorias',
        show:  this.userProfileService.checkPermissions('view_targetgroupannouncement'),
      }
    ];
    this.requestMenu = this.requestMenu.filter((item) => {
      return item.show;
    })
    this.announcementMenu = [
      {
        link: ['convocatorias'],
        icon: 'announcement',
        text: 'Convocatorias',
        show:  this.userProfileService.checkPermissions('view_announcement'),
      },
      {
        link: ['mis-convocatorias'],
        icon: 'pending',
        text: 'Mis Convocatorias',
        show:  true,
      },
      {
        link: ['mis-postulaciones'],
        icon: 'done_all',
        text: 'Me interesa',
        show: true,
      }
    ];
    this.announcementMenu = this.announcementMenu.filter((item) => {
      return item.show;
    })
    console.log(this.announcementMenu)
  }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 800px)']).subscribe((res) => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    });
  }
  ngOnInit(): void {
    this.userProfileService.getUserAdmin().subscribe((res: any) => {
      this.image = res.user_profile.picture ? res.user_profile.picture : "../../assets/images/user_default.jpg" ;
      this.username = res.username;
      this.name = res.first_name;
      this.rol = res.groups[0].name;
    });
  }

  onLogout(): void {
    this.authSvc.logout();
    this.routes.navigate(['']);
  }

  drilldown(index) {
    this.show = false;
    this.navigators[index] = 'drilldown';
  }
  profile() {
    this.userProfileService.getUserGeneral().subscribe((res: any) => {
       if (res.groups[0].profile_type.model === "superadminprofile") {
        this.routes.navigate(['/admin/perfil/superadmin']);
      } else if (res.groups[0].profile_type.model === "companyprofile") {
        this.routes.navigate(['/admin/perfil/empresa']);
        //ADECUAR CUANDO SE CREE MODELO EGRESADO
      } else if (res.groups[0].profile_type.model === "adminprofile") {
        this.routes.navigate(['/admin/perfil/admin']);
      } else if (res.groups[0].profile_type.model === "graduateprofile") {

        this.routes.navigate(['/admin/perfil/egresado']);
      }
    });
  }

  backFrom(index) {
    this.navigators[index] = 'hidden';
    this.show = true;
  }

  watchDrilldown() {
    return {
      'drill-root': this.navigators.some(
        (navigator) => navigator === 'drilldown'
      ),
    };
  }
}
