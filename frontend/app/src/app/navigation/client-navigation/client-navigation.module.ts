import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppMaterialModule } from "@app/app.material.module";
import { UserListComponent } from "@app/pages/admin/users/list/list.component";
import { PagesRoutes } from "@app/pages/pages.module";
import { ClientNavigationComponent } from "./client-navigation.component";

export const ClientNavigationRoute: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    ...PagesRoutes,
];
 
 @NgModule({
    declarations: [ClientNavigationComponent],
    imports: [
       RouterModule,
       CommonModule,
       AppMaterialModule
    ],
    exports: [ClientNavigationComponent]
 })
 export class ClientNavigationModule {}