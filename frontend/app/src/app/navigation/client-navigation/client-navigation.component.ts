import { Component, OnInit } from '@angular/core';
import {AuthService} from "@services";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-navigation',
  templateUrl: './client-navigation.component.html',
  styleUrls: ['./client-navigation.component.scss']
})
export class ClientNavigationComponent implements OnInit {
  storage: any
  constructor(
    private authSvc: AuthService,
    private routes: Router
  ) {
    this.storage = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit(): void {

  }
  onLogout(): void {
    this.authSvc.logout();
    this.routes.navigate(['/cliente/home']);
    this.storage = JSON.parse(localStorage.getItem('user'));
  }

}
