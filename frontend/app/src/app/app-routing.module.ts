import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavigationRoutes } from './navigation/navigation.module';

@NgModule({
  imports: [RouterModule.forRoot(NavigationRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
