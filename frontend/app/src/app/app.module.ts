import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app.material.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDisablingComponent} from "@shared/components/confirm-disabling/confirm-disabling.component";
import { CookieService } from 'ngx-cookie-service';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DetailActivity} from "@shared/components/detail-activity/detail-activity";
import {DetailManual} from "@shared/components/detail-manual/detail-manual";
import {ValidateUser} from "@shared/components/validate-user/validate-user";
import {ValidateActivity} from "@shared/components/validate-activity/validate-activity";
import { AnnouncementDetail } from './shared/components/detail-announcement/detail-announcement';
import {ValidateAnnoucement} from "@shared/components/validate-annoucement/validate-annoucement";
import { TargetGroupFormComponent } from './shared/components/target-group-announcement/form/form.component';
import { TargetGroupModalComponent } from './shared/components/target-group-announcement/target-group-announcement.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";

@NgModule({
  declarations: [
    AppComponent,
    ConfirmDisablingComponent,
    DetailActivity,
    AnnouncementDetail,
    DetailManual,
    ValidateUser,
    ValidateActivity,
    ValidateAnnoucement,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatSlideToggleModule,
    FlexLayoutModule,
    CKEditorModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent],
  entryComponents: [DetailActivity, DetailManual,AnnouncementDetail]
})
export class AppModule { }
