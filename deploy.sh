ssh -i $PRIVATE_KEY_DEV ubuntu@$SERVER_IP << 'ENDSSH'
  cd ../edwin/projects/egresados
  sudo su edwin
  source env/bin/activate
  cd project-monitoring-graduates-2021/
  git pull
  cd backend
  pip install -r requirements.txt
  python3 manage.py migrate
  deactivate
  exit
  sudo supervisorctl restart djangoegresados
ENDSSH
