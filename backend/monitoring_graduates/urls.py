"""monitoring_graduates URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from apps.graduate_distinctions.views import ListCountryAPIView
from .views import IndexAPIView


def trigger_error(request):
    division_by_zero = 1 / 0


urlpatterns = \
    [
        path('sentry-debug/', trigger_error),
        path('', IndexAPIView.as_view()),
        path('admin/', admin.site.urls),
        path('group/', include('apps.group.urls')),
        path('activity/', include('apps.activity.urls')),
        path('permission/', include('apps.permission.urls')),
        path('auth/', include('apps.authentication.urls')),
        path('user/', include('apps.user.urls')),
        path('history/', include('apps.history_user.urls')),
        path('manual/', include('apps.manual.urls')),
        path('job-position/', include('apps.job_position.urls')),
        path('employee/', include('apps.employee.urls')),
        path('identificator/', include('apps.identificators.urls')),
        path('profile/', include('apps.user_profile.urls')),
        path('instance/', include('apps.instance.urls')),
        path('company/', include('apps.company.urls')),
        path('language/', include('apps.graduate_language.urls')),
        path('academic/', include('apps.graduate_data_academic.urls')),
        path('projects/', include('apps.graduate_projects.urls')),
        path('distinctions/', include('apps.graduate_distinctions.urls')),
        path('institution/', include('apps.institution.urls')),
        path('work-experience/', include('apps.graduate_work_experience.urls')),
        path('country/', ListCountryAPIView.as_view()),
        path('achievements/', include('apps.graduate_achievements_academic.urls')),
        path('publications/', include('apps.graduate_publications.urls')),
        path('announcements/', include('apps.announcement.urls')),
        path('target-group-announcement/', include('apps.target_group_announcement.urls')),
        path('question/', include('apps.question.urls')),
        path('graduate-questions/', include('apps.graduate_questions.urls')),
        path('campus/', include('apps.campus.urls')),
        path('poll/', include('apps.poll.urls')),
        path('report/', include('apps.report.urls')),
        path('suggestion/', include('apps.suggestion_box.urls')),
        path('settings/', include('apps.site_settings.urls')),
        path('reset_password/', include('apps.reset_password.urls')),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
