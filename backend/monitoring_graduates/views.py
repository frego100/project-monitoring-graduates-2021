"""
Views for main access to server
"""
from rest_framework.response import Response
from rest_framework.views import APIView


class IndexAPIView(APIView):
    """
    Test View for index
    """
    def get(self, request):
        """
        GET Method for test view
        """
        return Response('This works' + str(self) +str(request))
