"""
Here the envoirement varaibles are build
"""
import environ

env = environ.Env(
    DEBUG=(bool, False),
    SECRET_KEY=(str, ''),
    ENGINE=(str, ''),
    NAME=(str, ''),
    USER_DATABASE=(str, ''),
    PASSWORD=(str, ''),
    HOST=(str, ''),
    PORT=(str, ''),
    DATABASE_URL=(str, ''),
    CORS_ORIGIN_ALLOW_ALL=(bool, False),
    CORS_ALLOW_CREDENTIALS=(bool, False),
    LANGUAGE_CODE=(str, 'en-us'),
    TIME_ZONE=(str, 'UTC'),
    USE_TZ=(str, False),
    EMAIL_HOST=(str, ''),
    EMAIL_PORT=(int, 0),
    EMAIL_HOST_USER=(str, ''),
    EMAIL_HOST_PASSWORD=(str, ''),
    EMAIL_USE_TLS=(bool, False),
    SENTRY_DSN=(str, ''),
    SENTRY_ENVIRONMENT=(str, 'development'),
    SENTRY_RELEASE=(str, 'development'),
)

environ.Env.read_env()
