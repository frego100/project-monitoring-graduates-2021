from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Suggestion(models.Model):
    user_pk = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='suggestion_user',
        null=True
    )
    text = models.CharField(
        max_length=512,
        blank=False,
        null=False
    )
    datetime = models.DateTimeField(
        auto_now_add=True
    )

    def __str__(self):
        return f'{type(self).__name__}({", ".join("%s=%s" % item for item in vars(self).items() if item[0][0] != "_")})'

    class Meta:
        verbose_name = 'Sugerencia'
        verbose_name_plural = 'Caja de Sugerencias'
