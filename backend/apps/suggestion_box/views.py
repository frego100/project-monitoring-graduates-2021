# Create your views here.
from django.http import QueryDict
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.permissions import IsAuthenticated

from apps.suggestion_box.models import Suggestion
from apps.suggestion_box.serializers import SuggestionSerializer
from monitoring_graduates.permission import ModelPermission


class SuggestionListCreateAPIView(generics.ListCreateAPIView):
    queryset = Suggestion.objects.all()
    serializer_class = SuggestionSerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'text': ['exact']
    }

    ordering_fields = [
        'id',
        'text',
    ]

    search_fields = [
        'text'
    ]


class SuggestionAuthListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = SuggestionSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'text': ['exact']
    }

    ordering_fields = [
        'id',
        'text',
    ]

    search_fields = [
        'text'
    ]

    def get_queryset(self):
        return Suggestion.objects.filter(user_pk=self.request.user)

    def create(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        request.data['user_pk'] = request.user.pk
        return super().create(request, *args, **kwargs)


class SuggestionRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Suggestion.objects.all()
    serializer_class = SuggestionSerializer
    permission_classes = [ModelPermission]


class SuggestionAuthRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SuggestionSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Suggestion.objects.filter(user_pk=self.request.user)

    def update(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        request.data['user_pk'] = request.user.pk
        return super().update(request, *args, **kwargs)
