from rest_framework import serializers

from apps.suggestion_box.models import Suggestion
from apps.user.serializer import UserLiteSerializer


class SuggestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Suggestion
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.user_pk:
            data['user_pk'] = UserLiteSerializer(instance=instance.user_pk).data
        return data
