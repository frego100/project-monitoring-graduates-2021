from django.contrib import admin

# Register your models here.
from apps.suggestion_box.models import Suggestion

admin.site.register(Suggestion)
