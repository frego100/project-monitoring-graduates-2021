"""
Urls for group module
"""
from django.urls import path

from apps.suggestion_box.views import SuggestionListCreateAPIView, SuggestionRetrieveUpdateDestroyAPIView, \
    SuggestionAuthListCreateAPIView, SuggestionAuthRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('', SuggestionListCreateAPIView.as_view()),
    path('<int:pk>', SuggestionRetrieveUpdateDestroyAPIView.as_view()),
    path('auth/', SuggestionAuthListCreateAPIView.as_view()),
    path('auth/<int:pk>', SuggestionAuthRetrieveUpdateDestroyAPIView.as_view()),
]
