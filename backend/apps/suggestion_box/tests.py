from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

# Create your tests here.
from apps.suggestion_box.models import Suggestion
from apps.suggestion_box.serializers import SuggestionSerializer


class SuggestionCRUDAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.crud_url = '/suggestion/'
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.suggestion = Suggestion.objects.create(
            user_pk=self.user,
            text='Testing question'
        )

    def test_list_suggestion(self):
        """
        Test list suggestions
        """
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_suggestion(self):
        """
        Test create suggestion
        """
        prev_len = len(Suggestion.objects.all())
        data = {
            "text": "Second suggestion"
        }
        response = self.client.post(self.crud_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(Suggestion.objects.all()))

    def test_retrieve_suggestion(self):
        """
        Test retrieve suggestion
        """
        response = self.client.get(self.crud_url + '{}'.format(self.suggestion.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, SuggestionSerializer(self.suggestion).data)

    def test_update_suggestion(self):
        """
        Test update suggestion
        """
        data = {
            "text": 'Second suggestion edited'
        }
        response = self.client.put(self.crud_url + '{}'.format(self.suggestion.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            SuggestionSerializer(
                Suggestion.objects.get(id=self.suggestion.id)
            ).data
        )

    def test_destroy_suggestion(self):
        """
        Test destroy suggestion
        """
        prev_len = len(Suggestion.objects.all())
        response = self.client.delete(self.crud_url + '{}'.format(self.suggestion.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(Suggestion.objects.all()))


class SuggestionCRUDAuthAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.crud_url = '/suggestion/auth/'
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.suggestion = Suggestion.objects.create(
            user_pk=self.user,
            text='Testing question'
        )

    def test_list_suggestion(self):
        """
        Test list suggestions
        """
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_suggestion(self):
        """
        Test create suggestion
        """
        prev_len = len(Suggestion.objects.filter(user_pk=self.user))
        data = {
            "user_pk": self.user.pk,
            "text": "Second suggestion"
        }
        response = self.client.post(self.crud_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(Suggestion.objects.filter(user_pk=self.user)))

    def test_retrieve_suggestion(self):
        """
        Test retrieve suggestion
        """
        response = self.client.get(self.crud_url + '{}'.format(self.suggestion.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, SuggestionSerializer(self.suggestion).data)

    def test_update_suggestion(self):
        """
        Test update suggestion
        """
        data = {
            "user_pk": self.user.pk,
            "text": 'Second suggestion edited'
        }
        response = self.client.put(self.crud_url + '{}'.format(self.suggestion.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            SuggestionSerializer(
                Suggestion.objects.get(id=self.suggestion.id)
            ).data
        )

    def test_destroy_suggestion(self):
        """
        Test destroy suggestion
        """
        prev_len = len(Suggestion.objects.filter(user_pk=self.user))
        response = self.client.delete(self.crud_url + '{}'.format(self.suggestion.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(Suggestion.objects.filter(user_pk=self.user)))
