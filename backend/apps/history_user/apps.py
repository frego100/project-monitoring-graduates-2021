from django.apps import AppConfig


class HistoryUserConfig(AppConfig):
    name = 'apps.history_user'
