from django.urls import path

from apps.history_user.views import HistoryUserListAPIView, HistoryUserRetrieveAPIView

urlpatterns = [
    path('', HistoryUserListAPIView.as_view()),
    path('<int:pk>', HistoryUserRetrieveAPIView.as_view()),
]
