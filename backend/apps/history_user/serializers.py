from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from rest_framework.serializers import ModelSerializer

from apps.activity.models import Activity
from apps.activity.serializer import ActivitySerializer
from apps.group.models import UserGroup
from apps.group.serializer import GroupLiteSerializer
from apps.history_user.models import HistoryUser
from apps.permission.serializer import ContentTypeSerializer, PermissionSerializer
from apps.user.serializer import UserSerializer


class LiteContentTypeSerializer(ModelSerializer):
    class Meta:
        model = ContentType
        fields = ['id', 'app_label', 'model']


class LiteUserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email']


class LiteHistoryUserSerializer(ModelSerializer):
    content_type = LiteContentTypeSerializer(read_only=True)
    user = LiteUserSerializer(read_only=True)

    class Meta:
        model = HistoryUser
        fields = '__all__'


class GenericSerializer(ModelSerializer):

    def to_representation(self, instance):
        if isinstance(instance, User):
            serializer = UserSerializer(instance)
        elif isinstance(instance, Activity):
            serializer = ActivitySerializer(instance)
        elif isinstance(instance, UserGroup):
            serializer = GroupLiteSerializer(instance)
        elif isinstance(instance, Permission):
            serializer = PermissionSerializer(instance)
        else:
            raise NotImplementedError('Unexpected type of object')
        return serializer.data


class DetailHistoryUserSerializer(ModelSerializer):
    content_type = ContentTypeSerializer(read_only=True)
    user = UserSerializer(read_only=True)
    content_object = GenericSerializer(read_only=True)

    class Meta:
        model = HistoryUser
        fields = '__all__'
