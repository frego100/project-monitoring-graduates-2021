from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class HistoryUser(models.Model):
    content_type = models.ForeignKey(
        ContentType,
        verbose_name=_('content_type'),
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User,
        verbose_name=_('User'),
        blank=False,
        null=False,
        on_delete=models.CASCADE
    )
    description = models.TextField(
        verbose_name=_('Description')
    )
    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Created")
    )
    object_id = models.IntegerField(
        null=True
    )
    content_object = GenericForeignKey('content_type', 'object_id')

    # def __str__(self):
    #     return "User: " + self.user.username + " Content_Type: " + self.content_type.name + " Description:  " + self.description + " Created: " + str(
    #         self.created) + " Content_object: " + str(self.object_id)

    class Meta:
        verbose_name = 'Historia de Usuario'
        verbose_name_plural = 'Historias de Usuario'
