from django.test import TestCase

# Create your tests here.
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

# Create your tests here.
from apps.history_user.models import HistoryUser
from apps.history_user.serializers import DetailHistoryUserSerializer
from apps.manual.models import Manual
from apps.manual.serializer import ManualSerializer


class HistoryUserAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.historic_user = HistoryUser.objects.create(
            user=self.user,
            description="Description Testing",
            content_object=self.user
        )

    def test_list_historic_user(self):
        """
        Test list historic_user
        """
        response = self.client.get('/history/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_historic_user(self):
        """
        Test retrieve historic_user
        """
        response = self.client.get('/history/{}'.format(self.historic_user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, DetailHistoryUserSerializer(self.historic_user).data)
