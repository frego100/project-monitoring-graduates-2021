# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.generics import ListAPIView, RetrieveAPIView

from apps.history_user.models import HistoryUser
from apps.history_user.serializers import DetailHistoryUserSerializer, LiteHistoryUserSerializer
from monitoring_graduates.permission import ModelPermission


class HistoryUserListAPIView(ListAPIView):
    serializer_class = LiteHistoryUserSerializer
    queryset = HistoryUser.objects.all()
    filter_backends = [filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend]
    permission_classes = [ModelPermission]
    search_fields = [
        'user__username',
        'user__email',
        'user__groups__name',
        'user__first_name',
        'user__last_name',
        'created'
    ]

    filterset_fields = {
        'user__username': ['exact'],
        'user__email': ['exact'],
        'user__groups__name': ['exact'],
        'user__first_name': ['exact'],
        'user__last_name': ['exact'],
        'created': ['gte', 'lte', 'exact', 'gt', 'lt']
    }

    ordering_fields = [
        'user__username',
        'user__email',
        'user__groups__name',
        'user__first_name',
        'user__last_name',
        'created'
    ]


class HistoryUserRetrieveAPIView(RetrieveAPIView):
    serializer_class = DetailHistoryUserSerializer
    queryset = HistoryUser.objects.all()
    permission_classes = [ModelPermission]
