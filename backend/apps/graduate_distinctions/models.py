from django.db import models


# Create your models here.
from django_countries.fields import CountryField

from apps.user_profile.models import GraduateProfile


class GraduateDistinctions(models.Model):
    """
    Model for Graduate Distinctions
    """
    name = models.CharField(
        max_length=3000,
        null=False,
        blank=False,
        verbose_name="Nombre"
    )
    date = models.DateField(
        null=False,
        verbose_name='Fecha de Obtención'
    )
    description = models.TextField(
        null=True,
        blank=True,
        verbose_name="Descripción"
    )
    reference_url = models.URLField(
        null=True,
        blank=False,
        verbose_name="URL de Referencia"
    )
    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=False,
        on_delete=models.CASCADE,
        related_name='distinctions',
        verbose_name="Perfil de Egresado"
    )
    country = CountryField(
        null=False,
        verbose_name="País"
    )

    class Meta:
        verbose_name = "Distinción de Egresado"
        verbose_name_plural = "Distinciones de Egresado"

