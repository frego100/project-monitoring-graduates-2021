from django.contrib import admin

# Register your models here.
from apps.graduate_distinctions.models import GraduateDistinctions


class GraduateDistinctionsAdmin(admin.ModelAdmin):
    inlines = []
    list_display = [
        'name',
        'date',
        'graduate_profile_pk'
    ]


admin.site.register(GraduateDistinctions, GraduateDistinctionsAdmin)
