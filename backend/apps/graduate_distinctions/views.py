from django_countries.data import COUNTRIES
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status

# Create your views here.
from rest_framework.filters import OrderingFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.graduate_distinctions.models import GraduateDistinctions
from apps.graduate_distinctions.serializers import GraduateDistinctionsSerializer, GraduateDistinctionsAuthSerializer
from apps.history_user.models import HistoryUser
from apps.user_profile.models import GraduateProfile
from monitoring_graduates.permission import ModelPermission


class ListCountryAPIView(APIView):
    """
    API View for list countries
    """
    def get(self, request):
        aux=[]
        json={}
        if self.request.user.is_authenticated:
            history = HistoryUser(
                user=self.request.user,
                description='Accedio al listado de paises'
            )
            history.save()
        
        nuevo=GraduateDistinctions._meta.get_field('country').choices
        for x, y in nuevo:
            json = {}
            json['code']=x
            json['name']=y
            aux.append(json)

        return Response(
            #data=COUNTRIES,
            aux,
            status=status.HTTP_200_OK
        )


class ListCreateGraduateDistinctionsAPIView(generics.ListCreateAPIView):
    """
    API View for create Graduate Distinctions
    """
    queryset = GraduateDistinctions.objects.all()
    permission_classes = [ModelPermission]
    serializer_class = GraduateDistinctionsSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'graduate_profile_pk',
        'country'
    ]
    ordering_fields = [
        'date',
    ]

    def get(self, request, *args, **kwargs):
        response = super(ListCreateGraduateDistinctionsAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de Distinciones de Egresado'
            )
            history.save()
        return response

    def post(self, request, *args, **kwargs):
        response = super(ListCreateGraduateDistinctionsAPIView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            data = GraduateDistinctions.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un registro en Distinciones de Egresado',
                content_object=data
            )
            history.save()
        return response


class RetrieveUpdateDestroyGraduateDistinctionAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [ModelPermission]
    serializer_class = GraduateDistinctionsSerializer
    queryset = GraduateDistinctions.objects.all()

    def get(self, request, *args, **kwargs):
        response = super(RetrieveUpdateDestroyGraduateDistinctionAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de  Distinción de Egresado'
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        response = super(RetrieveUpdateDestroyGraduateDistinctionAPIView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            data = GraduateDistinctions.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Edito un registro en Distinción de Egresados',
                content_object=data
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        response = super(RetrieveUpdateDestroyGraduateDistinctionAPIView, self).delete(request)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            history = HistoryUser(
                user=request.user,
                description='Elimino un registro de Distinción de Egresados'
            )
            history.save()
        return response


class ListCreateGraduateDistinctionsAPIViewAuth(generics.ListCreateAPIView):
    serializer_class = GraduateDistinctionsAuthSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'country'
    ]
    ordering_fields = [
        'name',
        'date'
    ]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateDistinctions.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def perform_create(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Agrego una distincióna a su propio perfil',
            content_object=instance
        )
        history.save()

    def get(self, request, *args, **kwargs):
        response = super(ListCreateGraduateDistinctionsAPIViewAuth, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de sus distinciones'
            )
            history.save()
        return response


class RetrieveUpdateDestroyGraduateDistinctionAPIViewAuth(generics.RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduateDistinctionsAuthSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateDistinctions.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = super(RetrieveUpdateDestroyGraduateDistinctionAPIViewAuth, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de distinciones desde su perfi',
                content_object=self.get_object()
            )
            history.save()
        return response

    def perform_update(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Actualizo una distinción en su propio perfil',
            content_object=instance
        )
        history.save()

    def perform_destroy(self, instance):
        instance.delete()
        history = HistoryUser(
            user=self.request.user,
            description='Eliminación un distinción en su propio perfil'
        )
        history.save()
