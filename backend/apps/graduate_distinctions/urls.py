from django.urls import path

from apps.graduate_distinctions.views import ListCreateGraduateDistinctionsAPIView, \
    RetrieveUpdateDestroyGraduateDistinctionAPIView, ListCreateGraduateDistinctionsAPIViewAuth, \
    RetrieveUpdateDestroyGraduateDistinctionAPIViewAuth

urlpatterns = [
    path('', ListCreateGraduateDistinctionsAPIView.as_view()),
    path('<int:pk>', RetrieveUpdateDestroyGraduateDistinctionAPIView.as_view()),
    path('auth/', ListCreateGraduateDistinctionsAPIViewAuth.as_view()),
    path('auth/<int:pk>', RetrieveUpdateDestroyGraduateDistinctionAPIViewAuth.as_view())
]
