from django_countries.fields import Country
from rest_framework import serializers

from apps.graduate_distinctions.models import GraduateDistinctions


class GraduateDistinctionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateDistinctions
        fields = '__all__'

    def to_representation(self, instance):
        data = super(GraduateDistinctionsSerializer, self).to_representation(instance)
        data['country'] = {
            'code': instance.country.code,
            'name': instance.country.name
        }
        return data


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class GraduateDistinctionsAuthSerializer(GraduateDistinctionsSerializer):
    class Meta:
        model = GraduateDistinctions
        fields = [
            'id',
            'name',
            'date',
            'description',
            'reference_url',
            'country'
        ]
