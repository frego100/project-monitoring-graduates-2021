# Generated by Django 3.2.4 on 2021-11-04 21:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('announcement', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='announcement',
            options={'permissions': (('validate_announcement', 'Puede validar una convocatoria'),), 'verbose_name': 'Convocatoria', 'verbose_name_plural': 'Convocatorias'},
        ),
    ]
