from django.db import models
from apps.company.models import Company
from django.contrib.auth.models import User

# Create your models here.


class Announcement(models.Model):
    
    name = models.CharField(
        blank=False,
        null=False,
        max_length=255
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='announcement_created_user',
    )
    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='announcement_updated_user',
        null=True
    )
    created = models.DateTimeField(
        auto_now_add=True
    )

    updated= models.DateTimeField(
        auto_now=True,
        null=True
    )
    is_validated = models.BooleanField(
        null=False,
        default=False
    )
    status = models.BooleanField(
        null=False,
        default=True
    )
    
    date_start = models.DateTimeField()

    date_end = models.DateTimeField()   
    description = models.TextField(
        blank=True,
        null=False
    )
    extra_file = models.FileField(
        upload_to='announcements/', 
        null=True, 
        blank=True)
    company_pk = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        related_name='announcement_company',
    )

    class Meta:
        verbose_name = 'Convocatoria'
        verbose_name_plural = 'Convocatorias'
        permissions = (
            ("validate_announcement", "Puede validar una convocatoria"),
        )


class UserPostulant(models.Model):
    class UserPostulantChoices(models.IntegerChoices):
        VALIDATE = 0, "Validado"
        INVALIDATE = 1, "Invalidate"

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=False,
        verbose_name='Usuario'
    )
    announcement = models.ForeignKey(
        Announcement,
        on_delete=models.CASCADE,
        null=False,
        verbose_name='Convocatoria'
    )
    is_active = models.BooleanField(
        null=False,
        default=True,
        verbose_name='Activo'
    )

    class Meta:
        verbose_name = 'Postulación'
        verbose_name_plural = 'Postulaciones'

    def __str__(self):
        return '{}_{}'.format(self.user, self.announcement)
