"""
Urls for group module
"""
from django.urls import path

from .views import (
    AnnouncementRetrieveUpdateDestroyAPIView,
    AnnouncementListApiView,
    AnnouncementReactivateAPIView,
    AnnouncementValidateAPIView,
    AnnouncementListPublicApiView,
    NotifyAnnouncementGroupAPIView,
    PostulateAnnouncementAPIView,
    MyAnnouncementListAPIView,
    PostulateAPIView,
    QuitPostulateAuthAPIView
)

urlpatterns = [
    path('', AnnouncementListApiView.as_view()),
    path('<int:pk>', AnnouncementRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', AnnouncementReactivateAPIView.as_view()),
    path('public/', AnnouncementListPublicApiView.as_view()),
    path('validate_email/<int:pk>', AnnouncementValidateAPIView.as_view()),
    path('my/', MyAnnouncementListAPIView.as_view()),
    path('postulate/', PostulateAPIView.as_view()),
    path('notify-group/', NotifyAnnouncementGroupAPIView.as_view()),
    path('postulates/<int:pk>', PostulateAnnouncementAPIView.as_view()),
    path('quit/<pk>', QuitPostulateAuthAPIView.as_view())
]
