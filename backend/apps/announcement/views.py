from django.http import QueryDict
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.graduate_data_academic.models import GraduateDataAcademic
from apps.history_user.models import HistoryUser
from apps.instance.models import ProfessionalProgram, Faculty
from apps.target_group_announcement.models import TargetGroupAnnouncement
from monitoring_graduates.permission import ModelPermission
from .models import Announcement, UserPostulant
from .permissions import AnnouncementValidatorPermission
from .serializer import (
    AnnouncementSerializer,
    AnnouncementSerializerLite,
    PostulateAnnouncementAuthSerializer,
    AnnouncementNotifyGroupSerializer,
    PostulateAnnouncementSerializer
)
# Create your views here.
from ..email_sender.utils import EmailSender


class AnnouncementRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API View for list and create a activity
    """
    permission_classes = [ModelPermission]
    queryset = Announcement.objects.all()
    serializer_class = AnnouncementSerializer

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            announcement = get_object_or_404(Announcement, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Recupero el detalle de una  convocatoria',
                content_object=announcement
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = True
        request.data['created_by'] = self.get_object().created_by.id
        request.data['updated_by'] = request.user.id
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            announcement = get_object_or_404(Announcement, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito una convocatoria',
                content_object=announcement
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        announcement = self.get_object()
        announcement.status = False
        announcement.updated_by = request.user
        announcement.save()
        history = HistoryUser(
            user=request.user,
            description='Borro de manera logica una convocatoria',
            content_object=announcement
        )
        history.save()
        return Response(
            status=status.HTTP_200_OK
        )


class AnnouncementListApiView(generics.ListCreateAPIView):
    """
    list Create Api View activity with filters
    """
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^name'
    ]
    filterset_fields = {
        'status': ['exact'],  # Con esto pueden filtrar actividades habilitadas o no habilitadas
        'is_validated': ['exact'],
        'date_start': ['gte'],
        'date_end': ['lte'],
        'company_pk': ['exact']
    }
    serializer_class = AnnouncementSerializer
    queryset = Announcement.objects.all()

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de convocatorias'
            )
            history.save()
        return response

    def post(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = True
        request.data['created_by'] = request.user.id
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            announcement = get_object_or_404(Announcement, pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo una nueva convocatoria',
                content_object=announcement
            )
            history.save()
        return response


class AnnouncementListPublicApiView(generics.ListAPIView):
    """
    list Create Api View activity with filters
    """
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^name'
    ]
    filterset_fields = {
        'date_start': ['gte'],
        'date_end': ['lte'],
        'company_pk': ['exact']
    }
    serializer_class = AnnouncementSerializer
    queryset = Announcement.objects.filter(is_validated=True, status=True).order_by('date_end')


class AnnouncementReactivateAPIView(generics.UpdateAPIView):
    """
    API View class for reactivate a activity
    """
    permission_classes = [ModelPermission]
    queryset = Announcement.objects.filter(status=False)
    serializer_class = AnnouncementSerializerLite

    def update(self, request, *args, **kwargs):
        announcement = self.get_object()
        announcement.status = True
        announcement.save()
        history = HistoryUser(
            user=request.user,
            description='Habilito de manera logica una convocatoria',
            content_object=announcement
        )
        history.save()
        return Response(
            self.serializer_class(announcement).data,
            status=status.HTTP_200_OK
        )


class AnnouncementValidateAPIView(APIView):
    serializer_class = AnnouncementSerializer
    queryset = Announcement.objects.all()
    permission_classes = [AnnouncementValidatorPermission]

    def post(self, request, **kwargs):
        announcement = get_object_or_404(Announcement, pk=int(kwargs['pk']))
        announcement.is_validated = request.data['is_validated']
        justification = request.data['justification']
        announcement.save()
        template_name = 'validate_announcement.html'
        sender = EmailSender()
        subject = 'VALIDACIÓN DE CONVOCATORIA: ' + announcement.name
        aux = 'ACEPTO' if announcement.is_validated else 'RECHAZO'
        emails = {announcement.created_by.email}
        # print(announcement.name, announcement.updated)
        data = {
            'announcement': announcement,
            'is_validated': aux,
            'justification': justification
        }
        sender.send_email(
            subject,
            emails,
            template_name,
            data
        )
        history = HistoryUser(
            user=request.user,
            description='Se valido una convocatoria',
            content_object=announcement
        )
        history.save()
        return Response(
            status=status.HTTP_200_OK
        )


class MyAnnouncementListAPIView(generics.ListAPIView):
    serializer_class = AnnouncementSerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^name'
    ]
    filterset_fields = {
        'status': ['exact'],  # Con esto pueden filtrar actividades habilitadas o no habilitadas
        'is_validated': ['exact'],
        'date_start': ['gte'],
        'date_end': ['lte']
    }

    def get_queryset(self):
        return Announcement.objects.filter(
            created_by=self.request.user
        )


class PostulateAPIView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = UserPostulant.objects.filter(is_active=True)
    serializer_class = PostulateAnnouncementAuthSerializer

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        data = serializer.validated_data
        prev_postulation = UserPostulant.objects.filter(
            user=self.request.user,
            announcement=data['announcement'],
            is_active=True
        )
        if len(prev_postulation) == 0:
            instance = serializer.save(user=self.request.user)
            history = HistoryUser(
                user=self.request.user,
                description='Se postulo en un convocatoria',
                content_object=instance
            )
            history.save()
        else:
            raise ValidationError({
                'detail': 'Ya estas registrado en esta convocatoria'
            })


class PostulateAnnouncementAPIView(generics.ListAPIView):
    permission_classes = [ModelPermission]
    queryset = UserPostulant.objects.filter(is_active=True)
    serializer_class = PostulateAnnouncementSerializer

    def get(self, request, *args, **kwargs):
        users = UserPostulant.objects.filter(is_active=True, announcement=kwargs['pk'])
        return Response(
            self.serializer_class(users, many=True).data,
            status=status.HTTP_200_OK
        )


class QuitPostulateAuthAPIView(generics.DestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = UserPostulant.objects.filter(is_active=True)
    serializer_class = PostulateAnnouncementAuthSerializer

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()
        history = HistoryUser(
            user=self.request.user,
            description='Se retiro de una convocatoria',
            content_object=instance
        )
        history.save()


class NotifyAnnouncementGroupAPIView(generics.CreateAPIView):
    permission_classes = [ModelPermission]
    serializer_class = AnnouncementNotifyGroupSerializer
    queryset = TargetGroupAnnouncement.objects.all()

    def post(self, request):

        if not request.user.has_perm('announcement.validate_announcement'):
            return Response({
                'Error': 'Sin permisos de validación no pueden enviarse correos'},
                status=status.HTTP_401_UNAUTHORIZED
            )

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        group_objective_program = data['group_objective_program']
        group_objective_faculty = data['group_objective_faculty']
        group_objective_area = data['group_objective_area']
        announcement = data['announcement']
        if len(group_objective_program) > 0 or len(group_objective_faculty) > 0 or len(group_objective_area) > 0:
            recipients_program = []
            recipients_faculty = []
            recipients_area = []
            if len(group_objective_program) > 0:
                try:
                    for obj in group_objective_program:
                        g_objective = TargetGroupAnnouncement.objects.get(id=obj)
                        instance = ProfessionalProgram.objects.get(id=g_objective.instance.id)
                        graduate_academic = GraduateDataAcademic.objects.filter(profesional_program_pk=instance.id)
                        recipients_program.extend(list(set(
                            i for i in graduate_academic.values_list('graduate_profile_pk__user__email', flat=True) if
                            bool(i))))
                except TargetGroupAnnouncement.DoesNotExist:
                    pass
            if len(group_objective_faculty) > 0:
                try:
                    for obj in group_objective_faculty:
                        g_objective = TargetGroupAnnouncement.objects.get(id=obj)
                        instances = ProfessionalProgram.objects.filter(faculty_field=g_objective.instance.id)
                        for instance in instances:
                            graduate_academic = GraduateDataAcademic.objects.filter(profesional_program_pk=instance.id)
                            recipients_faculty.extend(list(set(
                                i for i in graduate_academic.values_list('graduate_profile_pk__user__email', flat=True)
                                if bool(i))))
                except TargetGroupAnnouncement.DoesNotExist:
                    pass
            if len(group_objective_area) > 0:
                try:
                    for obj in group_objective_area:
                        g_objective = TargetGroupAnnouncement.objects.get(id=obj)
                        for faculty in Faculty.objects.filter(area_field=g_objective.instance.id):
                            instances = ProfessionalProgram.objects.filter(faculty_field=faculty.id)
                            for instance in instances:
                                graduate_academic = GraduateDataAcademic.objects.filter(
                                    profesional_program_pk=instance.id)
                                recipients_area.extend(list(set(i for i in graduate_academic.values_list(
                                    'graduate_profile_pk__user__email', flat=True) if bool(i))))
                except TargetGroupAnnouncement.DoesNotExist:
                    pass
            recipients = list(set(recipients_program + recipients_area + recipients_faculty))
            template_name = 'notify.html'
            sender = EmailSender()
            subject = 'CONVOCATORIA: ' + announcement.name
            emails = recipients
            data = {
                'announcement': announcement
            }
            try:
                sender.send_email(
                    subject,
                    emails,
                    template_name,
                    data,
                    file=announcement.extra_file.path
                )
            except:
                sender.send_email(
                    subject,
                    emails,
                    template_name,
                    data
                )
            history = HistoryUser(
                user=request.user,
                description='Envio una notificación de convocatoria'
            )
            history.save()
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "message": "Correos enviados"
                }
            )
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "message": "No se ha proporcionado grupos objetivo."
                }
            )
