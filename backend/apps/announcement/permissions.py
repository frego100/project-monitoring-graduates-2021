from rest_framework import permissions


class AnnouncementValidatorPermission(permissions.BasePermission):
    """
    Permission manager for type employee
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        return request.user.has_perm('announcement.validate_announcement')
