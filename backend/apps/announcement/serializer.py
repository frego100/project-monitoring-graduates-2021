from rest_framework import serializers

from apps.company.serializers import CompanySerializer
from apps.user.serializer import UserMiniSerializer, UserMiniAlternativeSerializer
from .models import Announcement, UserPostulant


class AnnouncementSerializer(serializers.ModelSerializer):
    """
    Serializer for Group model
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = Announcement
        fields = '__all__'

    def to_representation(self, instance):
        data = super(AnnouncementSerializer, self).to_representation(instance)
        if instance.created_by:
            data['created_by'] = UserMiniSerializer(instance.created_by, context=self.context).data
        if instance.updated_by:
            data['updated_by'] = UserMiniSerializer(instance.updated_by, context=self.context).data
        if instance.company_pk:
            data['company_pk'] = CompanySerializer(instance.company_pk, context=self.context).data
        return data


class AnnouncementSerializerLite(serializers.ModelSerializer):
    """
    Serializer for Group model
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = Announcement
        fields = '__all__'


class AnnouncementSerializerUserLite(serializers.ModelSerializer):
    """
    Serializer for Group model
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = Announcement
        fields = '__all__'
    
    def to_representation(self, instance):
        data = super(AnnouncementSerializerUserLite, self).to_representation(instance)
        if instance.created_by:
            data['created_by'] = UserMiniSerializer(instance.created_by, context=self.context).data
        return data


class PostulateAnnouncementAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPostulant
        fields = ['id', 'announcement', 'is_active']

    def to_representation(self, instance):
        data = super(PostulateAnnouncementAuthSerializer, self).to_representation(instance)
        data['announcement'] = AnnouncementSerializerUserLite(instance.announcement, context=self.context).data
        return data


class AnnouncementNotifyGroupSerializer(serializers.Serializer):
    """
    Serializer for notify activities in group
    """
    group_objective_program = serializers.ListField(child=serializers.IntegerField(), required=True)
    group_objective_faculty = serializers.ListField(child=serializers.IntegerField(), required=True)
    group_objective_area = serializers.ListField(child=serializers.IntegerField(), required=True)
    announcement = serializers.IntegerField(required=True)

    def validate_announcement(self, data):
        try:
            announcement_object = Announcement.objects.get(pk=data)
            return announcement_object
        except Announcement.DoesNotExist:
            raise serializers.ValidationError("Convocatoria no existe")


class PostulateAnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPostulant
        fields = ['id', 'user', 'is_active']

    def to_representation(self, instance):
        data = super(PostulateAnnouncementSerializer, self).to_representation(instance)
        data['user'] = UserMiniAlternativeSerializer(instance.user).data
        return data
