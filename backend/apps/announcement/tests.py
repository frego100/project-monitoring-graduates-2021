from datetime import datetime

import pytz
from django.conf.global_settings import TIME_ZONE
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory
from django.test import TestCase
from rest_framework import status, serializers
from rest_framework.test import APIClient

# Create your tests here.
from apps.announcement.models import Announcement, UserPostulant
from apps.announcement.serializer import AnnouncementSerializer, AnnouncementSerializerLite, \
    PostulateAnnouncementAuthSerializer, PostulateAnnouncementSerializer
from apps.company.models import Company
from apps.instance.models import Area


class AnnouncementAPIViewTestCase(TestCase):
    def setUp(self) -> None:
        self.model = Announcement
        self.list_create_serializer = AnnouncementSerializer
        self.client = APIClient()
        self.crud_url = '/announcements/'
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)
        self.user.save()
        self.file = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.company = Company.objects.create(
            company_name="7r31vTj1",
            social_reason="Zzex",
            identificator=None,
            ruc=647,
            company_activity="7dh",
            logo=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            is_active=True,
        )
        self.announcement = self.model.objects.create(
            name="PRQZ",
            created_by=self.user,
            updated_by=None,
            created=datetime.now(pytz.timezone(TIME_ZONE)),
            updated=None,
            is_validated=True,
            status=True,
            date_start=datetime.now(pytz.timezone(TIME_ZONE)),
            date_end=datetime.now(pytz.timezone(TIME_ZONE)),
            description="WhHGm42b",
            extra_file=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            company_pk=self.company
        )

    def test_list_announcement(self):
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.list_create_serializer(
                self.model.objects.all(),
                many=True,
                context={'request': RequestFactory().get(self.crud_url)}
            ).data,
            response.data['results']
        )

    def test_create_announcement(self):
        data = AnnouncementSerializerLite(self.announcement).data
        del data['updated_by']
        del data['extra_file']
        response = self.client.post(self.crud_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_announcement(self):
        data = AnnouncementSerializerLite(self.announcement).data
        del data['updated_by']
        del data['extra_file']
        response = self.client.put(self.crud_url + str(self.announcement.pk), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_announcement(self):
        response = self.client.get(self.crud_url + str(self.announcement.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_destroy_announcement(self):
        response = self.client.delete(self.crud_url + str(self.announcement.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ValidateEmailAPIViewTestCase(TestCase):
    def setUp(self) -> None:
        self.model = Announcement
        self.list_create_serializer = AnnouncementSerializer
        self.client = APIClient()
        self.crud_url = '/announcements/validate_email/'
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)
        self.user.save()
        self.file = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.company = Company.objects.create(
            company_name="7r31vTj1",
            social_reason="Zzex",
            identificator=None,
            ruc=647,
            company_activity="7dh",
            logo=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            is_active=True,
        )
        self.announcement = self.model.objects.create(
            name="PRQZ",
            created_by=self.user,
            updated_by=None,
            created=datetime.now(pytz.timezone(TIME_ZONE)),
            updated=None,
            is_validated=True,
            status=True,
            date_start=datetime.now(pytz.timezone(TIME_ZONE)),
            date_end=datetime.now(pytz.timezone(TIME_ZONE)),
            description="WhHGm42b",
            extra_file=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            company_pk=self.company
        )

    def test_validate_email(self):
        data = {
            "is_validated": True,
            "justification": "Why not?"
        }
        response = self.client.post(self.crud_url + str(self.announcement.pk), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AnnouncementPublicListAPIViewTestCase(TestCase):
    def setUp(self) -> None:
        self.model = Announcement
        self.list_create_serializer = AnnouncementSerializerLite
        self.client = APIClient()
        self.crud_url = '/announcements/public/'
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)
        self.user.save()
        self.file = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.company = Company.objects.create(
            company_name="7r31vTj1",
            social_reason="Zzex",
            identificator=None,
            ruc=647,
            company_activity="7dh",
            logo=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            is_active=True,
        )
        self.announcement = self.model.objects.create(
            name="PRQZ",
            created_by=self.user,
            updated_by=None,
            created=datetime.now(pytz.timezone(TIME_ZONE)),
            updated=None,
            is_validated=True,
            status=True,
            date_start=datetime.now(pytz.timezone(TIME_ZONE)),
            date_end=datetime.now(pytz.timezone(TIME_ZONE)),
            description="WhHGm42b",
            extra_file=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            company_pk=self.company
        )

    def test_list_announcement(self):
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Lo comento debido a que los campos no necesariamente los retorna en el mismo orden
        # self.assertEqual(
        #    self.list_create_serializer(
        #        self.model.objects.filter(is_validated=True, status=True).order_by('date_end'),
        #        many=True,
        #        context={'request': RequestFactory().get(self.crud_url)}
        #     ).data,
        #     response.data['results']
        # )


class ListMyAnnouncementAPIViewTestCase(TestCase):
    def setUp(self) -> None:
        self.model = Announcement
        self.list_create_serializer = AnnouncementSerializer
        self.client = APIClient()
        self.crud_url = '/announcements/my/'
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)
        self.user.save()
        self.file = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.company = Company.objects.create(
            company_name="7r31vTj1",
            social_reason="Zzex",
            identificator=None,
            ruc=647,
            company_activity="7dh",
            logo=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            is_active=True,
        )
        self.announcement = self.model.objects.create(
            name="PRQZ",
            created_by=self.user,
            updated_by=None,
            created=datetime.now(pytz.timezone(TIME_ZONE)),
            updated=None,
            is_validated=True,
            status=True,
            date_start=datetime.now(pytz.timezone(TIME_ZONE)),
            date_end=datetime.now(pytz.timezone(TIME_ZONE)),
            description="WhHGm42b",
            extra_file=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            company_pk=self.company
        )

    def test_list_my_announcement(self):
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.list_create_serializer(
                self.model.objects.filter(
                    created_by=self.user
                ),
                many=True,
                context={'request': RequestFactory().get(self.crud_url)}
            ).data,
            response.data['results']
        )


class PostulateMockSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPostulant
        fields = ['id', 'announcement', 'is_active']


class PostulateListCreateAPIViewTestCase(TestCase):
    def setUp(self) -> None:
        self.model = UserPostulant
        self.list_create_serializer = PostulateAnnouncementAuthSerializer
        self.client = APIClient()
        self.crud_url = '/announcements/postulate/'
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)
        self.user.save()
        self.file = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.company = Company.objects.create(
            company_name="7r31vTj1",
            social_reason="Zzex",
            identificator=None,
            ruc=647,
            company_activity="7dh",
            logo=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            is_active=True,
        )
        self.announcement = Announcement.objects.create(
            name="PRQZ",
            created_by=self.user,
            updated_by=None,
            created=datetime.now(pytz.timezone(TIME_ZONE)),
            updated=None,
            is_validated=True,
            status=True,
            date_start=datetime.now(pytz.timezone(TIME_ZONE)),
            date_end=datetime.now(pytz.timezone(TIME_ZONE)),
            description="WhHGm42b",
            extra_file=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            company_pk=self.company
        )
        self.user_postulant = self.model.objects.create(
            user=self.user,
            announcement=self.announcement,
            is_active=True
        )

    def test_list_my_postulations(self):
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.list_create_serializer(
                self.model.objects.filter(
                    user=self.user,
                    is_active=True
                ),
                many=True,
                context={'request': RequestFactory().get(self.crud_url)}
            ).data,
            response.data['results']
        )

    def test_create_my_postulation_error(self):
        data = PostulateMockSerializer(self.user_postulant).data
        response = self.client.post(self.crud_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['detail'], 'Ya estas registrado en esta convocatoria')

    def test_quit_postulate(self):
        response = self.client.delete('/announcements/quit/' + str(self.user_postulant.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_list_postulates(self):
        response = self.client.get('/announcements/postulates/' + str(self.announcement.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            PostulateAnnouncementSerializer(
                UserPostulant.objects.filter(
                    announcement=self.announcement,
                    is_active=True
                ),
                many=True,
                context={'request': RequestFactory().get('/announcements/postulates/')}
            ).data,
            response.data
        )

    def test_send_emails_for_users(self):
        area = Area.objects.create(
            name="WOA4CgL",
            is_active=True
        )
        data = {
            'group_objective_program': [0],
            'group_objective_faculty': [0],
            'group_objective_area': [area.pk],
            'announcement': [self.announcement.pk]
        }
        response = self.client.post('/announcements/notify-group/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
