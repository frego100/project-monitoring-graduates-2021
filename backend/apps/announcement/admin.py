from django.contrib import admin
from .models import Announcement, UserPostulant

# Register your models here.
admin.site.register(Announcement)
admin.site.register(UserPostulant)
