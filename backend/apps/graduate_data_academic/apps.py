from django.apps import AppConfig


class GraduateDataAcademicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.graduate_data_academic'
