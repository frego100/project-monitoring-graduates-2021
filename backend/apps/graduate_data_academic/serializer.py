from rest_framework import serializers

from .models import GraduateDataAcademic
from ..question.serializers import QuestionSerializer


class GraduateDataAcademicSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateDataAcademic
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.profesional_program_pk:
            data['profesional_program'] = {
                'id': instance.profesional_program_pk.pk,
                'name': instance.profesional_program_pk.name
            }
        if instance.campus_pk:
            data['campus'] = {
                'id': instance.campus_pk.pk,
                'name': instance.campus_pk.name
            }
        data['graduate_questions'] = QuestionSerializer(instance.graduate_questions, many=True).data
        return data


class GraduateDataAcademicSerializerAuth(serializers.ModelSerializer):
    class Meta:
        model = GraduateDataAcademic
        fields = [
            'id',
            'period_graduate',
            'is_student',
            'is_graduate',
            'year_of_university_studies',
            'profesional_program_pk',
            'year_graduate',
            'campus_pk',
            'graduate_questions'
        ]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.profesional_program_pk:
            data['profesional_program'] = {
                'id': instance.profesional_program_pk.pk,
                'name': instance.profesional_program_pk.name
            }
        if instance.campus_pk:
            data['campus'] = {
                'id': instance.campus_pk.pk,
                'name': instance.campus_pk.name
            }
        data['graduate_questions'] = QuestionSerializer(instance.graduate_questions, many=True).data
        return data
