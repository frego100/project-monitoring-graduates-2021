from django.urls import path
from .views import DataAcademicCreateListView, DataAcademicDetailView, DataAcademicCreateListViewAuth, \
    DataAcademicDetailAuthView

urlpatterns = [
    path('', DataAcademicCreateListView.as_view()),
    path('auth/', DataAcademicCreateListViewAuth.as_view()),
    path('<int:pk>', DataAcademicDetailView.as_view()),
    path('auth/<int:pk>', DataAcademicDetailAuthView.as_view())
]
