from django.db import models
# Create your models here.
from django.utils.translation import ugettext_lazy as _

from apps.campus.models import Campus
from apps.instance.models import ProfessionalProgram, Faculty
from apps.question.models import Question
from apps.user_profile.models import GraduateProfile


class GraduateDataAcademic(models.Model):
    """
       Profile for user graduate language
       """
    period = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D')
    )

    period_graduate = models.CharField(
        choices=period,
        verbose_name=_("period_graduate"),
        max_length=1,
        null=True
    )

    is_student = models.BooleanField(
        null=False
    )

    is_graduate = models.BooleanField(
        null=False
    )

    profesional_program_pk = models.ForeignKey(
        ProfessionalProgram,
        null=True,
        on_delete=models.SET_NULL,
        related_name='profesional_program'
    )

    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=False,
        on_delete=models.CASCADE,
        related_name='graduate_profile_academic'
    )

    year_of_university_studies = models.IntegerField(
        null=True,
        default=None
    )

    year_graduate = models.IntegerField(
        null=True
    )

    campus_pk = models.ForeignKey(
        Campus,
        null=True,
        on_delete=models.SET_NULL,
        related_name='graduate_profile_campus'
    )
    graduate_questions = models.ManyToManyField(
        Question,
        verbose_name='Preguntas de Egresado'
    )

    @staticmethod
    def get_graduate_by_professional_program(instance):
        graduate_profiles = []
        for graduate in GraduateDataAcademic.objects.filter(profesional_program_pk=instance):
            if graduate.graduate_profile_pk.pk not in graduate_profiles:
                graduate_profiles.append(graduate.graduate_profile_pk.pk)
        return graduate_profiles

    @staticmethod
    def get_graduate_by_faculty(instance):
        graduate_profiles = []

        programs = []
        for program in ProfessionalProgram.objects.filter(faculty_field=instance):
            if program.pk not in programs:
                programs.append(program.pk)

        for graduate in GraduateDataAcademic.objects.filter(profesional_program_pk__in=programs):
            if graduate.graduate_profile_pk.pk not in graduate_profiles:
                graduate_profiles.append(graduate.graduate_profile_pk.pk)

        return graduate_profiles

    @staticmethod
    def get_graduate_by_area(instance):
        graduate_profiles = []

        faculties = []
        for faculty in Faculty.objects.filter(area_field=instance):
            if faculty.pk not in faculties:
                faculties.append(faculty.pk)

        programs = []
        for program in ProfessionalProgram.objects.filter(faculty_field__in=faculties):
            if program.pk not in programs:
                programs.append(program.pk)

        for graduate in GraduateDataAcademic.objects.filter(profesional_program_pk__in=programs):
            if graduate.graduate_profile_pk.pk not in graduate_profiles:
                graduate_profiles.append(graduate.graduate_profile_pk.pk)

        return graduate_profiles

    class Meta:
        verbose_name = 'DataAcademic'
        verbose_name_plural = 'DataAcademic'
