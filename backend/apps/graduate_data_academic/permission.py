from rest_framework import permissions


class GraduateDataAcademicPermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'GET':
            return request.user.has_perm('graduate_data_academic.view_graduatedataacademic')
        if request.method == 'POST':
            return request.user.has_perm('graduate_data_academic.add_graduatedataacademic')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('graduate_data_academic.change_graduatedataacademic')
        if request.method == 'DELETE':
            return request.user.has_perm('graduate_data_academic.delete_graduatelanguage')
        return False
