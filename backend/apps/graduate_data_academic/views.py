from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, generics
from rest_framework.filters import (
    OrderingFilter
)
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated

from apps.history_user.models import HistoryUser
from .models import GraduateDataAcademic
from .permission import GraduateDataAcademicPermission
from .serializer import GraduateDataAcademicSerializer, GraduateDataAcademicSerializerAuth
# Create your views here.
from ..user_profile.models import GraduateProfile


class DataAcademicCreateListView(generics.ListCreateAPIView):
    serializer_class = GraduateDataAcademicSerializer
    queryset = GraduateDataAcademic.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'period_graduate',
        'is_student',
        'is_graduate',
        'profesional_program_pk__name',
        'graduate_profile_pk',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui',
        'year_graduate',
        'year_of_university_studies',
        'campus_pk__name',
        'campus_pk'
    ]
    ordering_fields = [
        'period_graduate',
        'year_graduate',
        'year_of_university_studies',
        'profesional_program_pk__name',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui',
        'campus_pk__name',
        'campus_pk'
    ]
    permission_classes = (GraduateDataAcademicPermission,)

    def post(self, request, *args, **kwargs):
        response = super(DataAcademicCreateListView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            data = GraduateDataAcademic.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un registro en GraduateDataAcademic ',
                content_object=data
            )
            history.save()
        return response


class DataAcademicDetailView(RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduateDataAcademicSerializer
    queryset = GraduateDataAcademic.objects.all()
    permission_classes = (GraduateDataAcademicPermission,)

    def get(self, request, *args, **kwargs):
        response = super(DataAcademicDetailView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de  GraduateDataAcademic'
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        response = super(DataAcademicDetailView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            data = GraduateDataAcademic.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Edito un registro en GraduateDataAcademic ',
                content_object=data
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        response = super(DataAcademicDetailView, self).delete(request)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            history = HistoryUser(
                user=request.user,
                description='Elimino un registro de  GraduateDataAcademic'
            )
            history.save()
        return response


class DataAcademicCreateListViewAuth(generics.ListCreateAPIView):
    serializer_class = GraduateDataAcademicSerializerAuth
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'period_graduate',
        'is_student',
        'is_graduate',
        'profesional_program_pk',
        'year_graduate',
        'year_of_university_studies',
        'campus_pk__name',
        'campus_pk'
    ]
    ordering_fields = [
        'period_graduate',
        'year_graduate',
        'year_of_university_studies',
        'profesional_program_pk',
        'campus_pk__name',
        'campus_pk'
    ]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateDataAcademic.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def perform_create(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Agrego información academica a su propio perfil',
            content_object=instance
        )
        history.save()

    def get(self, request, *args, **kwargs):
        response = super(DataAcademicCreateListViewAuth, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de su información academica'
            )
            history.save()
        return response


class DataAcademicDetailAuthView(RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduateDataAcademicSerializerAuth
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateDataAcademic.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = super(DataAcademicDetailAuthView, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de datos academicos desde su perfi',
                content_object=self.get_object()
            )
            history.save()
        return response

    def perform_update(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Actualizo información academica en su propio perfil',
            content_object=instance
        )
        history.save()

    def perform_destroy(self, instance):
        instance.delete()
        history = HistoryUser(
            user=self.request.user,
            description='Eliminación información academica a su propio perfil'
        )
        history.save()
