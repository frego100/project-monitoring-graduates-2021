from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
from apps.identificators.models import Identificator


# DECRAPED

class Institution(models.Model):
    class TypeOfInstitution(models.TextChoices):
        PRIVATE = 'PRI', _('Public')
        PUBLIC = 'PUB', _('Private')

    institution_name = models.CharField(
        unique=True,
        max_length=255,
        blank=False
    )
    social_reason = models.CharField(
        max_length=128,
        blank=False
    )
    identificator = models.ForeignKey(
        Identificator,
        on_delete=models.DO_NOTHING,
        null=False
    )
    ruc = models.PositiveBigIntegerField(
        unique=True,
        null=False
    )
    institution_activity = models.CharField(
        max_length=255,
        blank=False
    )
    is_active = models.BooleanField(
        null=False,
        default=True
    )
    logo = models.ImageField(
        null=True,
        upload_to='institution/logo/',
        default=None
    )
    type = models.CharField(
        max_length=3,
        blank=False,
        choices=TypeOfInstitution.choices,
        default=TypeOfInstitution.PUBLIC
    )

    class Meta:
        verbose_name = 'Institución'
        verbose_name_plural = 'Instituciones'

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items() if item[0][0] != '_')
        )
