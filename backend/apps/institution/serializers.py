from rest_framework import serializers

from apps.identificators.serializers import IdentificatorSerializer
from apps.institution.models import Institution


class InstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institution
        fields = '__all__'


class InstitutionDetailSerializer(serializers.ModelSerializer):
    identificator = IdentificatorSerializer()

    class Meta:
        model = Institution
        fields = '__all__'
