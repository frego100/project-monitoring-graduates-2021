from django.urls import path

from apps.institution.views import InstitutionListCreateAPIView, InstitutionRetrieveUpdateDestroyAPIView, \
    InstitutionReactivateAPIView

urlpatterns = [
    path('', InstitutionListCreateAPIView.as_view()),
    path('<int:pk>', InstitutionRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', InstitutionReactivateAPIView.as_view()),
]
