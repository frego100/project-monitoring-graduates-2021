# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.institution.models import Institution
from apps.institution.serializers import InstitutionSerializer, InstitutionDetailSerializer
from apps.institution.utils import log_entry_history_user
from monitoring_graduates.permission import ModelPermission


class InstitutionListCreateAPIView(generics.ListCreateAPIView):
    queryset = Institution.objects.all()
    serializer_class = InstitutionSerializer
    detail_serializer_class = InstitutionDetailSerializer
    model_class = Institution
    permission_classes = [IsAuthenticated]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'institution_name': ['exact'],
        'social_reason': ['exact'],
        'identificator__name': ['exact'],
        'ruc': ['exact'],
        'type': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'institution_name',
        'social_reason',
        'ruc',
        'type',
        'is_active'
    ]

    search_fields = [
        'institution_name',
        'social_reason',
        'identificator__name',
        'ruc',
        'type',
        'is_active'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Accedio al listado de instituciones'
            )
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            log_entry_history_user(
                request_user=request.user,
                message='Creo una institución',
                instance=get_object_or_404(self.model_class, pk=int(response.data['id']))
            )
        return Response(
            status=status.HTTP_201_CREATED,
            data=self.detail_serializer_class(
                get_object_or_404(
                    self.model_class,
                    pk=int(response.data['id'])
                )
            ).data
        )


class InstitutionRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Institution.objects.all()
    serializer_class = InstitutionSerializer
    detail_serializer_class = InstitutionDetailSerializer
    model_class = Institution
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.detail_serializer_class(instance, context=self.get_serializer_context())
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Recupero una institución',
                instance=get_object_or_404(self.model_class, pk=int(self.kwargs['pk']))
            )
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo una institución',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return Response(
            status=response.status_code,
            data=self.detail_serializer_class(
                get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            ).data
        )

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            log_entry_history_user(
                request_user=request.user,
                message='Elimino logicamente una institución',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class InstitutionReactivateAPIView(generics.UpdateAPIView):
    queryset = Institution.objects.filter(is_active=False)
    serializer_class = InstitutionSerializer
    detail_serializer_class = InstitutionDetailSerializer
    model_class = Institution
    permission_classes = [IsAuthenticated]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.detail_serializer_class(instance)
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Reactivo logicamente una institución',
                instance=get_object_or_404(self.model_class, pk=int(self.kwargs['pk']))
            )
        return response
