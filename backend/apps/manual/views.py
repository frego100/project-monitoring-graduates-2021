from email import message
from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.response import Response

from apps.history_user.models import HistoryUser
from apps.manual.models import Manual
from apps.manual.serializer import ManualSerializer
# Create your views here.
from apps.manual.permission import ManualPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import (
    OrderingFilter
)
from rest_framework.generics import   RetrieveUpdateDestroyAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
class ManualListView(generics.ListCreateAPIView):
    """
    View for get data from the all users
    """
    serializer_class = ManualSerializer
    queryset = Manual.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'name',
        'is_active'
    ]
    ordering_fields = [
        'name',
        'description'
    ]
    permission_classes = (ManualPermission,)

    def get(self, request, *args, **kwargs):
        response = super(ManualListView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de manuales'
            )
            history.save()
        return response
    
    def post(self, request, *args, **kwargs):
        response = super(ManualListView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            manual = Manual.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un manual ',
                content_object=manual
            )
            history.save()
        return response
class ManualListViewPublic(generics.ListAPIView):
    """
    View for get data from the all users
    """
    serializer_class = ManualSerializer
    queryset = Manual.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'name',
        'is_active'
    ]
    ordering_fields = [
        'name',
        'description'
    ]


class ManualDetailView(RetrieveUpdateDestroyAPIView):
    """
    View for get data detail from the specific user
    """
    serializer_class = ManualSerializer
    queryset = Manual.objects.filter()
    permission_classes = (ManualPermission,)
    
    def delete(self, request, *args, **kwargs):

        manual = get_object_or_404(Manual, pk=kwargs['pk'])
        if manual.is_active:
            manual.is_active = False
            manual.save()
            response= Response( {"message":"Manual removed"},status=status.HTTP_200_OK)
        else:
            response= Response({"message": "Manual is  disabled"}, status=status.HTTP_200_OK)

        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Elimino un manual ',
                content_object=manual
            )
            history.save()
        return response

class ManualReactivateView (RetrieveUpdateAPIView):
    
    serializer_class = ManualSerializer
    queryset = Manual.objects.filter()
    permission_classes = (ManualPermission,)
    def put (self, request, *args, **kwargs):
        manual = get_object_or_404(Manual, pk=kwargs['pk'])
        if not manual.is_active:
            manual.is_active = True
            manual.save()
            response= Response({"message": "Manual enabled successfully"}, status=status.HTTP_200_OK)
        else:
            response=  Response({"message": "Manual is  enabled."}, status=status.HTTP_200_OK)

        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Reactivo un manual',
                content_object=manual
            )
            history.save()
        return response

            