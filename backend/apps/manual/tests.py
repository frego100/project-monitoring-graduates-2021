from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

# Create your tests here.
from apps.manual.models import Manual
from apps.manual.serializer import ManualSerializer


class ManualAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.manual = Manual.objects.create(
            name="Testing Manual",
            description="Description Testing",
            link="https://docs.google.com/spreadsheets/d/10ZFhsC_GZCDJORMSQ8jLjK4r38jJyppT/edit#gid=1605156175",
            is_active=True
        )
        self.manual_deactivated = Manual.objects.create(
            name="Testing Manual deactivated",
            description="Description Testing",
            link="https://docs.google.com/spreadsheets/d/10ZFhsC_GZCDJORMSQ8jLjK4r38jJyppT/edit#gid=1605156175",
            is_active=False
        )

    def test_list_manual(self):
        """
        Test list manual
        """
        response = self.client.get('/manual/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_manual(self):
        """
        Test create manual
        """
        prev_len = len(Manual.objects.all())
        data = {
            "name": "Manual del usuario administrador",
            "description": "Nada",
            "link": "https://clockify.me/tracker",
            "is_active": True
        }

        response = self.client.post('/manual/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(Manual.objects.all()))

    def test_retrieve_manual(self):
        """
        Test retrieve manual
        """
        response = self.client.get('/manual/{}'.format(self.manual.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, ManualSerializer(self.manual).data)

    def test_update_manual(self):
        """
        Test update manual
        """
        data = {
            "name": "Manual del usuario administrador",
            "description": "Nada",
            "link": "https://clockify.me/tracker",
            "is_active": True
        }
        response = self.client.put('/manual/{}'.format(self.manual.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            ManualSerializer(
                Manual.objects.get(id=self.manual.id)
            ).data
        )

    def test_destroy_manual(self):
        """
        Test destroy manual
        """
        prev_len = len(Manual.objects.filter(is_active=True))
        response = self.client.delete('/manual/{}'.format(self.manual.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len - 1, len(Manual.objects.filter(is_active=True)))

    def test_reactive_manual(self):
        """
        Test reactive manual
        """
        prev_len = len(Manual.objects.filter(is_active=True))
        response = self.client.put('/manual/reactivate/{}'.format(self.manual_deactivated.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len + 1, len(Manual.objects.filter(is_active=True)))
