from django.db import models
from .validators import validate_file_extension


# Create your models here.
class Manual(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    description = models.TextField(null=True, blank=True)
    link = models.URLField(null=True, blank=True)
    file = models.FileField(upload_to='pdfs/', null=True, blank=True, validators=[validate_file_extension])
    is_active = models.BooleanField(default=True, null=False)

    class Meta:
        verbose_name = 'Manual'
        verbose_name_plural = 'Manuales'
