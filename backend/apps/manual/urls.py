from django.urls import path
from apps.manual.views import ManualListView, ManualDetailView,ManualReactivateView, ManualListViewPublic

urlpatterns = [
    path('', ManualListView.as_view()),
    path('public/', ManualListViewPublic.as_view()),
    path('<int:pk>', ManualDetailView.as_view()),
    path('reactivate/<int:pk>', ManualReactivateView.as_view())
]