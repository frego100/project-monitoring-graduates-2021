from rest_framework import permissions


class ManualPermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'GET':
            return request.user.has_perm('manual.view_manual')
        if request.method == 'POST':
            return request.user.has_perm('manual.add_manual')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('manual.change_manual')
        if request.method == 'DELETE':
            return request.user.has_perm('manual.delete_manual')
        return False
