from django.shortcuts import render
from rest_framework import status, generics
from .serializer import GraduateLanguageSerializer, GraduateLanguageSerializerAuth
from .models import GraduateLanguage
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from ..user_profile.models import GraduateProfile
from rest_framework.filters import (
    OrderingFilter
)
from .permission import GraduateLanguagePermission
from apps.history_user.models import HistoryUser
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated


# Create your views here.
class LanguageCreateListView(generics.ListCreateAPIView):
    serializer_class = GraduateLanguageSerializer
    queryset = GraduateLanguage.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'level_type',
        'language',
        'graduate_profile_pk',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui',
    ]
    ordering_fields = [
        'language',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui'
    ]
    permission_classes = (GraduateLanguagePermission,)

    def get(self, request, *args, **kwargs):
        response = super(LanguageCreateListView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de GraduateLanguage'
            )
            history.save()
        return response

    def post(self, request, *args, **kwargs):
        response = super(LanguageCreateListView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            language = GraduateLanguage.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un registro en GraduatedLanguage ',
                content_object=language
            )
            history.save()
        return response


class GetAllLanguagesView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        aux = []
        json = {}

        nuevo = GraduateLanguage._meta.get_field('language').choices
        for x, y in nuevo:
            json = {}
            json['code'] = x
            json['name'] = y
            aux.append(json)
        aux.sort(key=lambda row: row['name'])
        return Response(aux, status=200)


class LanguageDetailView(RetrieveUpdateDestroyAPIView):
    """
    """

    serializer_class = GraduateLanguageSerializer
    queryset = GraduateLanguage.objects.all()
    permission_classes = (GraduateLanguagePermission,)

    def get(self, request, *args, **kwargs):
        response = super(LanguageDetailView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de  GraduateLanguage'
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        response = super(LanguageDetailView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            language = GraduateLanguage.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Edito un registro en GraduatedLanguage ',
                content_object=language
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        response = super(LanguageDetailView, self).delete(request)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            history = HistoryUser(
                user=request.user,
                description='Elimino un registro de  GraduateLanguage'
            )
            history.save()
        return response


class LanguageCreateListViewAuth(generics.ListCreateAPIView):
    serializer_class = GraduateLanguageSerializerAuth
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'level_type',
        'language'
    ]
    ordering_fields = [
        'language',
    ]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateLanguage.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def perform_create(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Agrego un nuevo lenguaje   a su propio perfil',
            content_object=instance
        )
        history.save()

    def get(self, request, *args, **kwargs):
        response = super(LanguageCreateListViewAuth, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de sus idiomas en su propio perfil '
            )
            history.save()
        return response


class LanguageDetailAuthView(RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduateLanguageSerializerAuth
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateLanguage.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = super(LanguageDetailAuthView, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un lenguaje  desde su perfi',
                content_object=self.get_object()
            )
            history.save()
        return response

    def perform_update(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Actualizo un lenguaje  en su propio perfil',
            content_object=instance
        )
        history.save()

    def perform_destroy(self, instance):
        instance.delete()
        history = HistoryUser(
            user=self.request.user,
            description='Elimin+o un lenguaje en su proprio perfil '
        )
        history.save()
