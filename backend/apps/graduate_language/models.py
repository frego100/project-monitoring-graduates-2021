from django.db import models

# Create your models here.
from django.utils.translation import ugettext_lazy as _
from apps.user_profile.models import GraduateProfile
from .languages import languages

class GraduateLanguage(models.Model):
    """
       Profile for user graduate language
       """
    level =  (
        (1, 'BASIC'),
        (2, 'INTERMEDIATE'),
        (3, 'ADVANCED'),
    )
    level_type = models.IntegerField(
        choices=level,
        default=1,
        verbose_name=_("Level_Type")
    )
    
    language = models.CharField(
        choices=languages,
        max_length=2,
        null=True,
        verbose_name=_("Language_Type")
    )

    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=True,
        on_delete=models.CASCADE,
        related_name='graduate_profile'
    )
    class Meta:
        verbose_name = 'Lenguaje'
        verbose_name_plural = 'Lenguajes'

   