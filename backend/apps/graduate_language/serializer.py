from rest_framework import serializers
from .models import GraduateLanguage


class GraduateLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateLanguage
        fields = '__all__'
        
    def to_representation(self, instance):
        data = super(GraduateLanguageSerializer, self).to_representation(instance)
        
        if instance.language:
            data['language'] = {
                'code': instance.language,
                'name': instance.get_language_display()
            }
        
        return data

    


class GraduateLanguageSerializerAuth(serializers.ModelSerializer):
    class Meta:
        model = GraduateLanguage
        fields = [
            'id',
            'level_type',
            'language'
        ]
    def to_representation(self, instance):
        data = super(GraduateLanguageSerializerAuth, self).to_representation(instance)
        
        if instance.language:
            data['language'] = {
                'code': instance.language,
                'name': instance.get_language_display()
            }
        
        return data
