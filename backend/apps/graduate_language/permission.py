from rest_framework import permissions


class GraduateLanguagePermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'GET':
            return request.user.has_perm('graduate_language.view_graduatelanguage')
        if request.method == 'POST':
            return request.user.has_perm('graduate_language.add_graduatelanguage')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('graduate_language.change_graduatelanguage')
        if request.method == 'DELETE':
            return request.user.has_perm('graduate_language.delete_graduatelanguage')
        return False
