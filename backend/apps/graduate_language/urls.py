from django.urls import path
from .views import LanguageCreateListView,GetAllLanguagesView,LanguageDetailView, LanguageCreateListViewAuth, LanguageDetailAuthView

urlpatterns = [
    path('', LanguageCreateListView.as_view()),
    path('allLanguages/',GetAllLanguagesView.as_view()),
    path('<int:pk>', LanguageDetailView.as_view()),
    path('auth/', LanguageCreateListViewAuth.as_view()),
    path('auth/<int:pk>', LanguageDetailAuthView.as_view())

]
