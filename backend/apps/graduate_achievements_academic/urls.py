from django.urls import path

from apps.graduate_achievements_academic.views import GraduateAchievementsAcademicCreateListViewAuth, \
    GraduateAchievementsAcademicAuthRetrieveUpdateDestroyView, GradesListAPIView, \
    GraduateAchievementsAcademicCreateListCreateAPIView, GraduateAchievementsAcademicCreateRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('auth/', GraduateAchievementsAcademicCreateListViewAuth.as_view()),
    path('auth/<int:pk>', GraduateAchievementsAcademicAuthRetrieveUpdateDestroyView.as_view()),
    path('', GraduateAchievementsAcademicCreateListCreateAPIView.as_view()),
    path('<int:pk>', GraduateAchievementsAcademicCreateRetrieveUpdateDestroyAPIView.as_view()),
    path('grades/', GradesListAPIView.as_view())
]