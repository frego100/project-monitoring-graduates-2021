from django.apps import AppConfig


class GraduateAchievementsAcademicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.graduate_achievements_academic'
