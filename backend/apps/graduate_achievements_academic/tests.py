from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.
from rest_framework import status
from rest_framework.test import APIClient

from apps.graduate_achievements_academic.models import GraduateAchievementsAcademic
from apps.graduate_achievements_academic.serializers import GraduateAchievementsAcademicSerializer, \
    GraduateAchievementsAcademicAuthSerializer
from apps.identificators.models import Identificator
from apps.user_profile.models import GraduateProfile


class GraduateAchievementsAcademicAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.identificator = Identificator.objects.create(
            code='DNI',
            max_length=8,
            name='DNI',
            alphabet='NUM'
        )
        self.graduate_profile = GraduateProfile.objects.create(
            gender=True,
            cui=20173385,
            date_birthday='2000-04-12',
            web='https://clockify.me/tracker',
            address='Mi casa',
            user=self.user,
            identificator=self.identificator,
            document='72659413',
            cell_phone='987654321',
            phone='987654',
        )
        self.achievement = GraduateAchievementsAcademic.objects.create(
            graduate_profile_pk=self.graduate_profile,
            grade=1,
            title='Testing',
            country="PE"
        )

    def test_list_achievements(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduateAchievementsAcademic.objects.all())
        response = self.client.get('/achievements/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len, len(response.data['results']))

    def test_create_achievements(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduateAchievementsAcademic.objects.all())
        data = {
            "graduate_profile_pk": self.graduate_profile.id,
            "grade": 1,
            "country": "ES",
            "title": "Hola XD"
        }

        response = self.client.post('/achievements/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(GraduateAchievementsAcademic.objects.all()))

    def test_retrieve_achievements(self):
        """
        Test destroy activity
        """
        response = self.client.get('/achievements/{}'.format(self.achievement.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, GraduateAchievementsAcademicSerializer(self.achievement).data)

    def test_update_achievements(self):
        """
        Test destroy activity
        """
        data = {
            "graduate_profile_pk": self.graduate_profile.id,
            "grade": 1,
            "country": "ES",
            "title": "Hola XD"
        }
        response = self.client.put('/achievements/{}'.format(self.achievement.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            GraduateAchievementsAcademicSerializer(
                GraduateAchievementsAcademic.objects.get(id=self.achievement.id)
            ).data
        )

    def test_destroy_achievements(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduateAchievementsAcademic.objects.all())
        response = self.client.delete('/achievements/{}'.format(self.achievement.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(GraduateAchievementsAcademic.objects.all()))

    def test_list_achievements_auth(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduateAchievementsAcademic.objects.filter(graduate_profile_pk__user=self.user))
        response = self.client.get('/achievements/auth/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len, len(response.data['results']))

    def test_create_achievements_auth(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduateAchievementsAcademic.objects.filter(graduate_profile_pk__user=self.user))
        data = {
            "graduate_profile": self.graduate_profile.id,
            "grade": 1,
            "country": "ES",
            "title": "Hola XD"
        }

        response = self.client.post('/achievements/auth/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            prev_len + 1,
            len(GraduateAchievementsAcademic.objects.filter(graduate_profile_pk__user=self.user))
        )

    def test_retrieve_achievements_auth(self):
        """
        Test destroy activity
        """
        response = self.client.get('/achievements/auth/{}'.format(self.achievement.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, GraduateAchievementsAcademicAuthSerializer(self.achievement).data)

    def test_update_achievements_auth(self):
        """
        Test destroy activity
        """
        data = {
            "grade": 1,
            "country": "ES",
            "title": "Hola XD"
        }
        response = self.client.put('/achievements/auth/{}'.format(self.achievement.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            GraduateAchievementsAcademicAuthSerializer(
                GraduateAchievementsAcademic.objects.get(id=self.achievement.id)
            ).data
        )

    def test_destroy_achievements_auth(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduateAchievementsAcademic.objects.filter(graduate_profile_pk__user=self.user))
        response = self.client.delete('/achievements/{}'.format(self.achievement.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            prev_len - 1,
            len(GraduateAchievementsAcademic.objects.filter(graduate_profile_pk__user=self.user))
        )

    def test_types_grades(self):
        """
        Test destroy activity
        """
        response = self.client.get('/achievements/grades/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(GraduateAchievementsAcademic.Grades.choices))
