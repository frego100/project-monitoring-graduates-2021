from django.db import models


# Create your models here.
from django_countries.fields import CountryField

from apps.user_profile.models import GraduateProfile


class GraduateAchievementsAcademic(models.Model):
    class Grades(models.IntegerChoices):
        MASTER = 1, "Maestría"
        DOCTORATE = 2, "Doctorado"
        SPECIALITY = 3, 'Especialidad'

    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=False,
        on_delete=models.CASCADE,
        related_name='graduate_achievements_academic',
        verbose_name='Egresado'
    )
    grade = models.IntegerField(
        choices=Grades.choices,
        null=False,
        verbose_name='Grado Academico'
    )
    title = models.CharField(
        null=True,
        blank=False,
        verbose_name='Titulo',
        max_length=200,
        default=None
    )
    country = CountryField(
        null=False,
        verbose_name="País"
    )

    class Meta:
        verbose_name = 'Logros Academicos'
        verbose_name_plural = 'Logros Academicos'
