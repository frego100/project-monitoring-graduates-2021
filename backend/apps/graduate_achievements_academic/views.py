# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.graduate_achievements_academic.models import GraduateAchievementsAcademic
from apps.graduate_achievements_academic.serializers import GraduateAchievementsAcademicAuthSerializer, \
    GraduateAchievementsAcademicSerializer
from apps.graduate_projects.utils import log_entry_history_user
from apps.history_user.models import HistoryUser
from apps.user_profile.models import GraduateProfile
from monitoring_graduates.permission import ModelPermission


class GraduateAchievementsAcademicCreateListViewAuth(generics.ListCreateAPIView):
    serializer_class = GraduateAchievementsAcademicAuthSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
    ]
    filterset_fields = [
        'grade',
        'country'
    ]

    def get_queryset(self):
        return GraduateAchievementsAcademic.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def perform_create(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Agrego información de logros academicos a su propio perfil',
            content_object=instance
        )
        history.save()

    def get(self, request, *args, **kwargs):
        response = self.list(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
               user=request.user,
               description='Accedio al listado de su información de logros academicos'
            )
            history.save()
        return response


class GraduateAchievementsAcademicAuthRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduateAchievementsAcademicAuthSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduateAchievementsAcademic.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = super(GraduateAchievementsAcademicAuthRetrieveUpdateDestroyView, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de datos de logros academicos desde su perfi',
                content_object=self.get_object()
            )
            history.save()
        return response

    def perform_update(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Actualizo información de logro academico en su propio perfil',
            content_object=instance
        )
        history.save()

    def perform_destroy(self, instance):
        instance.delete()
        history = HistoryUser(
            user=self.request.user,
            description='Eliminación información de logro academico a su propio perfil'
        )
        history.save()


class GradesListAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        data = []
        for choice in GraduateAchievementsAcademic.Grades.choices:
            data.append({
                'id': choice[0],
                'name': choice[1]
            })
        history = HistoryUser(
            user=request.user,
            description='Accedio al listado de grados para logros academicos'
        )
        history.save()
        return Response(data)


class GraduateAchievementsAcademicCreateListCreateAPIView(generics.ListCreateAPIView):
    queryset = GraduateAchievementsAcademic.objects.all()
    serializer_class = GraduateAchievementsAcademicSerializer
    permission_classes = [ModelPermission]
    model_class = GraduateAchievementsAcademic
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]
    filterset_fields = {
        'graduate_profile_pk__cui': ['exact'],       
        'graduate_profile_pk': ['exact'],
        'grade': ['exact'],
        'title': ['exact'],
        'country': ['exact']
    }

    ordering_fields = [
        'id',
        'graduate_profile_pk__cui',
        'grade',
        'title',
        'country'
    ]

    search_fields = [
        'id',
        'graduate_profile_pk__cui',
        'grade',
        'title',
        'country'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Accedio al listado de experiencias de trabajo de los egresados'
            )
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            log_entry_history_user(
                request_user=request.user,
                message='Creo una experiencia de trabajo de egresado',
                instance=get_object_or_404(self.model_class, pk=int(response.data['id']))
            )
        return response


class GraduateAchievementsAcademicCreateRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GraduateAchievementsAcademic.objects.all()
    serializer_class = GraduateAchievementsAcademicSerializer
    permission_classes = [ModelPermission]
    model_class = GraduateAchievementsAcademic

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Recupero una experiencia de trabajo de egresado',
                instance=get_object_or_404(self.model_class, pk=int(self.kwargs['pk']))
            )
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo una una experiencia de trabajo de egresado',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            log_entry_history_user(
                request_user=request.user,
                message='Elimino una experiencia de trabajo de egresado'
            )
        return response
