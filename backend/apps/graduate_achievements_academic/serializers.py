from rest_framework import serializers

from apps.graduate_achievements_academic.models import GraduateAchievementsAcademic
from apps.user_profile.serializer import GraduateProfileSerializer


class GraduateAchievementsAcademicAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateAchievementsAcademic
        fields = ['id', 'grade', 'title', 'country']

    def to_representation(self, instance):
        data = super(GraduateAchievementsAcademicAuthSerializer, self).to_representation(instance)
        if instance.grade:
            data['grade'] = {
                'id': instance.grade,
                'name': GraduateAchievementsAcademic.Grades.labels[instance.grade - 1]
            }
        if instance.country:
            data['country'] = {
                'code': instance.country.code,
                'name': instance.country.name
            }
        return data


class GraduateAchievementsAcademicSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateAchievementsAcademic
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.graduate_profile_pk:
            data['graduate_profile_pk'] = GraduateProfileSerializer(
                instance=instance.graduate_profile_pk,
                context=self.context
            ).data
        if instance.grade:
            data['grade'] = {
                'id': instance.grade,
                'name': GraduateAchievementsAcademic.Grades.labels[instance.grade - 1]
            }
        if instance.country:
            data['country'] = {
                'code': instance.country.code,
                'name': instance.country.name
            }
        return data
