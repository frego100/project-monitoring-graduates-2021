from django.contrib import admin

# Register your models here.
from apps.activity.models import Activity, UserActivity, ObjectiveGroupActivity

admin.site.register(Activity)
admin.site.register(UserActivity)
admin.site.register(ObjectiveGroupActivity)