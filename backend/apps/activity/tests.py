"""
Tests for activity module
"""
from datetime import datetime
from unittest import mock

import pytz
from django.conf import settings
from django.conf.global_settings import TIME_ZONE
from django.contrib.auth.models import User
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from apps.activity.models import Activity, ObjectiveGroupActivity, UserActivity
from apps.activity.serializer import ActivitySerializer, ObjectiveGroupActivitySerializer
from apps.instance.models import Area, Faculty, ProfessionalProgram, Instance


class EnableDisableActivityAPIViewsTestCase(TestCase):

    def setUp(self):
        """
        Settings for test
        """
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.token = Token.objects.get_or_create(user=self.user)
        self.area = Area.objects.get_or_create(name='Ingenieria')
        self.faculty = Faculty.objects.get_or_create(
            name='Ingenierias de Producción y Servicios',
            area_field=self.area[0]
        )
        self.program = ProfessionalProgram.objects.get_or_create(
            name='Ingenierias de Sistemas',
            faculty_field=self.faculty[0]
        )
        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.second_user = User.objects.create_user("segundo_user")
        self.activity_enabled = Activity.objects.create(
            title='',
            description='',
            start_date=datetime.now(pytz.timezone(TIME_ZONE)),
            end_date=datetime.now(pytz.timezone(TIME_ZONE)),
            link='http://www.google.com',
            status=True,
            activity_type=1,
            owner=self.user,
            image=SimpleUploadedFile('small.gif', small_gif, content_type='image/gif'),
            is_validated=True
        )
        self.activity_invalid = Activity.objects.create(
            title='',
            description='',
            start_date=datetime.now(pytz.timezone(TIME_ZONE)),
            end_date=datetime.now(pytz.timezone(TIME_ZONE)),
            link='http://www.google.com',
            status=True,
            activity_type=1,
            owner=self.user,
            image=SimpleUploadedFile('small.gif', small_gif, content_type='image/gif'),
            is_validated=False
        )
        self.activity_disabled = Activity.objects.create(
            title='',
            description='',
            start_date=datetime.now(pytz.timezone(TIME_ZONE)),
            end_date=datetime.now(pytz.timezone(TIME_ZONE)),
            link='http://www.google.com',
            status=False,
            activity_type=1,
            owner=self.second_user,
            # image=SimpleUploadedFile('small.gif', small_gif, content_type='image/gif')
        )
        self.group_objective = ObjectiveGroupActivity.objects.create(
            text_invitation="Texto de invitacion",
            activity=self.activity_enabled,
            instance=Instance.objects.get(id=self.program[0].id)
        )
        self.user_activity = UserActivity.objects.create(
            activity=self.activity_enabled,
            user=self.second_user
        )

    def test_activity_public(self):
        """
        Test case when it is public
        """
        response = self.client.get('/activity/public/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_activity_does_not_exist(self):
        """
        Test case when the activity doesn't exists
        """
        response = self.client.delete('activity/30')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_activity_enable(self):
        """
        Test case when the user wants disable a enabled activity
        """
        previous_count = len(Activity.objects.filter(status=True))
        self.client.force_authenticate(self.user)
        response = self.client.delete('/activity/{}'.format(self.activity_enabled.id))
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK
        )
        self.assertEqual(
            len(Activity.objects.filter(status=True)),
            previous_count - 1
        )

    def test_notify_activity(self):
        """
        Test for notify by email, by user
        """
        response = self.client.post('/activity/notify/', data={
            'emails': [
                'nelsonafp12@gmail.com'
            ],
            'activity': self.activity_enabled.id
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_notify_group(self):
        """
        Test for notify by group
        """
        data = {
            "group_objective_program": [self.program[0].id],
            "group_objective_faculty": [self.faculty[0].id],
            "group_objective_area": [self.area[0].id],
            "activity": self.activity_enabled.id
        }
        response = self.client.post('/activity/notify-group/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_notify_user_activity(self):
        """
        Test for notify by user activity
        """
        data = {
            "activity": self.activity_enabled.id,
            "text": "Soy un texto muy importante"
        }
        response = self.client.post('/activity/notify-user-activity/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_activity(self):
        """
        Test retrieve activity
        """
        response = self.client.get('/activity/{}'.format(self.activity_disabled.id))
        serializer = ActivitySerializer(instance=self.activity_disabled)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_update_activity(self):
        """
        Test retrieve activity
        """
        data = {
            "title": "title",
            "description": "description",
            "start_date": "2021-10-22T14:04:19-05:00",
            "end_date": "2021-10-22T14:04:21-05:00",
            "link": "https://gitlab.com/frego100/project-monitoring-graduates-2021/-/wikis/Actividades",
            "meet_link": "https://gitlab.com/frego100/project-monitoring-graduates-2021/-/wikis/Actividades",
            "stream_link": "https://gitlab.com/frego100/project-monitoring-graduates-2021/-/wikis/Actividades",
            "activity_type": 1,
            "status": True,
            "is_validated": False,
            "can_register": False
        }

        response = self.client.put('/activity/{}'.format(self.activity_disabled.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_destroy_activity(self):
        """
        Test destroy activity
        """
        prev_len = len(Activity.objects.filter(status=True))
        response = self.client.delete('/activity/{}'.format(self.activity_enabled.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len - 1, len(Activity.objects.filter(status=True)))

    def test_reactivate_activity(self):
        """
        Test destroy activity
        """
        prev_len = len(Activity.objects.filter(status=True))
        response = self.client.put('/activity/reactivate/{}'.format(self.activity_disabled.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len + 1, len(Activity.objects.filter(status=True)))

    def test_validate_activity(self):
        """
        Test destroy activity
        """
        prev_len = len(Activity.objects.filter(is_validated=True))
        response = self.client.put('/activity/validate/{}'.format(self.activity_invalid.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len + 1, len(Activity.objects.filter(is_validated=True)))

    def test_invalidate_activity(self):
        """
        Test invalidate activity
        """
        prev_len = len(Activity.objects.filter(is_validated=True))
        response = self.client.put('/activity/invalidate/{}'.format(self.activity_enabled.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len - 1, len(Activity.objects.filter(is_validated=True)))

    def test_group_objective_activity_list(self):
        """
        Test List Group Objective for Activity
        """
        response = self.client.get('/activity/group_objective/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            ObjectiveGroupActivitySerializer(ObjectiveGroupActivity.objects.all(), many=True).data,
            response.data['results']
        )

    def test_group_objective_activity_create(self):
        """
        Test Create Group Objective for Activity
        """
        data = {
            "text_invitation": "Texto de inviacion",
            "activity": self.activity_enabled.id,
            "instance": self.faculty[0].id
        }
        prev_len = len(ObjectiveGroupActivity.objects.all())
        response = self.client.post('/activity/group_objective/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(ObjectiveGroupActivity.objects.all()), prev_len)

    def test_list_activity_type(self):
        """
        Test List Group Objective for Activity
        """
        response = self.client.get('/activity/types/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(Activity.Types.choices))

    def test_list_activity_user(self):
        """
        Test List Group Objective for Activity
        """
        response = self.client.get('/activity/user/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_activity_user(self):
        """
        Test Unroll User for Activity when it can't register
        """
        self.activity_enabled.can_register = True
        self.activity_enabled.save()
        data = {
            "activity": self.activity_enabled.id,
            "user": self.user.id
        }
        response = self.client.get('/activity/user/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_validate_activity_email(self):
        """
        Test Unroll User for Activity when it can't register
        """
        data = {
            "is_validated": True,
            "justification": "Testing"
        }
        response = self.client.post('/activity/validate_email/{}'.format(self.activity_invalid.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_my_activities(self):
        """
        Test Unroll User for Activity when it can't register
        """
        response = self.client.get('/activity/my/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), len(Activity.objects.filter(owner=self.user)))

    def test_list_activities_to_validate(self):
        """
        Test Unroll User for Activity when it can't register
        """
        response = self.client.get('/activity/to_validate/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), len(Activity.objects.filter(is_validated=False)))

    def test_unroll_user_activity(self):
        """
        Test Unroll User for Activity when it can't register
        """
        prev_len = len(UserActivity.objects.filter(is_active=True))
        response = self.client.delete('/activity/delete/enroll/{}'.format(self.user_activity.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len-1, len(UserActivity.objects.filter(is_active=True)))

    def test_enroll_user_activity_auth(self):
        """
        Test Unroll User for Activity when it can't register
        """
        self.activity_enabled.can_register = True
        self.activity_enabled.save()
        prev_len = len(UserActivity.objects.filter(is_active=True))
        response = self.client.post('/activity/enroll/{}'.format(self.activity_enabled.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len+1, len(UserActivity.objects.filter(is_active=True)))

    def test_retrieve_my_activities(self):
        """
        Test Unroll User for Activity when it can't register
        """
        response = self.client.get('/activity/my/{}'.format(self.activity_enabled.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_my_activities_no_belonging(self):
        """
        Test Unroll User for Activity when it can't register
        """
        response = self.client.get('/activity/my/{}'.format(self.activity_disabled.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
