"""
Views About Module Activity
"""
from django.contrib.auth.models import User
from django.http import QueryDict
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.activity.models import Activity, ObjectiveGroupActivity, UserActivity
from apps.activity.serializer import (
    ActivitySerializer,
    ActivityNotifySerializer,
    ObjectiveGroupActivitySerializer,
    UserActivitySerializer,
    ActivityLiteSerializer,
    EnrollUserActivitySerializer,
    ActivityNotifyGroupSerializer,
    ActivityUserNotifySerializer,
    MyActivitySerializer,
    ActivityUserMultipleSerializer
)
from apps.email_sender.utils import EmailSender
from apps.graduate_data_academic.models import GraduateDataAcademic
from apps.history_user.models import HistoryUser
from apps.instance.models import ProfessionalProgram, Faculty
from monitoring_graduates.permission import ModelPermission

"""
    Para todas las EPs de actividades se aplican permisos
    - Permisos basados en roles (group) [POST, PUT, PATCH, DELETE] DjangoModelPermission
        and view permissions (ModelPermission) [HEAD, GET, OPTIONS]
"""


class ActivityRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API View for list and create a activity
    """
    permission_classes = [ModelPermission]
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            activity = get_object_or_404(Activity, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Recupero el detalle de una actividad',
                content_object=activity
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = True
        request.data['owner'] = self.get_object().owner.id
        request.data['updated_by'] = request.user.id
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            activity = get_object_or_404(Activity, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito una actividad',
                content_object=activity
            )
            history.save()
        return response

    def patch(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = True
        request.data['owner'] = self.get_object().owner.id
        request.data['updated_by'] = request.user.id
        response = super().patch(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            activity = get_object_or_404(Activity, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito una actividad',
                content_object=activity
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for enable and disable activity
        """
        activity = self.get_object()
        activity.status = False
        activity.updated_by = request.user
        activity.save()
        history = HistoryUser(
            user=request.user,
            description='Borro de manera logica una actividad',
            content_object=activity
        )
        history.save()
        return Response(
            self.serializer_class(activity).data,
            status=status.HTTP_200_OK
        )


class ActivityListApiView(generics.ListCreateAPIView):
    """
    list Create Api View activity with filters
    """
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^title',
        '^description'
    ]
    filterset_fields = {
        'status': ['exact'],  # Con esto pueden filtrar actividades habilitadas o no habilitadas
        'is_validated': ['exact'],
        'start_date': ['gte'],
        'end_date': ['lte'],
    }
    serializer_class = ActivitySerializer
    queryset = Activity.objects.all()

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de usuarios'
            )
            history.save()
        return response

    def post(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = True
        request.data['owner'] = request.user.id
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            user = get_object_or_404(Activity, pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo una nueva actividad',
                content_object=user
            )
            history.save()
        return response


class ActivityListViewPublic(generics.ListAPIView):
    """
    View for get data from the all users
    """
    serializer_class = ActivitySerializer
    queryset = Activity.objects.filter(
        status=True,
        is_validated=True
    )
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^title',
        '^description'
    ]
    filterset_fields = {
        'start_date': ['gte'],
        'end_date': ['lte'],
    }


class NotifyActivityAPIView(generics.CreateAPIView):
    """
    View for notify activity
    """
    permission_classes = [ModelPermission]
    serializer_class = ActivityNotifySerializer
    queryset = Activity.objects.all()

    def post(self, request):
        """
        POST method for view
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        activity = data['activity']
        template_name = 'notify_activity.html'
        sender = EmailSender()
        subject = 'INVITACIÓN A ' + activity.title
        emails = data['emails']
        data = {
            'activity': activity
        }
        sender.send_email(
            subject,
            emails,
            template_name,
            data,
            file=activity.image.path
        )
        history = HistoryUser(
            user=request.user,
            description='Envio una notificación de una actividad.'
        )
        history.save()
        return Response(
            status=status.HTTP_200_OK
        )


class NotifyUserActivityAPIView(generics.CreateAPIView):
    """
    View for notify activity
    """
    permission_classes = [ModelPermission]
    serializer_class = ActivityUserNotifySerializer
    queryset = UserActivity.objects.all()

    def post(self, request):
        """
        POST method for view
        """
        if not request.user.has_perm('activity.validate_activity'):
            return Response({
                'Error': 'Sin permisos de validación no pueden enviarse correos'},
                status=status.HTTP_401_UNAUTHORIZED
            )
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        activity = data['activity']
        text = data['text']
        template_name = 'notify_activity_information.html'
        sender = EmailSender()
        subject = 'NOTIFICACIÓN DE LA ACTIVIDAD: ' + activity.title
        users = UserActivity.objects.filter(activity=activity)
        emails = list(set(i for i in users.values_list('user__email', flat=True) if bool(i)))
        data = {
            'activity': activity,
            'text': text
        }
        sender.send_email(
            subject,
            emails,
            template_name,
            data,
            file=activity.image.path
        )
        history = HistoryUser(
            user=request.user,
            description='Envio una notificación de una actividad.'
        )
        history.save()
        return Response(
            status=status.HTTP_200_OK,
            data={
                "message": "Correos enviados"
            }
        )


class NotifyActivityGroupAPIView(generics.CreateAPIView):
    """
    View for notify activity
    """
    permission_classes = [ModelPermission]
    serializer_class = ActivityNotifyGroupSerializer
    queryset = ObjectiveGroupActivity.objects.all()

    def post(self, request):
        """
        POST method for view
        """
        if not request.user.has_perm('activity.validate_activity'):
            return Response({
                'Error': 'Sin permisos de validación no pueden enviarse correos'},
                status=status.HTTP_401_UNAUTHORIZED
            )
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        group_objective_program = data['group_objective_program']
        group_objective_faculty = data['group_objective_faculty']
        group_objective_area = data['group_objective_area']
        activity = data['activity']
        if len(group_objective_program) > 0 or len(group_objective_faculty) > 0 or len(group_objective_area) > 0:
            recipients_program = []
            recipients_faculty = []
            recipients_area = []
            if len(group_objective_program) > 0:
                try:
                    for obj_instance in set(group_objective_program):
                        graduate_academic = GraduateDataAcademic.objects.filter(profesional_program_pk=obj_instance)
                        recipients_program.extend(list(set(
                            i for i in graduate_academic.values_list('graduate_profile_pk__user__email', flat=True) if
                            bool(i))))
                except GraduateDataAcademic.DoesNotExist:
                    pass
            if len(group_objective_faculty) > 0:
                try:
                    for obj_instance in set(group_objective_faculty):
                        for instance in ProfessionalProgram.objects.filter(faculty_field=obj_instance):
                            graduate_academic = GraduateDataAcademic.objects.filter(profesional_program_pk=instance.id)
                            recipients_faculty.extend(list(set(
                                i for i in graduate_academic.values_list('graduate_profile_pk__user__email', flat=True)
                                if bool(i))))
                except GraduateDataAcademic.DoesNotExist:
                    pass
            if len(group_objective_area) > 0:
                try:
                    for obj_instance in set(group_objective_area):
                        for faculty in Faculty.objects.filter(area_field=obj_instance):
                            for instance in ProfessionalProgram.objects.filter(faculty_field=faculty.id):
                                graduate_academic = GraduateDataAcademic.objects.filter(
                                    profesional_program_pk=instance.id)
                                recipients_area.extend(list(set(i for i in graduate_academic.values_list(
                                    'graduate_profile_pk__user__email', flat=True) if bool(i))))
                except GraduateDataAcademic.DoesNotExist:
                    pass
            recipients = list(set(recipients_program + recipients_area + recipients_faculty))
            template_name = 'notify_group_activity.html'
            sender = EmailSender()
            subject = 'INVITACIÓN A ' + activity.title
            emails = recipients
            g_objective = ObjectiveGroupActivity.objects.filter(activity=activity.id)
            if len(g_objective) > 0:
                data = {
                    'activity': activity,
                    'text_group': g_objective.first().text_invitation,
                }
                sender.send_email(
                    subject,
                    emails,
                    template_name,
                    data,
                    file=activity.image.path
                )
                history = HistoryUser(
                    user=request.user,
                    description='Envio una notificación de una actividad.'
                )
                history.save()
                return Response(
                    status=status.HTTP_200_OK,
                    data={
                        "message": "Correos enviados"
                    }
                )
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data={
                        "message": "Esta actividad no tiene grupos objetivo."
                    }
                )
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "message": "No se ha proporcionado instancias."
                }
            )


class ActivityReactivateAPIView(generics.UpdateAPIView):
    """
    API View class for reactivate a activity
    """
    permission_classes = [ModelPermission]
    queryset = Activity.objects.filter(status=False)
    serializer_class = ActivitySerializer

    def update(self, request, *args, **kwargs):
        """
        PUT or PATCH method for API View class
        """
        activity = self.get_object()
        activity.status = True
        activity.save()
        history = HistoryUser(
            user=request.user,
            description='Habilito de manera logica una actividad',
            content_object=activity
        )
        history.save()
        return Response(
            self.serializer_class(activity).data,
            status=status.HTTP_200_OK
        )


class ActivityValidatedApiView(generics.UpdateAPIView):
    serializer_class = ActivitySerializer
    queryset = Activity.objects.filter(is_validated=False)
    permission_classes = [ModelPermission]

    def put(self, request, *args, **kwargs):
        activity = Activity.objects.get(pk=int(kwargs['pk']))
        activity.is_validated = True
        activity.save()
        serializer = self.serializer_class(
            instance=activity,
            context=self.get_serializer_context()
        )
        history = HistoryUser(
            user=request.user,
            description='Se valido una actividad',
            content_object=activity
        )
        history.save()
        return Response(
            serializer.data,
            status=status.HTTP_200_OK
        )


class ActivityInvalidatedApiView(generics.UpdateAPIView):
    permission_classes = [ModelPermission]
    queryset = Activity.objects.filter(is_validated=True)
    serializer_class = ActivitySerializer

    def put(self, request, *args, **kwargs):
        activity = Activity.objects.get(pk=int(kwargs['pk']))
        activity.is_validated = False
        activity.save()
        serializer = self.serializer_class(
            instance=activity,
            context=self.get_serializer_context()
        )
        history = HistoryUser(
            user=request.user,
            description='Se invalido una actividad',
            content_object=activity
        )
        history.save()
        return Response(
            serializer.data,
            status=status.HTTP_200_OK
        )

    def patch(self, request, *args, **kwargs):
        return self.put(request, *args, **kwargs)


class ObjectiveGroupActivityListApiView(generics.ListCreateAPIView):
    """
    list Create Api View activity with filters
    """
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
    ]
    filterset_fields = [
        'activity'
    ]
    serializer_class = ObjectiveGroupActivitySerializer
    queryset = ObjectiveGroupActivity.objects.all()

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de grupos objetivos de actividad'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        instances = serializer.save()
        for instance in instances:
            history = HistoryUser(
                user=self.request.user,
                description='Creo una nueva actividad',
                content_object=instance
            )
            history.save()


class ActivityTypesListAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        history = HistoryUser(
            user=request.user,
            description='Accedio al listado de tipos de actividades'
        )
        history.save()

        data = []
        for choice in Activity.Types.choices:
            data.append({
                'id': choice[0],
                'name': choice[1]
            })

        return Response(data)

        # return Response(Activity.Types.choices)


class UserActivityListCreate(generics.ListCreateAPIView):
    queryset = UserActivity.objects.filter(is_active=True)
    serializer_class = UserActivitySerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
    ]
    filterset_fields = [
        'activity'
    ]

    def perform_create(self, serializer):
        data = serializer.validated_data
        if not data['activity'].can_register:
            raise ValidationError({'detail': 'Esta actividad no permite inscripciones'})
        if len(UserActivity.objects.filter(activity=data['activity'], user=data['user'])) > 0:
            raise ValidationError({'detail': 'El usuario fue inscrito en esta actividad previamente'})
        instance = serializer.save()
        history = HistoryUser(
            user=self.request.user,
            description='Inscribio a un usuario en una actividad',
            content_object=instance
        )
        history.save()

    def get(self, request, *args, **kwargs):
        history = HistoryUser(
            user=self.request.user,
            description='Reviso el listado de inscripciones'
        )
        history.save()
        return self.list(request, *args, **kwargs)


class UserActivityInscriptionMultiple(generics.CreateAPIView):
    """
    View for create multiple activity user
    """
    permission_classes = [ModelPermission]
    serializer_class = ActivityUserMultipleSerializer
    queryset = UserActivity.objects.filter(is_active=True)

    def post(self, request, **kwargs):
        """
        POST method for view
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        activity = data['activity']
        for user in data['users']:
            if len(UserActivity.objects.filter(activity=activity, user=user)) == 0:
                UserActivity(activity=activity, user=user).save()
        self.perform_create(serializer)
        return Response(status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        HistoryUser(
            user=self.request.user,
            description='Inscribio usuarios en una actividad',
        ).save()


class ActivityValidatedEmailApiView(APIView):
    """
    View for notify activity
    """
    serializer_class = ActivitySerializer
    queryset = Activity.objects.all()

    def post(self, request, **kwargs):
        if not request.user.has_perm('activity.validate_activity'):
            return Response({
                'Error': 'No tiene permiso para validar actividades'},
                status=status.HTTP_401_UNAUTHORIZED
            )
        activity = Activity.objects.get(pk=int(kwargs['pk']))
        activity.is_validated = request.data['is_validated']
        activity.save()
        serializer = self.serializer_class(
            instance=activity
        )
        template_name = 'validate_activity.html'
        sender = EmailSender()
        subject = 'VALIDACIÓN DE ACTIVIDAD: ' + activity.title
        aux = 'ACEPTO' if activity.is_validated == True else 'RECHAZO'
        emails = {activity.owner.email}

        data = {
            'activity': activity,
            'is_validated': aux,
            'justification': request.data['justification']
        }
        sender.send_email(
            subject,
            emails,
            template_name,
            data
        )
        history = HistoryUser(
            user=request.user,
            description='Se valido una actividad',
            content_object=activity
        )
        history.save()
        return Response(
            status=status.HTTP_200_OK
        )


class MyActivityListAPIView(generics.ListAPIView):
    serializer_class = ActivitySerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^title',
        '^description'
    ]
    filterset_fields = {
        'status': ['exact'],  # Con esto pueden filtrar actividades habilitadas o no habilitadas
        'is_validated': ['exact'],
        'start_date': ['gte'],
        'end_date': ['lte'],
    }

    def get_queryset(self):
        return Activity.objects.filter(
            owner=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = self.list(request, args, kwargs)
        history = HistoryUser(
            user=request.user,
            description='Accedio al listado de sus actividades'
        )
        history.save()

        return response


class ActivityToValidateListApiView(generics.ListAPIView):
    """
    list Create Api View activity with filters
    """
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    ]
    search_fields = [
        '^title',
        '^description'
    ]
    filterset_fields = {
        'status': ['exact'],  # Con esto pueden filtrar actividades habilitadas o no habilitadas
        'is_validated': ['exact'],
        'start_date': ['gte'],
        'end_date': ['lte'],
    }
    serializer_class = ActivitySerializer
    queryset = Activity.objects.filter(is_validated=False)

    def get(self, request, *args, **kwargs):
        if not request.user.has_perm('activity.validate_activity'):
            return Response({
                'Error': 'No tiene permiso para validar actividades'},
                status=status.HTTP_401_UNAUTHORIZED
            )
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de actividades que faltan por validar'
            )
            history.save()
        return response


class UserActivityDestroyAPIView(generics.DestroyAPIView):
    queryset = UserActivity.objects.filter(is_active=True)
    serializer_class = UserActivitySerializer
    permission_classes = [ModelPermission]

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            # log_entry_history_user(
            # request_user=request.user,
            # message='Elimino la inscripción de un usuario a una actividad.',
            # instance=get_object_or_404(UserActivity, pk=self.kwargs['pk'])
            # )
            return Response({"message": "Inscripción eliminada"}, status=status.HTTP_200_OK)
        return response

    # def perform_destroy(self, instance):
    #  instance.is_active = False
    #  instance.save()


class EnrollInActivityAPIView(generics.CreateAPIView):
    queryset = Activity.objects.filter(status=True, can_register=True)
    serializer_class = EnrollUserActivitySerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        if UserActivity.objects.filter(user=request.user, activity=self.get_object()).count() == 0:
            instance = UserActivity.objects.create(user=request.user, activity=self.get_object())
            serializer = self.get_serializer(instance)
            response = Response(serializer.data)
            if response.status_code == status.HTTP_200_OK and \
                    isinstance(request.user, User):
                history = HistoryUser(
                    user=request.user,
                    description='Se inscribio a una actividad',
                    content_object=instance
                )
                history.save()
            return response
        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={
                    "message": "Usted ya esta registrado en esta actividad."
                }
            )


class MyActivityDetailAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = MyActivitySerializer
    permission_classes = [ModelPermission]

    def get_queryset(self):
        return Activity.objects.filter(
            owner=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio a una de sus actividades'
            )
            history.save()

        return response

    def put(self, request, *args, **kwargs):
        activity = get_object_or_404(Activity, pk=kwargs['pk'])
        if activity.is_validated:
            response = Response({"message": "No puedes editar una actividad validada"}, status=status.HTTP_200_OK)
        else:
            response = super(MyActivityDetailAPIView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Edito su propia actividad',
                content_object=activity
            )
            history.save()
        return response


class GroupObjectiveDestroyAPIView(generics.DestroyAPIView):
    permission_classes = [ModelPermission]
    serializer_class = ObjectiveGroupActivitySerializer
    queryset = ObjectiveGroupActivity.objects.all()
