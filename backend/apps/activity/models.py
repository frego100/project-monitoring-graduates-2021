from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from apps.instance.models import Instance


class Activity(models.Model):
    class Types(models.IntegerChoices):
        WORKSHOP = 1, "Taller"
        WORKSHOP_COURSE = 2, "Curso Taller"
        WEBINAR = 3, "Webinar"
        WEBINAR_CYCLE = 4, "Ciclo de Webinars"
        CONFERENCE = 5, "Conferencia"
        JOB_FAIR = 6, "Feria Laboral"
        TRAINING = 7, "Capacitación"
        CONVERSATION = 8, "Conversatorio"
        WORKSHOP_CYCLE = 9, "Ciclo de talleres"
        SEMINAR = 10, "Seminario"
        SMALL_TALK = 11, "Charla"
        MEETING_OF_GRADUATES = 12, "Encuentro de egresados"
        COURSE = 13, "Curso"
        POLL = 14, "Encuesta"
        INTERVIEW = 15, "Entrevistas"
        ANNOUNCEMENT = 16, "Convocatoria"
        NOTICE = 17, "Anuncio"
        OTHERS = 18, "Otros"

    title = models.CharField(
        blank=False,
        null=False,
        max_length=255
    )
    description = models.CharField(
        blank=True,
        null=False,
        max_length=5000
    )
    start_date = models.DateTimeField()

    end_date = models.DateTimeField()

    link = models.URLField(
        default=None,
        blank=True,
        null=True
    )

    meet_link = models.URLField(
        default=None,
        blank=True,
        null=True
    )

    stream_link = models.URLField(
        default=None,
        blank=True,
        null=True
    )

    activity_type = models.IntegerField(
        choices=Types.choices,
        null=False
    )

    image = models.ImageField(
        default=None,
        upload_to='activity/image/',
        null=True
    )

    status = models.BooleanField(
        null=False,
        default=True
    )

    is_validated = models.BooleanField(
        null=False,
        default=False
    )

    can_register = models.BooleanField(
        null=False,
        default=False
    )

    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='activity_owner_user',
    )

    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='activity_updated_user',
        null=True
    )

    created_at = models.DateTimeField(
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        auto_now=True,
        null=True
    )

    def delete(self, using=None, keep_parents=False):
        self.status = False
        self.save()

    def __str__(self):
        return f'{type(self).__name__}({", ".join("%s=%s" % item for item in vars(self).items() if item[0][0] != "_")})'

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'
        permissions = (
            ("validate_activity", "Puede validar una actividad"),
        )


class ObjectiveGroupActivity(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, null=False)
    instance = models.ForeignKey(Instance, on_delete=models.CASCADE, null=False)
    text_invitation = models.TextField(null=False, blank=False)

    class Meta:
        verbose_name = "Grupo Objetivo de Actividades"
        verbose_name_plural = "Grupos Objetivo de Actividades"


class UserActivity(models.Model):
    activity = models.ForeignKey(
        Activity,
        related_name='subscribers',
        null=False,
        verbose_name='Actividad',
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User,
        related_name='subscriptions',
        null=False,
        verbose_name='Usuario',
        on_delete=models.CASCADE
    )
    is_active = models.BooleanField(
        default=True,
        null=False,
        verbose_name='Activo'
    )

    @staticmethod
    def get_graduate_with_inscription_activity(activity):
        graduate_users = []
        for graduate in UserActivity.objects.filter(activity=activity):
            if graduate.user.pk not in graduate_users:
                graduate_users.append(graduate.user.pk)
        return graduate_users

    class Meta:
        verbose_name = 'Inscripción'
        verbose_name_plural = 'Inscripciones'
