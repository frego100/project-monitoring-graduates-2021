"""
Serializer for group module
"""
from rest_framework import serializers

from apps.activity.models import Activity, ObjectiveGroupActivity, UserActivity
from apps.instance.models import Area, Faculty, ProfessionalProgram
from django.contrib.auth.models import User
from apps.instance.serializers import AreaSerializer, FacultySerializer, ProfessionalProgramSerializer
from apps.user.serializer import UserMiniSerializer, UserLiteSerializer


class ActivitySerializer(serializers.ModelSerializer):
    """
    Serializer for Group model
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = Activity
        fields = '__all__'

    def to_representation(self, instance):
        data = super(ActivitySerializer, self).to_representation(instance)
        if instance.owner:
            data['owner'] = UserMiniSerializer(instance.owner, context=self.context).data
        if instance.activity_type:
            data['activity_type'] = {
                'id': instance.activity_type,
                'name': Activity.Types.labels[instance.activity_type - 1]
            }
        if instance.updated_by:
            data['updated_by'] = UserMiniSerializer(instance.updated_by, context=self.context).data
        return data


class ActivityLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = '__all__'


class ActivityNotifySerializer(serializers.Serializer):
    """
    Serializer for notify activties
    """
    emails = serializers.ListField(child=serializers.EmailField())
    activity = serializers.IntegerField(required=True)

    def validate_activity(self, data):
        """
        Validator for activity id
        """
        try:
            activity_object = Activity.objects.get(pk=data)
            return activity_object
        except Activity.DoesNotExist:
            raise serializers.ValidationError("Actividad no existe")


class ActivityUserNotifySerializer(serializers.Serializer):
    """
    Serializer for notify activties
    """
    activity = serializers.IntegerField(required=True)
    text = serializers.CharField(required=True)

    def validate_activity(self, data):
        """
        Validator for activity id
        """
        try:
            activity_object = Activity.objects.get(pk=data)
            return activity_object
        except Activity.DoesNotExist:
            raise serializers.ValidationError("Actividad no existe")


class ActivityNotifyGroupSerializer(serializers.Serializer):
    """
    Serializer for notify activities in group
    """
    group_objective_program = serializers.ListField(child=serializers.IntegerField(), required=True)
    group_objective_faculty = serializers.ListField(child=serializers.IntegerField(), required=True)
    group_objective_area = serializers.ListField(child=serializers.IntegerField(), required=True)
    activity = serializers.IntegerField(required=True)

    def validate_activity(self, data):
        """
        Validator for activity id
        """
        try:
            activity_object = Activity.objects.get(pk=data)
            return activity_object
        except Activity.DoesNotExist:
            raise serializers.ValidationError("Actividad no existe")


class ActivityUserMultipleSerializer(serializers.Serializer):
    """
    Serializer for create multiple user in activity
    """
    users = serializers.ListField(child=serializers.IntegerField(), required=True)
    activity = serializers.IntegerField(required=True)

    def validate_activity(self, data):
        """
        Validator for activity id
        """
        try:
            activity_object = Activity.objects.get(pk=data, can_register=True)
            return activity_object
        except Activity.DoesNotExist:
            raise serializers.ValidationError("Actividad no permite inscripciones")

    def validate_users(self, data):
        """
        Validator for list users-id
        """
        list_users = []
        for user in list(set(data)):
            try:
                user_aux = User.objects.get(id=user)
                list_users.append(user_aux)
            except User.DoesNotExist:
                pass
        if len(list_users) == 0:
            raise serializers.ValidationError("Sin usuarios para registrar")
        return list_users


class ObjectiveGroupActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjectiveGroupActivity
        fields = '__all__'

    def to_representation(self, instance):
        data = super(ObjectiveGroupActivitySerializer, self).to_representation(instance)
        if instance.instance:
            data['instance'] = self.get_instance_serializer(instance.instance).data
        return data

    def get_instance_serializer(self, instance):
        if len(Area.objects.filter(id=instance.id)):
            return AreaSerializer(Area.objects.get(pk=instance.id))
        if len(Faculty.objects.filter(id=instance.id)):
            return FacultySerializer(Faculty.objects.get(pk=instance.id))
        if len(ProfessionalProgram.objects.filter(id=instance.id)):
            return ProfessionalProgramSerializer(ProfessionalProgram.objects.get(pk=instance.id))


class UserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserActivity
        fields = '__all__'

    def to_representation(self, instance):
        data = super(UserActivitySerializer, self).to_representation(instance)
        if instance.user:
            data['user'] = UserMiniSerializer(instance.user, context=self.context).data
        return data


class EnrollActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        exclude = ['status', 'is_validated', 'can_register', 'owner', 'updated_by', 'created_at', 'updated_at']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.activity_type:
            data['activity_type'] = {
                'id': instance.activity_type,
                'name': Activity.Types.labels[instance.activity_type - 1]
            }
        return data


class EnrollUserActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserActivity
        fields = '__all__'

    def to_representation(self, instance):
        data = super(EnrollUserActivitySerializer, self).to_representation(instance)
        if instance.user:
            data['user'] = UserLiteSerializer(
                instance=instance.user,
                context=self.context
            ).data
        if instance.activity:
            data['activity'] = EnrollActivitySerializer(
                instance=instance.activity,
                context=self.context
            ).data
        return data


class MyActivitySerializer(serializers.ModelSerializer):
    """
    Serializer for Group model
    """
    is_validated = serializers.BooleanField(read_only=True)
    status = serializers.BooleanField(read_only=True)

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = Activity
        fields = [
            'title',
            'description',
            'start_date',
            'end_date',
            'link',
            'meet_link',
            'stream_link',
            'activity_type',
            'image',
            'status',
            'is_validated',
            'can_register',
            'updated_by',
            'created_at',
            'updated_at'
        ]

    def to_representation(self, instance):
        data = super(MyActivitySerializer, self).to_representation(instance)
        if instance.owner:
            data['owner'] = UserMiniSerializer(instance.owner, context=self.context).data
        if instance.activity_type:
            data['activity_type'] = {
                'id': instance.activity_type,
                'name': Activity.Types.labels[instance.activity_type - 1]
            }
        if instance.updated_by:
            data['updated_by'] = UserMiniSerializer(instance.updated_by, context=self.context).data
        return data
