"""
Urls for group module
"""
from django.urls import path

from apps.activity.views import (
    ActivityRetrieveUpdateDestroyAPIView,
    ActivityListApiView,
    NotifyActivityAPIView,
    ActivityReactivateAPIView,
    ActivityValidatedApiView,
    ActivityInvalidatedApiView,
    ActivityListViewPublic,
    ObjectiveGroupActivityListApiView,
    ActivityTypesListAPIView,
    UserActivityListCreate,
    ActivityValidatedEmailApiView,
    MyActivityListAPIView,
    ActivityToValidateListApiView,
    UserActivityDestroyAPIView,
    EnrollInActivityAPIView,
    NotifyActivityGroupAPIView,
    NotifyUserActivityAPIView,
    MyActivityDetailAPIView,
    UserActivityInscriptionMultiple, GroupObjectiveDestroyAPIView
)

urlpatterns = [
    path('', ActivityListApiView.as_view()),
    path('public/', ActivityListViewPublic.as_view()),
    path('notify/', NotifyActivityAPIView.as_view(), name='notify'),
    path('notify-group/', NotifyActivityGroupAPIView.as_view(), name='notify-group'),
    path('notify-user-activity/', NotifyUserActivityAPIView.as_view(), name='notify-user-activity'),
    path('<int:pk>', ActivityRetrieveUpdateDestroyAPIView.as_view(), name='retrieve'),
    path('reactivate/<int:pk>', ActivityReactivateAPIView.as_view(), name='reactivate'),
    path('validate/<int:pk>', ActivityValidatedApiView.as_view()),
    path('invalidate/<int:pk>', ActivityInvalidatedApiView.as_view()),
    path('group_objective/', ObjectiveGroupActivityListApiView.as_view()),
    path('group_objective/<pk>', GroupObjectiveDestroyAPIView.as_view()),
    path('types/', ActivityTypesListAPIView.as_view()),
    path('user/', UserActivityListCreate.as_view()),
    path('user-multiple/', UserActivityInscriptionMultiple.as_view()),
    path('validate_email/<int:pk>', ActivityValidatedEmailApiView.as_view()),
    path('my/', MyActivityListAPIView.as_view()),
    path('to_validate/', ActivityToValidateListApiView.as_view()),
    path('delete/enroll/<int:pk>', UserActivityDestroyAPIView.as_view()),
    path('enroll/<int:pk>', EnrollInActivityAPIView.as_view()),
    path('my/<int:pk>', MyActivityDetailAPIView.as_view()),
]
