from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import (
    OrderingFilter,
    SearchFilter
)
from rest_framework.generics import (
    ListCreateAPIView,
    get_object_or_404,
    RetrieveUpdateDestroyAPIView,
    ListAPIView,
    UpdateAPIView
)

from apps.employee.permissions import TypeEmployeeCreateListPermission
from monitoring_graduates.permission import ModelPermission
from apps.history_user.models import HistoryUser
from rest_framework.response import Response
from apps.employee.models import TypeEmployee
from apps.employee.serializer import (
    TypeEmployeeSerializer,
    TypeEmployeeLiteSerializer
)


class TypeEmployeeListCreateAPIView(ListCreateAPIView):
    """
    API View for list and create a type employee
    """
    permission_classes = [TypeEmployeeCreateListPermission]
    serializer_class = TypeEmployeeSerializer

    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'name': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'name',
        'is_active'
    ]

    search_fields = [
        'name'
    ]

    def get_queryset(self):
        """
        Method QuerySet, only super admin can view all data
        """
        if not self.request.user.is_superuser:
            return TypeEmployee.objects.filter(is_active=True)
        return TypeEmployee.objects.all()

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de tipos de empleado'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            instance = get_object_or_404(TypeEmployee, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Creo un nuevo tipo de empleado',
                content_object=instance
            )
            history_user.save()
        return response


class TypeEmployeeRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    """
    API View for see, edit and delete type employee
    """
    permission_classes = [ModelPermission]
    serializer_class = TypeEmployeeSerializer

    def get_queryset(self):
        """
        Method QuerySet, only super admin can view all data
        """
        if not self.request.user.is_superuser:
            return TypeEmployee.objects.all()
        return TypeEmployee.objects.all()

    def get(self, request, *args, **kwargs):
        """
        Method GET of HTTP for see one type employee
        """
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            instance = get_object_or_404(TypeEmployee, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Recupero el detalle de un tipo de empleado',
                content_object=instance
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        """
        Method PUT of HTTP for edit type employee
        """
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            instance = get_object_or_404(TypeEmployee, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito un tipo de empleado',
                content_object=instance
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for disable type employee
        """
        instance = self.get_object()
        instance.is_active = False
        instance.save()
        history = HistoryUser(
            user=request.user,
            description='Borro de manera lógica un tipo de empleado',
            content_object=instance
        )
        history.save()
        return Response(
            self.serializer_class(instance).data,
            status=status.HTTP_200_OK
        )


class TypeEmployeeListLiteAPIView(ListAPIView):
    pagination_class = None
    permission_classes = [TypeEmployeeCreateListPermission]
    serializer_class = TypeEmployeeLiteSerializer
    queryset = TypeEmployee.objects.filter(is_active=True)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name']
    ordering_fields = ['name']


class TypeEmployeeReactivateAPIView(UpdateAPIView):
    """
    API View class for reactivate a type employee
    """
    permission_classes = [ModelPermission]
    queryset = TypeEmployee.objects.filter(is_active=False)
    serializer_class = TypeEmployeeSerializer

    def update(self, request, *args, **kwargs):
        """
        PUT or PATCH method for API View class
        """
        instance = self.get_object()
        instance.is_active = True
        instance.save()
        history = HistoryUser(
            user=request.user,
            description='Habilito de manera lógica un tipo de empleado',
            content_object=instance
        )
        history.save()
        return Response(
            self.serializer_class(instance).data,
            status=status.HTTP_200_OK
        )
