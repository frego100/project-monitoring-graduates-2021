"""
Permissions for employee module
"""
from rest_framework import permissions


class TypeEmployeeCreateListPermission(permissions.BasePermission):
    """
    Permission manager for type employee
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'POST':
            return request.user.has_perm('employee.add_typeemployee')
        return True
