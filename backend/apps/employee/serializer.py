from rest_framework.serializers import ModelSerializer

from apps.employee.models import TypeEmployee


class TypeEmployeeSerializer(ModelSerializer):
    class Meta:
        model = TypeEmployee
        fields = '__all__'

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True


class TypeEmployeeLiteSerializer(ModelSerializer):
    class Meta:
        model = TypeEmployee
        fields = ('id', 'name')

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True
