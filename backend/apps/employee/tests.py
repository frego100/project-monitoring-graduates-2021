from django.test import TestCase
from rest_framework import status
from .models import TypeEmployee
from .serializer import TypeEmployeeSerializer
from rest_framework.test import APIClient
from django.contrib.auth.models import Permission
from apps.group.models import UserGroup
from django.contrib.auth.models import User
from rest_framework.exceptions import PermissionDenied


class TestEmployeeModels(TestCase):
    """
    Test cases for API Models  from type employee module
    """

    def setUp(self):
        """
        Configuration test cases
        """
        self.type_employee1 = TypeEmployee.objects.create(name="type1")

    def test_employee_is_created(self):
        """
        Test case for create type employee
        """
        self.assertEquals(self.type_employee1.name,'type1')
        self.assertEquals(self.type_employee1.is_active,True)

class TestEmployeeViews(TestCase):
    """
    Test cases for API Views  from type employee module
    """

    def setUp(self):
        """
        Configuration test cases
        """
        self.client = APIClient()
        self.user = User.objects.get_or_create(username='testuser')[0]
        self.group = UserGroup.objects.create(name='Test Group 1', state=True)
        permissions_list = Permission.objects.all()
        self.group.permissions.set(permissions_list)
        self.user.groups.add(self.group)
        self.client.force_authenticate(self.user)
        self.type_employee1 = TypeEmployee.objects.create(name="type1")
        self.type_employee2 = TypeEmployee.objects.create(name="type2")

    def test_list_type_employee(self):
        """
        Test case for list type employee with view
        """
        response = self.client.get('/employee/type-employee/')
        serializer = TypeEmployeeSerializer(TypeEmployee.objects.filter(is_active=True), many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['results'], serializer.data)
        self.assertEqual(2, response.data['count'])

    def test_create_type_employee(self):
        """
        Test case for create type employee with view
        """
        data = {
            "name": "Test Create Type Employee",
            "is_active": True
        }
        count = len(TypeEmployee.objects.all())
        response = self.client.post('/employee/type-employee/', data)
        serializer = TypeEmployeeSerializer(TypeEmployee.objects.get(name="Test Create Type Employee"))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(len(TypeEmployee.objects.all()), count + 1)

    def test_see_type_employee(self):
        """
        Test case for see a type employee with view
        """
        response = self.client.get('/employee/type-employee/'+ str(self.type_employee1.id))
        serializer = TypeEmployeeSerializer(TypeEmployee.objects.get(id=self.type_employee1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_update_type_employee(self):
        """
        Test case for update a type employee with view
        """
        data = {
            "name": "Empleado123",
        }
        response = self.client.put('/employee/type-employee/'+ str(self.type_employee1.id),data)
        serializer = TypeEmployeeSerializer(TypeEmployee.objects.get(id=self.type_employee1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data["name"], "Empleado123")

    def test_delete_type_employee(self):
        """
        Test case for delete a type employee with view
        """
        response = self.client.delete('/employee/type-employee/'+ str(self.type_employee1.id))
        serializer = TypeEmployeeSerializer(TypeEmployee.objects.get(id=self.type_employee1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data["is_active"], False)

class TypeEmployeeReactivateAPIViewTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username='whitecat')
        self.url_path = '/employee/type-employee/reactivate/'
        self.instance = TypeEmployee.objects.create(name="TipoEmpleado",
                                                    is_active=False)

    def test_reactivate_with_authenticate_user_with_permissions(self):
        # Set permission for this user
        self.user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        self.client.force_authenticate(self.user)
        response = self.client.put(self.url_path + str(self.instance.id))

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["is_active"], True)

    def test_reactivate_authenticate_user_without_permissions(self):
        # Force Authentication
        self.client.force_authenticate(self.user)
        response = self.client.put(self.url_path + str(self.instance.id))

        # Confirm if the status code is a success in this case the success code is 403 OK
        self.assertEqual(response.status_code, PermissionDenied.status_code)