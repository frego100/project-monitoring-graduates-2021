"""
Urls for Type Employee
"""
from django.urls import path

from apps.employee.views import (
    TypeEmployeeListCreateAPIView,
    TypeEmployeeRetrieveUpdateDestroyAPIView,
    TypeEmployeeListLiteAPIView,
    TypeEmployeeReactivateAPIView
)

urlpatterns = [
    path('type-employee/', TypeEmployeeListCreateAPIView.as_view()),
    path('type-employee/lite/', TypeEmployeeListLiteAPIView.as_view()),
    path('type-employee/<int:pk>', TypeEmployeeRetrieveUpdateDestroyAPIView.as_view()),
    path('type-employee/reactivate/<int:pk>', TypeEmployeeReactivateAPIView.as_view()),
]