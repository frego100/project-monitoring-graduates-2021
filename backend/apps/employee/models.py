from django.db import models


class TypeEmployee(models.Model):
    """
    Class TypeEmployee
    """

    name = models.CharField(
        max_length=256,
        verbose_name="Name",
        null=False,
        blank=False
    )
    is_active = models.BooleanField(
        null=False,
        default=True,
        verbose_name="Active"
    )

    class Meta:
        verbose_name = "Tipo de Empleado"
        verbose_name_plural = "Tipo de Empleados"
