from django.contrib import admin
from apps.identificators.models import Identificator

# Register your models here.
admin.site.register(Identificator)
