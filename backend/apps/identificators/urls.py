from django.urls import path

from apps.identificators.views import IdentificatorRetrieveUpdateDestroyAPIView, IdentificatorListCreateAPIView, IdentificatorReactivateAPIView

urlpatterns = [
    path('', IdentificatorListCreateAPIView.as_view()),
    path('<int:pk>', IdentificatorRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', IdentificatorReactivateAPIView.as_view(), name='reactivate'),
]
