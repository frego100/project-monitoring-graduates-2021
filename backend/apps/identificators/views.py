"""
Views for identificator
"""
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response

from apps.history_user.models import HistoryUser
from apps.identificators.models import Identificator
from apps.identificators.serializers import IdentificatorSerializer
# Create your views here.
from monitoring_graduates.permission import ModelPermission


class IdentificatorListCreateAPIView(generics.ListCreateAPIView):
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    queryset = Identificator.objects.all()
    serializer_class = IdentificatorSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]
    filterset_fields = {
        'id': ['exact'],
        'code': ['exact'],
        'max_length': ['exact'],
        'name': ['exact'],
        'alphabet': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'code',
        'max_length',
        'name',
        'alphabet',
        'is_active'
    ]

    search_fields = [
        'id',
        'code',
        'max_length',
        'name',
        'alphabet',
        'is_active'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de identificadores'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            instance = get_object_or_404(Identificator, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Creo un nuevo identificador',
                content_object=instance
            )
            history_user.save()
        return response


class IdentificatorRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API View for list and create a activity
    """
    permission_classes = (ModelPermission,)
    queryset = Identificator.objects.all()
    serializer_class = IdentificatorSerializer

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            identificator = get_object_or_404(Identificator, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Recupero el detalle de un identificador',
                content_object=identificator
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            identificator = get_object_or_404(Identificator, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito un identificador',
                content_object=identificator
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for enable and disable identificator
        """
        identificator = self.get_object()
        identificator.is_active = False
        identificator.save()
        history = HistoryUser(
            user=request.user,
            description='Borro de manera logica un identificador',
            content_object=identificator
        )
        history.save()
        return Response(
            self.serializer_class(identificator).data,
            status=status.HTTP_200_OK
        )


class IdentificatorReactivateAPIView(generics.UpdateAPIView):
    """
    API View class for reactivate a identificator
    """
    permission_classes = (ModelPermission,)
    queryset = Identificator.objects.filter(is_active=False)
    serializer_class = IdentificatorSerializer

    def update(self, request, *args, **kwargs):
        """
        PUT or PATCH method for API View class
        """
        instance = self.get_object()
        instance.is_active = True
        instance.save()
        history = HistoryUser(
            user=request.user,
            description='Habilito de manera lógica un identificador',
            content_object=instance
        )
        history.save()
        return Response(
            self.serializer_class(instance).data,
            status=status.HTTP_200_OK
        )