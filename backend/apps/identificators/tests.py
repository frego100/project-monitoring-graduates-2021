from django.contrib.auth.models import User, Permission
from django.test import TestCase
from rest_framework import status
from rest_framework.exceptions import ErrorDetail, PermissionDenied
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory, force_authenticate, APIClient

from apps.history_user.models import HistoryUser
from apps.identificators.models import Identificator
from apps.identificators.serializers import IdentificatorSerializer
from apps.identificators.views import IdentificatorListCreateAPIView


# Create your tests here.
class IdentificatorListCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.paginator = LimitOffsetPagination()
        self.view = IdentificatorListCreateAPIView.as_view()
        self.serializer = IdentificatorSerializer

        self.url_path_list_create = '/identificator/'

        self.list_queryset = None
        self.list_request = None
        self.create_request = None
        self.create_queryset = None

    def paginate_queryset(self, request):
        return list(self.paginator.paginate_queryset(self.list_queryset, request))

    def get_paginated_content(self, queryset):
        response = self.paginator.get_paginated_response(queryset)
        return response.data

    def test_list_authenticate_user_without_permissions(self):
        # Init var for this test
        self.list_request = self.factory.get(self.url_path_list_create)
        self.list_queryset = Identificator.objects.all()
        # Create a example job position
        Identificator.objects.create(code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.list_request, user=user)

        # Create a Django Rest Framework Request to call functions in the paginator class
        request = Request(self.list_request)
        # Result after execute paginator
        queryset_result = self.paginate_queryset(request)
        # Result after execute serializer
        serialized = self.serializer(queryset_result, many=True)
        # Result after execute serializer and paginator
        content_result = self.get_paginated_content(serialized.data)
        # Call the List API View
        response = self.view(self.list_request)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer and pager
        self.assertEqual(response.data, content_result)

        # Count if the total of items is equal to the size of the queryset
        self.assertEqual(len(queryset_result), self.list_queryset.count())

    def test_create_with_authenticate_user_with_permissions(self):
        # Create a example job position
        instance = Identificator(code='DNI',
                                 max_length=200,
                                 name="Documento Nacional de Identidad",
                                 alphabet='NUM',
                                 is_active=True)
        # Serialized data
        serialized = self.serializer(instance, many=False)
        # Create a Request
        self.create_request = self.factory.post(self.url_path_list_create, serialized.data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)
        # Check if the JobPosition is created
        self.create_queryset = Identificator.objects.get(name="Documento Nacional de Identidad")

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Confirm if the response data is equal to the database register data
        self.assertEqual(response.data, self.serializer(self.create_queryset).data)

        # Confirm if the ep create history user
        self.assertEqual(1, HistoryUser.objects.all().count())

    def test_create_authenticate_user_without_permissions(self):
        # Create a example job position
        instance = Identificator(code='DNI',
                                 max_length=200,
                                 name="Documento Nacional de Identidad",
                                 alphabet='NUM',
                                 is_active=True)
        # Serialized data
        serialized = self.serializer(instance, many=False)
        # Create a Request
        self.create_request = self.factory.post(self.url_path_list_create, serialized.data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # No set permissions for this user
        # user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)

        # Confirm if the status code is a success in this case the success code is 403 OK
        self.assertEqual(response.status_code, PermissionDenied.status_code)

        self.assertEqual(response.data,
                         {
                             'detail': ErrorDetail(string=PermissionDenied.default_detail,
                                                   code=PermissionDenied.default_code)
                         })


class IdentificatorReactivateAPIViewTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username='whitecat')
        self.url_path = '/identificator/reactivate/'
        self.instance = Identificator.objects.create(code='DNI',
                                                max_length=200,
                                                name="Documento Nacional de Identidad",
                                                alphabet='NUM',
                                                is_active=False)

    def test_reactivate_with_authenticate_user_with_permissions(self):
        # Set permission for this user
        self.user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        self.client.force_authenticate(self.user)
        response = self.client.put(self.url_path + str(self.instance.id))

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["is_active"], True)

    def test_reactivate_authenticate_user_without_permissions(self):
        # Force Authentication
        self.client.force_authenticate(self.user)
        response = self.client.put(self.url_path + str(self.instance.id))

        # Confirm if the status code is a success in this case the success code is 403 OK
        self.assertEqual(response.status_code, PermissionDenied.status_code)