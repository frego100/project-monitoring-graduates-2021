"""
Serializers for Identificator module
"""
from rest_framework import serializers

from apps.identificators.models import Identificator


class IdentificatorSerializer(serializers.ModelSerializer):
    """
    Serializer for identificator
    """
    class Meta:
        model = Identificator
        fields = '__all__'