from django.db import models


# Create your models here.
class Identificator(models.Model):
    REGEX_CHOICES = (
        ("NUM", "NUMÉRICO"),
        ("ALN", "ALFANUMÉRICO")
    )

    code = models.CharField(
        max_length=20,
        null=False
    )
    max_length = models.IntegerField(
        null=False
    )
    name = models.CharField(
        max_length=500,
        null=False
    )
    alphabet = models.CharField(
        null=False,
        max_length=30,
        default='NUM',
        choices=REGEX_CHOICES
    )
    is_active = models.BooleanField(
        null=False,
        default=True
    )

    class Meta:
        verbose_name = 'Identificador'
        verbose_name_plural = 'Identificadores'
