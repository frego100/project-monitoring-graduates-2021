from django.urls import path

from apps.instance.views import AreaRetrieveUpdateDestroyAPIView, AreaReactivateAPIView, FacultyListCreateAPIView, \
    ProfessionalProgramRetrieveUpdateDestroyAPIView, ProfessionalProgramReactivateAPIView

from apps.instance.views import (
    FacultyRetrieveUpdateDestroyAPIView,
    FacultyListByArea,
    FacultyReactivateAPIView,
    AreaListCreateAPIView,
    ProfessionalProgramListCreateAPIView,
    AreaListLiteAPIView,
    FacultyListLiteAPIView,
    ProfessionalProgramListLiteAPIView
)

urlpatterns = [
    path('area', AreaListCreateAPIView.as_view()),
    path('area/lite/', AreaListLiteAPIView.as_view()),
    path('area/<int:pk>', AreaRetrieveUpdateDestroyAPIView.as_view()),
    path('area/reactivate/<int:pk>', AreaReactivateAPIView.as_view()),
    path('faculty/<int:pk>', FacultyRetrieveUpdateDestroyAPIView.as_view()),
    path('faculty/by/area/<int:area_pk>', FacultyListByArea.as_view()),
    path('faculty/reactivate/<int:pk>', FacultyReactivateAPIView.as_view()),
    path('faculty', FacultyListCreateAPIView.as_view()),
    path('faculty/lite/', FacultyListLiteAPIView.as_view()),
    path('professional-program/', ProfessionalProgramListCreateAPIView.as_view()),
    path('professional-program/lite/', ProfessionalProgramListLiteAPIView.as_view()),
    path('professional-program/<int:pk>', ProfessionalProgramRetrieveUpdateDestroyAPIView.as_view()),
    path('professional-program/reactivate/<int:pk>', ProfessionalProgramReactivateAPIView.as_view()),
]
