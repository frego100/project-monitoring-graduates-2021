from django.contrib import admin

# Register your models here.
from apps.instance.models import Area
from apps.instance.models import Faculty
from apps.instance.models import ProfessionalProgram

admin.site.register(Area)
admin.site.register(Faculty)
admin.site.register(ProfessionalProgram)
