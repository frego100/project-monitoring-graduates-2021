from rest_framework import permissions


class AreaPermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
     
        if view.__class__.__name__== 'AreaListCreateAPIView':

            if request.method == 'GET':
                #return request.user.has_perm('instance.add_area')
                return True
            if request.method == 'POST':
                return request.user.has_perm('instance.view_area')
        
        return False
