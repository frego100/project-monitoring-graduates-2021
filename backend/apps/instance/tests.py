from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.exceptions import PermissionDenied, ErrorDetail
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.test import APIClient
from rest_framework.test import APIRequestFactory, force_authenticate

from apps.group.models import UserGroup
from apps.instance.serializers import AreaSerializer, ProfessionalProgramSerializer, ProfessionalProgramDetailSerializer
from apps.instance.views import AreaRetrieveUpdateDestroyAPIView, AreaReactivateAPIView, FacultyListCreateAPIView, \
    ProfessionalProgramReactivateAPIView, ProfessionalProgramRetrieveUpdateDestroyAPIView, ProfessionalProgramListCreateAPIView
from .models import Faculty, Area, ProfessionalProgram
from .serializers import FacultySerializer

# Create your tests here.
from ..history_user.models import HistoryUser


class ProfessionalProgramListCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.paginator = LimitOffsetPagination()
        self.view = ProfessionalProgramListCreateAPIView.as_view()
        self.serializer = ProfessionalProgramSerializer
        self.permission = Permission.objects.all()

        self.url_path_list_create = '/instance/professional-program/'
        self.id_test = 1

        self.list_queryset = None
        self.list_request = None
        self.create_request = None
        self.create_queryset = None

    def paginate_queryset(self, request):
        return list(self.paginator.paginate_queryset(self.list_queryset, request))

    def get_paginated_content(self, queryset):
        response = self.paginator.get_paginated_response(queryset)
        return response.data

    def test_list(self):
        # Init var for this test
        self.list_request = self.factory.get(self.url_path_list_create)
        self.list_queryset = ProfessionalProgram.objects.all()
        # Create a example job position
        Area.objects.create(id=self.id_test, name="Test")
        Faculty.objects.create(id=self.id_test, name="Test 2", area_field_id=self.id_test)
        ProfessionalProgram.objects.create(id=self.id_test, name="Test 3", faculty_field_id=self.id_test)
        # Create a Django Rest Framework Request to call functions in the paginator class
        request = Request(self.list_request)
        # Result after execute paginator
        queryset_result = self.paginate_queryset(request)
        # Result after execute serializer
        serialized = self.serializer(queryset_result, many=True)
        # Result after execute serializer and paginator
        content_result = self.get_paginated_content(serialized.data)
        # Call the List API View
        response = self.view(self.list_request)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer and pager
        self.assertEqual(response.data, content_result)

        # Count if the total of items is equal to the size of the queryset
        self.assertEqual(len(queryset_result), self.list_queryset.count())

class ProfessionalProgramRetrieveUpdateDestroyViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = ProfessionalProgramRetrieveUpdateDestroyAPIView.as_view()
        self.serializer = ProfessionalProgramSerializer
        self.permission = Permission.objects.all()

        self.url_path_retrieve_update_destroy = '/instance/professional-program'
        self.id_test = 1

        self.retrieve_queryset = None
        self.retrieve_request = None
        self.update_request = None
        self.update_queryset = None
        self.destroy_request = None
        self.destroy_queryset = None

    def test_retrieve(self):
        # Init var for this test
        self.retrieve_request = self.factory.get(self.url_path_retrieve_update_destroy)
        Area.objects.create(id=self.id_test, name="Test")
        Faculty.objects.create(id=self.id_test, name="Test", area_field_id=self.id_test)
        self.retrieve_queryset = ProfessionalProgram.objects.create(id=self.id_test, name='Test',
                                                                    faculty_field_id=self.id_test)
        # Call the Create API View
        response = self.view(self.retrieve_request, pk=self.id_test)
        # Serialized data
        serialized = ProfessionalProgramDetailSerializer(self.retrieve_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)

    def test_update(self):
        # Create a Job Position for this test
        Area.objects.create(id=self.id_test, name="Test")
        Faculty.objects.create(id=self.id_test, name="Test", area_field_id=self.id_test)
        ProfessionalProgram.objects.create(id=self.id_test, name='Test', faculty_field_id=self.id_test)
        # Create a example job position
        instance = ProfessionalProgram(id=self.id_test, name="Test[update]", faculty_field_id=self.id_test)
        # Serialized data
        serialized = self.serializer(instance, many=False)

        # Init var for this test PUT Method
        self.update_request = self.factory.put(self.url_path_retrieve_update_destroy, serialized.data, format='json')
        self.update_queryset = ProfessionalProgram.objects.get(name="Test")

        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.update_request, user=user)

        # Call the Update API View
        response = self.view(self.update_request, pk=self.id_test)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data, serialized.data)

    def test_destroy(self):
        # Create a Job Position for this test
        Area.objects.create(id=self.id_test, name="Test")
        Faculty.objects.create(id=self.id_test, name="Test", area_field_id=self.id_test)
        instance = ProfessionalProgram.objects.create(id=self.id_test, name='Test', faculty_field_id=self.id_test)
        # Serialized data
        serialized = self.serializer(instance, many=False)

        # Init var for this test PUT Method
        self.destroy_request = self.factory.delete(self.url_path_retrieve_update_destroy, serialized.data,
                                                   format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.destroy_request, user=user)

        # Call the Update API View
        response = self.view(self.destroy_request, pk=self.id_test)

        self.destroy_queryset = ProfessionalProgram.objects.get(name="Test")

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data, None)

        # Check if the logic delete is successful
        self.assertEqual(self.destroy_queryset.is_active, False)


class ProfessionalProgramReactivateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = ProfessionalProgramReactivateAPIView.as_view()
        self.serializer = ProfessionalProgramSerializer
        self.permission = Permission.objects.all()

        self.url_path_reactivate = '/instance/professional-program/reactivate'
        self.id_test = 1

        self.reactivate_request = None
        self.reactivate_queryset = None

    def test_reactivate(self):
        # Init var for this test
        Area.objects.create(id=self.id_test, name="Test")
        Faculty.objects.create(id=self.id_test, name="Test", area_field_id=self.id_test)
        ProfessionalProgram.objects.create(id=self.id_test, name='Test', faculty_field_id=self.id_test, is_active=False)

        self.reactivate_request = self.factory.put(self.url_path_reactivate)

        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.reactivate_request, user=user)

        # Call the API View
        response = self.view(self.reactivate_request, pk=self.id_test)
        # Select in BD the instance
        self.reactivate_queryset = ProfessionalProgram.objects.get(id=self.id_test)
        # Serialized data
        serialized = self.serializer(self.reactivate_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)


class AreaRetrieveUpdateDestroyViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = AreaRetrieveUpdateDestroyAPIView.as_view()
        self.serializer = AreaSerializer
        self.permission = Permission.objects.all()

        self.url_path_retrieve_update_destroy = '/instance/area'
        self.id_test = 1

        self.retrieve_queryset = None
        self.retrieve_request = None
        self.update_request = None
        self.update_queryset = None
        self.destroy_request = None
        self.destroy_queryset = None

    def test_retrieve(self):
        # Init var for this test
        self.retrieve_request = self.factory.get(self.url_path_retrieve_update_destroy)
        self.retrieve_queryset = Area.objects.create(name='Test')
        self.id_test = self.retrieve_queryset.id
        # Call the Create API View
        response = self.view(self.retrieve_request, pk=self.id_test)
        # Serialized data
        serialized = self.serializer(self.retrieve_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)

    def test_update(self):
        # Create a Job Position for this test
        Area.objects.create(id=self.id_test, name="Test")
        # Create a example job position
        instance = Area(id=self.id_test, name="Test[update]")
        # Serialized data
        serialized = self.serializer(instance, many=False)

        # Init var for this test PUT Method
        self.update_request = self.factory.put(self.url_path_retrieve_update_destroy, serialized.data, format='json')
        self.update_queryset = Area.objects.get(name="Test")

        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.update_request, user=user)

        # Call the Update API View
        response = self.view(self.update_request, pk=self.id_test)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data, serialized.data)

    def test_destroy(self):
        # Create a Job Position for this test
        instance = Area.objects.create(id=self.id_test, name="Test")
        # Serialized data
        serialized = self.serializer(instance, many=False)

        # Init var for this test PUT Method
        self.destroy_request = self.factory.delete(self.url_path_retrieve_update_destroy, serialized.data,
                                                   format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.destroy_request, user=user)

        # Call the Update API View
        response = self.view(self.destroy_request, pk=self.id_test)

        self.destroy_queryset = Area.objects.get(name="Test")

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data, None)

        # Check if the logic delete is successful
        self.assertEqual(self.destroy_queryset.is_active, False)


class AreaReactivateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = AreaReactivateAPIView.as_view()
        self.serializer = AreaSerializer
        self.permission = Permission.objects.all()

        self.url_path_reactivate = '/instance/area/reactivate'
        self.id_test = 1

        self.reactivate_request = None
        self.reactivate_queryset = None

    def test_reactivate(self):
        # Init var for this test
        Area.objects.create(id=self.id_test, name='Test', is_active=False)
        self.reactivate_request = self.factory.put(self.url_path_reactivate)

        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.reactivate_request, user=user)

        # Call the API View
        response = self.view(self.reactivate_request, pk=self.id_test)
        # Select in BD the instance
        self.reactivate_queryset = Area.objects.get(id=self.id_test)
        # Serialized data
        serialized = self.serializer(self.reactivate_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)


class TestFacultyModels(TestCase):
    """
    Test cases for API Models  from type employee module
    """

    def setUp(self):
        """
        Configuration test cases
        """
        self.area_1 = Area.objects.create(name="INGENIERIA")
        self.faculty_1 = Faculty.objects.create(name="PRODUCCION Y SERVICIOS", area_field=self.area_1)

    def test_employee_is_created(self):
        """
        Test case for create type employee
        """
        self.assertEquals(self.faculty_1.name, 'PRODUCCION Y SERVICIOS')
        self.assertEquals(self.faculty_1.is_active, True)


class JobPositionListCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.paginator = LimitOffsetPagination()
        self.view = FacultyListCreateAPIView.as_view()
        self.serializer = FacultySerializer

        self.url_path_list_create = '/instance/faculty'

        self.id_test = 1
        self.list_queryset = None
        self.list_request = None
        self.create_request = None
        self.create_queryset = None

    def paginate_queryset(self, request):
        return list(self.paginator.paginate_queryset(self.list_queryset, request))

    def get_paginated_content(self, queryset):
        response = self.paginator.get_paginated_response(queryset)
        return response.data

    def test_list(self):
        # Init var for this test
        self.list_request = self.factory.get(self.url_path_list_create)
        self.list_queryset = Faculty.objects.all()
        # Create a example job position
        Area.objects.create(id=self.id_test, name="Test")
        Faculty.objects.create(id=self.id_test, name="Test 2", area_field_id=self.id_test)
        # Create a Django Rest Framework Request to call functions in the paginator class
        request = Request(self.list_request)
        # Result after execute paginator
        queryset_result = self.paginate_queryset(request)
        # Result after execute serializer
        serialized = self.serializer(queryset_result, many=True)
        # Result after execute serializer and paginator
        content_result = self.get_paginated_content(serialized.data)
        # Call the List API View
        response = self.view(self.list_request)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer and pager
        self.assertEqual(response.data, content_result)

        # Count if the total of items is equal to the size of the queryset
        self.assertEqual(len(queryset_result), self.list_queryset.count())

    def test_create_with_authenticate_user_with_permissions(self):
        # Create a example job position
        Area.objects.create(id=2, name="Test")
        instance = Faculty(name="Test", area_field_id=2)
        instance.save()
        # Serialized data
        serialized = self.serializer(instance, many=False)
        # Create a Request
        self.create_request = self.factory.post(self.url_path_list_create, serialized.data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)
        # Check if the Faculty is created
        self.create_queryset = Faculty.objects.all().last()

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Confirm if the response data is equal to the database register data
        self.assertEqual(response.data, self.serializer(self.create_queryset).data)

        # Confirm if the ep create history user
        self.assertEqual(1, HistoryUser.objects.all().count())

    def test_create_authenticate_user_without_permissions(self):
        # Create a example job position
        job_position = Faculty(name="Admin")
        # Serialized data
        serialized = self.serializer(job_position, many=False)
        # Create a Request
        self.create_request = self.factory.post(self.url_path_list_create, serialized.data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # No set permissions for this user
        # user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)

        # Confirm if the status code is a success in this case the success code is 403 OK
        self.assertEqual(response.status_code, PermissionDenied.status_code)

        self.assertEqual(response.data,
                         {
                             'detail': ErrorDetail(string=PermissionDenied.default_detail,
                                                   code=PermissionDenied.default_code)
                         })


class FacultyViews(TestCase):
    """
    Test cases for API Views  from type employee module
    """

    def setUp(self):
        """
        Configuration test cases
        """
        self.client = APIClient()
        self.user = User.objects.get_or_create(username='testuser')[0]
        self.group = UserGroup.objects.create(name='Test Group 1', state=True)
        permissions_list = Permission.objects.all()
        self.group.permissions.set(permissions_list)
        self.user.groups.add(self.group)
        self.client.force_authenticate(self.user)
        self.area_1 = Area.objects.create(name="INGENIERIA")
        self.faculty_1 = Faculty.objects.create(name="FACULTAD 1", area_field=self.area_1)
        self.faculty_2 = Faculty.objects.create(name="FACULTAD 2", area_field=self.area_1)

    def test_see_faculty(self):
        """
        Test case for see a faculty with view
        """
        response = self.client.get('/instance/faculty/' + str(self.faculty_1.id))
        serializer = FacultySerializer(Faculty.objects.get(id=self.faculty_1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_update_faculty(self):
        """
        Test case for update a faculty with view
        """
        data = {
            "name": "FACULTAD 3",
        }
        response = self.client.patch('/instance/faculty/' + str(self.faculty_1.id), data)
        serializer = FacultySerializer(Faculty.objects.get(id=self.faculty_1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data["name"], "FACULTAD 3")

    def test_delete_faculty(self):
        """
        Test case for delete a faculty with view
        """
        response = self.client.delete('/instance/faculty/' + str(self.faculty_1.id))
        serializer = FacultySerializer(Faculty.objects.get(id=self.faculty_1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data["is_active"], False)

    def test_reactivate_faculty(self):
        """
        Test case for delete a faculty with view
        """
        self.client.delete('/instance/faculty/' + str(self.faculty_1.id))
        response = self.client.put('/instance/faculty/reactivate/' + str(self.faculty_1.id))
        serializer = FacultySerializer(Faculty.objects.get(id=self.faculty_1.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data["is_active"], True)
