"""
Serializers for instance module
"""
from apps.instance.models import Faculty, ProfessionalProgram
from rest_framework import serializers

from apps.instance.models import Area


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = '__all__'


class FacultySerializer(serializers.ModelSerializer):
    """
    Serializer for faculty
    """

    class Meta:
        model = Faculty
        fields = '__all__'


class FacultyLiteSerializer(serializers.ModelSerializer):
    """
    Serializer for faculty
    """

    class Meta:
        model = Faculty
        fields = ('id', 'name')


class ProfessionalProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfessionalProgram
        fields = '__all__'


class ProfessionalProgramDetailSerializer(serializers.ModelSerializer):
    faculty_field = FacultyLiteSerializer(read_only=True)

    class Meta:
        model = ProfessionalProgram
        fields = '__all__'


class ProfessionalProgramLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfessionalProgram
        fields = ['id', 'name']
