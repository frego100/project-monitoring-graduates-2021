# Create your views here.
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response
from apps.history_user.models import HistoryUser
from apps.instance.models import Area, ProfessionalProgram
from apps.instance.models import Faculty
from apps.instance.serializers import AreaSerializer, ProfessionalProgramSerializer, ProfessionalProgramDetailSerializer
from apps.instance.serializers import FacultySerializer
from monitoring_graduates.permission import ModelPermission
from .permission import AreaPermission
from rest_framework.permissions import IsAuthenticated


class AreaRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer
    # Permisos de lectura habilitados
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Accedio a un area universitaria',
                content_object=instance
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un area universitaria',
                content_object=instance
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Elimino un area universitaria',
                content_object=instance
            )
            history.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class AreaReactivateAPIView(generics.UpdateAPIView):
    queryset = Area.objects.filter(is_active=False)
    serializer_class = AreaSerializer
    # Permisos de lectura habilitados
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)


class FacultyListCreateAPIView(generics.ListCreateAPIView):
    queryset = Faculty.objects.all()
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = FacultySerializer

    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'name': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'name',
        'is_active'
    ]

    search_fields = [
        'id',
        'name'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de facultades'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            instance = get_object_or_404(Faculty, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Creo una nueva facultad',
                content_object=instance
            )
            history_user.save()
        return response


class FacultyListLiteAPIView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)
    queryset = Faculty.objects.filter(is_active=True)
    serializer_class = FacultySerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]
    filterset_fields = {
        'name': ['exact'],
        'area_field': ['exact'],
    }

    ordering_fields = [
        'id',
        'name',
        'area_field'
    ]

    search_fields = [
        'name'
    ]


class FacultyRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API View for list and create a faculty
    """
    permission_classes = (ModelPermission,)
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer

    def get(self, request, *args, **kwargs):
        """
        Method GET of HTTP for see a faculty
        """
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            faculty = get_object_or_404(Faculty, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Recupero el detalle de una facultad',
                content_object=faculty
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        """
        Method PUT of HTTP for edit faculty
        """
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            faculty = get_object_or_404(Faculty, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito una facultad',
                content_object=faculty
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for disable faculty
        """
        faculty = self.get_object()
        faculty.is_active = False
        faculty.save()
        history = HistoryUser(
            user=request.user,
            description='Borro de manera lógica una facultad',
            content_object=faculty
        )
        history.save()
        return Response(
            self.serializer_class(faculty).data,
            status=status.HTTP_200_OK
        )


class FacultyReactivateAPIView(generics.UpdateAPIView):
    """
    API View class for reactivate a faculty
    """
    permission_classes = (ModelPermission,)
    queryset = Faculty.objects.filter(is_active=False)
    serializer_class = FacultySerializer

    def update(self, request, *args, **kwargs):
        """
        PUT or PATCH method for API View class
        """
        faculty = self.get_object()
        faculty.is_active = True
        faculty.save()
        history = HistoryUser(
            user=request.user,
            description='Habilitó de manera lógica una facultad',
            content_object=faculty
        )
        history.save()
        return Response(
            self.serializer_class(faculty).data,
            status=status.HTTP_200_OK
        )


class FacultyListByArea(generics.ListAPIView):
    """
    Generic View used to list Faculty by Area
    """
    pagination_class = None
    permission_classes = (ModelPermission,)
    queryset = Faculty.objects.all().filter(is_active=True)
    serializer_class = FacultySerializer

    def get(self, request, area_pk, *args, **kwargs):
        """
        Method that updates the queryset

        :param area_pk: Area's Id

        :return: QuerySet with Faculty by Area
        """
        area = Area.objects.get(id=area_pk)
        self.queryset = self.queryset.filter(area_field=area)
        return super(FacultyListByArea, self).get(request, *args, **kwargs)


class ProfessionalProgramListCreateAPIView(generics.ListCreateAPIView):
    queryset = ProfessionalProgram.objects.all()
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = ProfessionalProgramSerializer

    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'name',
        'is_active',
        'faculty_field',
    }

    ordering_fields = [
        'id',
        'name',
        'is_active'
    ]

    search_fields = [
        'id',
        'name'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de programas profesionales'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            instance = get_object_or_404(ProfessionalProgram, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Creo un nuevo programa profesional',
                content_object=instance
            )
            history_user.save()
        return response


class ProfessionalProgramListLiteAPIView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)
    queryset = ProfessionalProgram.objects.filter(is_active=True)
    serializer_class = ProfessionalProgramSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]
    filterset_fields = {
        'name',
        'faculty_field',
        'faculty_field__area_field',
    }
    ordering_fields = [
        'id',
        'name',
    ]
    search_fields = [
        'name'
    ]


class ProfessionalProgramRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProfessionalProgram.objects.all()
    serializer_class = ProfessionalProgramSerializer
    # Permisos de lectura habilitados
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ProfessionalProgramDetailSerializer(instance)
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio a un programa profesional',
                content_object=instance
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un programa profesional',
                content_object=instance
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Elimino un programa profesional',
                content_object=instance
            )
            history.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class ProfessionalProgramReactivateAPIView(generics.UpdateAPIView):
    queryset = ProfessionalProgram.objects.filter(is_active=False)
    serializer_class = ProfessionalProgramSerializer
    # Permisos de lectura habilitados
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)


class AreaListCreateAPIView(generics.ListCreateAPIView):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer

    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'name': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'name',
        'is_active'
    ]

    search_fields = [
        'id',
        'name'
    ]
    permission_classes = (AreaPermission,)

    def get(self, request, *args, **kwargs):
        response = super(AreaListCreateAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            if self.request.user.is_authenticated:
                history = HistoryUser(
                    user=request.user,
                    description='Accedio al listado de areas'
                )
                history.save()
        return response

    def post(self, request, *args, **kwargs):
        response = super(AreaListCreateAPIView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            area = Area.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo una area ',
                content_object=area
            )
            history.save()
        return response


class AreaListLiteAPIView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (IsAuthenticated,)
    queryset = Area.objects.filter(is_active=True)
    serializer_class = AreaSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]
    filterset_fields = {
        'name': ['exact'],
    }
    ordering_fields = [
        'id',
        'name',
    ]
    search_fields = [
        'name'
    ]
