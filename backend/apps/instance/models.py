from django.db import models


# Create your models here.
class Instance(models.Model):
    is_active = models.BooleanField(
        default=True,
        verbose_name='Activo'
    )

    class Meta:
        verbose_name = 'Instancia Universitaria'
        verbose_name_plural = 'Instancias Universitarias'


class Area(Instance):
    name = models.CharField(
        max_length=15,
        null=False
    )

    class Meta:
        verbose_name = 'Area Universitaria'
        verbose_name_plural = 'Areas Universitarias'

    def __str__(self):
        return self.name


class Faculty(Instance):
    name = models.CharField(
        max_length=50,
        null=False
    )
    area_field = models.ForeignKey(
        Area,
        verbose_name='Area',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Facultad'
        verbose_name_plural = 'Facultades'

    def __str__(self):
        return self.name


class ProfessionalProgram(Instance):
    name = models.CharField(
        max_length=60,
        null=False
    )
    faculty_field = models.ForeignKey(
        Faculty,
        verbose_name='Facultad',
        on_delete=models.CASCADE
    )


    class Meta:
        verbose_name = 'Programa Profesional'
        verbose_name_plural = 'Programas Profesionales'

    def __str__(self):
        return self.name