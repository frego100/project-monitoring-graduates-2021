from django.contrib import admin

# Register your models here.
from solo.admin import SingletonModelAdmin

from apps.site_settings.models import SiteConfiguration

admin.site.register(SiteConfiguration, SingletonModelAdmin)
