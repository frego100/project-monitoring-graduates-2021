from django.db import models

# Create your models here.
from solo.models import SingletonModel


class SiteConfiguration(SingletonModel):
    link_facebook = models.URLField(
        verbose_name='Facebook',
        null=True,
        blank=False
    )
    link_twitter = models.URLField(
        verbose_name='Twitter',
        null=True,
        blank=False
    )
    link_youtube = models.URLField(
        verbose_name='Youtube',
        null=True,
        blank=False
    )
    link_linkedin = models.URLField(
        verbose_name='Linkedin',
        null=True,
        blank=False
    )
    link_instagram = models.URLField(
        verbose_name='Instagram',
        null=True,
        blank=False
    )
    link_other = models.URLField(
        verbose_name='Otro',
        null=True,
        blank=False
    )
    principal_text = models.TextField(
        verbose_name='Texto Principal',
        null=True,
        blank=False
    )
    principal_announcement = models.TextField(
        verbose_name='Anuncio Principal',
        null=True,
        blank=False
    )

    class Meta:
        verbose_name = 'Configuración de Sistema'
        verbose_name_plural = 'Configuraciónes de Sistema'
