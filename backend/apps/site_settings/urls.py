from django.urls import path

from apps.site_settings.views import SiteConfigurationRetrieveUpdateAPIView, \
    SiteConfigurationPublicRetrieveUpdateAPIView

urlpatterns = [
    path('', SiteConfigurationRetrieveUpdateAPIView.as_view()),
    path('public/', SiteConfigurationPublicRetrieveUpdateAPIView.as_view())
]
