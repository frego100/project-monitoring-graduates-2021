from rest_framework import generics


# Create your views here.
from rest_framework.permissions import DjangoModelPermissions

from apps.history_user.models import HistoryUser
from apps.site_settings.models import SiteConfiguration
from apps.site_settings.serializers import SiteConfigurationSerializer


class SiteConfigurationRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = SiteConfiguration.objects.all()
    serializer_class = SiteConfigurationSerializer
    permission_classes = [DjangoModelPermissions]

    def get_object(self):
        return SiteConfiguration.get_solo()

    def get(self, request, *args, **kwargs):
        history = HistoryUser(
            user=request.user,
            description='Accedio a la configuración publica del sistema',
            content_object=self.get_object()
        )
        history.save()
        return self.retrieve(request, *args, **kwargs)

    def perform_update(self, serializer):
        instance = serializer.save()
        history = HistoryUser(
            user=self.request.user,
            description='Modifico la configuración publica del sistema',
            content_object=instance
        )
        history.save()


class SiteConfigurationPublicRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = SiteConfiguration.objects.all()
    serializer_class = SiteConfigurationSerializer
    permission_classes = []

    def get_object(self):
        return SiteConfiguration.get_solo()
