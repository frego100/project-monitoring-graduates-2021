from django.contrib.auth.models import User, Permission
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate, APIClient

# Create your tests here.
from apps.site_settings.serializers import SiteConfigurationSerializer
from apps.site_settings.views import SiteConfigurationPublicRetrieveUpdateAPIView


class SiteConfigurationRetrieveUpdateDestroyViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = SiteConfigurationPublicRetrieveUpdateAPIView.as_view()
        self.serializer = SiteConfigurationSerializer
        self.permission = Permission.objects.all()

        self.url_path_retrieve_update_destroy = '/settings/'
        self.id_test = 1

        self.user = User(
            username='admin',
            password='password',
            is_superuser=True
        )
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_retrieve(self):
        # Init var for this test
        self.retrieve_request = self.factory.get(self.url_path_retrieve_update_destroy)
        # Call the Create API View
        response = self.view(self.retrieve_request)
        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update(self):
        data = {
            "link_facebook": "https://www.facebook.com",
            "link_twitter": "https://www.facebook.com",
            "link_youtube": "https://www.facebook.com",
            "link_linkedin": "https://www.facebook.com",
            "link_instagram": "https://www.facebook.com",
            "link_other": "https://www.facebook.com",
            "principal_text": "https://www.facebook.com",
            "principal_announcement": "https://www.facebook.com"
        }
        # Init var for this test PUT Method
        self.update_request = self.factory.put(self.url_path_retrieve_update_destroy, data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.update_request, user=user)
        # Call the Update API View
        response = self.view(self.update_request)
        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class SiteConfigPublicAPIViewTestCase(TestCase):
    def test_retrieve(self):
        response = APIClient().get('/settings/public/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
