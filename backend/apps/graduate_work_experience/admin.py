from django.contrib import admin

# Register your models here.
from apps.graduate_work_experience.models import GraduateWorkExperience

admin.site.register(GraduateWorkExperience)
