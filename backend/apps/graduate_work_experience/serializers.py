from rest_framework import serializers

from apps.graduate_work_experience.models import GraduateWorkExperience
from apps.company.serializers import CompanyLiteSerializerOfCompanyProfile
from apps.user_profile.serializer import GraduateProfileSerializer


class GraduateWorkExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateWorkExperience
        fields = '__all__'


class GraduateWorkExperienceDetailSerializer(serializers.ModelSerializer):
    graduate_profile_pk = GraduateProfileSerializer()
    institution = CompanyLiteSerializerOfCompanyProfile()

    class Meta:
        model = GraduateWorkExperience
        fields = '__all__'


class GraduateWorkExperienceAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateWorkExperience
        exclude = ['graduate_profile_pk']


class GraduateWorkExperienceAuthDetailSerializer(serializers.ModelSerializer):
    institution = CompanyLiteSerializerOfCompanyProfile()

    class Meta:
        model = GraduateWorkExperience
        exclude = ['graduate_profile_pk']
