# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.graduate_work_experience.models import GraduateWorkExperience
from apps.graduate_work_experience.serializers import GraduateWorkExperienceSerializer, \
    GraduateWorkExperienceDetailSerializer, GraduateWorkExperienceAuthSerializer, \
    GraduateWorkExperienceAuthDetailSerializer
from apps.graduate_work_experience.utils import log_entry_history_user
from apps.user_profile.models import GraduateProfile
from monitoring_graduates.permission import ModelPermission


class GraduateWorkExperienceListCreateAPIView(generics.ListCreateAPIView):
    queryset = GraduateWorkExperience.objects.all()
    serializer_class = GraduateWorkExperienceSerializer
    detail_serializer_class = GraduateWorkExperienceDetailSerializer
    model_class = GraduateWorkExperience
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'institution__company_name': ['exact'],
        'institution__ruc': ['exact'],
        'graduate_profile_pk': ['exact'],
        'graduate_profile_pk__cui': ['exact'],
        'graduate_profile_pk__is_active': ['exact'],
        'charge': ['exact'],
        'date_start': ['exact', 'lt', 'gt'],
        'date_end': ['exact', 'lt', 'gt'],
        'is_current': ['exact']
    }

    ordering_fields = [
        'id',
        'institution__company_name',
        'institution__ruc',
        'graduate_profile_pk__cui',
        'graduate_profile_pk__is_active',
        'charge',
        'date_start',
        'date_end',
        'is_current'
    ]

    search_fields = [
        'id',
        'institution__company_name',
        'institution__ruc',
        'graduate_profile_pk__cui',
        'graduate_profile_pk__is_active',
        'charge',
        'date_start',
        'date_end',
        'is_current'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Accedio al listado de experiencias de trabajo de los egresados'
            )
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            log_entry_history_user(
                request_user=request.user,
                message='Creo una experiencia de trabajo de egresado',
                instance=get_object_or_404(self.model_class, pk=int(response.data['id']))
            )
        return Response(
            status=response.status_code,
            data=self.detail_serializer_class(
                get_object_or_404(
                    self.model_class,
                    pk=int(response.data['id'])
                )
            ).data
        )


class GraduateWorkExperienceRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GraduateWorkExperience.objects.all()
    serializer_class = GraduateWorkExperienceSerializer
    detail_serializer_class = GraduateWorkExperienceDetailSerializer
    model_class = GraduateWorkExperience

    permission_classes = [ModelPermission]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.detail_serializer_class(instance)
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Recupero una experiencia de trabajo de egresado',
                instance=get_object_or_404(self.model_class, pk=int(self.kwargs['pk']))
            )
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo una una experiencia de trabajo de egresado',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return Response(
            status=response.status_code,
            data=self.detail_serializer_class(
                get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            ).data
        )

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            log_entry_history_user(
                request_user=request.user,
                message='Elimino una experiencia de trabajo de egresado'
            )
        return response


class GraduateWorkExperienceAuthListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = GraduateWorkExperienceAuthSerializer
    detail_serializer_class = GraduateWorkExperienceAuthDetailSerializer
    model_class = GraduateWorkExperience
    permission_classes = [IsAuthenticated]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'institution__company_name': ['exact'],
        'institution__ruc': ['exact'],
        'charge': ['exact'],
        'date_start': ['exact', 'lt', 'gt'],
        'date_end': ['exact', 'lt', 'gt'],
        'is_current': ['exact']
    }

    ordering_fields = [
        'id',
        'institution__company_name',
        'institution__ruc',
        'charge',
        'date_start',
        'date_end',
        'is_current'
    ]

    search_fields = [
        'id',
        'institution__company_name',
        'institution__ruc',
        'charge',
        'date_start',
        'date_end',
        'is_current'
    ]

    def get_queryset(self):
        return self.model_class.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Accedio a su propio listado de experiencia de proyectos'
            )
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            log_entry_history_user(
                request_user=request.user,
                message='Creo una experiencia en proyecto de egresado propio',
                instance=get_object_or_404(self.model_class, pk=int(response.data['id']))
            )
        return Response(
            status=response.status_code,
            data=self.detail_serializer_class(
                get_object_or_404(
                    self.model_class,
                    pk=int(response.data['id'])
                )
            ).data
        )

    def perform_create(self, serializer):
        serializer.save(
            graduate_profile_pk=get_object_or_404(
                GraduateProfile,
                user=self.request.user
            )
        )


class GraduateWorkExperienceAuthRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = GraduateWorkExperienceAuthSerializer
    detail_serializer_class = GraduateWorkExperienceAuthDetailSerializer
    model_class = GraduateWorkExperience
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.model_class.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.detail_serializer_class(instance)
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Recupero una propia experiencia en proyecto de egresado',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return response

    def perform_update(self, serializer):
        serializer.save(
            graduate_profile_pk=get_object_or_404(
                GraduateProfile,
                user=self.request.user
            )
        )

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo una propia experiencia en proyecto de egresado',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return Response(
            status=response.status_code,
            data=self.detail_serializer_class(
                get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            ).data
        )

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            log_entry_history_user(
                request_user=request.user,
                message='Elimino una propia experiencia en proyecto de egresado'
            )
        return response
