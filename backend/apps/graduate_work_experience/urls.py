from django.urls import path

from apps.graduate_work_experience.views import GraduateWorkExperienceListCreateAPIView, \
    GraduateWorkExperienceRetrieveUpdateDestroyAPIView, GraduateWorkExperienceAuthListCreateAPIView, \
    GraduateWorkExperienceAuthRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('', GraduateWorkExperienceListCreateAPIView.as_view()),
    path('<int:pk>', GraduateWorkExperienceRetrieveUpdateDestroyAPIView.as_view()),
    path('auth/', GraduateWorkExperienceAuthListCreateAPIView.as_view()),
    path('auth/<int:pk>', GraduateWorkExperienceAuthRetrieveUpdateDestroyAPIView.as_view()),
]
