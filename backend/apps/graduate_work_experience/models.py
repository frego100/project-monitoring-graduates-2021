from django.db import models

# Create your models here.
# from apps.institution.models import Institution
from apps.company.models import Company
from apps.user_profile.models import GraduateProfile

# SE ACTUALIZO LA RELACION A COMPANY, POR TEMAS DE INTEGRACION DE DATOS
class GraduateWorkExperience(models.Model):
    institution = models.ForeignKey(
        Company,
        on_delete=models.SET_NULL,
        null=True,
        related_name='institution_work_experience'
    )
    charge = models.CharField(
        max_length=128,
        blank=False,
        null=False
    )
    date_start = models.DateField()
    date_end = models.DateField()
    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=False,
        on_delete=models.CASCADE,
        related_name='graduate_profile_work_experience'
    )
    is_current = models.BooleanField(
        default=True,
        blank=False,
        null=False
    )

    class Meta:
        verbose_name = 'Experiencia de Trabajo del Egresado'
        verbose_name_plural = 'Experiencias de Trabajo de los Egresados'

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items() if item[0][0] != '_')
        )
