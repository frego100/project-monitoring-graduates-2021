from django.apps import AppConfig


class GraduateWorkExperienceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.graduate_work_experience'
