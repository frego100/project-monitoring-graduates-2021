"""
Views for user module
"""
import random
import re
from io import BytesIO

import pandas
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import IntegrityError
# Create your views here.
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from pandas import Series
from rest_framework import status, generics
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import (
    UpdateAPIView, RetrieveUpdateDestroyAPIView
)
from rest_framework.permissions import (
    IsAuthenticated
)
from rest_framework.response import Response

from apps.email_sender.utils import EmailSender
from apps.graduate_data_academic.models import GraduateDataAcademic
from apps.group.models import UserGroup
from apps.history_user.models import HistoryUser
from apps.identificators.models import Identificator
from apps.instance.models import ProfessionalProgram
from apps.user.models import PreRegistration
from apps.user.permissions import UserPermission, MyselfOrSuperuserPermission
from apps.user.serializer import (
    UserPreRegistrationSerializer,
    UserPreRegistrationCreateSerializer, FileUploadFileSerializer
)
from apps.user.serializer import UserSerializer, UserCreateSerializer, UserLiteSerializer, UserInformationSerializer
from apps.user_profile.models import GraduateProfile, UserProfile
from monitoring_graduates.permission import ModelPermission


class UserListView(generics.ListCreateAPIView):
    """
    View for get data from the all users
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()  # .filter(is_superuser=True, is_staff=True)
    filter_backends = [DjangoFilterBackend, OrderingFilter,SearchFilter]
    filterset_fields = ['is_active', 'groups']
    ordering_fields = ['username', 'first_name', 'last_name', 'email']
    search_fields = [
        '^username',
        '^first_name',
        '^last_name',
        '^email'
    ]
    permission_classes = (UserPermission,)

    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        if response.status_code == status.HTTP_201_CREATED:
            user = User.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un nuevo usuario',
                content_object=user
            )
            history.save()
        return response

    def get(self, request, *args, **kwargs):
        response = super(UserListView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de usuarios'
            )
            history.save()
        return response


class UserDetailView(RetrieveUpdateDestroyAPIView):
    """
    View for get data detail from the specific user
    """
    serializer_class = UserSerializer
    queryset = User.objects.filter()
    permission_classes = (UserPermission,)

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            user = get_object_or_404(User, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Accedio al detalle de un usuario',
                content_object=user
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            user = get_object_or_404(User, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito un usuario',
                content_object=user
            )
            history.save()
        return response

    def patch(self, request, *args, **kwargs):
        response = super().patch(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            user = get_object_or_404(User, pk=kwargs['pk'])
            history = HistoryUser(
                user=request.user,
                description='Edito parcialmente un usuario',
                content_object=user
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        """
        DELETE Method for RetrieveUpdate View
        """
        user = get_object_or_404(User, pk=kwargs['pk'])
        if user.is_active:
            user.is_active = False
            user.save()
            history = HistoryUser(
                user=request.user,
                description='Borro de manera logica un usuario',
                content_object=user
            )
            history.save()
            response = Response({"message": "Successful operation."}, status=status.HTTP_200_OK)
        else:
            response = Response({"message": "User is not enabled."}, status=status.HTTP_200_OK)
        return response


class UserReactivateView(UpdateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.filter()
    permission_classes = (UserPermission,)

    def put(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=kwargs['pk'])
        if not user.is_active:
            user.is_active = True
            user.save()
            history = HistoryUser(
                user=request.user,
                description='Habilito de manera logica un usuario',
                content_object=user
            )
            history.save()
            return Response({"message": "Successful operation."}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "User is enabled."}, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        return self.put(self, request, *args, **kwargs)


class UserListLiteAPIView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (IsAuthenticated, UserPermission)
    serializer_class = UserLiteSerializer
    queryset = User.objects.filter(is_active=True)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['groups']
    ordering_fields = ['username', 'first_name', 'last_name']


class UserInformationUpdateAPIView(generics.UpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserInformationSerializer
    permission_classes = [MyselfOrSuperuserPermission]

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Actualizo su propia información personal',
                content_object=instance
            )
            history.save()
        return response


class UserPreRegistrationListAPIView(generics.ListAPIView):
    queryset = PreRegistration.objects.all()
    serializer_class = UserPreRegistrationSerializer
    filter_backends = [
        DjangoFilterBackend,
        SearchFilter
    ]
    filterset_fields = [
        'state',
        'user__groups__id'
    ]
    search_fields = [
        '^user__groups__name',
        '^user__first_name',
        '^user__last_name',
        '^user__email',
    ]
    permission_classes = (ModelPermission,)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de pre registro de usuarios'
            )
            history.save()
        return response


class UserPreRegistrationCreateApiView(generics.CreateAPIView):
    queryset = PreRegistration.objects.all()
    serializer_class = UserPreRegistrationCreateSerializer

    def post(self, request):
        try:
            response = super(UserPreRegistrationCreateApiView, self).post(request)
            return Response(data=response.data,
                            status=status.HTTP_201_CREATED)

        except IntegrityError:
            return Response({"message": "El nombre de usuario ya esta en uso"},
                            status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'message': f'{e}'},
                            status=status.HTTP_400_BAD_REQUEST)


class ValidatePreRegistrationAPIView(generics.UpdateAPIView):
    queryset = PreRegistration.objects.all()
    permission_classes = [ModelPermission]

    def put(self, request, *args, **kwargs):
        pre_registration = self.get_object()
        validated = False
        sender = EmailSender()
        if 'validated' in self.request.data:
            validated = bool(request.data['validated'])
        if validated:
            pre_registration.state = True
            pre_registration.save()
            sender.send_email(
                'VALIDACIÓN DE USUARIO',
                [pre_registration.user.email],
                'user_validated.html',
                {},
                file=None
            )
            # Log for create user and update user data
            profile = UserProfile.objects.get(user=pre_registration.user)
            if profile:
                profile.created_by = None
                profile.updated_by = request.user
            # ========================================
            pre_registration.user.is_active = True
            pre_registration.user.save()
            history = HistoryUser(
                user=request.user,
                description='Valido el usuario',
                content_object=pre_registration.user
            )
            history.save()
            return Response(
                {
                    'detail': 'Usuario Validado'
                },
                status=status.HTTP_200_OK
            )
        else:
            sender.send_email(
                'VALIDACIÓN DE USUARIO',
                [pre_registration.user.email],
                'user_unvalidated.html',
                {},
                file=None
            )
            pre_registration.user.delete()
            history = HistoryUser(
                user=request.user,
                description='Elimino una solicitud de usuario'
            )
            history.save()
            return Response(
                {
                    'detail': 'Usuario Eliminado'
                },
                status=status.HTTP_200_OK
            )


class UploadFileGraduates(generics.CreateAPIView):
    class Error:
        def __init__(self, row: Series, error_description):
            self.id = row['#']
            self.full_name = '{} {}'.format(row['Nombres'], row['Apellidos'])
            self.description = error_description

        @property
        def data(self):
            return {
                'id': self.id,
                'full_name': self.full_name,
                'description': self.description
            }

    class Data:
        def __init__(self):
            self.success_count = 0
            self.errors = []

        @property
        def data(self):
            return {
                'success': self.success_count,
                'errors': [error.data for error in self.errors]
            }

    class SendEmail:
        def __init__(self):
            self.flag = False
            self.username = None
            self.password = None

    permission_classes = [ModelPermission]
    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = FileUploadFileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file_obj = request.FILES['file']
        file_data = BytesIO(file_obj.read())
        df = pandas.read_excel(file_data)
        data = UploadFileGraduates.Data()
        roles = UserGroup.objects.filter(
            profile_type=ContentType.objects.get_for_model(GraduateProfile)
        )
        df.apply(lambda row: self.__load_row(row, data, roles, request.user), axis=1)
        return Response(data.data)

    def __load_row(self, row: Series, data: Data, roles, user: User):
        send_email = UploadFileGraduates.SendEmail()
        sender = EmailSender()
        profile = self.get_graduate_profile(row, data, roles, send_email)
        data_academic = self.get_data_academic(row, profile, data)
        if data_academic and profile and profile.user:
            if profile.created_by:
                profile.update_by = user
            else:
                profile.created_by = user
            profile.user.save()
            profile.save()
            for role in roles:
                role.user_set.add(profile.user)
            data_academic.save()
            data.success_count += 1
            if send_email.flag:
                sender.send_email(
                    'Registro en OSE UNSA',
                    [profile.user.email],
                    'send_password.html',
                    {
                        'username': send_email.username,
                        'password': send_email.password
                    }
                )

    @staticmethod
    def get_data_academic(row: Series, profile: GraduateProfile, data: Data):
        program_verify = ProfessionalProgram.objects.filter(name=row['Carrera'])
        if len(program_verify) == 0:
            label_error = 'Programa Profesional no encontrado'
            error = UploadFileGraduates.Error(row, label_error)
            data.errors.append(error)
            return
        program_verify_prev = GraduateDataAcademic.objects.filter(
            graduate_profile_pk=profile,
            profesional_program_pk=program_verify[0]
        )
        if len(program_verify_prev) > 0:
            label_error = 'Egresado previamente registrado en este programa profesional'
            error = UploadFileGraduates.Error(row, label_error)
            data.errors.append(error)
            return
        data_academic = GraduateDataAcademic()
        data_academic.graduate_profile_pk = profile
        data_academic.profesional_program_pk = program_verify[0]

        try:
            data_academic.year_graduate = int(row['Año de Graduación'])
        except ValueError:
            data_academic.year_graduate = None

        try:
            data_academic.year_of_university_studies = int(row['Grado de Estudios Universitarios'])
        except ValueError:
            data_academic.year_of_university_studies = None

        if row['Es Egresado'] == 'Sí':
            data_academic.is_graduate = True
            data_academic.is_student = False
        else:
            data_academic.is_graduate = False
            data_academic.is_student = True
        return data_academic

    @staticmethod
    def validate_user(row: Series, data: Data, send_email: SendEmail):
        if not UploadFileGraduates.validate_email(row):
            label_error = 'Correo Electronico no pertenece a la universidad'
            error = UploadFileGraduates.Error(row, label_error)
            data.errors.append(error)
            return
        user = User()
        user.first_name = row['Nombres']
        user.last_name = row['Apellidos']
        user.email = row['Correo']
        UploadFileGraduates.generate_username_password(row, user)
        send_email.flag = True
        send_email.password = user.password
        send_email.username = user.username
        user.set_password(user.password)
        user.is_active = True
        return user

    @staticmethod
    def get_graduate_profile(row: Series, data: Data, roles, send_email: SendEmail):
        verify_cui = GraduateProfile.objects.filter(cui=row['CUI'])
        if len(verify_cui) > 0:
            return verify_cui[0]
        else:
            user = UploadFileGraduates.validate_user(row, data, send_email)
            if user:
                profile = UploadFileGraduates.validate_profile_data(row, data, user)
                return profile

    @staticmethod
    def validate_profile_data(row: Series, data: Data, user: User):
        profile = GraduateProfile()
        profile.user = user
        identificator = Identificator.objects.filter(code=row['Documento'])
        if len(identificator) == 0:
            label_error = 'No se encontro el tipo de documento ingresado'
            error = UploadFileGraduates.Error(row, label_error)
            data.errors.append(error)
            return None
        profile.identificator = identificator[0]
        verify_document = UserProfile.objects.filter(identificator=identificator[0], document=row['Nro Documento'])
        if len(verify_document) > 0:
            label_error = 'Documento de Identificacion duplicado'
            error = UploadFileGraduates.Error(row, label_error)
            data.errors.append(error)
            return None
        profile.document = row['Nro Documento']
        profile.cui = row['CUI']
        gender = {
            'M': True,
            'F': False
        }
        profile.gender = gender[row['Genero']]
        return profile

    @staticmethod
    def generate_username_password(row: Series, user: User):
        first_name = "".join([min_str[0:2] for min_str in user.last_name.split(" ")])
        last_name = "".join([min_str[0:2] for min_str in user.first_name.split(" ")])
        username = "{}{}{}".format(first_name, last_name, row["CUI"]).lower()
        user.password = UploadFileGraduates.generate_password(username)
        user.username = username

    @staticmethod
    def generate_password(seed):
        seed_list = list(seed)
        for i, c in enumerate(seed_list):
            i_rand = random.randint(0, len(seed_list) - 1)
            seed_list[i] = seed_list[i_rand]
            seed_list[i_rand] = c
        return "".join(seed_list)

    @staticmethod
    def validate_email(row: Series):
        regex_str = '^.*@unsa.edu.pe$'
        return re.search(regex_str, row['Correo'])
