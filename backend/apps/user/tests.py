"""
Test case for user module
"""
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APIRequestFactory, force_authenticate

from apps.group.models import UserGroup
from apps.user.models import PreRegistration
from apps.user.serializer import UserPreRegistrationSerializer
from apps.user.serializer import UserSerializer, UserInformationSerializer
# Create your tests here.
from apps.user.views import UserInformationUpdateAPIView
from monitoring_graduates import settings


class UserAPIViewsTestCase(TestCase):
    """
    Test cases for API Views  from group module
    """

    def setUp(self):
        """
        Configuration for test cases
        """
        self.user = User.objects.create_superuser(username='admin', password='qwertyuiop')
        self.client = APIClient()
        self.client.login(username='admin', password='qwertyuiop')
        self.client.force_authenticate(user=self.user)
        User.objects.create(username='user01test', email="edawin1@gmail.com", is_active=True)
        User.objects.create(username='user02test', email="edawin2@gmail.com", is_active=True)
        User.objects.create(username='user03test', email="edawin3@gmail.com", is_active=False)

    def test_list_users(self):
        """
        Test case for list group
        """
        response = self.client.get('/user/')
        serializer = UserSerializer(User.objects.all().filter(is_superuser=True, is_staff=True))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Pagination causes the response being different
        # self.assertEqual(response.data, serializer.data)


class UserInformationUpdateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = UserInformationUpdateAPIView.as_view()
        self.serializer = UserInformationSerializer

        self.url_path_update = '/user/information'
        self.id_test = 1

        self.retrieve_queryset = None
        self.retrieve_request = None
        self.update_request = None
        self.update_queryset = None
        self.destroy_request = None
        self.destroy_queryset = None

    def test_update_with_admin_user_but_not_himself(self):
        # Init var for this test
        instance = User.objects.create(username="whitecat", password=make_password("12345678"))
        instance.username = "whitecat_2"
        instance.save()
        # Serialized data
        serialized = self.serializer(instance, many=False)

        # Init var for this test PUT Method
        data = serialized.data.copy()
        data['password'] = instance.password
        self.update_request = self.factory.put(self.url_path_update, data, format='json')

        # Auth User
        user = User.objects.create(username='darkcat', is_superuser=True)
        # Force Authentication
        force_authenticate(self.update_request, user=user)

        # Call the Update API View
        response = self.view(self.update_request, pk=instance.pk)

        self.update_queryset = User.objects.get(username="whitecat_2")

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data["username"], serialized.data["username"])

    def test_update_with_himself_user(self):
        # Init var for this test
        instance = User.objects.create(id=self.id_test, username="whitecat", password=make_password("12345678"))
        instance.username = "whitecat_2"
        # Serialized data
        serialized = self.serializer(instance, many=False)

        data = serialized.data.copy()
        data['password'] = instance.password

        # Init var for this test PUT Method
        self.update_request = self.factory.put(self.url_path_update, data, format='json')
        # Auth User
        auth_user = User.objects.get(username="whitecat")
        # Force Authentication
        force_authenticate(self.update_request, user=auth_user)

        # Call the Update API View
        response = self.view(self.update_request, pk=self.id_test)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data["username"], serialized.data["username"])


class UserPreRegisterViewTestCase(TestCase):
    """
    Test cases for API Views  from user at pre-register
    """

    def setUp(self):
        """
        Configuration for test cases
        """
        self.client = APIClient()
        self.user = User.objects.get_or_create(username='testuser')[0]
        self.group = UserGroup.objects.create(name='Test Group 1', state=True)
        permissions_list = Permission.objects.all()
        self.group.permissions.set(permissions_list)
        self.user.groups.add(self.group)
        self.client.force_authenticate(self.user)
        PreRegistration.objects.create(user=self.user, state=False)

    def test_list_pre_register_user(self):
        """
        Test case for list of pre register
        """
        response = self.client.get('/user/list-pre-registration/')
        serializer = UserPreRegistrationSerializer(PreRegistration.objects.all())
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UploadExcelFileWithGraduatesAPIViewTestCase(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.file = open(str(settings.BASE_DIR.parent) + '\\apps\\user\\data-test\\File Format.xlsx', 'rb')
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)

    def test_upload_excel_file(self):
        data = {
            "file": self.file
        }
        response = self.client.post('/user/excel_upload/', data, format='multipart')
        # print(str(response.data))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
