from django.urls import path

from apps.user.views import UserListView, UserDetailView, UserReactivateView, UserListLiteAPIView, \
    UserPreRegistrationListAPIView, UserPreRegistrationCreateApiView, \
    UserInformationUpdateAPIView, ValidatePreRegistrationAPIView, UploadFileGraduates

urlpatterns = [
    path('', UserListView.as_view()),
    path('<int:pk>', UserDetailView.as_view()),
    path('lite/', UserListLiteAPIView.as_view()),
    path('reactivate/<int:pk>', UserReactivateView.as_view()),
    path('information/<int:pk>', UserInformationUpdateAPIView.as_view()),
    path('validate/<int:pk>', ValidatePreRegistrationAPIView.as_view()),
    # url('send/',SendImg2Home.as_view())
    path('list-pre-registration/', UserPreRegistrationListAPIView.as_view()),
    path('create-pre-registration/', UserPreRegistrationCreateApiView.as_view()),
    path('excel_upload/', UploadFileGraduates.as_view()),
]
