"""
Serializer for auth_user module
"""
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from rest_framework import serializers
from apps.user.models import PreRegistration
from apps.identificators.models import Identificator
from apps.user_profile.models import AdminProfile, SuperAdminProfile, UserProfile, CompanyProfile, GraduateProfile
from apps.instance.models import ProfessionalProgram, Faculty, Instance, Area
from apps.job_position.models import JobPosition
from apps.employee.models import TypeEmployee

from apps.group.models import UserGroup
from apps.group.serializer import GroupExtraLiteSerializer, GroupLiteSerializer, GroupSerializer
from apps.user.models import PreRegistration


class AuthUserSerializer(serializers.ModelSerializer):
    """
    Serializer for Group model
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = User
        fields = '__all__'

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True


class UserSerializer(serializers.ModelSerializer):
    """
    User Serializer ModelSerializer
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'is_superuser',
            'is_active',
            'last_login',
            'date_joined',
            'groups',
            'user_permissions'
        ]

    def to_representation(self, instance):
        data = super(UserSerializer, self).to_representation(instance)
        try:
            data['profile'] = instance.profiles.id
        except User.profiles.RelatedObjectDoesNotExist:
            data['profile'] = None

        groups = []
        for group in instance.groups.all():
            groups.append(UserGroup.objects.get(pk=group.pk))
        data['groups'] = GroupLiteSerializer(instance=groups, many=True).data
        return data


class UserGraduateSerializer(serializers.ModelSerializer):
    """
    User Serializer ModelSerializer
    """

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'is_active',
            'last_login',
            'date_joined',
        ]

    def to_representation(self, instance):
        data = super(UserGraduateSerializer, self).to_representation(instance)
        try:
            data['profile'] = instance.profiles.id
        except User.profiles.RelatedObjectDoesNotExist:
            data['profile'] = None
        return data


class UserExtendSerializer(serializers.ModelSerializer):
    """
    User Serializer ModelSerializer
    """

    # groups = GroupExtraLiteSerializer(many=True, read_only=True)

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'is_superuser',
            'is_active',
            'last_login',
            'date_joined',
            'groups',
            'user_permissions'
        ]

    def to_representation(self, instance):
        data = super(UserExtendSerializer, self).to_representation(instance)
        groups_id = [group.id for group in instance.groups.all()]
        data['groups'] = GroupLiteSerializer(
            many=True,
            instance=UserGroup.objects.filter(id__in=groups_id),
            context=self.context
        ).data
        data['profile'] = {}
        super_admin_profile = SuperAdminProfile.objects.filter(user=instance)
        if len(super_admin_profile) > 0:
            from apps.user_profile.serializer import SuperAdminProfileSerializer
            data['profile'] = SuperAdminProfileSerializer(
                instance=super_admin_profile[0],
                context=self.context
            ).data
        super_admin_profile = AdminProfile.objects.filter(user=instance)
        if len(super_admin_profile) > 0:
            from apps.user_profile.serializer import AdminProfileSerializer
            data['profile'] = AdminProfileSerializer(
                instance=super_admin_profile[0],
                context=self.context
            ).data
        super_admin_profile = CompanyProfile.objects.filter(user=instance)
        if len(super_admin_profile) > 0:
            from apps.user_profile.serializer import CompanyProfileSerializer
            data['profile'] = CompanyProfileSerializer(
                instance=super_admin_profile[0],
                context=self.context
            ).data

        graduate_profile = GraduateProfile.objects.filter(user=instance)
        if len(graduate_profile) > 0:
            from apps.user_profile.serializer import GraduateProfileDetailSerializer
            data['profile'] = GraduateProfileDetailSerializer(
                instance=graduate_profile[0],
                context=self.context
            ).data
        return data


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        # Implemented for other
        # validated_data["is_staff"] = True
        # validated_data["is_superuser"] = True
        validated_data["password"] = make_password(validated_data["password"])
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super(UserCreateSerializer, self).to_representation(instance)
        groups = []
        for group in instance.groups.all():
            groups.append(UserGroup.objects.get(pk=group.pk))
        data['groups'] = GroupLiteSerializer(instance=groups, many=True).data
        return data


class UserLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class UserInformationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'password',
            'first_name',
            'last_name',
            'email',
            'groups'
        ]

    def update(self, instance, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        data = super(UserInformationSerializer, self).to_representation(instance)
        groups = []
        for group in instance.groups.all():
            groups.append(UserGroup.objects.get(pk=group.pk))
        data['groups'] = GroupLiteSerializer(instance=groups, many=True).data
        return data


class UserPreRegistrationSerializer(serializers.ModelSerializer):
    user = UserExtendSerializer(read_only=True, many=False)

    class Meta:
        model = PreRegistration
        fields = '__all__'


class UserPreRegistrationCreateSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    email = serializers.CharField(write_only=True, required=True)
    username = serializers.CharField(write_only=True, required=True)
    first_name = serializers.CharField(write_only=True, required=True)
    last_name = serializers.CharField(write_only=True, required=True)
    password = serializers.CharField(write_only=True, required=True)
    group_id = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = PreRegistration
        fields = [
            'id',
            'user',
            'email',
            'username',
            'first_name',
            'last_name',
            'password',
            'group_id',
        ]

    def create(self, validated_data):
        username = validated_data.pop('username', None)
        password = validated_data.pop('password', None)
        email = validated_data.pop('email', None)
        group_id = validated_data.pop('group_id', None)
        g = UserGroup.objects.get(id=group_id)
        user = User.objects.create_user(
            username=username,
            password=password,
            email=email
        )
        user.is_superuser = False
        user.first_name = validated_data['first_name']
        user.last_name = validated_data['last_name']
        user.is_staff = False
        user.is_active = False
        user.save()
        g.user_set.add(user)
        PreRegistration.objects.create(
            user=user,
            state=False,
        )
        return user


class UserMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name']

    def to_representation(self, instance):
        data = super(UserMiniSerializer, self).to_representation(instance)
        data['profile'] = None
        if hasattr(instance, 'profiles'):
            super_admin_profile = SuperAdminProfile.objects.filter(id=instance.profiles.id)
            if len(super_admin_profile) > 0:
                from apps.user_profile.serializer import SuperAdminMiniSerializer
                data['profile'] = SuperAdminMiniSerializer(super_admin_profile[0], context=self.context).data
            else:
                admin_profile = AdminProfile.objects.filter(id=instance.profiles.id)
                if len(admin_profile) > 0:
                    from apps.user_profile.serializer import AdminMiniSerializer
                    data['profile'] = AdminMiniSerializer(admin_profile[0], context=self.context).data
                else:
                    company_profile = CompanyProfile.objects.filter(id=instance.profiles.id)
                    if len(company_profile) > 0:
                        from apps.user_profile.serializer import CompanyMiniSerializer
                        data['profile'] = CompanyMiniSerializer(company_profile[0], context=self.context).data
                    else:
                        graduate_profile = GraduateProfile.objects.filter(id=instance.profiles.id)
                        if len(graduate_profile) > 0:
                            from apps.user_profile.serializer import GraduateMiniSerializer
                            data['profile'] = GraduateMiniSerializer(graduate_profile[0], context=self.context).data
                        else:
                            from apps.user_profile.serializer import UserProfileMiniSerializer
                            data['profile'] = UserProfileMiniSerializer(instance.profiles, context=self.context).data
        return data


class UserMiniAlternativeSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email']

    def to_representation(self, instance):
        data = super(UserMiniAlternativeSerializer, self).to_representation(instance)
        data['profile'] = None
        if hasattr(instance, 'profiles'):
            super_admin_profile = SuperAdminProfile.objects.filter(id=instance.profiles.id)
            if len(super_admin_profile) > 0:
                from apps.user_profile.serializer import SuperAdminMiniSerializer
                data['profile'] = SuperAdminMiniSerializer(super_admin_profile[0], context=self.context).data
            else:
                admin_profile = AdminProfile.objects.filter(id=instance.profiles.id)
                if len(admin_profile) > 0:
                    from apps.user_profile.serializer import AdminMiniSerializer
                    data['profile'] = AdminMiniSerializer(admin_profile[0], context=self.context).data
                else:
                    company_profile = CompanyProfile.objects.filter(id=instance.profiles.id)
                    if len(company_profile) > 0:
                        from apps.user_profile.serializer import CompanyMiniSerializer
                        data['profile'] = CompanyMiniSerializer(company_profile[0], context=self.context).data
                    else:
                        graduate_profile = GraduateProfile.objects.filter(id=instance.profiles.id)
                        if len(graduate_profile) > 0:
                            from apps.user_profile.serializer import GraduateMiniSerializer
                            data['profile'] = GraduateMiniSerializer(graduate_profile[0], context=self.context).data
                        else:
                            from apps.user_profile.serializer import UserProfileMiniSerializer
                            data['profile'] = UserProfileMiniSerializer(instance.profiles, context=self.context).data
        return data

class FileUploadFileSerializer(serializers.Serializer):
    file = serializers.FileField(required=True)
