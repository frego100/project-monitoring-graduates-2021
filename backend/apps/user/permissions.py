from django.contrib.auth.models import User
from rest_framework import permissions


class UserPermission(permissions.BasePermission):
    def has_permission(self, request, view):

        if request.method == 'GET':
            return request.user.has_perm('auth.view_user')
        if request.method == 'POST':
            return request.user.has_perm('auth.add_user')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('auth.change_user')
        if request.method == 'DELETE':
            return request.user.has_perm('auth.delete_user')
        return False


class MyselfOrSuperuserPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if not (isinstance(obj, User) and isinstance(request.user, User)):
            return False
        else:
            return (obj == request.user and obj.is_active) or request.user.is_superuser
