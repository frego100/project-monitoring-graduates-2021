# Create your models here.
from django.contrib.auth.models import User
from django.db import models


class PreRegistration(models.Model):
    """
    User profile generic
    """
    user = models.ForeignKey(
        User,
        null=False,
        on_delete=models.CASCADE,
        verbose_name='User'
    )
    state = models.BooleanField(
        default=False
    )

    class Meta:
        verbose_name = 'Pre registro de Usuario'
        verbose_name_plural = 'Pre registro de Usuario'
