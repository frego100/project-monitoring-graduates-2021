from django.contrib import admin

# Register your models here.
from apps.graduate_questions.models import GraduateQuestions

admin.site.register(GraduateQuestions)
