from django.apps import AppConfig


class GraduateQuestionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.graduate_questions'
