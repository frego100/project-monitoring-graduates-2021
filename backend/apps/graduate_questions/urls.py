from django.urls import path

from apps.graduate_questions.views import GraduateQuestionsReactivateAPIView, GraduateQuestionsListCreateAPIView, \
    GraduateQuestionsRetrieveUpdateDestroyAPIView, GraduateQuestionsListCreateAuthAPIView, \
    GraduateQuestionsRetrieveUpdateDestroyAuthAPIView, GraduateQuestionsReactivateAuthAPIView

urlpatterns = [
    path('', GraduateQuestionsListCreateAPIView.as_view()),
    path('<int:pk>', GraduateQuestionsRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', GraduateQuestionsReactivateAPIView.as_view()),
    path('auth/', GraduateQuestionsListCreateAuthAPIView.as_view()),
    path('auth/<int:pk>', GraduateQuestionsRetrieveUpdateDestroyAuthAPIView.as_view()),
    path('auth/reactivate/<int:pk>', GraduateQuestionsReactivateAuthAPIView.as_view()),
]
