from django.db import models

# Create your models here.
from apps.question.models import Question
from apps.user_profile.models import GraduateProfile


class GraduateQuestions(models.Model):
    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=True,
        on_delete=models.CASCADE,
        related_name='graduate_profile_graduate_questions'
    )

    question_pk = models.ForeignKey(
        Question,
        null=True,
        on_delete=models.CASCADE,
        related_name='graduate_questions'
    )

    value = models.BooleanField(
        null=False
    )

    is_active = models.BooleanField(
        null=False,
        default=True,
        verbose_name="Active"
    )

    def __str__(self):
        return f'{type(self).__name__}({", ".join("%s=%s" % item for item in vars(self).items() if item[0][0] != "_")})'

    class Meta:
        verbose_name = "Preguntas de egresados"
        verbose_name_plural = "Preguntas de los egresados"
