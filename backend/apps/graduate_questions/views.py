from django.contrib.auth.models import User
from django.http import QueryDict
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.graduate_questions.models import GraduateQuestions
from apps.graduate_questions.serializers import GraduateQuestionsSerializer
from apps.history_user.models import HistoryUser
from apps.user_profile.models import GraduateProfile
from monitoring_graduates.permission import ModelPermission


class GraduateQuestionsListCreateAPIView(generics.ListCreateAPIView):
    queryset = GraduateQuestions.objects.all()
    serializer_class = GraduateQuestionsSerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'question_pk': ['exact'],
        'graduate_profile_pk': ['exact'],
        'value': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'value',
        'is_active'
    ]

    search_fields = [
        'value'
    ]

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and isinstance(request.user, User):
            instance = GraduateQuestions.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un pregunta de egresado',
                content_object=instance
            )
            history.save()
        return response


class GraduateQuestionsRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GraduateQuestions.objects.all()
    serializer_class = GraduateQuestionsSerializer

    permission_classes = [ModelPermission]

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Accedio a una pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Actualizo una pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Deshabilito un pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class GraduateQuestionsReactivateAPIView(generics.UpdateAPIView):
    queryset = GraduateQuestions.objects.filter(is_active=False)
    serializer_class = GraduateQuestionsSerializer
    permission_classes = [ModelPermission]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Habilito una pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response


class GraduateQuestionsListCreateAuthAPIView(generics.ListCreateAPIView):
    serializer_class = GraduateQuestionsSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'question_pk': ['exact'],
        'value': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'value',
        'is_active'
    ]

    search_fields = [
        'value'
    ]

    def get_queryset(self):
        return GraduateQuestions.objects.filter(graduate_profile_pk__user=self.request.user)

    def post(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        # Set graduate_profile_pk
        request.data['graduate_profile_pk'] = GraduateProfile.objects.get(user=request.user).pk
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and isinstance(request.user, User):
            instance = GraduateQuestions.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un pregunta de egresado',
                content_object=instance
            )
            history.save()
        return response


class GraduateQuestionsRetrieveUpdateDestroyAuthAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = GraduateQuestionsSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return GraduateQuestions.objects.filter(graduate_profile_pk__user=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Accedio a una pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response

    def update(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        # Set graduate_profile_pk
        request.data['graduate_profile_pk'] = GraduateProfile.objects.get(user=request.user).pk
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Actualizo una pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Deshabilito un pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class GraduateQuestionsReactivateAuthAPIView(generics.UpdateAPIView):
    serializer_class = GraduateQuestionsSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return GraduateQuestions.objects.filter(graduate_profile_pk__user=self.request.user, is_active=False)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateQuestions, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Habilito una pregunta de egresado',
                content_object=instance
            )
            history_user.save()
        return response
