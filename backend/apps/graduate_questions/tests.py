from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient


# Create your tests here.
from apps.graduate_publications.models import GraduatePublications
from apps.graduate_questions.models import GraduateQuestions
from apps.graduate_questions.serializers import GraduateQuestionsSerializer
from apps.identificators.models import Identificator
from apps.question.models import Question
from apps.user_profile.models import GraduateProfile


class GraduateQuestionsAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.identificator = Identificator.objects.create(
            code='DNI',
            max_length=8,
            name='DNI',
            alphabet='NUM'
        )
        self.graduate_profile = GraduateProfile.objects.create(
            gender=True,
            cui=20173385,
            date_birthday='2000-04-12',
            web='https://clockify.me/tracker',
            address='Mi casa',
            user=self.user,
            identificator=self.identificator,
            document='72659413',
            cell_phone='987654321',
            phone='987654',
        )
        self.question = Question.objects.create(
            name='Question for testing'
        )
        self.second_question = Question.objects.create(
            name='Second Question for testing'
        )
        self.question_profile = GraduateQuestions.objects.create(
            graduate_profile_pk=self.graduate_profile,
            question_pk=self.question,
            value=False
        )

    def test_list_questions_profile(self):
        """
        Test list question profile
        """
        response = self.client.get('/graduate-questions/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_questions_profile(self):
        """
        Test create question profile
        """
        prev_len = len(GraduateQuestions.objects.all())
        data = {
            "graduate_profile_pk": self.graduate_profile.id,
            "question_pk": self.second_question.id,
            "value": True
        }

        response = self.client.post('/graduate-questions/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(GraduateQuestions.objects.all()))

    def test_retrieve_questions_profile(self):
        """
        Test retrieve question profile
        """
        response = self.client.get('/graduate-questions/{}'.format(self.question_profile.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, GraduateQuestionsSerializer(self.question_profile).data)

    def test_update_questions_profile(self):
        """
        Test update question profile
        """
        data = {
            "graduate_profile_pk": self.graduate_profile.id,
            "question_pk": self.second_question.id,
            "value": True
        }
        response = self.client.put('/graduate-questions/{}'.format(self.question_profile.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            GraduateQuestionsSerializer(
                GraduateQuestions.objects.get(id=self.question_profile.id)
            ).data
        )

    def test_destroy_questions_profile(self):
        """
        Test destroy question profile
        """
        prev_len = len(GraduateQuestions.objects.filter(is_active=True))
        response = self.client.delete('/graduate-questions/{}'.format(self.question_profile.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(GraduateQuestions.objects.filter(is_active=True)))

    # def test_list_questions_profile_auth(self):
    #    """
    #    Test list question profile for user authenticated
    #    """
    #    prev_len = len(GraduateQuestions.objects.filter(graduate_profile_pk__user=self.user))
    #    response = self.client.get('/graduate-questions/auth/')
    #    self.assertEqual(response.status_code, status.HTTP_200_OK)
    #    self.assertEqual(prev_len, len(response.data['results']))

    # def test_create_questions_profile_auth(self):
    #    """
    #    Test create question profile for user authenticated
    #    """
    #    prev_len = len(GraduateQuestions.objects.filter(graduate_profile_pk__user=self.user))
    #    data = {
    #        "question_pk": self.second_question.id,
    #        "value": True
    #    }

    #    response = self.client.post('/graduate-questions/auth/', data)
    #    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #    self.assertEqual(prev_len + 1, len(GraduateQuestions.objects.filter(graduate_profile_pk__user=self.user)))

    # def test_retrieve_questions_profile_auth(self):
    #    """
    #    Test retrieve question profile for user authenticated
    #    """
    #    response = self.client.get('/graduate-questions/auth/{}'.format(self.question_profile.id))
    #    self.assertEqual(response.status_code, status.HTTP_200_OK)
    #    self.assertEqual(response.data, GraduateQuestionsAuthSerializer(self.question_profile).data)

    # def test_update_questions_profile_auth(self):
    #    """
    #    Test update question profile for user authenticated
    #    """
    #    data = {
    #        "question_pk": self.second_question.id,
    #        "value": True
    #    }

    #    response = self.client.put('/graduate-questions/auth/{}'.format(self.question_profile.id), data)
    #    self.assertEqual(response.status_code, status.HTTP_200_OK)
    #    self.assertEqual(
    #        response.data,
    #        GraduateQuestionsAuthSerializer(
    #            GraduatePublications.objects.get(id=self.question_profile.id)
    #        ).data
    #    )

    # def test_destroy_questions_profile_auth(self):
    #    """
    #    Test destroy question profile for user authenticated
    #    """
    #    prev_len = len(GraduatePublications.objects.filter(graduate_profile_pk__user=self.user, is_active=True))
    #    response = self.client.delete('/graduate-questions/auth/{}'.format(self.question_profile.id))
    #    self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    #    self.assertEqual(prev_len - 1, len(GraduatePublications.objects.filter(
    #    graduate_profile_pk__user=self.user, is_active=True)))
