from rest_framework import serializers

from apps.graduate_questions.models import GraduateQuestions
from apps.question.serializers import QuestionSerializer


class GraduateQuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateQuestions
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.question_pk:
            data['question_pk'] = QuestionSerializer(
                instance.question_pk,
                context=self.context,
                many=False
            ).data
        return data
