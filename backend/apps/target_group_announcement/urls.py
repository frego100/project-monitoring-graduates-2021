from django.urls import path

from apps.target_group_announcement.views import TargetGroupAnnouncementListCreateAPIView, \
    TargetGroupAnnouncementReactivateAPIView, TargetGroupAnnouncementRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('', TargetGroupAnnouncementListCreateAPIView.as_view()),
    path('<int:pk>', TargetGroupAnnouncementRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', TargetGroupAnnouncementReactivateAPIView.as_view())
]
