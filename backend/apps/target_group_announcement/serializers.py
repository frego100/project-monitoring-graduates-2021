from rest_framework import serializers

from apps.announcement.serializer import AnnouncementSerializerLite
from apps.instance.models import Faculty, Area, ProfessionalProgram
from apps.instance.serializers import AreaSerializer, FacultySerializer, ProfessionalProgramSerializer
from apps.target_group_announcement.models import TargetGroupAnnouncement


def get_instance_serializer(instance, context):
    if len(Area.objects.filter(id=instance.id)):
        return AreaSerializer(Area.objects.get(pk=instance.id), context=context)
    if len(Faculty.objects.filter(id=instance.id)):
        return FacultySerializer(Faculty.objects.get(pk=instance.id), context=context)
    if len(ProfessionalProgram.objects.filter(id=instance.id)):
        return ProfessionalProgramSerializer(ProfessionalProgram.objects.get(pk=instance.id), context=context)


class TargetGroupAnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetGroupAnnouncement
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.announcement:
            data['announcement'] = AnnouncementSerializerLite(instance=instance.announcement, context=self.context).data
        if instance.instance:
            data['instance'] = get_instance_serializer(instance.instance, context=self.context).data
        return data
