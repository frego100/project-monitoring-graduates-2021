from django.apps import AppConfig


class TargetGroupAnnouncementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.target_group_announcement'
