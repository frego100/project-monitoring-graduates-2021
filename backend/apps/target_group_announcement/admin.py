from django.contrib import admin

# Register your models here.
from apps.target_group_announcement.models import TargetGroupAnnouncement

admin.site.register(TargetGroupAnnouncement)
