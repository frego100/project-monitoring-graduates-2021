from datetime import datetime

import pytz
from django.conf.global_settings import TIME_ZONE
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory
from django.test import TestCase
from rest_framework import status, serializers
from rest_framework.test import APIClient

# Create your tests here.
from apps.announcement.models import Announcement
from apps.company.models import Company
# Create your tests here.
from apps.instance.models import Area
from apps.target_group_announcement.models import TargetGroupAnnouncement
from apps.target_group_announcement.serializers import TargetGroupAnnouncementSerializer


class MockSerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetGroupAnnouncement
        fields = '__all__'


class TargetGroupAnnouncementAPIViewTestCase(TestCase):

    def setUp(self) -> None:
        self.model = TargetGroupAnnouncement
        self.list_create_serializer = TargetGroupAnnouncementSerializer
        self.client = APIClient()
        self.crud_url = '/target-group-announcement/'
        self.reactivate_url = '/target-group-announcement/reactivate/'
        self.user = User.objects.create_user('user_test', is_superuser=True)
        self.client.force_authenticate(self.user)
        self.user.save()
        self.file = (
            b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
            b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
            b'\x02\x4c\x01\x00\x3b'
        )
        self.company = Company.objects.create(
            company_name="7r31vTj1",
            social_reason="Zzex",
            identificator=None,
            ruc=647,
            company_activity="7dh",
            logo=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            is_active=True,
        )
        self.announcement = Announcement.objects.create(
            name="PRQZ",
            created_by=self.user,
            updated_by=None,
            created=datetime.now(pytz.timezone(TIME_ZONE)),
            updated=None,
            is_validated=True,
            status=True,
            date_start=datetime.now(pytz.timezone(TIME_ZONE)),
            date_end=datetime.now(pytz.timezone(TIME_ZONE)),
            description="WhHGm42b",
            extra_file=SimpleUploadedFile('small.gif', self.file, content_type='image/gif'),
            company_pk=self.company
        )
        self.instance = Area.objects.create(
            name="889P8q",
            is_active=True
        )
        self.target_group_announcement = self.model.objects.create(
            announcement=self.announcement,
            instance=self.instance,
            text_invitation="eFyUQ",
            is_active=True,
        )

    def test_list_announcement(self):
        response = self.client.get(self.crud_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.list_create_serializer(
                self.model.objects.all(),
                many=True,
                context={'request': RequestFactory().get(self.crud_url)}
            ).data,
            response.data['results']
        )

    def test_create_target_group_announcement(self):
        data = MockSerializer(self.target_group_announcement).data
        response = self.client.post(self.crud_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_target_group_announcement(self):
        data = MockSerializer(self.target_group_announcement).data
        response = self.client.put(self.crud_url + str(self.target_group_announcement.pk), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_target_group_announcement(self):
        response = self.client.get(self.crud_url + str(self.target_group_announcement.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_destroy_target_group_announcement(self):
        response = self.client.delete(self.crud_url + str(self.target_group_announcement.pk))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_reactivate_target_group_announcement(self):
        self.target_group_announcement.is_active = False
        self.target_group_announcement.save()
        response = self.client.put(self.reactivate_url + str(self.target_group_announcement.pk))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
