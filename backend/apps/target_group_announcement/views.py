from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response

from apps.history_user.models import HistoryUser
from apps.target_group_announcement.models import TargetGroupAnnouncement
from apps.target_group_announcement.serializers import TargetGroupAnnouncementSerializer


class TargetGroupAnnouncementListCreateAPIView(generics.ListCreateAPIView):
    queryset = TargetGroupAnnouncement.objects.all()
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = TargetGroupAnnouncementSerializer

    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'announcement__name': ['exact'],
        'announcement': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'announcement__name',
        'is_active'
    ]

    search_fields = [
        'id',
        'announcement__name',
        'is_active'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de grupos objetivo de convocatorias'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            instance = get_object_or_404(TargetGroupAnnouncement, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Creo un nuevo grupo objetivo de convocatorias',
                content_object=instance
            )
            history_user.save()
        return response


class TargetGroupAnnouncementRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = TargetGroupAnnouncement.objects.all()
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = TargetGroupAnnouncementSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Accedio a un grupo objetivo de convocatorias',
                content_object=instance
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un grupo objetivo de convocatorias',
                content_object=instance
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Elimino un grupo objetivo de convocatorias',
                content_object=instance
            )
            history.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class TargetGroupAnnouncementReactivateAPIView(generics.UpdateAPIView):
    queryset = TargetGroupAnnouncement.objects.filter(is_active=False)
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = TargetGroupAnnouncementSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)
