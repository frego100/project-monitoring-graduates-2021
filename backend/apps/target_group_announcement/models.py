from django.db import models

# Create your models here.
from apps.announcement.models import Announcement
from apps.instance.models import Instance


class TargetGroupAnnouncement(models.Model):
    announcement = models.ForeignKey(
        Announcement,
        on_delete=models.CASCADE,
        related_name='announcement_target_announcement'
    )

    instance = models.ForeignKey(
        Instance,
        on_delete=models.CASCADE,
        related_name='instance_target_announcement'
    )

    text_invitation = models.CharField(
        max_length=512,
        null=False,
        blank=False
    )

    is_active = models.BooleanField(
        default=True,
        null=False
    )

    def __str__(self):
        return f'{type(self).__name__}({", ".join("%s=%s" % item for item in vars(self).items() if item[0][0] != "_")})'

    class Meta:
        verbose_name = 'Grupo Objetivo de Convocatorias'
        verbose_name_plural = 'Grupos Objetivo de Convocatorias'
