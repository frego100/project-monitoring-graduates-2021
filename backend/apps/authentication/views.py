"""
Views for authentication module
"""
# Create your views here.
from django.contrib.auth.models import User
from rest_framework import status, generics
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.authentication.serializers import UserAuthenticatedSerializer, UserAuthSerializer
from apps.graduate_projects.utils import log_entry_history_user
from apps.history_user.models import HistoryUser


class AuthTokenAPIView(ObtainAuthToken):
    """
    View for Authentication User by username and password
    """

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if not user.is_active:
            return Response({
                'Error': 'Su usuario ha sido deshabilitado comuniquese con un super administrador'},
                status=status.HTTP_401_UNAUTHORIZED
            )
        token = Token.objects.get_or_create(user=user)

        history = HistoryUser(
            user=user,
            description='Inicio una sesión de usuario'
        )
        history.save()

        return Response({
            'token': token[0].key,
        })


class AuthUserAPIView(APIView):
    """
    View for get data from the current user authenticated
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """
        GET Method for API View
        """
        if not request.user.is_active:
            return Response({
                'detail': 'Usuario deshabilitado'
            },
                status.HTTP_401_UNAUTHORIZED)

        history = HistoryUser(
            user=request.user,
            description='Solicito información de su propio usuario',
            content_object=request.user
        )
        history.save()

        return Response(
            UserAuthenticatedSerializer(
                instance=request.user,
                context={
                    'request': self.request
                }
            ).data,
            status=status.HTTP_200_OK
        )


class LogoutAPIView(ObtainAuthToken):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        request.user.auth_token.delete()

        history = HistoryUser(
            user=request.user,
            description='Cerro una sesión de usuario'
        )
        history.save()

        return Response(status=status.HTTP_200_OK)


class AuthUserInformationUpdateAPIView(generics.UpdateAPIView):
    queryset = User.objects.all()
    model_class = User
    serializer_class = UserAuthSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.get_queryset().get(pk=self.request.user.id)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo su perfil de usuario',
                instance=get_object_or_404(self.model_class, pk=response.data['id'])
            )
        return response


class DeactivateUserAuthenticated(generics.DestroyAPIView):
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()
