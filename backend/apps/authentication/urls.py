"""monitoring_graduates URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from apps.authentication.views import AuthTokenAPIView, AuthUserAPIView, LogoutAPIView, \
    AuthUserInformationUpdateAPIView, DeactivateUserAuthenticated

urlpatterns = [
    path('', AuthTokenAPIView.as_view()),
    path('user/', AuthUserAPIView.as_view()),
    path('logout/', LogoutAPIView.as_view()),
    path('update/information/', AuthUserInformationUpdateAPIView.as_view()),
    path('deactivate/', DeactivateUserAuthenticated.as_view())
]
