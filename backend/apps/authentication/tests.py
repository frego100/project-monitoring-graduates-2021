"""
Test cases for authentication module
"""
from rest_framework import status
from rest_framework.test import APIClient
from django.test import TestCase
from django.contrib.auth.models import User

class AuthenticationAPIViewsTestCase(TestCase):
    """
    Test cases for API Views  from authentication module
    """

    def setUp(self):
        """
        Configuration for test cases
        """
        self.client = APIClient()
        self.user = User.objects.get_or_create(username='testuser')[0]
        self.client.force_authenticate(self.user)
        self.user_url_verify_user = '/auth/user/'

    def test_user_verification(self):
        """
        Test case for user verification
        """
        response_verify_user = self.client.get(self.user_url_verify_user)
        self.assertEqual(response_verify_user.status_code, status.HTTP_200_OK)