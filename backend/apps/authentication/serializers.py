"""
Serializers for Authentication
"""
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework import serializers

from apps.group.models import UserGroup
from apps.group.serializer import GroupSerializer
from apps.user_profile.models import SuperAdminProfile, AdminProfile, CompanyProfile, GraduateProfile
from apps.user_profile.serializer import SuperAdminSerializer, AdminSerializer, CompanyProfileSerializer, \
    GraduateProfileDetailSerializer


class UserAuthenticatedSerializer(serializers.ModelSerializer):
    """
    Serializer for User authenticated
    """

    class Meta:
        """
        Meta class for this
        """
        model = get_user_model()
        fields = (
            'username',
            'first_name',
            'last_name',
            'groups'
        )

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True

    def to_representation(self, instance):
        """
        Method modified to present data
        """
        data = super(__class__, self).to_representation(instance)
        groups = []
        for group in instance.groups.all():
            groups.append(UserGroup.objects.get(pk=group.id))
        data['groups'] = GroupSerializer(groups, many=True).data
        user_profile = SuperAdminProfile.objects.all().filter(user=instance)
        if len(user_profile) != 0:
            data['user_profile'] = SuperAdminSerializer(
                instance=user_profile[0],
                context=self.context
            ).data
        else:
            user_profile = AdminProfile.objects.all().filter(user=instance)
            if len(user_profile) != 0:
                data['user_profile'] = AdminSerializer(
                    instance=user_profile[0],
                    context=self.context
                ).data
            else:
                user_profile = CompanyProfile.objects.all().filter(user=instance)
                if len(user_profile) != 0:
                    data['user_profile'] = CompanyProfileSerializer(
                        instance=user_profile[0],
                        context=self.context
                    ).data
                else:
                    user_profile = GraduateProfile.objects.all().filter(user=instance)
                    if len(user_profile) != 0:
                        data['user_profile'] = GraduateProfileDetailSerializer(
                            instance=user_profile[0],
                            context=self.context
                        ).data
        return data


class UserAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ['is_staff', 'date_joined', 'last_login', 'password', 'is_superuser', 'groups', 'user_permissions']
