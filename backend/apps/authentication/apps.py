"""
Configuration for apps in authentication module
"""
from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    """
    Configuration for authentication appication
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.authentication'
