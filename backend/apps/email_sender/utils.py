"""
Util functions for email sender
"""
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template


class EmailSender:
    """
    Class to send email
    """

    def __init__(
            self,
            email_host = settings.EMAIL_HOST_USER
    ):
        """
        Set host email
        """
        self.email_host = email_host

    def send_email(
            self,
            subject,
            emails,
            template_name,
            data,
            file = None
    ):
        """
        Function to send email
        """
        template = get_template(template_name)
        content = template.render(data)
        message = EmailMultiAlternatives(
            subject,  # Titulo
            '',
            settings.EMAIL_HOST_USER,
            emails
        )  #
        message.attach_alternative(content, 'text/html')
        if file:
            message.attach_file(file)
        message.send()