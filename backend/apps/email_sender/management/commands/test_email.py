"""
Commands for email sender model
"""
from django.core.management import BaseCommand

from apps.email_sender.utils import EmailSender


class Command(BaseCommand):
    """
    Command to test email
    """

    def handle(self, *args, **options):
        """
        Process to test send email
        """
        sender = EmailSender()
        subject = 'Asunto del Correo'
        emails = ['nelsonafp12@gmail.com']
        template_name = 'test_email_template.html'
        data = {
            'data': 'soy data'
        }
        sender.send_email(
            subject,
            emails,
            template_name,
            data
        )