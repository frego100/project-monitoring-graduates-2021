# Create your views here.
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from apps.campus.models import Campus
from apps.campus.serializers import CampusSerializer
from apps.history_user.models import HistoryUser
from monitoring_graduates.permission import ModelPermission


class CampusListCreateAPIView(generics.ListCreateAPIView):
    queryset = Campus.objects.all()
    serializer_class = CampusSerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'name': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'name',
        'is_active'
    ]

    search_fields = [
        'name'
    ]

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and isinstance(request.user, User):
            instance = Campus.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un campus',
                content_object=instance
            )
            history.save()
        return response


class CampusRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Campus.objects.all()
    serializer_class = CampusSerializer
    permission_classes = [ModelPermission]

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Campus, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Accedio a una campus',
                content_object=instance
            )
            history_user.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Campus, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Actualizo un campus',
                content_object=instance
            )
            history_user.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Campus, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Deshabilito un campus',
                content_object=instance
            )
            history_user.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class CampusReactivateAPIView(generics.UpdateAPIView):
    queryset = Campus.objects.filter(is_active=False)
    serializer_class = CampusSerializer
    permission_classes = [ModelPermission]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Campus, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Habilito un Campus',
                content_object=instance
            )
            history_user.save()
        return response
