from django.db import models


# Create your models here.
class Campus(models.Model):

    name = models.CharField(
        blank=False,
        null=False,
        max_length=255
    )

    is_active = models.BooleanField(
        default=True,
        verbose_name='Activo'
    )

    def __str__(self):
        return f'{type(self).__name__}({", ".join("%s=%s" % item for item in vars(self).items() if item[0][0] != "_")})'

    class Meta:
        verbose_name = 'Campus'
        verbose_name_plural = 'Campus'
