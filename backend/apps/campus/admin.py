from django.contrib import admin

# Register your models here.
from apps.campus.models import Campus

admin.site.register(Campus)
