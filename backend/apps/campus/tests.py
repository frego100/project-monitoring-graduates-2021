from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient


# Create your tests here.
from apps.campus.models import Campus
from apps.campus.serializers import CampusSerializer


class GraduateAchievementsAcademicAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.campus = Campus.objects.create(
            name='Campus question'
        )
        self.campus_deactivated = Campus.objects.create(
            name='Campus question',
            is_active=False
        )

    def test_list_campus(self):
        """
        Test list campus
        """
        response = self.client.get('/campus/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_campus(self):
        """
        Test create campus
        """
        prev_len = len(Campus.objects.all())
        data = {
            "name": "Third Question"
        }

        response = self.client.post('/campus/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(Campus.objects.all()))

    def test_retrieve_campus(self):
        """
        Test retrieve campus
        """
        response = self.client.get('/campus/{}'.format(self.campus.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, CampusSerializer(self.campus).data)

    def test_update_campus(self):
        """
        Test update campus
        """
        data = {
            "name": 'Campus edited'
        }
        response = self.client.put('/campus/{}'.format(self.campus.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            CampusSerializer(
                Campus.objects.get(id=self.campus.id)
            ).data
        )

    def test_destroy_campus(self):
        """
        Test destroy campus
        """
        prev_len = len(Campus.objects.filter(is_active=True))
        response = self.client.delete('/campus/{}'.format(self.campus.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(Campus.objects.filter(is_active=True)))

    def test_reactive_campus(self):
        """
        Test reactive campus
        """
        prev_len = len(Campus.objects.filter(is_active=True))
        response = self.client.put('/campus/reactivate/{}'.format(self.campus_deactivated.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len + 1, len(Campus.objects.filter(is_active=True)))
