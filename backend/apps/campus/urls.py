from django.urls import path

from apps.campus.views import CampusListCreateAPIView, CampusRetrieveUpdateDestroyAPIView, CampusReactivateAPIView

urlpatterns = [
    path('', CampusListCreateAPIView.as_view()),
    path('<int:pk>', CampusRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', CampusReactivateAPIView.as_view()),
]
