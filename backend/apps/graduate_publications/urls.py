from django.urls import path
from .views import (
    PublicationCreateListView,
    PublicationDetailView,
    PublicationListType,
    PublicationListFunction,
    PublicationCreateListViewAuth,
    PublicationDetailAuthView

)

urlpatterns = [
    path('', PublicationCreateListView.as_view()),
    path('<int:pk>', PublicationDetailView.as_view()),
    path('type_publication/', PublicationListType.as_view()),
    path('function_type/', PublicationListFunction.as_view()),
    path('auth/', PublicationCreateListViewAuth.as_view()),
    path('auth/<int:pk>', PublicationDetailAuthView.as_view())

]
