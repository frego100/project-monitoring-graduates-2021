from django.apps import AppConfig


class GraduatePublicationsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.graduate_publications'
