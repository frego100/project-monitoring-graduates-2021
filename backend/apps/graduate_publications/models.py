from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.user_profile.models import GraduateProfile


# Create your models here.
class GraduatePublications(models.Model):
    class Types(models.IntegerChoices):
    
        NEWSLETTER_ARTICLE= 1, 'Artículo boletín'
        ARTICLE_IN_SCIENTIFIC_JOURNAL= 2, 'Artículo en revista científica'
        MAGAZINE_ARTICLE=3, 'Artículo magazine'
        NEWSPAPER_ARTICLE= 4,'Artículo periódico '
        CHAPTER_OF_THE_BOOK= 5,'Capítulo de libro'
        DISSERTATION= 6, 'Dissertación'
        WORK_DOCUMENT=7, 'Documento de trabajo'
        DICTIONARY_ENTRY=8, 'Entrada de diccionario '
        ENCYCLOPEDIA_ENCYCLOPEDIA_ENTRY= 9, 'Entrada Enciclopedia/Enciclopedia '
        FASCICLE=10, 'Fascículo'
        BOOK=11, 'Libro'
        EDITED_BOOK=12, 'Libro editado'
        HANDBOOK=13, 'Manual '
        PREFACE_EPILOGUE=14, 'Prefacio, Epilogo'
        SUPERVISED_STUDENT_PUBLICATION=15, 'Publicación estudiantil supervisada '
        ONLINE_RESOURCE=16, 'Recurso en línea'
        REPORT=17, 'Reporte'
        BOOK_REVIEW=18, 'Reseña de libro '
        TEST=19, 'Test '
        TRANSLATION=20, 'Traducción '
        WEBSITE=21, 'Website '
    
    type_publication= models.IntegerField(
        choices=Types.choices,
        null=False
    )
    title= models.CharField(
        max_length=500,
        null=False
        )
    description= models.TextField()

    class Function(models.IntegerChoices):
    
        AUTHOR=1, 'Autor'
        ASSIGNEE=2, 'Cesionario'
        CO_INVENTOR=3, 'Co-inventor'
        CO_INVESTIGATOR=4, 'Co-investigador '
        EDITOR=5, 'Editor'
        POST_GRADUATE_STUDENT=6, 'Estudiante de posgrado'
        POSTDOCTORAL_RESEARCHER=7, 'Investigador postdoctoral'
        PRINCIPAL_INVESTIGATOR=8, 'Investigador principal'
        BOSS_OR_TRANSLATOR=9, 'Jefe o traductor'
        ANOTHER_INVENTOR=10, 'Otro inventor '
        SUPPORT_STAFF=11, 'Personal de apoyo'
        
    function_type= models.IntegerField(
        choices=Function.choices,
        null=False
    )
    DOI= models.CharField(
        max_length=500,
        null=True
        )
    reference_url=models.URLField(
        default=None,
        null=True
    )
    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=True,
        on_delete=models.CASCADE,
        related_name='graduate_profile_publications'
    )
    
