from rest_framework import serializers
from .models import GraduatePublications


class GraduatePublicationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduatePublications
        fields = '__all__'
        
    def to_representation(self, instance):
        data = super(GraduatePublicationsSerializer, self).to_representation(instance)
        if instance.type_publication:
            data['type_publication'] = {
                'id': instance.type_publication,
                'name': GraduatePublications.Types.labels[instance.type_publication - 1]
            }
        if instance.function_type:
            data['function_type'] = {
                'id': instance.function_type,
                'name': GraduatePublications.Function.labels[instance.function_type - 1]
            }
        return data





class GraduatePublicationsAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduatePublications
        fields = [
            'id',
            'type_publication',
            'title',
            'description',
            'function_type',
            'DOI',
            'reference_url'
        ]
        
    def to_representation(self, instance):
        data = super(GraduatePublicationsAuthSerializer, self).to_representation(instance)
        if instance.type_publication:
            data['type_publication'] = {
                'id': instance.type_publication,
                'name': GraduatePublications.Types.labels[instance.type_publication - 1]
            }
        if instance.function_type:
            data['function_type'] = {
                'id': instance.function_type,
                'name': GraduatePublications.Function.labels[instance.function_type - 1]
            }
        return data