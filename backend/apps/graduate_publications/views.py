from django.shortcuts import render
from monitoring_graduates.permission import ModelPermission
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import (
    OrderingFilter
)
from apps.history_user.models import HistoryUser
from rest_framework.generics import   RetrieveUpdateDestroyAPIView
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from .serializer import GraduatePublicationsSerializer,GraduatePublicationsAuthSerializer
from .models import GraduatePublications
from ..user_profile.models import GraduateProfile


# Create your views here.
class PublicationCreateListView(generics.ListCreateAPIView):
    serializer_class = GraduatePublicationsSerializer
    queryset = GraduatePublications.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'type_publication',
        'title',
        'function_type',
        'DOI',
        'graduate_profile_pk',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui',
    ]
    ordering_fields = [
        'title',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui'
    ]
    permission_classes = [ModelPermission]

    def get(self, request, *args, **kwargs):
        response = super(PublicationCreateListView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de GraduatePublications'
            )
            history.save()
        return response

    def post(self, request, *args, **kwargs):
        response = super(PublicationCreateListView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            language = GraduatePublications.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un registro en GraduatePublications ',
                content_object=language
            )
            history.save()
        return response




class PublicationDetailView(RetrieveUpdateDestroyAPIView):
    """
    """
    
    serializer_class = GraduatePublicationsSerializer
    queryset = GraduatePublications.objects.all()
    permission_classes = [ModelPermission]
    
    def get  (self, request, *args, **kwargs):
        response = super(PublicationDetailView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de  GraduatePublications'
            )
            history.save()
        return response  

    def put(self, request, *args, **kwargs):
        response = super(PublicationDetailView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            language = GraduatePublications.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Edito un registro en GraduatePublications ',
                content_object=language
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        response = super(PublicationDetailView, self).delete(request)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            history = HistoryUser(
                user=request.user,
                description='Elimino un registro de  GraduatePublications'
            )
            history.save()
        return response  


class PublicationListType(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, *args, **kwargs):
        aux=[]
        json={}

        nuevo=GraduatePublications.Types.choices
        for x, y in nuevo:
            json = {}
            json['id']=x
            json['name']=y
            aux.append(json)
        return Response(aux,status=200)


class PublicationListFunction(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request, *args, **kwargs):
        aux=[]
        json={}

        nuevo=GraduatePublications.Function.choices
        for x, y in nuevo:
            json = {}
            json['id']=x
            json['name']=y
            aux.append(json)
        return Response(aux,status=200)
   


class PublicationCreateListViewAuth(generics.ListCreateAPIView):
    serializer_class = GraduatePublicationsAuthSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'type_publication',
        'title',
        'function_type',
        'DOI'
    ]
    ordering_fields = [
        'title'
    ]
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduatePublications.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def perform_create(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Agrego una nueva publicación a su propio perfil',
            content_object=instance
        )
        history.save()

    def get(self, request, *args, **kwargs):
        response = super(PublicationCreateListViewAuth, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de sus publicaciones en su propio perfil '
            )
            history.save()
        return response


class PublicationDetailAuthView (RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduatePublicationsAuthSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return GraduatePublications.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def get(self, request, *args, **kwargs):
        response = super(PublicationDetailAuthView, self).get(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo una publicación   desde su perfi',
                content_object=self.get_object()
            )
            history.save()
        return response

    def perform_update(self, serializer):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        instance = serializer.save(graduate_profile_pk=profile)
        history = HistoryUser(
            user=self.request.user,
            description='Actualizo una publicación en su propio perfil   en su propio perfil',
            content_object=instance
        )
        history.save()

    def perform_destroy(self, instance):
        instance.delete()
        history = HistoryUser(
            user=self.request.user,
            description='Elimin+o una publicación  en su proprio perfil '
        )
        history.save()


