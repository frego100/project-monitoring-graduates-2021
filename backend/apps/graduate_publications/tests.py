from django.contrib.auth.models import User
from django.test import TestCase

# Create your tests here.
from rest_framework import status
from rest_framework.test import APIClient

from apps.graduate_publications.models import GraduatePublications
from apps.graduate_publications.serializer import GraduatePublicationsSerializer, GraduatePublicationsAuthSerializer
from apps.identificators.models import Identificator
from apps.user_profile.models import GraduateProfile


class GraduatePublicationsAcademicAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.identificator = Identificator.objects.create(
            code='DNI',
            max_length=8,
            name='DNI',
            alphabet='NUM'
        )
        self.graduate_profile = GraduateProfile.objects.create(
            gender=True,
            cui=20173385,
            date_birthday='2000-04-12',
            web='https://clockify.me/tracker',
            address='Mi casa',
            user=self.user,
            identificator=self.identificator,
            document='72659413',
            cell_phone='987654321',
            phone='987654',
        )
        self.publication = GraduatePublications.objects.create(
            graduate_profile_pk=self.graduate_profile,
            type_publication=1,
            title='Testing',
            description="Hello there",
            function_type=1,
            DOI="11123213",
            reference_url="https://gitlab.com/frego100/project-monitoring-graduates-2021/-/wikis/pages?page=2"
        )

    def test_list_publications(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduatePublications.objects.all())
        response = self.client.get('/publications/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len, len(response.data['results']))

    def test_create_publications(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduatePublications.objects.all())
        data = {
            "type_publication": 1,
            "title": "Nueva Publicación3",
            "description": "Atrasada",
            "function_type": 10,
            "DOI": "10.1109/iWEM.2018.8536669",
            "reference_url": "https://clockify.me/tracker",
            "graduate_profile_pk": self.graduate_profile.id
        }

        response = self.client.post('/publications/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(GraduatePublications.objects.all()))

    def test_retrieve_publications(self):
        """
        Test destroy activity
        """
        response = self.client.get('/publications/{}'.format(self.publication.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, GraduatePublicationsSerializer(self.publication).data)

    def test_update_publications(self):
        """
        Test destroy activity
        """
        data = {
            "type_publication": 1,
            "title": "Nueva Publicación3",
            "description": "Atrasada",
            "function_type": 10,
            "DOI": "10.1109/iWEM.2018.8536669",
            "reference_url": "https://clockify.me/tracker",
            "graduate_profile_pk": self.graduate_profile.id
        }
        response = self.client.put('/publications/{}'.format(self.publication.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            GraduatePublicationsSerializer(
                GraduatePublications.objects.get(id=self.publication.id)
            ).data
        )

    def test_destroy_publications(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduatePublications.objects.all())
        response = self.client.delete('/publications/{}'.format(self.publication.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(GraduatePublications.objects.all()))

    def test_list_publications_auth(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduatePublications.objects.filter(graduate_profile_pk__user=self.user))
        response = self.client.get('/publications/auth/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len, len(response.data['results']))

    def test_create_publications_auth(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduatePublications.objects.filter(graduate_profile_pk__user=self.user))
        data = {
            "type_publication": 2,
            "title": "Nueva Publicación creada por el mismo",
            "description": "Atrasada",
            "function_type": 10,
            "DOI": "10.1109/iWEM.2018.8536688",
            "reference_url": "https://clockify.me/tracker"
        }

        response = self.client.post('/publications/auth/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(GraduatePublications.objects.filter(graduate_profile_pk__user=self.user)))

    def test_retrieve_publications_auth(self):
        """
        Test destroy activity
        """
        response = self.client.get('/publications/auth/{}'.format(self.publication.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, GraduatePublicationsAuthSerializer(self.publication).data)

    def test_update_publications_auth(self):
        """
        Test destroy activity
        """
        data = {
            "type_publication": 2,
            "title": "Nueva Publicación creada por el mismo",
            "description": "Atrasada",
            "function_type": 10,
            "DOI": "10.1109/iWEM.2018.8536688",
            "reference_url": "https://clockify.me/tracker"
        }
        response = self.client.put('/publications/auth/{}'.format(self.publication.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            GraduatePublicationsAuthSerializer(
                GraduatePublications.objects.get(id=self.publication.id)
            ).data
        )

    def test_destroy_publications_auth(self):
        """
        Test destroy activity
        """
        prev_len = len(GraduatePublications.objects.filter(graduate_profile_pk__user=self.user))
        response = self.client.delete('/publications/auth/{}'.format(self.publication.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(GraduatePublications.objects.filter(graduate_profile_pk__user=self.user)))

    def test_types_type_publication(self):
        """
        Test destroy activity
        """
        response = self.client.get('/publications/type_publication/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(GraduatePublications.Types.choices))

    def test_types_type_function_type(self):
        """
        Test destroy activity
        """
        response = self.client.get('/publications/function_type/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(GraduatePublications.Function.choices))
