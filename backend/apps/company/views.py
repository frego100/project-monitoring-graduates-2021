# Create your views here.
from django.contrib.auth.models import User
from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response

from apps.company.models import Company
from apps.company.serializers import CompanyDetailSerializer, CompanyLiteSerializer, CompanySerializer
from apps.history_user.models import HistoryUser
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import (
    OrderingFilter
)
from apps.company.permission import CompanyPermission

class CompanyRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanyLiteSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = CompanyDetailSerializer(instance, context=self.get_serializer_context())
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Company, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Accedio a una compañia',
                content_object=instance
            )
            history_user.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        self.get_object()
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Company, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Actualizo una compañia',
                content_object=instance
            )
            history_user.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Company, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Deshabilito una compañia',
                content_object=instance
            )
            history_user.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class CompanyReactivateAPIView(generics.UpdateAPIView):
    serializer_class = CompanyLiteSerializer
    queryset = Company.objects.filter(is_active=False)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Company, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Habilito una compañia',
                content_object=instance
            )
            history_user.save()
        return response


class CompanyCreateListAPIView (generics.ListCreateAPIView):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'company_name',
        'ruc',
        'company_activity',
        'is_active'
    ]
    ordering_fields = [
        'company_name',
    ]
    
    permission_classes = (CompanyPermission,)

    def get(self, request, *args, **kwargs):
        response = super(CompanyCreateListAPIView, self).get(request)
        return response
    
    def post(self, request, *args, **kwargs):
        response = super(CompanyCreateListAPIView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            manual = Company.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo una empresa ',
                content_object=manual   
            )
            history.save()
        return response