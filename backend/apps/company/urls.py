from django.urls import path

from apps.company.views import CompanyRetrieveUpdateDestroyAPIView, CompanyReactivateAPIView, CompanyCreateListAPIView

urlpatterns = [
    path('<int:pk>', CompanyRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', CompanyReactivateAPIView.as_view()),
    path('', CompanyCreateListAPIView.as_view()),
]
