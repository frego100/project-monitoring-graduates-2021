"""
Serializers for Company model
"""
from rest_framework import serializers

from apps.company.models import Company
from apps.identificators.serializers import IdentificatorSerializer


class CompanySerializer(serializers.ModelSerializer):
    """
    Serializer for company model
    """

    class Meta:
        """
        Meta class for serializer
        """
        model = Company
        fields = '__all__'

    def to_representation(self, instance):
        """
        Method that design the way how the model is going to be showed
        """
        data = super(CompanySerializer, self).to_representation(instance=instance)
        data['identificator'] = IdentificatorSerializer(instance=instance.identificator).data
        if instance.logo:
            data['logo'] = self.context['request'].build_absolute_uri(instance.logo.url)
        return data


class CompanyAuthSerializer(serializers.ModelSerializer):
    """
       Serializer for company model
    """

    class Meta:
        """
        Meta class for serializer
        """
        model = Company
        fields = '__all__'

    def to_representation(self, instance):
        """
        Method that design the way how the model is going to be showed
        """
        data = super().to_representation(instance=instance)
        if instance.logo:
            data['logo'] = self.context['request'].build_absolute_uri(instance.logo.url)
        return data


class CompanyDetailSerializer(serializers.ModelSerializer):
    identificator = IdentificatorSerializer(many=False)

    class Meta:
        model = Company
        fields = '__all__'


class CompanyLiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class CompanyLiteSerializerOfCompanyProfile(serializers.ModelSerializer):
    class Meta:
        model = Company
        exclude = ['logo']
