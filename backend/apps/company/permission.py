from rest_framework import permissions


class CompanyPermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'POST':
            return request.user.has_perm('company.add_company')
        return True
