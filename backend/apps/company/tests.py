from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from apps.company.models import Company
from apps.company.serializers import CompanySerializer, CompanyDetailSerializer, CompanyLiteSerializer
from apps.company.views import CompanyRetrieveUpdateDestroyAPIView, CompanyReactivateAPIView
# Create your tests here.
from apps.identificators.models import Identificator


# Create your tests here.


class CompanyRetrieveUpdateDestroyViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = CompanyRetrieveUpdateDestroyAPIView.as_view()
        self.serializer = CompanySerializer
        self.permission = Permission.objects.all()

        self.url_path_retrieve_update_destroy = '/company/'
        self.id_test = 1

        self.retrieve_queryset = None
        self.retrieve_request = None
        self.update_request = None
        self.update_queryset = None
        self.destroy_request = None
        self.destroy_queryset = None

    def test_retrieve(self):
        # Init var for this test
        self.retrieve_request = self.factory.get(self.url_path_retrieve_update_destroy)
        Identificator.objects.create(id=self.id_test, code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        self.retrieve_queryset = Company.objects.create(id=self.id_test, company_name='Test',
                                                        identificator_id=self.id_test)
        # Call the Create API View
        response = self.view(self.retrieve_request, pk=self.id_test)
        # Serialized data
        serialized = CompanyDetailSerializer(self.retrieve_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)

    def test_destroy(self):
        # Create a Job Position for this test
        Identificator.objects.create(id=self.id_test, code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        instance = Company.objects.create(id=self.id_test, company_name='Test', identificator_id=self.id_test)
        # Serialized data
        serialized = self.serializer(instance, many=False)

        # Init var for this test PUT Method
        self.destroy_request = self.factory.delete(self.url_path_retrieve_update_destroy, serialized.data,
                                                   format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.destroy_request, user=user)

        # Call the Update API View
        response = self.view(self.destroy_request, pk=self.id_test)

        self.destroy_queryset = Company.objects.get(company_name="Test")

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data, None)

        # Check if the logic delete is successful
        self.assertEqual(self.destroy_queryset.is_active, False)


class CompanyReactivateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = CompanyReactivateAPIView.as_view()
        self.serializer = CompanyLiteSerializer
        self.permission = Permission.objects.all()

        self.url_path_reactivate = '/company/reactivate'
        self.id_test = 1

        self.reactivate_request = None
        self.reactivate_queryset = None

    def test_reactivate(self):
        # Init var for this test
        Identificator.objects.create(id=self.id_test, code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        Company.objects.create(id=self.id_test, company_name='Test', identificator_id=self.id_test, is_active=False)

        self.reactivate_request = self.factory.put(self.url_path_reactivate)

        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.reactivate_request, user=user)

        # Call the API View
        response = self.view(self.reactivate_request, pk=self.id_test)
        # Select in BD the instance
        self.reactivate_queryset = Company.objects.get(id=self.id_test)
        # Serialized data
        serialized = self.serializer(self.reactivate_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)
