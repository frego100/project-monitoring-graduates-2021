from django.db import models

from apps.identificators.models import Identificator


# Create your models here.
class Company(models.Model):
    company_name = models.CharField(
        max_length=200,
        null=False
    )
    social_reason = models.CharField(
        max_length=200,
        blank=True,
        null=True
    )
    identificator = models.ForeignKey(
        Identificator,
        null=True,
        on_delete=models.SET_NULL,
        related_name='identificator'
    )
    ruc = models.CharField(
        max_length=11,
        null=False
    )
    company_activity = models.CharField(
        max_length=200,
        blank=True,
        null=True
    )
    logo = models.ImageField(
        blank=True,
        null=True,
        upload_to='company/logo/',
        default=None
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name='Activo'
    )

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items() if item[0][0] != '_')
        )
