"""
Configuration for permission module
"""
from django.apps import AppConfig


class PermissionConfig(AppConfig):
    """
    App configuration for permission application
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.permission'
