"""
Serializers for permission module
"""
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers


class ContentTypeSerializer(serializers.ModelSerializer):
    """
        Serializer for the content_type model
    """

    class Meta:
        model = ContentType
        fields = '__all__'

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True

    def to_representation(self, instance):
        data = super(ContentTypeSerializer, self).to_representation(instance)
        model = ContentType.objects.get(model=instance.model).model_class()
        data['model_name'] = ''
        if model:
            data['model_name'] = str(model._meta.verbose_name.title())
        return data


class PermissionSerializer(serializers.ModelSerializer):
    """
    Serializer for the permission model
    """
    content_type = ContentTypeSerializer(many=False, read_only=True)

    class Meta:
        """
        Meta class for permission serializer
        """
        model = Permission
        fields = '__all__'

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True
