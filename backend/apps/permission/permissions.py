"""
Permissions for permission module
"""
from rest_framework import permissions


class PermissionforPermission(permissions.BasePermission):
    """
    Permission manager for permissionviews
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'GET':
            return request.user.has_perm('auth.view_permission')
        if request.method == 'POST':
            return request.user.has_perm('auth.add_permission')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('auth.change_permission')
        if request.method == 'DELETE':
            return request.user.has_perm('auth.delete_permission')
        return False
