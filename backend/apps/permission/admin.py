"""
Admin configuration for group module
"""
from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Permission

admin.site.register(Permission)
