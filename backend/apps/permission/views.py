"""
Views for permission module
"""
from django.contrib.auth.models import Permission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
# Create your views here.
from rest_framework.filters import OrderingFilter,SearchFilter

from apps.history_user.models import HistoryUser
from apps.permission.permissions import PermissionforPermission
from apps.permission.serializer import PermissionSerializer


class PermissionListAPIView(generics.ListAPIView):
    """
    List API View for permission
    """
    serializer_class = PermissionSerializer
    queryset = Permission.objects.all()
    filter_backends = [DjangoFilterBackend, OrderingFilter,SearchFilter]
    filterset_fields = ['content_type']
    ordering_fields = ['name', 'content_type']
    search_fields = [
        '^name'
    ]
    permission_classes = (PermissionforPermission,)

    def get(self, request):
        response = super(PermissionListAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de permisos'
            )
            history.save()
        return response
