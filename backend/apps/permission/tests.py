"""
Test case for group module
"""
from django.test import TestCase
from django.contrib.auth.models import Permission, User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from apps.permission.serializer import PermissionSerializer


class PermissionAPIViewsTestCase(TestCase):
    """
    Test cases for API Views  from group module
    """

    def setUp(self):
        """
        Configuration for test cases
        """
        self.user = User(
            username='admin',
            password='password',
            is_superuser=True
        )
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_list_permission(self):
        """
        Test case for list permission
        """
        response = self.client.get('/permission/')
        serializer = PermissionSerializer(Permission.objects.all(), many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)