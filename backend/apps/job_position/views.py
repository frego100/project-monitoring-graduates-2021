# Create your views here.
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import ListCreateAPIView, get_object_or_404, RetrieveUpdateAPIView, UpdateAPIView
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response

from apps.history_user.models import HistoryUser
from apps.job_position.models import JobPosition
from apps.job_position.serializer import JobPositionSerializer
from monitoring_graduates.permission import ModelPermission


class JobPositionListCreateAPIView(ListCreateAPIView):
    queryset = JobPosition.objects.all()
    # Permisos de lectura habilitados
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]

    serializer_class = JobPositionSerializer

    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'name': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'name'
    ]

    search_fields = [
        'id',
        'name'
    ]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de puestos'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            instance = get_object_or_404(JobPosition, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Creo una nuevo puesto',
                content_object=instance
            )
            history_user.save()
        return response


class JobPositionRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    # Permisos de lectura habilitados
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    queryset = JobPosition.objects.all()
    serializer_class = JobPositionSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(JobPosition, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Accedio un puesto',
                content_object=instance
            )
            history_user.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(JobPosition, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Actualizo un puesto',
                content_object=instance
            )
            history_user.save()
        return response

class JobPositionReactivateAPIView(UpdateAPIView):
    permission_classes = (ModelPermission,)
    queryset = JobPosition.objects.filter(is_active=False)
    serializer_class = JobPositionSerializer

    def put(self, request, pk, format=None):
        job_position = self.get_object()
        job_position.is_active = True
        job_position.save()
        history = HistoryUser(
            user=request.user,
            description='Habilito un puesto de trabajo',
            content_object=job_position
        )
        history.save()
        return Response(
            self.serializer_class(job_position).data,
            status=status.HTTP_200_OK
        )