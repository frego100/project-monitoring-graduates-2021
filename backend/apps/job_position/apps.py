from django.apps import AppConfig


class JobPositionConfig(AppConfig):
    name = 'apps.job_position'
