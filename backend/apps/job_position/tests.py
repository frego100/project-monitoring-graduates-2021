from django.contrib.auth.models import User, Permission
from django.test import TestCase
from rest_framework import status
from rest_framework.exceptions import ErrorDetail, PermissionDenied
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory, force_authenticate, APIClient

from apps.history_user.models import HistoryUser
from apps.job_position.models import JobPosition
# Create your tests here.
from apps.job_position.serializer import JobPositionSerializer
from apps.job_position.views import JobPositionListCreateAPIView, JobPositionRetrieveUpdateAPIView


class JobPositionModelTestCase(TestCase):

    def setUp(self) -> None:
        JobPosition.objects.create(name="Administrator")

    def test_job_position_str(self):
        job = JobPosition.objects.get(name="Administrator")
        self.assertEqual(job.__str__(), "JobPosition(id={}, name={}, is_active={})".format(
            job.id, job.name, job.is_active
        ))


class JobPositionListCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.paginator = LimitOffsetPagination()
        self.view = JobPositionListCreateAPIView.as_view()
        self.serializer = JobPositionSerializer

        self.url_path_list_create = '/job-position/'

        self.list_queryset = None
        self.list_request = None
        self.create_request = None
        self.create_queryset = None

    def paginate_queryset(self, request):
        return list(self.paginator.paginate_queryset(self.list_queryset, request))

    def get_paginated_content(self, queryset):
        response = self.paginator.get_paginated_response(queryset)
        return response.data

    def test_list(self):
        # Init var for this test
        self.list_request = self.factory.get(self.url_path_list_create)
        self.list_queryset = JobPosition.objects.all()
        # Create a example job position
        JobPosition.objects.create(name="Administrator")
        # Create a Django Rest Framework Request to call functions in the paginator class
        request = Request(self.list_request)
        # Result after execute paginator
        queryset_result = self.paginate_queryset(request)
        # Result after execute serializer
        serialized = self.serializer(queryset_result, many=True)
        # Result after execute serializer and paginator
        content_result = self.get_paginated_content(serialized.data)
        # Call the List API View
        response = self.view(self.list_request)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer and pager
        self.assertEqual(response.data, content_result)

        # Count if the total of items is equal to the size of the queryset
        self.assertEqual(len(queryset_result), self.list_queryset.count())

    def test_create_with_authenticate_user_with_permissions(self):
        # Create a example job position
        job_position = JobPosition(name="Admin")
        # Serialized data
        serialized = self.serializer(job_position, many=False)
        # Create a Request
        self.create_request = self.factory.post(self.url_path_list_create, serialized.data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)
        # Check if the JobPosition is created
        self.create_queryset = JobPosition.objects.get(name="Admin")

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Confirm if the response data is equal to the database register data
        self.assertEqual(response.data, self.serializer(self.create_queryset).data)

        # Confirm if the ep create history user
        self.assertEqual(1, HistoryUser.objects.all().count())

    def test_create_authenticate_user_without_permissions(self):
        # Create a example job position
        job_position = JobPosition(name="Admin")
        # Serialized data
        serialized = self.serializer(job_position, many=False)
        # Create a Request
        self.create_request = self.factory.post(self.url_path_list_create, serialized.data, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # No set permissions for this user
        # user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)

        # Confirm if the status code is a success in this case the success code is 403 OK
        self.assertEqual(response.status_code, PermissionDenied.status_code)

        self.assertEqual(response.data,
                         {
                             'detail': ErrorDetail(string=PermissionDenied.default_detail,
                                                   code=PermissionDenied.default_code)
                         })


class JobPositionRetrieveUpdateDestroyViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = JobPositionRetrieveUpdateAPIView.as_view()
        self.serializer = JobPositionSerializer
        self.permission = Permission.objects.all()

        self.url_path_retrieve_update_destroy = '/job-position/'
        self.id_test = 1

        self.retrieve_queryset = None
        self.retrieve_request = None
        self.update_request = None
        self.update_queryset = None
        self.destroy_request = None
        self.destroy_queryset = None

        self.user = User(
            username='admin',
            password='password',
            is_superuser=True
        )
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_retrieve(self):
        # Init var for this test
        self.retrieve_request = self.factory.get(self.url_path_retrieve_update_destroy)
        self.retrieve_queryset = JobPosition.objects.create(name='Admin')
        # Call the Create API View
        response = self.view(self.retrieve_request, pk=self.retrieve_queryset.pk)
        # Serialized data
        serialized = self.serializer(self.retrieve_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        self.assertEqual(response.data, serialized.data)

    def test_update(self):
        # Create a Job Position for this test
        JobPosition.objects.create(id=self.id_test, name="Admin")
        # Create a example job position
        job_position = JobPosition(id=self.id_test, name="Admin[update]")
        # Serialized data
        serialized = self.serializer(job_position, many=False)

        # Init var for this test PUT Method
        self.update_request = self.factory.put(self.url_path_retrieve_update_destroy, serialized.data, format='json')
        self.update_queryset = JobPosition.objects.get(name="Admin")

        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(self.permission)
        # Force Authentication
        force_authenticate(self.update_request, user=user)

        # Call the Update API View
        response = self.view(self.update_request, pk=self.id_test)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response data is equal to serialized data
        self.assertEqual(response.data, serialized.data)
