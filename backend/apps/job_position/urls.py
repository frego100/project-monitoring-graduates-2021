"""
Urls for group module
"""
from django.urls import path

from apps.job_position.views import JobPositionListCreateAPIView, JobPositionRetrieveUpdateAPIView, JobPositionReactivateAPIView

urlpatterns = [
    path('', JobPositionListCreateAPIView.as_view()),
    path('<int:pk>', JobPositionRetrieveUpdateAPIView.as_view()),
    path('reactivate/<int:pk>/', JobPositionReactivateAPIView.as_view()),
]
