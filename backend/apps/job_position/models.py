from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.

class JobPosition(models.Model):
    """ Este modelo representa los puestos de trabajo de la OSE"""

    name = models.CharField(
        verbose_name=_('name_job_position'),
        max_length=255,
        null=False,
        blank=False,
    )
    is_active = models.BooleanField(
        default=True
    )

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items() if item[0][0] != '_')
        )

    class Meta:
        verbose_name = 'Puesto'
        verbose_name_plural = 'Puestos'
