from django.contrib import admin

from apps.job_position.models import JobPosition

# Register your models here.
admin.site.register(JobPosition)
