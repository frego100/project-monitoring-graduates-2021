from rest_framework.serializers import ModelSerializer

from apps.job_position.models import JobPosition


class JobPositionSerializer(ModelSerializer):
    class Meta:
        model = JobPosition
        fields = '__all__'

        def __str__(self):
            return str(self.model)

        def __bool__(self):
            return True
