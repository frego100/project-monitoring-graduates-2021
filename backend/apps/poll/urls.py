from django.urls import path
from .views import PollListCreateView, PollDetailView

urlpatterns = [
    path('', PollListCreateView.as_view()),
    path('<int:pk>', PollDetailView.as_view())
]