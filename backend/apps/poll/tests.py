from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

# Create your tests here.
from apps.poll.models import Poll
from apps.poll.serializer import PollSerializer


class GraduateAchievementsAcademicAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.poll = Poll.objects.create(
            name='Poll Testing',
            link_poll='https://gitlab.com/frego100/project-monitoring-graduates-2021/-/merge_requests/447',
            description='Description Testing',
            period_days=5,
            is_to_company=True,
            is_to_graduate=False
        )

    def test_list_polls(self):
        """
        Test list polls
        """
        response = self.client.get('/poll/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_poll(self):
        """
        Test create poll
        """
        prev_len = len(Poll.objects.all())
        data = {
            "name": "Second Poll",
            "link_poll": 'https://gitlab.com/frego100/project-monitoring-graduates-2021/-/merge_requests/447',
            "description": 'Description Testing',
            "period_days": 5,
            "is_to_company": True,
            "is_to_graduate": False
        }

        response = self.client.post('/poll/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(Poll.objects.all()))

    def test_retrieve_poll(self):
        """
        Test retrieve poll
        """
        response = self.client.get('/poll/{}'.format(self.poll.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, PollSerializer(self.poll).data)

    def test_update_poll(self):
        """
        Test update poll
        """
        data = {
            "name": "Second Poll",
            "link_poll": 'https://gitlab.com/frego100/project-monitoring-graduates-2021/-/merge_requests/447',
            "description": 'Description Testing',
            "period_days": 5,
            "is_to_company": True,
            "is_to_graduate": False
        }
        response = self.client.put('/poll/{}'.format(self.poll.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            PollSerializer(
                Poll.objects.get(id=self.poll.id)
            ).data
        )

    def test_destroy_poll(self):
        """
        Test destroy poll
        """
        prev_len = len(Poll.objects.all())
        response = self.client.delete('/poll/{}'.format(self.poll.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(Poll.objects.all()))
