from rest_framework import serializers
from .models import Poll


class PollSerializer(serializers.ModelSerializer):
    date_next_notify = serializers.DateTimeField(read_only=True)
    class Meta:
        model = Poll
        fields = '__all__'