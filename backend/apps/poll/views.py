from rest_framework import status, generics
from datetime import datetime
import pytz
from apps.history_user.models import HistoryUser
from .models import Poll
from monitoring_graduates.permission import ModelPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import (
    OrderingFilter
)
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from .serializer import PollSerializer
from django.core.management import call_command


class PollListCreateView(generics.ListCreateAPIView):
    """
    View for get data from the all users
    """
    serializer_class = PollSerializer
    queryset = Poll.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'name',
        'is_to_company',
        'is_to_graduate'
    ]
    ordering_fields = [
        'name'
    ]
    permission_classes = [ModelPermission]
    
    def post(self, request, *args, **kwargs):
        response = super(PollListCreateView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            poll = Poll.objects.get(pk=int(response.data['id']))
            utc_tz = pytz.timezone("UTC")
            current_date = datetime.now().astimezone(utc_tz)
            poll.date_next_notify = current_date.date()
            poll.save()
            call_command('poll_diffusion')
            history = HistoryUser(
                user=request.user,
                description='Creo una encuesta ',
                content_object=poll
            )
            history.save()
        return response


class PollDetailView(RetrieveUpdateDestroyAPIView):
    """
    View for get data detail from the specific user
    """
    serializer_class = PollSerializer
    queryset = Poll.objects.filter()
    permission_classes = [ModelPermission]

    def put(self, request, *args, **kwargs):
        response = super(PollDetailView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            poll = Poll.objects.get(pk=int(response.data['id']))
            utc_tz = pytz.timezone("UTC")
            current_date = datetime.now().astimezone(utc_tz)
            poll.date_next_notify = current_date.date()
            poll.save()
            history = HistoryUser(
                user=request.user,
                description='Modifico una encuesta',
                content_object=poll
            )
            history.save()
        return response
    
    def delete(self, request, *args, **kwargs):

        response = super(PollDetailView, self).delete(request)
        if response.status_code == status.HTTP_200_OK:
            poll = Poll.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Elimino una encuesta',
                content_object=poll
            )
            history.save()
        return response

