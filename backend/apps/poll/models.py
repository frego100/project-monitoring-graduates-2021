from django.db import models

# Create your models here.

class Poll (models.Model):
    name=models.CharField(max_length=255, null=False)
    link_poll=models.URLField(null=False,blank=False)
    description=models.TextField(null=True, blank=True)
    period_days=models.IntegerField(null=False)
    is_to_company=models.BooleanField(null=False)
    is_to_graduate=models.BooleanField(null=False)
    date_next_notify=models.DateTimeField(null=True, blank=True)
