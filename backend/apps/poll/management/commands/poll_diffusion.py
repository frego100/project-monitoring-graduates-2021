from datetime import datetime, timedelta
import pytz
from django.core.management.base import BaseCommand
from apps.poll.models import Poll
from apps.user_profile.models import GraduateProfile, CompanyProfile
from apps.email_sender.utils import EmailSender


class Command(BaseCommand):
    help = 'Diffusion poll'

    def handle(self, *args, **options):
        utc_tz = pytz.timezone("UTC")
        current_date = datetime.now().astimezone(utc_tz)
        template_name = 'notify_poll.html'
        sender = EmailSender()
        subject = 'NOTIFICACIÓN DE ENCUESTA OSE'
        for poll in Poll.objects.filter(is_to_graduate=True):
            if str(poll.date_next_notify.strftime('%Y-%m-%d')) == str(current_date.date()):
                days_aux = poll.period_days
                if days_aux <= 0:  # no loop
                    poll.period_days = 1
                    days_aux = 1
                poll.date_next_notify = current_date.date() + timedelta(days=days_aux)
                poll.save()
                graduates = GraduateProfile.objects.filter(is_active=True)
                emails_graduate = list(set(i for i in graduates.values_list('user__email', flat=True) if bool(i)))
                data = {
                    'poll': poll
                }
                sender.send_email(
                    subject,
                    emails_graduate,
                    template_name,
                    data
                )

        for poll in Poll.objects.filter(is_to_company=True):
            if str(poll.date_next_notify.strftime('%Y-%m-%d')) == str(current_date.date()):
                if not poll.is_to_graduate:
                    days_aux = poll.period_days
                    if days_aux <= 0:  # no loop
                        poll.period_days = 1
                        days_aux = 1
                    poll.date_next_notify = current_date.date() + timedelta(days=days_aux)
                    poll.save()
                graduates = CompanyProfile.objects.filter(is_active=True)
                emails_company = list(set(i for i in graduates.values_list('user__email', flat=True) if bool(i)))
                data = {
                    'poll': poll
                }
                sender.send_email(
                    subject,
                    emails_company,
                    template_name,
                    data
                )
