"""
Models for group module
"""
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.db import models

# Create your models here.


class UserGroup(Group):
    """
    Model group that inherits from Django default Group model
    """
    profile_type = models.ForeignKey(
        ContentType,
        null=True,
        default=None,
        verbose_name='Tipo de Perfil',
        on_delete=models.SET_NULL,
        limit_choices_to=models.Q(app_label='user_profile') & ~models.Q(app_label='user_profile', model='userprofile')
    )
    state = models.BooleanField(
        default=True,
        verbose_name='Activo'
    )
    is_public=models.BooleanField(
        default=True,
        verbose_name='Publico'
    )

    def delete(self, using=None, keep_parents=False):
        """
        Method to logical delete
        """
        self.state = False
        self.save()

    def reinstate(self):
        """
        Method for reinstate a logical delete
        """
        self.state = True
        self.save()

    class Meta:
        verbose_name = 'Rol de Usuario'
        verbose_name_plural = 'Roles de Usuario'
