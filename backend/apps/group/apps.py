"""
App Configurations for module
"""
from django.apps import AppConfig


class GroupConfig(AppConfig):
    """
    Configuration for group application
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.group'
