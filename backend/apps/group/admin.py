"""
Configuration in Django Admin for group module
"""
from django.contrib import admin

# Register your models here.
from apps.group.models import UserGroup

admin.site.register(UserGroup)
