"""
Test case for group module
"""
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.test import APIClient, APIRequestFactory, force_authenticate

from apps.group.models import UserGroup
from apps.group.serializer import (
    GroupSerializer,
    GroupExtraLiteSerializer
)
# Create your tests here.
from apps.group.views import ContentTypeProfileAPIView
from apps.permission.serializer import ContentTypeSerializer


class GroupModelTestCase(TestCase):
    """
    Test cases for Group model
    """

    def setUp(self):
        """
        Configuration for test cases
        """
        UserGroup.objects.create(name='Test Group 1', state=True)
        UserGroup.objects.create(name='Test Group 2', state=True)
        UserGroup.objects.create(name='Test Group 3', state=False)

    def test_deactivate_group(self):
        """
        Test case for deactivate a group
        """
        group = UserGroup.objects.get(name="Test Group 1")
        group.delete()
        self.assertEqual(len(UserGroup.objects.all()), 3)
        self.assertEqual(len(UserGroup.objects.filter(state=True)), 1)

    def test_reactivate_group(self):
        """
        Test case for reactivate a group
        """
        group = UserGroup.objects.get(name="Test Group 3")
        group.reinstate()
        self.assertEqual(len(UserGroup.objects.all()), 3)
        self.assertEqual(len(UserGroup.objects.filter(state=True)), 3)


class GroupAPIViewsTestCase(TestCase):
    """
    Test cases for API Views  from group module
    """

    def setUp(self):
        """
        Configuration for test cases
        """
        self.client = APIClient()
        self.user = User.objects.get_or_create(username='testuser')[0]
        self.user.is_superuser = True
        self.user.save()
        self.group = UserGroup.objects.create(name='Test Group 1', state=True)
        UserGroup.objects.create(name='Test Group 2', state=True)
        UserGroup.objects.create(name='Test Group 3', state=False)

    def test_list_groups(self):
        """
        Test case for list group
        """
        self.client.force_authenticate(self.user)
        response = self.client.get('/group/')
        serializer = GroupSerializer(UserGroup.objects.filter(state=True), many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Pagination does the response being different
        # self.assertEqual(response.data, serializer.data)

    def test_list_lite_groups(self):
        """
        Test case for list lite group
        """
        permissions_list = Permission.objects.all()
        self.group.permissions.set(permissions_list)
        self.user.groups.add(self.group)
        self.client.force_authenticate(self.user)
        response = self.client.get('/group/lite/')
        serializer = GroupExtraLiteSerializer(UserGroup.objects.filter(state=True), many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_valid_register(self):
        """
        Test case for valid register group in API VIEW
        """
        data = {
            "name": "Test"
        }
        count = len(UserGroup.objects.all())
        self.client.force_authenticate(self.user)
        response = self.client.post('/group/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(UserGroup.objects.all()), count + 1)

    def test_invalid_register(self):
        """
        Test case for invalid register group in API VIEW
        """
        data = {}
        count = len(UserGroup.objects.all())
        self.client.force_authenticate(self.user)
        response = self.client.post('/group/', data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(UserGroup.objects.all()), count)


class ContentTypeUserProfileListAPIView(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.paginator = LimitOffsetPagination()
        self.view = ContentTypeProfileAPIView.as_view()
        self.serializer = ContentTypeSerializer

        self.url_path_list = '/group/content-type-profile/'

        self.list_queryset = None
        self.list_request = None

    def paginate_queryset(self, request):
        return list(self.paginator.paginate_queryset(self.list_queryset, request))

    def get_paginated_content(self, queryset):
        response = self.paginator.get_paginated_response(queryset)
        return response.data

    def test_list_authenticate_user_with_permissions(self):
        # Init var for this test
        self.list_request = self.factory.get(self.url_path_list)
        self.list_queryset = ContentType.objects.filter(app_label='user_profile').exclude(model='userprofile')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.list_request, user=user)

        # Create a Django Rest Framework Request to call functions in the paginator class
        request = Request(self.list_request)
        # Result after execute paginator
        queryset_result = self.paginate_queryset(request)
        # Result after execute serializer
        serialized = self.serializer(queryset_result, many=True)
        # Result after execute serializer and paginator
        content_result = self.get_paginated_content(serialized.data)
        # Call the List API View
        response = self.view(self.list_request)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer and pager
        self.assertEqual(response.data, content_result)

        # Count if the total of items is equal to the size of the queryset
        self.assertEqual(len(queryset_result), self.list_queryset.count())
