"""
Urls for group module
"""
from django.urls import path

from apps.group.views import (
    GroupListCreateAPIView,
    GroupUpdateRetrieveAPIView,
    AssignPermissionToGroupAPIView,
    GroupReactivateAPIView,
    GroupListLiteAPIView,
    ContentTypeProfileAPIView,
    GroupListAPIView
)

urlpatterns = [
    path('', GroupListCreateAPIView.as_view()),
    path('public/', GroupListAPIView.as_view()),
    path('<int:pk>/', GroupUpdateRetrieveAPIView.as_view()),
    path('lite/', GroupListLiteAPIView.as_view()),
    path('reactivate/<int:pk>/', GroupReactivateAPIView.as_view()),
    path('permission/', AssignPermissionToGroupAPIView.as_view()),
    path('content-type-profile/', ContentTypeProfileAPIView.as_view()),
]
