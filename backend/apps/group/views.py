"""
Views for Group Module
"""
from django.contrib.auth.models import Permission, User
from django.contrib.contenttypes.models import ContentType
from django.http import Http404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import OrderingFilter
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.group.models import UserGroup
from apps.group.permissions import GroupPermission
from apps.group.serializer import (
    GroupSerializer,
    GroupExtraLiteSerializer,
    GroupLiteSerializer
)
from apps.history_user.models import HistoryUser
from apps.permission.serializer import ContentTypeSerializer
from monitoring_graduates.permission import ModelPermission


class GroupListCreateAPIView(generics.ListCreateAPIView):
    """
    API View for list and create a group
    """
    queryset = UserGroup.objects.all()
    serializer_class = GroupLiteSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'state',
        'is_public'
    ]
    ordering_fields = [
        'state',
        'name'
    ]
    permission_classes = (GroupPermission,)

    def get(self, request):
        response = super(GroupListCreateAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de roles de usuario'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super(GroupListCreateAPIView, self).create(request, args, kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            group = UserGroup.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo und nuevo rol de usuario',
                content_object=group
            )
            history.save()
        return response

class GroupListAPIView(generics.ListAPIView):
    """
    API View for list and create a group
    """
    queryset = UserGroup.objects.all().all().filter(is_public=True)
    serializer_class = GroupLiteSerializer
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'state'
    ]
    ordering_fields = [
        'state',
        'name'
    ]



   


class GroupUpdateRetrieveAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    API View for list and create a group
    """
    queryset = UserGroup.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (GroupPermission,)

    def get(self, request, pk):
        response = super(GroupUpdateRetrieveAPIView, self).get(request, pk)
        if response.status_code == status.HTTP_200_OK:
            group = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Accedio a un rol de usuario',
                content_object=group
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super(GroupUpdateRetrieveAPIView, self).update(request, args, kwargs)
        if response.status_code == status.HTTP_200_OK:
            group = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un rol de usuario',
                content_object=group
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        response = super(GroupUpdateRetrieveAPIView, self).delete(request, args, kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            group = UserGroup.objects.get(pk=int(kwargs['pk']))
            history = HistoryUser(
                user=request.user,
                description='Deshabilito un rol de usuario',
                content_object=group
            )
            history.save()
        return response


class GroupReactivateAPIView(APIView):
    permission_classes = (GroupPermission,)

    def get_object(self, pk):
        try:
            return UserGroup.objects.get(pk=pk)
        except UserGroup.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        user_group = self.get_object(pk)
        user_group.reinstate()
        history = HistoryUser(
            user=request.user,
            description='Habilito un rol de usuario',
            content_object=user_group
        )
        history.save()
        return Response({"detail": "This user group reactivate successfully"}, status=status.HTTP_200_OK)


class AssignPermissionToGroupAPIView(APIView):
    """
    API View for manipulate permissions in a group
    """
    permission_classes = (GroupPermission,)

    def put(self, request):
        """
        POST method to assign a permission to a group
        """
        data = request.data
        if not "group" in data:
            return Response({
                "detail": "No se envio un grupo"
            }, status.HTTP_400_BAD_REQUEST)
        if not "permission" in data:
            return Response({
                "detail": "No se envio un permiso"
            }, status.HTTP_400_BAD_REQUEST)
        group = get_object_or_404(UserGroup, pk=int(data['group']))
        permission = get_object_or_404(Permission, pk=int(data['permission']))
        group.permissions.add(permission)

        history = HistoryUser(
            user=request.user,
            description='Cambio los permisos de un rol de usuario',
            content_object=group
        )
        history.save()

        return Response(
            GroupSerializer(group).data,
            status=status.HTTP_200_OK
        )


class GroupListLiteAPIView(generics.ListAPIView):
    pagination_class = None
    permission_classes = (GroupPermission,)
    serializer_class = GroupExtraLiteSerializer
    queryset = UserGroup.objects.filter(state=True)
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name']
    ordering_fields = ['name']


class ContentTypeProfileAPIView(generics.ListAPIView):
    queryset = ContentType.objects.filter(app_label='user_profile').exclude(model='userprofile')
    serializer_class = ContentTypeSerializer
    permission_classes = [ModelPermission]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['model']
    ordering_fields = ['model']

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and isinstance(request.user, User):
            history_user = HistoryUser(
                user=request.user,
                description='Accedio al listado de ContentType de Perfiles'
            )
            history_user.save()
        return response
