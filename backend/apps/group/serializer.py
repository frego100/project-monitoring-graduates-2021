"""
Serializer for group module
"""
from rest_framework import serializers
from apps.group.models import UserGroup
from apps.permission.serializer import PermissionSerializer, ContentTypeSerializer


class GroupSerializer(serializers.ModelSerializer):
    """
    Serializer for Group model
    """
    class Meta:
        """
        Meta class for GroupSerializer
        """
        model = UserGroup
        fields = '__all__'

        def __str__(self):
            return  str(self.model)

        def __bool__(self):
            return True

    def to_representation(self, instance):
        """
        Method modified to present data
        """
        data = super(__class__, self).to_representation(instance)
        data['permissions'] = PermissionSerializer(instance.permissions, many=True).data
        if instance.profile_type:
            data['profile_type'] = ContentTypeSerializer(instance.profile_type).data
        return data


class GroupLiteSerializer(serializers.ModelSerializer):
    """
    Serializer for Group model lite
    """

    class Meta:
        """
        Meta class for GroupLiteSerializer
        """
        model = UserGroup
        fields = '__all__'

        def __str__(self):
            return  str(self.model)

        def __bool__(self):
            return True

    def to_representation(self, instance):
        data = super(GroupLiteSerializer, self).to_representation(instance)
        if instance.profile_type:
            data['profile_type'] = ContentTypeSerializer(instance.profile_type).data
        return data

class GroupExtraLiteSerializer(serializers.ModelSerializer):
    """
    Serializer for Group model simple extra lite
    """
    class Meta:
        """
        Meta class for GroupExtraLiteSerializer
        """
        model = UserGroup
        fields = ('id', 'name')

        def __str__(self):
            return  str(self.model)

        def __bool__(self):
            return True