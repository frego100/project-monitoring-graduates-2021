"""
Permissions for grups module
"""
from rest_framework import permissions


class GroupPermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'GET':
            return request.user.has_perm('group.view_usergroup')
        if request.method == 'POST':
            return request.user.has_perm('group.add_usergroup')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('group.change_usergroup')
        if request.method == 'DELETE':
            return request.user.has_perm('group.delete_usergroup')
        return False
