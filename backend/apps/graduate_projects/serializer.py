from rest_framework import serializers

from .models import GraduateProjects


class GraduateProjectsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateProjects
        fields = '__all__'


class GraduateProjectAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateProjects
        exclude = ['graduate_profile_pk']
