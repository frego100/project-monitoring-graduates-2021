from django.urls import path
from .views import ProjectsCreateListView, ProjectDetailView, GraduateProjectListCreateAPIView, \
    GraduateProjectRetrieveUpdateDestroyAPIView

urlpatterns = [
    path('', ProjectsCreateListView.as_view()),
    path('<int:pk>', ProjectDetailView.as_view()),
    path('auth/', GraduateProjectListCreateAPIView.as_view()),
    path('auth/<int:pk>', GraduateProjectRetrieveUpdateDestroyAPIView.as_view()),
]
