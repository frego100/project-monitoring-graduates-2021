from django.contrib.auth.models import User

from apps.history_user.models import HistoryUser


def log_entry_history_user(request_user: User, message: str, instance=None):
    if isinstance(request_user, User):
        if instance is not None:
            history = HistoryUser(
                user=request_user,
                description=message,
                content_object=instance
            )
            history.save()
        else:
            history = HistoryUser(
                user=request_user,
                description=message
            )
            history.save()
