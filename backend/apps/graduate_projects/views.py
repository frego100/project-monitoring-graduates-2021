from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, generics
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated

from apps.history_user.models import HistoryUser
from .models import GraduateProjects
from .permission import GraduateProjectPermission
from .serializer import GraduateProjectsSerializer, GraduateProjectAuthSerializer
# Create your views here.
from .utils import log_entry_history_user
from ..user_profile.models import GraduateProfile


class ProjectsCreateListView(generics.ListCreateAPIView):
    serializer_class = GraduateProjectsSerializer
    queryset = GraduateProjects.objects.all()
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter
    ]
    filterset_fields = [
        'type_project',
        'title',
        'type_role',
        'date_start',
        'date_end',
        'graduate_profile_pk',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui'
    ]
    ordering_fields = [
        'title',
        'date_start',
        'date_end',
        'graduate_profile_pk__user__username',
        'graduate_profile_pk__cui'
    ]
    permission_classes = (GraduateProjectPermission,)

    def get(self, request, *args, **kwargs):
        response = super(ProjectsCreateListView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de GraduateProjects'
            )
            history.save()
        return response

    def post(self, request, *args, **kwargs):
        response = super(ProjectsCreateListView, self).post(request)
        if response.status_code == status.HTTP_201_CREATED:
            data = GraduateProjects.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo un registro en GraduateProjects ',
                content_object=data
            )
            history.save()
        return response


class ProjectDetailView(RetrieveUpdateDestroyAPIView):
    """
    """
    serializer_class = GraduateProjectsSerializer
    queryset = GraduateProjects.objects.all()
    permission_classes = (GraduateProjectPermission,)

    def get(self, request, *args, **kwargs):
        response = super(ProjectDetailView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Obtuvo un registro de  GraduateProjects'
            )
            history.save()
        return response

    def put(self, request, *args, **kwargs):
        response = super(ProjectDetailView, self).put(request)
        if response.status_code == status.HTTP_200_OK:
            data = GraduateProjects.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Edito un registro en GraduateProjects ',
                content_object=data
            )
            history.save()
        return response

    def delete(self, request, *args, **kwargs):
        response = super(ProjectDetailView, self).delete(request)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            history = HistoryUser(
                user=request.user,
                description='Elimino un registro de  GraduateProjects'
            )
            history.save()
        return response


class GraduateProjectListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = GraduateProjectAuthSerializer
    model_class = GraduateProjects
    permission_classes = [IsAuthenticated]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'id': ['exact'],
        'code': ['exact'],
        'type_project': ['exact'],
        'title': ['exact'],
        'type_role': ['exact'],
        'date_start': ['exact', 'lt', 'gt'],
        'date_end': ['exact', 'lt', 'gt'],
        'reference_url': ['exact']
    }

    ordering_fields = [
        'id',
        'code',
        'type_project',
        'title',
        'type_role',
        'date_start',
        'date_end',
        'reference_url'
    ]

    search_fields = [
        'id',
        'code',
        'type_project',
        'title',
        'type_role',
        'date_start',
        'date_end',
        'reference_url'
    ]

    def get_queryset(self):
        return self.model_class.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Accedio a su propio listado de proyectos de egresado'
            )
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            log_entry_history_user(
                request_user=request.user,
                message='Creo un proyecto de egresado propio',
                instance=get_object_or_404(self.model_class, pk=int(response.data['id']))
            )
        return response

    def perform_create(self, serializer):
        serializer.save(
            graduate_profile_pk=get_object_or_404(
                GraduateProfile,
                user=self.request.user
            )
        )


class GraduateProjectRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = GraduateProjectAuthSerializer
    model_class = GraduateProjects
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.model_class.objects.filter(
            graduate_profile_pk__user=self.request.user
        )

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Recupero un propio proyecto de egresado',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return response

    def perform_update(self, serializer):
        serializer.save(
            graduate_profile_pk=get_object_or_404(
                GraduateProfile,
                user=self.request.user
            )
        )

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo un propio proyecto de egresado',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            log_entry_history_user(
                request_user=request.user,
                message='Elimino un propio proyecto de egresado'
            )
        return response
