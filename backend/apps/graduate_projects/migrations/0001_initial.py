# Generated by Django 3.2.4 on 2021-10-22 11:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('user_profile', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GraduateProjects',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=10, unique=True)),
                ('type_project', models.IntegerField(choices=[(1, 'Desarrollo Experimental'), (2, 'Desarrollo Tecnológico'), (3, 'Investigación aplicada '), (4, 'Investigación Básica')], default=4, verbose_name='type_project')),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=300)),
                ('type_role', models.IntegerField(choices=[(1, 'Investigador principal'), (2, 'Investigador Asociado'), (3, 'Investigador Colaborador'), (4, 'Desarrollador Tecnológico'), (5, 'Técnico'), (6, 'Innovador'), (7, 'Emprendedor'), (8, 'Otros')], verbose_name='type_role')),
                ('date_start', models.DateField()),
                ('date_end', models.DateField()),
                ('reference_url', models.URLField(blank=True, null=True)),
                ('graduate_profile_pk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='graduate_profile_project', to='user_profile.graduateprofile')),
            ],
            options={
                'verbose_name': 'Project',
                'verbose_name_plural': 'Projects',
            },
        ),
    ]
