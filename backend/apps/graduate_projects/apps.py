from django.apps import AppConfig


class GraduateProjectsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.graduate_projects'
