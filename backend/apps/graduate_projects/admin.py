from django.contrib import admin

from .models import GraduateProjects

# Register your models here.
admin.site.register(GraduateProjects)
