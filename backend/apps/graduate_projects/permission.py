from rest_framework import permissions


class GraduateProjectPermission(permissions.BasePermission):
    """
    Permission manager for user groups
    """

    def has_permission(self, request, view):
        """
        Permission Verifier
        """
        if request.method == 'GET':
            return request.user.has_perm('graduate_projects.view_graduateprojects')
        if request.method == 'POST':
            return request.user.has_perm('graduate_projects.add_graduateprojects')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('graduate_projects.change_graduateprojects')
        if request.method == 'DELETE':
            return request.user.has_perm('graduate_projects.delete_graduateprojects')
        return False
