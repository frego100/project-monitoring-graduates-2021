from django.db import models
# Create your models here.
from django.utils.translation import ugettext_lazy as _

from apps.user_profile.models import GraduateProfile


class GraduateProjects(models.Model):
    code = models.CharField(
        max_length=10,
        null=False,
        unique=True
    )
    type = (
        (1, 'Desarrollo Experimental'),
        (2, 'Desarrollo Tecnológico'),
        (3, 'Investigación aplicada '),
        (4, 'Investigación Básica')
    )
    type_project = models.IntegerField(
        choices=type,
        verbose_name=_("type_project"),
        default=type[3][0]
    )
    title = models.CharField(
        null=False,
        max_length=255
    )
    description = models.CharField(
        max_length=300
    )

    role = (
        (1, 'Investigador principal'),
        (2, 'Investigador Asociado'),
        (3, 'Investigador Colaborador'),
        (4, 'Desarrollador Tecnológico'),
        (5, 'Técnico'),
        (6, 'Innovador'),
        (7, 'Emprendedor'),
        (8, 'Otros')
    )
    type_role = models.IntegerField(
        choices=role,
        verbose_name=_("type_role")
    )
    date_start = models.DateField()
    date_end = models.DateField()
    reference_url = models.URLField(null=True, blank=True)

    graduate_profile_pk = models.ForeignKey(
        GraduateProfile,
        null=False,
        on_delete=models.CASCADE,
        related_name='graduate_profile_project'
    )

    class Meta:
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items() if item[0][0] != '_')
        )
