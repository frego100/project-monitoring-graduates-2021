# Create your views here.
from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from apps.history_user.models import HistoryUser
from apps.question.models import Question
from apps.question.serializers import QuestionSerializer
from monitoring_graduates.permission import ModelPermission


class QuestionListCreateAPIView(generics.ListCreateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [ModelPermission]
    filter_backends = [
        DjangoFilterBackend,
        OrderingFilter,
        SearchFilter
    ]

    filterset_fields = {
        'name': ['exact'],
        'is_active': ['exact']
    }

    ordering_fields = [
        'id',
        'name',
        'is_active'
    ]

    search_fields = [
        'name'
    ]

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED and isinstance(request.user, User):
            instance = Question.objects.get(pk=int(response.data['id']))
            history = HistoryUser(
                user=request.user,
                description='Creo una pregunta',
                content_object=instance
            )
            history.save()
        return response


class QuestionRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [ModelPermission]

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Question, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Accedio a una pregunta',
                content_object=instance
            )
            history_user.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Question, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Actualizo un pregunta',
                content_object=instance
            )
            history_user.save()
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Question, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Deshabilito un pregunta',
                content_object=instance
            )
            history_user.save()
        return response

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class QuestionReactivateAPIView(generics.UpdateAPIView):
    queryset = Question.objects.filter(is_active=False)
    serializer_class = QuestionSerializer
    permission_classes = [ModelPermission]

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        # Reinstate instance
        instance.is_active = True
        instance.save()
        serializer = self.get_serializer(instance)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(Question, pk=int(response.data['id']))
            history_user = HistoryUser(
                user=request.user,
                description='Habilito una pregunta',
                content_object=instance
            )
            history_user.save()
        return response


class QuestionListPublicAPIView(generics.ListAPIView):
    serializer_class = QuestionSerializer
    queryset = Question.objects.filter(is_active=True)
    pagination_class = None
