from django.contrib import admin

# Register your models here.
from apps.question.models import Question

admin.site.register(Question)
