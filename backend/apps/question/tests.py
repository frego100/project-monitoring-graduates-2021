from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient


# Create your tests here.
from apps.question.models import Question
from apps.question.serializers import QuestionSerializer


class GraduateAchievementsAcademicAPIViewsTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user('user_test')
        self.user.is_superuser = True
        self.client.force_authenticate(self.user)
        self.user.save()
        self.question = Question.objects.create(
            name='Testing question'
        )

    def test_list_question(self):
        """
        Test list questions
        """
        response = self.client.get('/question/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_question(self):
        """
        Test create question
        """
        prev_len = len(Question.objects.all())
        data = {
            "name": "Second Question"
        }

        response = self.client.post('/question/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(prev_len + 1, len(Question.objects.all()))

    def test_retrieve_question(self):
        """
        Test retrieve question
        """
        response = self.client.get('/question/{}'.format(self.question.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, QuestionSerializer(self.question).data)

    def test_update_question(self):
        """
        Test update question
        """
        data = {
            "name": 'Second Question edited'
        }
        response = self.client.put('/question/{}'.format(self.question.id), data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            QuestionSerializer(
                Question.objects.get(id=self.question.id)
            ).data
        )

    def test_destroy_question(self):
        """
        Test destroy question
        """
        prev_len = len(Question.objects.filter(is_active=True))
        response = self.client.delete('/question/{}'.format(self.question.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(prev_len - 1, len(Question.objects.filter(is_active=True)))

    def test_reactive_question(self):
        """
        Test reactive question
        """
        self.question.is_active = False
        self.question.save()
        prev_len = len(Question.objects.filter(is_active=True))
        response = self.client.put('/question/reactivate/{}'.format(self.question.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(prev_len + 1, len(Question.objects.filter(is_active=True)))
