from django.db import models


# Create your models here.
class Question(models.Model):
    name = models.CharField(
        null=False,
        blank=False,
        max_length=511
    )

    is_active = models.BooleanField(
        null=False,
        default=True,
        verbose_name="Active"
    )

    def __str__(self):
        return f'{type(self).__name__}({", ".join("%s=%s" % item for item in vars(self).items() if item[0][0] != "_")})'

    class Meta:
        verbose_name = "Pregunta"
        verbose_name_plural = "Preguntas"
