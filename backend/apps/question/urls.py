from django.urls import path

from apps.question.views import QuestionListCreateAPIView, QuestionRetrieveUpdateDestroyAPIView, \
    QuestionReactivateAPIView, QuestionListPublicAPIView

urlpatterns = [
    path('', QuestionListCreateAPIView.as_view()),
    path('<int:pk>', QuestionRetrieveUpdateDestroyAPIView.as_view()),
    path('reactivate/<int:pk>', QuestionReactivateAPIView.as_view()),
    path('public/', QuestionListPublicAPIView.as_view())
]
