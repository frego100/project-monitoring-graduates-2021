from django.urls import path
from .views import GraduateProfileReportApiView,AdminReportApiView,SuperAdminReportApiView

urlpatterns = [
    path('graduate/', GraduateProfileReportApiView.as_view()),
    path('admin/', AdminReportApiView.as_view()),
    path('superadmin/', SuperAdminReportApiView.as_view()),
]