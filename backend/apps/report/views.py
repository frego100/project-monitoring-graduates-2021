from django.db.models import fields
from django.http import HttpResponse

from django.http import response
from django.shortcuts import get_object_or_404, render
from apps.instance.models import Area, Faculty, ProfessionalProgram
from apps.instance.models import Instance
from apps.announcement.models import Announcement
from apps.activity.models import Activity
from rest_framework.views import APIView
from apps.user_profile.models import GraduateProfile, AdminProfile, SuperAdminProfile, CompanyProfile
from openpyxl.utils import get_column_letter
from apps.instance.models import ProfessionalProgram
from apps.graduate_data_academic.models import GraduateDataAcademic
from apps.graduate_questions.models  import GraduateQuestions
# Create your views here.
from openpyxl import Workbook, workbook
from openpyxl.styles import Font
from datetime import datetime
import time 
from rest_framework import generics, status
from apps.graduate_achievements_academic.models import GraduateAchievementsAcademic
from rest_framework.response import Response
from apps.graduate_work_experience.models import GraduateWorkExperience
from rest_framework.permissions import IsAuthenticated
from apps.company.models import Company
class GraduateProfileReportApiView(APIView):
    
    def insert_initial(self,columns,sheet):
        columns.insert(0, "Escuela")
        sheet.append(columns)
        return sheet
    
    def get_graduates(self,i,start_date,start_end, graduated, student ):
        graduates=""
        if start_date!=""  and start_end!="" :
            graduates= GraduateDataAcademic.objects.filter(profesional_program_pk= i,graduate_profile_pk__user__date_joined__lte=start_end,graduate_profile_pk__user__date_joined__gte=start_date )
        elif start_date!="":
            graduates= GraduateDataAcademic.objects.filter(profesional_program_pk= i,graduate_profile_pk__user__date_joined__gte=start_date )
        elif start_end!="" :
            graduates= GraduateDataAcademic.objects.filter(profesional_program_pk= i,graduate_profile_pk__user__date_joined__lte=start_end )
        elif graduated != "":
            graduates= GraduateDataAcademic.objects.filter(profesional_program_pk= i, is_graduate= graduated )
        elif student != "":
            graduates= GraduateDataAcademic.objects.filter(profesional_program_pk= i, is_student= student )
        else: 
            graduates= GraduateDataAcademic.objects.filter(profesional_program_pk= i)
        return graduates  
    
    def insert_data(self, data,sheet, columns,leter,profesional_program):
        for j in range (0, len(data["results"])):
            sheet.append(list(data["results"][j].values()) )
        
        until= leter+(len(data["results"])-1)
        sheet.merge_cells(f'A{leter}:A{until}')
        sheet[f'A{leter}']= profesional_program.name

        return until+1

    def get_all_graduates(self, professional_program,start_date,start_end, graduated, student, columns, sheet):
        data={"results":[]} #
        leter= 2#
        sheet= self.insert_initial(columns,sheet)
        profesional_program_send= ""
        for i in professional_program:
            graduates= self.get_graduates(i,start_date,start_end,graduated,student) 
            for graduate in graduates:
                profesional_program_send= graduate.profesional_program_pk
                json= {} 
                json["Escuela"]= ""
                if "F. Creación" in columns:
                    objDate= graduate.graduate_profile_pk.user.date_joined
                    json["F. Creación"]= datetime.strftime(objDate,'%Y/%m/%d')
                if "Apellidos" in columns:
                    try:
                        json["Apellidos"]= graduate.graduate_profile_pk.user.last_name
                    except:
                        json["Apellidos"]= ""
                if "Nombres" in columns:
                    try:
                        json["Nombres"]= graduate.graduate_profile_pk.user.first_name
                    except:
                        json["Nombres"]= ""
                if "DNI" in columns:
                    try:
                        json["DNI"]= graduate.graduate_profile_pk.document
                    except:
                        json["DNI"]= ""
                if "CUI" in columns:
                    try:
                        json["CUI"]= graduate.graduate_profile_pk.cui
                    except:
                        json["CUI"]= ""
                if "Género" in columns:
                    try:
                        if graduate.graduate_profile_pk.gender :
                            json["Género"]= "Mujer"
                        else:
                            json["Género"]= "Hombre"
                    except:
                        json["Género"]= ""
                if "C.institucional" in columns:
                    try:
                        json["C.institucional"]= graduate.graduate_profile_pk.user.email
                    except:
                        json["C.institucional"]= ""
                if "C.personal/alternativo" in columns:
                    try:
                        json["C.personal/alternativo"]= graduate.graduate_profile_pk.email
                    except:
                        json["C.personal/alternativo"]= ""
                if "F.Nacimiento" in columns:
                    try:
                        objDate= graduate.graduate_profile_pk.date_birthday
                        json["F.Nacimiento"]= datetime.strftime(objDate,'%Y/%m/%d')
                    except:
                        json["F.Nacimiento"]= ""
                if "Celular" in columns:
                    try:
                        json["Celular"]= graduate.graduate_profile_pk.cell_phone
                    except:
                        json["Celular"]= ""
                if "Dirección" in columns:
                    try:
                        json["Dirección"]= graduate.graduate_profile_pk.address
                    except:
                        json["Dirección"]= ""
                if "Condición" in columns:
                    try:
                        if graduate.is_student:
                            json["Condición"]= "Estudiante"
                        elif graduate.is_graduate:
                            cadena= "Egresado"
                            achivments= GraduateAchievementsAcademic.objects.filter(graduate_profile_pk= graduate.graduate_profile_pk)
                            for achivment in achivments:
                                cadena+= "/"+achivment.get_grade_display()
                            json["Condición"]= cadena
                        else :
                            json["Condición"]= ""

                    except:
                        json["Condición"]= ""

                if "Sede" in columns:
                    try:
                        json["Sede"]= graduate.campus_pk.name
                    except:
                        json["Sede"]= ""

                if "Area" in columns:
                    try:
                        json["Area"]= graduate.profesional_program_pk.faculty_field.area_field.name
                    except:
                        json["Area"]= ""
                
                if "Facultad" in columns:
                    try:
                        json["Facultad"]= graduate.profesional_program_pk.faculty_field.name
                    except:
                        json["Facultad"]= ""
                
                if "Periodo" in columns:
                    try:
                        json["Periodo"]= graduate.period_graduate
                    except:
                        json["Periodo"]= ""
                if "Experiencia L." in columns:
                    try:
                        works= GraduateWorkExperience.objects.filter(graduate_profile_pk=graduate.graduate_profile_pk)
                        meses=0
                        for work in works:
                            d1 = work.date_start
                            d2 = work.date_end
                            meses += (abs((d2 - d1).days)) 
                        json["Experiencia L."]=int( meses/365) 
                    except:
                        json["Experiencia L."]= ""

                if "Condición de Trabajo" in columns:
                    try:
                        condicion=""
                        question=GraduateQuestions.objects.filter(question_pk=1,graduate_profile_pk= graduate.graduate_profile_pk)
                        if len(question)!=0:
                            if question[0].value== True:
                                condicion+= "Trabaja actualmente,"
                        question= GraduateQuestions.objects.filter(question_pk=2,graduate_profile_pk= graduate.graduate_profile_pk)
                        if len(question)!= 0 :
                            if question[0].value== True:
                                condicion+= "Realiza practicas actualmente,"
                        question= GraduateQuestions.objects.filter( question_pk=3,graduate_profile_pk= graduate.graduate_profile_pk) 
                        if len(question)!= 0 :
                            if question[0].value== True:
                                condicion+= "Busca trabajo,"
                        question=GraduateQuestions.objects.filter( question_pk=4,graduate_profile_pk= graduate.graduate_profile_pk)
                        if len(question)!= 0 :
                            if question[0].value== True:
                                condicion+= "Busca practicas"
                        json["Condición de Trabajo"]=condicion 
                    except:
                        json["Condición de Trabajo"]= ""
                data["results"].append(json)      
            
            if len(data["results"])!= 0 : 
                leter= self.insert_data(data,sheet,len(columns)+1,leter, profesional_program_send)
            data={"results":[]}

    def post(self, request, *args, **kwargs):
        columns=request.data["columns"]
        professional_program=request.data["professional_program"]
        start_date=request.data["start_date"]
        start_end=request.data["start_end"]
        #data={"results":[]} #
        book=Workbook()
        sheet=book.active
        if len( professional_program)  == 0:
            professional_program= ProfessionalProgram.objects.values_list('id', flat=True)
        self.get_all_graduates(professional_program, start_date,start_end,"","",columns,sheet)
        
        try:
            response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename=reporte_egresados.xlsx'

            book.save(response)

            return response
    
        except:
            return Response("message: Hubo un error al descargar el documento",status=status.HTTP_400_BAD_REQUEST)
        

class AdminReportApiView(APIView):

    permission_classes = [IsAuthenticated]
    def insert_initial_normal(self,columns,sheet):
        columns.insert(0, "#")
        sheet.append(columns)
        return sheet

    def get_all_announcements(self, columns,sheet):
        data={"results":[]}
        self.insert_initial_normal(columns,sheet)
        announcements=Announcement.objects.all()
        cont=1
        for announcement in announcements:
            json= {} 
            json["#"]=cont
            if "F. Creación" in columns:
                objDate= announcement.created
                json["F. Creación"]= datetime.strftime(objDate,'%Y/%m/%d %H:%M:%S')
            
            if "Nombre de la empresa" in columns:
                try:
                    json["Nombre de la empresa"]=announcement.company_pk.company_name
                except:
                    json["Nombre de la empresa"]=""  
            
            if "Condición" in columns:
                try:
                    if announcement.status:
                        json["Condición"]="Activo"
                    else:
                        json["Condición"]="Vencido"  
                except:
                    json["Condición"]=""

            if "Empleo/practicas" in columns:
                try:
                    json["Empleo/practicas"]=announcement.description
                except:
                    json["Empleo/practicas"]=""

            if "Titulo del puesto de trabajo buscado" in columns:
                try:
                    json["Titulo del puesto de trabajo buscado"]=announcement.name
                except:
                    json["Titulo del puesto de trabajo buscado"]=""

            if "F. Inicio de la convocatoria" in columns:
                try:
                    objDate= announcement.date_start
                    json["F. Inicio de la convocatoria"]=datetime.strftime(objDate,'%Y/%m/%d ')
                except:
                    json["F. Inicio de la convocatoria"]=""
            
            if "F. de Cierre de la convocatoria" in columns:
                try:
                    objDate= announcement.date_end
                    json["F. de Cierre de la convocatoria"]=datetime.strftime(objDate,'%Y/%m/%d')
                except:
                    json["F. de Cierre de la convocatoria"]=""
            
            if "Nº de vacantes" in columns:
                try:
                    json["Nº de vacantes"]=""
                except:
                    json["Nº de vacantes"]=""

            data["results"].append(json)  
            cont+=1  
 
        if len(data["results"])!= 0 : 
            self.insert_data(data,sheet) 
            data={"results":[]}


    def get_all_activities(self, columns,sheet):
        data={"results":[]}
        self.insert_initial_normal(columns,sheet)
        activities=Activity.objects.all()
        cont=1
        for activity in activities:
            json= {} 
            json["#"]=cont
            if "F. Creación" in columns:
                objDate= activity.created_at
                json["F. Creación"]= datetime.strftime(objDate,'%Y/%m/%d %H:%M:%S')
            
            if "Nombre de la actividad" in columns:
                try:
                    json["Nombre de la actividad"]=activity.title
                except:
                    json["Nombre de la actividad"]=""  
            
            if "Fecha" in columns:
                try:
                    objDate= activity.start_date
                    json["Fecha"]=datetime.strftime(objDate,'%Y/%m/%d')
                except:
                    json["Fecha"]=""

            if "Hora" in columns:
                try:
                    objDate= activity.start_date
                    json["Hora"]=datetime.strftime(objDate,'%H:%M:%S')
                except:
                    json["Hora"]=""

            if "Lugar" in columns:
                try:
                    json["Lugar"]=""
                except:
                    json["Lugar"]=""

            if "Enlace" in columns:
                try:
                    json["Enlace"]=activity.link
                except:
                    json["Enlace"]=""
            
            data["results"].append(json)  
            cont+=1  
 
        if len(data["results"])!= 0 : 
            self.insert_data(data,sheet) 
            data={"results":[]}

    def insert_initial(self,columns,sheet):
        aux=columns
        try:
            aux.remove("Nº de usuarios egresados")  
        except:  
            pass
        try:
            aux.remove("Nº de usuarios alumnos")  
        except:  
            pass
        try:
            aux.remove("Nº de convocatorias")  
        except:  
            pass
        try:
            aux.remove("Nº de actividades")  
        except:  
            pass
        
        try:
            aux.remove("Nº de usuarios empresas")  
        except:  
            pass
        columns.insert(0, "#")
        sheet.append(aux)
        return sheet
    
    def get_admins(self, start_date,start_end):
        admins=""
        if start_date!=""  and start_end!="" :
            admins= AdminProfile.objects.filter(user__date_joined__lte=start_end,user__date_joined__gte=start_date )
        elif start_date!="":
            admins= AdminProfile.objects.filter(user__date_joined__gte=start_date )
        elif start_end!="" :
            admins= AdminProfile.objects.filter(user__date_joined__lte=start_end )
        else: 
            admins= AdminProfile.objects.all()
        return admins  
    
    def insert_data(self, data,sheet):
        for j in range (0, len(data["results"])):
            sheet.append(list(data["results"][j].values()) )



    def post(self, request, *args, **kwargs):
        columns=request.data["columns"]
        start_date=request.data["start_date"]
        start_end=request.data["start_end"]
        data={"results":[]}
        book=Workbook()
        sheet=book.active
        if "Nº de usuarios egresados" in columns:
            egresados= GraduateProfileReportApiView()
            sheetEgresados= book.create_sheet("Nº de usuarios egresados")
            professional_program= ProfessionalProgram.objects.values_list('id', flat=True)
            columns_egresados= [
                "F. Creación",
                "Apellidos",
                "Nombres",
                "DNI",
                "CUI",
                "Género",
                "C.institucional",
                "C.personal/alternativo",
                "F.Nacimiento",
                "Periodo"
            ]

            egresados.get_all_graduates(professional_program,"","", True, "",columns_egresados, sheetEgresados)

        if "Nº de usuarios alumnos" in columns:
            egresados= GraduateProfileReportApiView()
            sheetEgresadosStudents= book.create_sheet("Nº de usuarios alumnos")
            professional_program= ProfessionalProgram.objects.values_list('id', flat=True)
            columns_egresados= [
                "F. Creación",
                "Apellidos",
                "Nombres",
                "DNI",
                "CUI",
                "Género",
                "C.institucional",
                "C.personal/alternativo",
                "F.Nacimiento",
                "Periodo"
            ]

            egresados.get_all_graduates(professional_program,"","", "", True,columns_egresados, sheetEgresadosStudents)

        if "Nº de convocatorias" in columns:
            sheetConvocatorias= book.create_sheet("Nº de convocatorias")
            columns_announcement= [
            "F. Creación",
            "Nombre de la empresa",
            "Condición",
            "Empleo/practicas",
            "Titulo del puesto de trabajo buscado",
            "F. Inicio de la convocatoria",
            "F. de Cierre de la convocatoria",
            "Nº de vacantes"
            ]

            self.get_all_announcements(columns_announcement,sheetConvocatorias)


        if "Nº de actividades" in columns:
            sheetActividades= book.create_sheet("Nº de actividades")
            columns_activity= [
            "F. Creación",
            "Nombre de la actividad",
            "Fecha",
            "Hora",
            "Lugar",
            "Enlace"
            ]
            self.get_all_activities(columns_activity,sheetActividades)


        
        sheet= self.insert_initial(columns,sheet)
        admins= self.get_admins(start_date,start_end) 
        cont= 1 
        for admin in admins:
                json= {} 
                json["#"]= cont 
                if "F. Creación" in columns:
                    objDate= admin.user.date_joined
                    json["F. Creación"]= datetime.strftime(objDate,'%Y/%m/%d')
                if "Area" in columns:
                    try:
                        aux= Area.objects.filter(instance_ptr_id= admin.instance)
                        if len(aux)  !=0  :
                            json["Area"]= aux[0].name
                        else:
                           json["Area"]= "" 
                    except:
                        json["Area"]= ""
                
                if "Facultad" in columns:
                    try:
                        aux=Faculty.objects.filter(instance_ptr_id= admin.instance)
                        if len( aux)  !=0  :
                            json["Area"]= aux[0].area_field.name
                            json["Facultad"]= aux[0].name
                        else:
                            json["Facultad"]= ""
                    except:
                        json["Facultad"]= ""

                if "Escuela Profesional" in columns:
                    try:
                        aux= ProfessionalProgram.objects.filter(instance_ptr_id= admin.instance)
                        if len(aux)  !=0  :
                            json["Escuela Profesional"]= aux[0].name
                            json["Facultad"]= aux[0].faculty_field.name 
                            json["Area"]= aux[0].faculty_field.area_field.name
                        
                        else:
                            json["Escuela Profesional"]= ""
                    except:
                        json["Escuela Profesional"]= ""

                if "Cargo/Puesto" in columns:
                    try:
                        json["Cargo/Puesto"]= admin.job_position.name
                    except:
                        json["Cargo/Puesto"]= ""
                if "Docente o Administrativo" in columns:
                    try:
                        json["Docente o Administrativo"]= admin.type_employee.name
                    except:
                        json["Docente o Administrativo"]= ""

                if "Apellidos y nombres" in columns:
                    try:
                        json["Apellidos y nombres"]= admin.user.first_name + admin.user.last_name
                    except:
                        json["Apellidos y nombres"]= ""
                if "DNI" in columns:
                    try:
                        json["DNI"]=admin.document
                    except:
                        json["DNI"]= ""
                if "Email" in columns:
                    try:
                        json["Email"]= admin.user.email
                    except:
                        json["Email"]= ""
                if "Celular" in columns:
                    try:
                        json["Celular"]= admin.cell_phone
                    except:
                        json["Celular"]= ""
                
                data["results"].append(json)   
                cont+=1   
            
        if len(data["results"])!= 0 : 
                self.insert_data(data,sheet)
        data={"results":[]}

        try:
            response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename=reporte_administradores.xlsx'

            book.save(response)

            return response
    
        except:
            return Response("message: Hubo un error al descargar el documento",status=status.HTTP_400_BAD_REQUEST)
         

class SuperAdminReportApiView(APIView):

    permission_classes = [IsAuthenticated]
   
    def get_superadmins(self, start_date,start_end):
        superadmins=""
        if start_date!=""  and start_end!="" :
            superadmins= SuperAdminProfile.objects.filter(user__date_joined__lte=start_end,user__date_joined__gte=start_date )
        elif start_date!="":
            superadmins= SuperAdminProfile.objects.filter(user__date_joined__gte=start_date )
        elif start_end!="" :
            superadmins= SuperAdminProfile.objects.filter(user__date_joined__lte=start_end )
        else: 
            superadmins= SuperAdminProfile.objects.all()
        return superadmins  
    
    def get_all_companies(self,columns,sheet):
        admin= AdminReportApiView()
        data={"results":[]}
        admin.insert_initial_normal(columns,sheet)
        companies=Company.objects.all()
        cont=1
        for company in companies:
            aux=CompanyProfile.objects.filter(company=company)
            print(aux)
            json= {} 
            json["#"]=cont
            try:
                if "F. Creación" in columns:
                    objDate= aux[0].user.date_joined        
                    json["F. Creación"]= datetime.strftime(objDate,'%Y/%m/%d')
            except:
                json["F. Creación"]= ""
            if "R. Social o Nombre" in columns:
                try:
                    if company.social_reason!="":
                        json["R. Social o Nombre"]=company.social_reason
                    else:
                        json["R. Social o Nombre"]=company.company_name
                except:
                    json["R. Social o Nombre"]=""  
            
            if "RUC" in columns:
                try:
                    json["RUC"]=company.ruc
                except:
                    json["RUC"]=""

            if "Rubro/actividad" in columns:
                try:
                    json["Rubro/actividad"]=company.company_activity
                except:
                    json["Rubro/actividad"]=""

            if "Apellidos y nombres" in columns:
                try:
                  
                    json["Apellidos y nombres"]=aux[0].user.first_name +" "+ aux[0].user.last_name
                except:
                    json["Apellidos y nombres"]=""

            if "DNI" in columns:
                try:
                    json["DNI"]=aux[0].document
                except:
                    json["DNI"]=""
            if "Email" in columns:
                try:
                    json["Email"]=aux[0].user.email
                except:
                    json["Email"]=""
            if "Celular" in columns:
                try:
                    json["Celular"]=aux[0].cell_phone
                except:
                    json["Celular"]=""
            
            data["results"].append(json)  
            cont+=1  
 
        if len(data["results"])!= 0 : 
            admin.insert_data(data,sheet) 
            data={"results":[]}

    def post(self, request, *args, **kwargs):
        columns=request.data["columns"]
        start_date=request.data["start_date"]
        start_end=request.data["start_end"]
        data={"results":[]}
        book=Workbook()
        sheet=book.active
        if "Nº de usuarios egresados" in columns:
            egresados= GraduateProfileReportApiView()
            sheetEgresados= book.create_sheet("Nº de usuarios egresados")
            professional_program= ProfessionalProgram.objects.values_list('id', flat=True)
            columns_egresados= [
                "F. Creación",
                "Apellidos",
                "Nombres",
                "DNI",
                "CUI",
                "Género",
                "C.institucional",
                "C.personal/alternativo",
                "F.Nacimiento",
                "Periodo"
            ]

            egresados.get_all_graduates(professional_program,"","", True, "",columns_egresados, sheetEgresados)

        if "Nº de usuarios alumnos" in columns:
            egresados= GraduateProfileReportApiView()
            sheetEgresadosStudents= book.create_sheet("Nº de usuarios alumnos")
            professional_program= ProfessionalProgram.objects.values_list('id', flat=True)
            columns_egresados= [
                "F. Creación",
                "Apellidos",
                "Nombres",
                "DNI",
                "CUI",
                "Género",
                "C.institucional",
                "C.personal/alternativo",
                "F.Nacimiento",
                "Periodo"
            ]

            egresados.get_all_graduates(professional_program,"","", "", True,columns_egresados, sheetEgresadosStudents)

        if "Nº de convocatorias" in columns:
            announcements= AdminReportApiView()
            sheetConvocatorias= book.create_sheet("Nº de convocatorias")
            columns_announcement= [
            "F. Creación",
            "Nombre de la empresa",
            "Condición",
            "Empleo/practicas",
            "Titulo del puesto de trabajo buscado",
            "F. Inicio de la convocatoria",
            "F. de Cierre de la convocatoria",
            "Nº de vacantes"
            ]

            announcements.get_all_announcements(columns_announcement,sheetConvocatorias)


        if "Nº de actividades" in columns:
            activities= AdminReportApiView()
            sheetActividades= book.create_sheet("Nº de actividades")
            columns_activity= [
            "F. Creación",
            "Nombre de la actividad",
            "Fecha",
            "Hora",
            "Lugar",
            "Enlace"
            ]
            activities.get_all_activities(columns_activity,sheetActividades)
        
        if "Nº de usuarios empresas" in columns:
            sheetCompany= book.create_sheet("Nº de usuarios empresas")
            columns_company= [
            "F. Creación",
            "R. Social o Nombre",
            "RUC",
            "Rubro/actividad",
            "Apellidos y nombres",
            "DNI ",
            "Email",
            "Celular"
            ]
            self.get_all_companies(columns_company,sheetCompany)

        admin= AdminReportApiView()
        sheet= admin.insert_initial(columns,sheet)
        superadmins= self.get_superadmins(start_date,start_end) 
        cont= 1 
        for superadmin in superadmins:
                json= {} 
                json["#"]= cont 
                if "F. Creación" in columns:
                    objDate= superadmin.user.date_joined
                    json["F. Creación"]= datetime.strftime(objDate,'%Y/%m/%d')
                
                if "Oficina/Programa" in columns:
                    json["Oficina/Programa"]= ""
                
                if "Cargo/Puesto" in columns:
                    try:
                        json["Cargo/Puesto"]= superadmin.job_position.name
                    except:
                        json["Cargo/Puesto"]= ""

                if "Apellidos y nombres" in columns:
                    try:
                        json["Apellidos y nombres"]= superadmin.user.first_name + superadmin.user.last_name
                    except:
                        json["Apellidos y nombres"]= ""
                if "DNI" in columns:
                    try:
                        json["DNI"]=superadmin.document
                    except:
                        json["DNI"]= ""
                if "Email" in columns:
                    try:
                        json["Email"]= superadmin.user.email
                    except:
                        json["Email"]= ""
                if "Celular" in columns:
                    try:
                        json["Celular"]= superadmin.cell_phone
                    except:
                        json["Celular"]= ""
                
                data["results"].append(json)   
                cont+=1   
            
        if len(data["results"])!= 0 : 
                admin.insert_data(data,sheet)
        data={"results":[]}

        try:
            response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            response['Content-Disposition'] = 'attachment; filename=reporte_superadministradores.xlsx'

            book.save(response)

            return response
    
        except:
            return Response("message: Hubo un error al descargar el documento",status=status.HTTP_400_BAD_REQUEST)
         


