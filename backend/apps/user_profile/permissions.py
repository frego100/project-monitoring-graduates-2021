from django.contrib.auth.models import User
from rest_framework import permissions


class UserProfilePermission(permissions.BasePermission):
    def has_permission(self, request, view):

        if request.method == 'GET':
            return request.user.has_perm('user_profile.view_superadminprofile')
        if request.method == 'POST':
            # return request.user.has_perm('user_profile.add_superadminprofile')
            return True
        if request.method == 'PUT':
            return request.user.has_perm('user_profile.change_superadminprofile')
        return False


class SuperAdminProfilePermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method == 'GET':
            return request.user.has_perm('user_profile.view_superadminprofile')
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('user_profile.change_superadminprofile')
        return False


class AdminProfilePermission(permissions.BasePermission):
    def has_permission(self, request, view):

        if request.method == 'GET':
            return request.user.has_perm('user_profile.view_adminprofile')
        if request.method == 'POST':
            # return request.user.has_perm('user_profile.add_adminprofile')
            return True
        return False


class GraduateProfilePermission(permissions.BasePermission):
    def has_permission(self, request, view):

        if request.method == 'GET':
            return request.user.has_perm('user_profile.view_graduateprofile')
        if request.method == 'POST':
            # return request.user.has_perm('user_profile.add_graduateprofile')
            return True
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.has_perm('user_profile.change_superadminprofile')
        return False


class MyselfOrSuperuserPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if not (isinstance(obj, User) and isinstance(request.user, User)):
            return False
        else:
            return (obj == request.user and obj.is_active) or request.user.is_superuser
