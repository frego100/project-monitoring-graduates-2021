from rest_framework import serializers

from apps.employee.serializer import TypeEmployeeSerializer
from apps.identificators.serializers import IdentificatorSerializer
from apps.job_position.serializer import JobPositionSerializer
from apps.user.serializer import UserSerializer, UserGraduateSerializer
from .models import SuperAdminProfile, AdminProfile, UserProfile, CompanyProfile, GraduateProfile
from ..company.models import Company
from ..company.serializers import CompanySerializer, CompanyAuthSerializer, CompanyLiteSerializerOfCompanyProfile
from ..instance.models import Area, Faculty, ProfessionalProgram
from ..instance.serializers import AreaSerializer, FacultySerializer, ProfessionalProgramSerializer


class UserProfileSerializer(serializers.ModelSerializer):
    picture = serializers.SerializerMethodField()

    class Meta:
        """
        Meta class for Profile Serializer
        """
        model = UserProfile
        fields = '__all__'

    def get_picture(self, obj):
        if obj.picture:
            return self.context['request'].build_absolute_uri(obj.picture.url)
        return None


class SuperAdminSerializer(serializers.ModelSerializer):
    class Meta:
        """
        Meta class for Super Admin Profile Serializer
        """
        model = SuperAdminProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.picture:
            data['picture'] = self.context['request'].build_absolute_uri(instance.picture.url)
        return data

class SuperAdminPreviousValidationSerializer(serializers.ModelSerializer):
    class Meta:
        """
        Meta class for Admin Serializer
        """
        model = SuperAdminProfile
        exclude = ('user', )

class AdminSerializer(serializers.ModelSerializer):
    picture = serializers.SerializerMethodField()

    class Meta:
        """
        Meta class for Admin Serializer
        """
        model = AdminProfile
        fields = '__all__'

    def get_picture(self, obj):
        if obj.picture:
            return self.context['request'].build_absolute_uri(obj.picture.url)
        return None


class AdminPreviousValidationSerializer(serializers.ModelSerializer):
    class Meta:
        """
        Meta class for Admin Serializer
        """
        model = AdminProfile
        exclude = ('user',)


class SuperAdminProfileSerializer(serializers.ModelSerializer):
    """
    SuperAdminProfileSerializer Serializer ModelSerializer
    """

    user = UserSerializer(read_only=True)
    identificator = IdentificatorSerializer(read_only=True)
    job_position = JobPositionSerializer(read_only=True)
    type_employee = TypeEmployeeSerializer(read_only=True)

    class Meta:
        """
        Meta class for Group Serializer
        """
        model = SuperAdminProfile
        fields = '__all__'


class AdminProfileSerializer(serializers.ModelSerializer):
    class Meta:
        """
        Meta class for Admin Profile Serializer
        """
        model = AdminProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super(AdminProfileSerializer, self).to_representation(instance)
        if instance.picture:
            data['picture'] = self.context['request'].build_absolute_uri(instance.picture.url)
        if instance.instance:
            data['instance'] = self.get_instance_serializer(instance.instance).data
        data['identificator'] = IdentificatorSerializer(instance.identificator).data
        data['job_position'] = JobPositionSerializer(instance.job_position).data
        data['type_employee'] = TypeEmployeeSerializer(instance.type_employee).data
        return data

    def get_instance_serializer(self, instance):
        if len(Area.objects.filter(id=instance.id)):
            return AreaSerializer(Area.objects.get(pk=instance.id))
        if len(Faculty.objects.filter(id=instance.id)):
            return FacultySerializer(Faculty.objects.get(pk=instance.id))
        if len(ProfessionalProgram.objects.filter(id=instance.id)):
            return ProfessionalProgramSerializer(ProfessionalProgram.objects.get(pk=instance.id))


class AdminProfileSerializer2(serializers.ModelSerializer):
    """
    Admin profile Serializer ModelSerializer alternative
    """

    user = UserSerializer(read_only=True)
    identificator = IdentificatorSerializer(read_only=True)
    job_position = JobPositionSerializer(read_only=True)
    type_employee = TypeEmployeeSerializer(read_only=True)

    class Meta:
        """
        Meta class for Admin profile Serializer
        """
        model = AdminProfile
        fields = '__all__'


class CompanyProfileSerializer(serializers.ModelSerializer):
    """
    Serializer for company profile
    """

    class Meta:
        """
        Meta class for serializer
        """
        model = CompanyProfile
        fields = '__all__'

    def to_representation(self, instance):
        """
        Method that design the way how the model is going to be showed
        In attributes Identificator and Company we change the default data for serializers related
        """
        data = super(CompanyProfileSerializer, self).to_representation(instance=instance)
        data['identificator'] = IdentificatorSerializer(instance=instance.identificator).data
        data['company'] = CompanySerializer(instance=instance.company, context=self.context).data
        if instance.picture:
            data['picture'] = self.context['request'].build_absolute_uri(instance.picture.url)
        return data


class CompanyProfileDetailSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    identificator = IdentificatorSerializer()
    company = CompanySerializer()

    class Meta:
        model = CompanyProfile
        fields = '__all__'


class CompanyProfileCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyProfile
        fields = '__all__'


class CompanyProfilePreviousValidationSerializer(serializers.ModelSerializer):
    company = CompanyLiteSerializerOfCompanyProfile()

    class Meta:
        """
        Meta class for Admin Serializer
        """
        model = CompanyProfile
        exclude = ('user',)


class PictureProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['picture']


class LogoProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['logo']


class UserProfileMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super(UserProfileMiniSerializer, self).to_representation(instance)
        if instance.identificator:
            data['identificator'] = instance.identificator.name
        request = self.context.get('request', None)
        if instance.picture and request is not None:
            data['picture'] = request.build_absolute_uri(instance.picture.url)
        elif instance.picture:
            data['picture'] = instance.picture.url
        return data


class GraduateCVSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateProfile
        fields = ['cv']
        extra_kwargs = {'cv': {'required': True}}

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.cv:
            data['cv'] = self.context['request'].build_absolute_uri(instance.cv.url)
        return data


class GraduateProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.cv:
            request = self.context.get('request', None)
            if request is not None:
                data['cv'] = request.build_absolute_uri(instance.cv.url)
            else:
                data['cv'] = instance.cv.url
            # data['cv'] = self.context['request'].build_absolute_uri(instance.cv.url)
        return data


class GraduateProfilePreviousValidationSerializer(serializers.ModelSerializer):
    class Meta:
        """
        Meta class for Admin Serializer
        """
        model = GraduateProfile
        exclude = ('user',)


class GraduateProfileDetailSerializer(serializers.ModelSerializer):
    user = UserGraduateSerializer(read_only=True)
    identificator = IdentificatorSerializer(read_only=True)

    class Meta:
        model = GraduateProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.cv:
            data['cv'] = self.context['request'].build_absolute_uri(instance.cv.url)
        return data


class GraduateAuthProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = GraduateProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super(GraduateAuthProfileSerializer, self).to_representation(instance)
        if instance.identificator:
            data['identificator'] = IdentificatorSerializer(
                instance=instance.identificator,
                context=self.context
            ).data
        if instance.cv:
            data['cv'] = self.context['request'].build_absolute_uri(instance.cv.url)
        return data


class GraduateMiniSerializer(UserProfileMiniSerializer):
    class Meta:
        model = GraduateProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.cv:
            data['cv'] = self.context['request'].build_absolute_uri(instance.cv.url)
        return data


class SuperAdminMiniSerializer(UserProfileMiniSerializer):
    class Meta:
        model = SuperAdminProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super(SuperAdminMiniSerializer, self).to_representation(instance)
        if instance.job_position:
            data['job_position'] = instance.job_position.name
        if instance.type_employee:
            data['type_employee'] = instance.type_employee.name
        return data


class AdminMiniSerializer(UserProfileMiniSerializer):
    class Meta:
        model = AdminProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super(AdminMiniSerializer, self).to_representation(instance)
        if instance.job_position:
            data['job_position'] = instance.job_position.name
        if instance.type_employee:
            data['type_employee'] = instance.type_employee.name
        if instance.instance:
            if len(Area.objects.filter(id=instance.instance.id)):
                data['instance'] = Area.objects.get(pk=instance.instance.id).name
            if len(Faculty.objects.filter(id=instance.instance.id)):
                data['instance'] = Faculty.objects.get(pk=instance.instance.id).name
            if len(ProfessionalProgram.objects.filter(id=instance.instance.id)):
                data['instance'] = ProfessionalProgram.objects.get(pk=instance.instance.id).name
        return data


class CompanyMiniSerializer(UserProfileMiniSerializer):
    class Meta:
        model = CompanyProfile
        fields = '__all__'

    def to_representation(self, instance):
        data = super(CompanyMiniSerializer, self).to_representation(instance)
        if instance.company:
            data['company'] = instance.company.company_name
        return data


class SuperAdminAuthSerializer(serializers.ModelSerializer):
    class Meta:
        """
        Meta class for Super Admin Profile Serializer
        """
        model = SuperAdminProfile
        fields = '__all__'

    def to_representation(self, instance):
        serialized_data = super().to_representation(instance)
        if instance.picture:
            serialized_data['picture'] = self.context['request'].build_absolute_uri(instance.picture.url)
        if instance.identificator:
            serialized_data['identificator'] = IdentificatorSerializer(instance.identificator).data
        if instance.job_position:
            serialized_data['job_position'] = JobPositionSerializer(instance.job_position).data
        if instance.type_employee:
            serialized_data['type_employee'] = TypeEmployeeSerializer(instance.type_employee).data
        return serialized_data


class CompanyProfileAuthSerializer(serializers.ModelSerializer):
    """
    Serializer for company profile
    """
    company = CompanyAuthSerializer()

    class Meta:
        """
        Meta class for serializer
        """
        model = CompanyProfile
        fields = '__all__'

    def create(self, validated_data):
        company_data = validated_data.pop('company')
        company_data['identificator'] = company_data['identificator'].id
        serializer_company = CompanyAuthSerializer(
            data=company_data
        )
        serializer_company.is_valid(raise_exception=True)
        company = serializer_company.save()
        validated_data['company'] = company
        return super().create(validated_data)

    def update(self, instance, validated_data):
        company_data = validated_data.pop('company')
        if "identificator" in company_data:
            company_data['identificator'] = company_data['identificator'].id
        serializer_company = CompanyAuthSerializer(
            instance=instance.company,
            data=company_data,
            partial=True
        )
        serializer_company.is_valid(raise_exception=True)
        serializer_company.save()
        return super().update(instance, validated_data)

    def to_representation(self, instance):
        """
        Method that design the way how the model is going to be showed
        In attributes Identificator and Company we change the default data for serializers related
        """
        data = super().to_representation(instance=instance)
        if instance.picture:
            data['picture'] = self.context['request'].build_absolute_uri(instance.picture.url)
        if instance.identificator:
            data['identificator'] = IdentificatorSerializer(instance=instance.identificator, context=self.context).data
        if instance.company:
            data['company'] = CompanyAuthSerializer(instance.company, context=self.context).data
        return data


class ValidateDataSerializer(serializers.Serializer):
    """
    Serializer for validate user profile
    """
    type = serializers.CharField(required=True)
    data = serializers.JSONField(required=True)

    def validate_type(self, data):
        """
        Validator for profiles
        """
        if data != 'graduateprofile'and data != 'companyprofile' and data != 'adminprofile' and data != 'superadminprofile':
            raise serializers.ValidationError("No es un tipo valido")
        else:
            return data
