"""
Models for user profiles
"""
import os

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

from apps.company.models import Company
from apps.employee.models import TypeEmployee
from apps.identificators.models import Identificator
from apps.instance.models import Instance
from apps.job_position.models import JobPosition


def validate_cv_extension(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.doc', '.docx', '.epub', '.gdoc', '.odt', '.oth', '.ott', '.rtf']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


# Create your models here. [Abstract]
class UserProfile(models.Model):
    """
    User profile generic
    """
    user = models.OneToOneField(
        User,
        null=False,
        on_delete=models.CASCADE,
        related_name='profiles'
    )
    identificator = models.ForeignKey(
        Identificator,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        related_name='profiles'
    )
    document = models.CharField(
        max_length=200,
        null=False
    )
    cell_phone = models.CharField(
        max_length=15,
        null=True,
        default=None
    )
    phone = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        default=None
    )
    picture = models.ImageField(
        null=True,
        upload_to='profile/picture/',
        default=None
    )
    email = models.EmailField(
        max_length=255,
        null=True,
        blank=True,
        default=None
    )
    is_active = models.BooleanField(
        default=True
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='profile_created_user',
        null=True,
        default=None
    )
    updated_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='profile_updated_user',
        null=True,
        default=None
    )

    class Meta:
        verbose_name = 'Perfil de Usuario'
        verbose_name_plural = 'Perfiles de Usuario'


class SuperAdminProfile(UserProfile):
    """
    Profile for user admin users
    """
    job_position = models.ForeignKey(
        JobPosition,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Position'
    )
    type_employee = models.ForeignKey(
        TypeEmployee,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Tipo de Empleado'
    )

    class Meta:
        verbose_name = 'Perfil de Usuario Super Administrador'
        verbose_name_plural = 'Perfiles de Usuario Super Administrador'


class AdminProfile(UserProfile):
    """
    Profile for user admin users
    """
    instance = models.ForeignKey(
        Instance,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Instance'
    )
    job_position = models.ForeignKey(
        JobPosition,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Position'
    )
    type_employee = models.ForeignKey(
        TypeEmployee,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name='Tipo de Empleado'
    )
    validated = models.BooleanField(
        default=True
    )

    class Meta:
        verbose_name = 'Perfil de Usuario Administrador'
        verbose_name_plural = 'Perfiles de Usuario Administrador'


class CompanyProfile(UserProfile):
    """
    Profile for user admin users
    """
    company = models.OneToOneField(
        Company,
        blank=False,
        null=False,
        related_name='company',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Perfil de Usuario Empresa'
        verbose_name_plural = 'Perfiles de Usuario Empresa'


class GraduateProfile(UserProfile):
    """
       Profile for user graduate users
       """
    gender = models.BooleanField(
        default=False,
        null=False
    )
    cui = models.IntegerField(
        default=0,
        null=False,
        unique=True
    )
    date_birthday = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        default=None,
    )
    web = models.URLField(
        blank=True,
        max_length=500,
        null=True,
        default=None,
    )
    address = models.CharField(
        blank=True,
        null=True,
        default=None,
        max_length=255
    )
    is_dcp = models.BooleanField(
        null=False,
        default=False,
        verbose_name='Es DCP'
    )
    description_dcp = models.CharField(
        max_length=400,
        null=True,
        default=None,
        verbose_name='Descripción'
    )

    cv = models.FileField(
        upload_to='graduate-profile/cv/',
        null=True,
        blank=True,
        validators=[validate_cv_extension]
    )

    class Meta:
        verbose_name = 'Perfil de Usuario Egresado'
        verbose_name_plural = 'Perfiles de Usuario Egresado'

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items() if item[0][0] != '_')
        )
