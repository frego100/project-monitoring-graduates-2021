import datetime

from django.contrib.auth.models import User, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.request import Request
from rest_framework.test import APIClient
from rest_framework.test import APIRequestFactory, force_authenticate

# Create your tests here.
from apps.company.models import Company
from apps.identificators.models import Identificator
from apps.user_profile.models import SuperAdminProfile, GraduateProfile
from apps.user_profile.serializer import SuperAdminProfileSerializer, CompanyProfileCreateSerializer, \
    GraduateProfileSerializer
from apps.user_profile.views import SuperAdminProfileRetrieveUpdateAPIView, CompanyProfileCreateAPIView, \
    GraduateProfileListCreateAPIView


# Create your tests here.
class SuperAdminProfileRetrieveUpdateViewTestCase(TestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.view = SuperAdminProfileRetrieveUpdateAPIView.as_view()
        self.serializer = SuperAdminProfileSerializer
        self.model = SuperAdminProfile
        self.permission = Permission.objects.all()

        self.url_path_retrieve_update_destroy = '/profile/super-admin'
        self.id_test = 1

        self.retrieve_queryset = None
        self.retrieve_request = None
        self.update_request = None
        self.update_queryset = None
        self.destroy_request = None
        self.destroy_queryset = None

        # Auth User
        self.auth_user = User.objects.create(username='whitecat')
        # Set permission for this user
        self.auth_user.user_permissions.set(self.permission)

        self.mock_picture = SimpleUploadedFile('test.jpg', b'whatevercontentsyouwant')
        self.mock_user = User.objects.create(username='darkcat')
        self.mock_model = self.model(id=self.id_test, user=self.mock_user, picture=self.mock_picture, document='12345',
                                     cell_phone='959124912')

    def test_retrieve(self):
        # Init var for this test
        self.retrieve_request = self.factory.get(self.url_path_retrieve_update_destroy)
        self.mock_model.save()
        # Force Authentication
        force_authenticate(self.retrieve_request, user=self.auth_user)

        # Call the Create API View
        response = self.view(self.retrieve_request, pk=self.id_test)

        self.retrieve_queryset = self.model.objects.filter(id=self.id_test).first()

        # Serialized data
        serialized = self.serializer(self.retrieve_queryset)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer
        # self.assertEqual(response.data, serialized.data)


class ProfileUpdateViewTestCase(TestCase):
    def setUp(self):
        """
        Configuration test cases
        """
        self.client = APIClient()
        self.user = User.objects.get_or_create(username='testuser')[0]
        self.user_profile = \
            SuperAdminProfile.objects.get_or_create(document='999', cell_phone='235234', user=self.user)[0]
        self.client.force_authenticate(self.user)

    def test_update(self):
        """
        Test case for update current profile
        """
        profile_type = ContentType.objects.get_for_model(self.user_profile, for_concrete_model=False)
        data = {
            "document": "2354353",
            "cell_phone": "99994545",
            "profile_type": 'SUPER-ADMIN'
        }

        response = self.client.patch('/profile/update-current-profile/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CompanyProfileCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.factory = APIRequestFactory()

        self.view = CompanyProfileCreateAPIView.as_view()
        self.serializer = CompanyProfileCreateSerializer

        self.url_path_create = '/profile/company'
        self.id_test = 1
        self.create_request = None
        self.create_queryset = None

    def test_create_with_authenticate_user_with_permissions(self):
        # Create a example job position
        Identificator.objects.create(id=self.id_test, code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        User.objects.create(id=self.id_test + 1, username='othercat')
        company = Company.objects.create(id=self.id_test, company_name='Test',
                                         identificator_id=self.id_test)
        json = {
            "user": self.id_test + 1,
            "identificator": None,
            "document": "DNI",
            "cell_phone": 23454534,
            "is_active": False,
            "phone": None,
            "company": company.pk
        }
        # Create a Request
        self.create_request = self.factory.post(self.url_path_create, json, format='json')
        # Auth User
        user = User.objects.create(username='whitecat')
        # Set permission for this user
        user.user_permissions.set(Permission.objects.all())
        # Force Authentication
        force_authenticate(self.create_request, user=user)
        # Call the Create API View
        response = self.view(self.create_request)
        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_authenticate_user_without_permissions(self):
        # Create a example job position
        Identificator.objects.create(id=self.id_test, code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        User.objects.create(id=self.id_test + 1, username='othercat')
        company = Company.objects.create(id=self.id_test, company_name='Test',
                                         identificator_id=self.id_test)
        json = {
            "user": self.id_test + 1,
            "identificator": None,
            "document": "DNI",
            "cell_phone": 23454534,
            "is_active": False,
            "phone": None,
            "company": company.pk
        }
        # Create a Request
        self.create_request = self.factory.post(self.url_path_create, json, format='json')
        # Call the Create API View
        response = self.view(self.create_request)
        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class GraduateProfileListCreateViewTestCase(TestCase):

    def setUp(self) -> None:
        self.factory = APIRequestFactory()
        self.paginator = LimitOffsetPagination()
        self.view = GraduateProfileListCreateAPIView.as_view()
        self.serializer = GraduateProfileSerializer

        self.url_path_list_create = '/profile/graduate/'
        self.id_test = 1
        self.list_queryset = None
        self.list_request = None
        self.create_request = None
        self.create_queryset = None

    def paginate_queryset(self, request):
        return list(self.paginator.paginate_queryset(self.list_queryset, request))

    def get_paginated_content(self, queryset):
        response = self.paginator.get_paginated_response(queryset)
        return response.data

    def test_list(self):
        # Init var for this test
        self.list_request = self.factory.get(self.url_path_list_create)
        self.list_queryset = GraduateProfile.objects.all()
        # Create a example job position
        User.objects.create(id=self.id_test + 1, username='othercat')
        Identificator.objects.create(id=self.id_test, code='DNI',
                                     max_length=200,
                                     name="Documento Nacional de Identidad",
                                     alphabet='NUM',
                                     is_active=True)
        company_instance = Company.objects.create(id=self.id_test, company_name='Test', identificator_id=self.id_test)
        GraduateProfile.objects.create(
            gender=True,
            cui=20170612,
            date_birthday=datetime.date.today(),
            web='https://app.ed.team',
            address='xdxdxdxdxdxdxd',
            document=12345678,
            cell_phone=1231234,
            phone=123454,
            user_id=self.id_test + 1,
            identificator_id=self.id_test,
            is_active=True
        )
        # Create a Django Rest Framework Request to call functions in the paginator class
        request = Request(self.list_request)
        # Result after execute paginator
        queryset_result = self.paginate_queryset(request)
        # Result after execute serializer
        serialized = self.serializer(queryset_result, many=True)
        # Result after execute serializer and paginator
        content_result = self.get_paginated_content(serialized.data)
        user = User.objects.create(id=1000, username='darkcat')
        user.user_permissions.set(Permission.objects.all())
        # Call the List API View
        force_authenticate(self.list_request, user)
        response = self.view(self.list_request)

        # Confirm if the status code is a success in this case the success code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm if the response data is equal to the result after running serializer and pager
        self.assertEqual(response.data, content_result)

        # Count if the total of items is equal to the size of the queryset
        self.assertEqual(len(queryset_result), self.list_queryset.count())
