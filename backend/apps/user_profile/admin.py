from django.contrib import admin

from apps.user_profile.models import UserProfile, SuperAdminProfile, AdminProfile, CompanyProfile, GraduateProfile

# Register your models here.

admin.site.register(UserProfile)
admin.site.register(SuperAdminProfile)
admin.site.register(AdminProfile)
admin.site.register(CompanyProfile)
admin.site.register(GraduateProfile)
