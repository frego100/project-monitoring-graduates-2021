# Create your views here.
from datetime import datetime
from io import BytesIO

import numpy as np
import pandas
import xlsxwriter as xlsxwriter
from django.contrib.auth.models import User
from django.http import QueryDict, HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, generics
from rest_framework.exceptions import ValidationError
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.generics import ListCreateAPIView, UpdateAPIView
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.history_user.models import HistoryUser
from monitoring_graduates.permission import ModelPermission
from .models import SuperAdminProfile, AdminProfile, CompanyProfile, UserProfile, GraduateProfile
from .permissions import UserProfilePermission, SuperAdminProfilePermission, \
    GraduateProfilePermission
from .serializer import (
    SuperAdminProfileSerializer,
    SuperAdminSerializer,
    AdminProfileSerializer,
    AdminSerializer,
    AdminProfileSerializer2, CompanyProfileCreateSerializer, PictureProfileSerializer,
    LogoProfileSerializer, GraduateProfileSerializer, GraduateProfileDetailSerializer, GraduateAuthProfileSerializer,
    CompanyProfileDetailSerializer, CompanyProfileAuthSerializer, SuperAdminAuthSerializer, ValidateDataSerializer,
    SuperAdminPreviousValidationSerializer,
    AdminPreviousValidationSerializer, CompanyProfilePreviousValidationSerializer,
    GraduateProfilePreviousValidationSerializer, GraduateCVSerializer
)
from ..activity.models import UserActivity
from ..company.models import Company
from ..company.serializers import CompanyLiteSerializerOfCompanyProfile
from ..graduate_data_academic.models import GraduateDataAcademic
from ..graduate_projects.utils import log_entry_history_user


class UserProfileListCreateAPIView(ListCreateAPIView):
    serializer_class = SuperAdminProfileSerializer
    queryset = SuperAdminProfile.objects.all()
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = [
        'user__username',
        'user__email',
        'document',
        'email',
        'cell_phone',
        'is_active',
        'job_position__name',
        'identificator__name',
        'type_employee__name'
    ]
    ordering_fields = ['user__username', 'user__first_name', 'user__last_name']
    permission_classes = (UserProfilePermission,)

    def post(self, request, *args, **kwargs):

        serializer = SuperAdminSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            user_profile = SuperAdminProfile.objects.get(pk=int(response.data['id']))
            user_profile.created_by = request.user
            user_profile.save()
            history = HistoryUser(
                user=request.user,
                description='Creo un perfil de superusuario',
                content_object=user_profile
            )
            history.save()

        return response

    def get(self, request, *args, **kwargs):
        response = super(UserProfileListCreateAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de perfiles de superusuarios'
            )
            history.save()
        return response


class SuperAdminProfileRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = SuperAdminSerializer
    retrieve_serializer_class = SuperAdminProfileSerializer
    queryset = SuperAdminProfile.objects.all()
    permission_classes = [SuperAdminProfilePermission]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.retrieve_serializer_class(instance)
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Recupero un SuperAdmin',
                content_object=instance
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            instance.updated_by = request.user
            instance.save()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un SuperAdmin',
                content_object=instance
            )
            history.save()
        return response


class PerfilReactivateView(UpdateAPIView):
    serializer_class = SuperAdminProfileSerializer
    queryset = SuperAdminProfile.objects.filter()
    permission_classes = (UserProfilePermission,)

    def put(self, request, *args, **kwargs):
        superadmin = get_object_or_404(SuperAdminProfile, pk=kwargs['pk'])
        if not superadmin.is_active:
            superadmin.is_active = True
            superadmin.save()
            history = HistoryUser(
                user=request.user,
                description='Habilito de manera logica un perfil de superadministrador',
                content_object=superadmin
            )
            history.save()
            return Response({"message": "Successful operation."}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "User is enabled."}, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        return self.put(self, request, *args, **kwargs)


class AdminProfileListCreateAPIView(ListCreateAPIView):
    serializer_class = AdminProfileSerializer2
    queryset = AdminProfile.objects.all()
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = [
        'user__username',
        'user__email',
        'document',
        'email',
        'cell_phone',
        'is_active',
        'job_position__name',
        'identificator__name',
        'type_employee__name',
        'validated'
    ]
    ordering_fields = ['user__username', 'user__first_name', 'user__last_name']

    # permission_classes = (AdminProfilePermission,)

    def post(self, request, *args, **kwargs):

        serializer = AdminSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            response = Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if response.status_code == status.HTTP_201_CREATED and \
                isinstance(request.user, User):
            user_profile = AdminProfile.objects.get(pk=int(response.data['id']))
            user_profile.created_by = request.user
            user_profile.save()
            history = HistoryUser(
                user=request.user,
                description='Creo un perfil de Administrador',
                content_object=user_profile
            )
            history.save()

        return response

    def get(self, request, *args, **kwargs):
        response = super(AdminProfileListCreateAPIView, self).get(request)
        if response.status_code == status.HTTP_200_OK:
            history = HistoryUser(
                user=request.user,
                description='Accedio al listado de perfiles de administradores'
            )
            history.save()
        return response


class AdminProfileRetrieveUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AdminProfileSerializer
    queryset = AdminProfile.objects.all()
    permission_classes = [ModelPermission]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.serializer_class(
            instance=instance,
            context=self.get_serializer_context()
        )
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Recupero un Perfil de Admnistrador',
                content_object=instance
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            instance.updated_by = request.user
            instance.save()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un perfil de Administrador',
                content_object=instance
            )
            history.save()
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for enable and disable admin profile
        """
        profile = self.get_object()
        profile.is_active = False
        profile.save()
        history = HistoryUser(
            user=request.user,
            description='Borro de manera logica un perfil de usuario',
            content_object=profile
        )
        history.save()
        return Response(
            self.serializer_class(
                instance=profile,
                context=self.get_serializer_context()
            ).data,
            status=status.HTTP_200_OK
        )


class UserUpdateInformationProfile(generics.UpdateAPIView):
    """
    API Rest view for get and edit the authenticated user's profile
    """
    permission_classes = [IsAuthenticated]
    profiles_keys = ['ADMIN', 'SUPER-ADMIN', 'COMPANY', 'GRADUATE']

    def get_object(self):
        """
        Method for get object for the view
        The subject object's type depends the kind of profile the user have
        """
        if len(SuperAdminProfile.objects.filter(user=self.request.user)):
            return SuperAdminProfile.objects.get(user=self.request.user)
        if len(AdminProfile.objects.filter(user=self.request.user)):
            return AdminProfile.objects.get(user=self.request.user)
        if len(CompanyProfile.objects.filter(user=self.request.user)):
            return CompanyProfile.objects.get(user=self.request.user)
        if len(GraduateProfile.objects.filter(user=self.request.user)):
            return GraduateProfile.objects.get(user=self.request.user)

    def get_serializer_class(self):
        """
        Method for get object's class serializer for the view
        The subject object's type depends the kind of profile the user have
        """
        if self.request.user.is_authenticated:
            if len(SuperAdminProfile.objects.filter(user=self.request.user)):
                return SuperAdminAuthSerializer
            if len(AdminProfile.objects.filter(user=self.request.user)):
                return AdminProfileSerializer
            if len(CompanyProfile.objects.filter(user=self.request.user)):
                return CompanyProfileAuthSerializer
            if len(GraduateProfile.objects.filter(user=self.request.user)):
                return GraduateAuthProfileSerializer

    def get_lite_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_lite_serializer_class()
        kwargs.setdefault('context', self.get_serializer_context())
        return serializer_class(*args, **kwargs)

    def get_lite_serializer_class(self):
        if self.request.user.is_authenticated:
            if len(SuperAdminProfile.objects.filter(user=self.request.user)):
                return SuperAdminSerializer
            if len(AdminProfile.objects.filter(user=self.request.user)):
                return AdminSerializer
            if len(CompanyProfile.objects.filter(user=self.request.user)):
                return CompanyProfileCreateSerializer
            if len(GraduateProfile.objects.filter(user=self.request.user)):
                return GraduateProfileSerializer

    def create_new_profile(self, profile_type, provided_data, request):
        temp_instance = self.get_object()
        previous_data = self.get_lite_serializer(instance=temp_instance)
        instance_data = previous_data.data
        instance_data._mutable = True
        instance_data.update(provided_data)
        if 'picture' in request.data:
            instance_data['picture'] = request.data['picture']
        if temp_instance.picture:
            instance_data['picture'] = temp_instance.picture
        else:
            del instance_data['picture']
        if profile_type == self.profiles_keys[0]:
            serialized_new_data = AdminProfileSerializer(
                data=instance_data,
                context=self.get_serializer_context()
            )
        if profile_type == self.profiles_keys[1]:
            serialized_new_data = SuperAdminAuthSerializer(
                data=instance_data,
                context=self.get_serializer_context()
            )
        if profile_type == self.profiles_keys[2]:
            serialized_new_data = CompanyProfileAuthSerializer(
                data=instance_data,
                context=self.get_serializer_context()
            )
        if profile_type == self.profiles_keys[3]:
            serialized_new_data = GraduateAuthProfileSerializer(
                data=instance_data,
                context=self.get_serializer_context()
            )
        self.get_object().delete()
        try:
            serialized_new_data.is_valid(raise_exception=True)
            serialized_new_data.save()
            return Response(serialized_new_data.data)
        except ValidationError:
            temp_instance.save()
            raise ValidationError(serialized_new_data.errors)
        except:
            temp_instance.save()
            return Response({
                "message": "Some attributes are required."
            })

    def update(self, request, *args, **kwargs):
        """
        Overwritten method update for can handle the different type of profile and user historic
        """
        if 'profile_type' in request.data and \
                not self.check_same_instance(self.get_object(), request.data['profile_type']):
            if request.data['profile_type'] in self.profiles_keys:
                return self.create_new_profile(request.data.pop('profile_type'), request.data, request)
            else:
                return Response(
                    status=status.HTTP_400_BAD_REQUEST,
                    data={
                        "message": "Your profile_type provided doesn't match with [ADMIN, SUPER-ADMIN, COMPANY, "
                                   "GRADUATE] "
                    })
        else:
            if isinstance(request.data, QueryDict):
                request.data._mutable = True
            request.data['user'] = request.user.id
            response = super().update(request, *args, **kwargs)
            if response.status_code == status.HTTP_200_OK and \
                    isinstance(request.user, User):
                instance = self.get_object()
                history = HistoryUser(
                    user=request.user,
                    description='Actualizo su propio perfil',
                    content_object=instance
                )
                history.save()
            return response

    def check_same_instance(self, instance, profile_type):
        if type(instance) == AdminProfile and profile_type == self.profiles_keys[0]:
            return True
        if type(instance) == SuperAdminProfile and profile_type == self.profiles_keys[1]:
            return True
        if type(instance) == CompanyProfile and profile_type == self.profiles_keys[2]:
            return True
        if type(instance) == GraduateProfile and profile_type == self.profiles_keys[3]:
            return True
        return False


class CompanyProfileCreateAPIView(generics.CreateAPIView):
    serializer_class = CompanyProfileCreateSerializer
    queryset = CompanyProfile.objects.all()

    # permission_classes = [DjangoModelPermissions]

    def create(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = True
        if isinstance(request.data['company'], dict):
            serializer = CompanyLiteSerializerOfCompanyProfile(data=request.data['company'])
            serializer.is_valid(raise_exception=True)
            company = serializer.save()
            """history = HistoryUser(
                user=request.user,
                description='Creo una Empresa pars un Perfil de Empresa',
                content_object=company
            )
            history.save()"""
            request.data['company'] = company.pk
        if isinstance(request.data, QueryDict):  # optional
            request.data._mutable = False
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            company_profile = self.get_object()
            company_profile.created_by = request.user
            company_profile.save()
            history = HistoryUser(
                user=request.user,
                description='Creo un Perfil de Empresa',
                content_object=company_profile
            )
            history.save()
        return response


class UpdateProfilePictureAuth(generics.UpdateAPIView):
    """
    View for update picture in a profile for a authenticated user
    """
    serializer_class = PictureProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        profile = get_object_or_404(UserProfile, user=self.request.user)
        return profile


class UpdateCompanyProfileLogoAuth(generics.UpdateAPIView):
    """
    View for update logo in a company profile for a authenticated user
    """

    serializer_class = LogoProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        profile = get_object_or_404(CompanyProfile, user=self.request.user)
        return profile.company


class CompanyProfileUpdateAPIView(generics.UpdateAPIView):
    serializer_class = CompanyProfileCreateSerializer
    queryset = CompanyProfile.objects.all()
    # permission_classes = [DjangoModelPermissions]
    permission_classes = [IsAuthenticated]

    def put(self, request, *args, **kwargs):
        if isinstance(request.data, QueryDict):
            request.data._mutable = True
        if isinstance(request.data['company'], dict):
            perfil = get_object_or_404(CompanyProfile, pk=kwargs['pk'])
            instance = get_object_or_404(Company, id=perfil.company.id)
            serializer = CompanyLiteSerializerOfCompanyProfile(instance, data=request.data['company'])
            serializer.is_valid(raise_exception=True)
            company = serializer.save()
            history = HistoryUser(
                user=request.user,
                description='Actualizo los datos de una empresa y  Perfil de Empresa',
                content_object=company
            )
            history.save()
            request.data['company'] = company.pk
        if isinstance(request.data, QueryDict):
            request.data._mutable = False
        response = super().put(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            company_profile = self.get_object()
            company_profile.updated_by = request.user
            company_profile.save()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un Perfil de Empresa',
                content_object=company_profile
            )
            history.save()
        return response


class GraduateProfileListCreateAPIView(generics.ListCreateAPIView):
    queryset = GraduateProfile.objects.all()
    serializer_class = GraduateProfileSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = [
        'user__username',
        'user__email',
        'document',
        'cell_phone',
        'is_active',
        'identificator__name',
        'cui',
        'address'
    ]
    ordering_fields = ['user__username', 'user__first_name', 'user__last_name', 'cui']
    permission_classes = [GraduateProfilePermission]

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Listo los perfiles de egresados'
            )
            history.save()
        return response

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = get_object_or_404(GraduateProfile, pk=kwargs['pk'])
            instance.created_by = request.user
            instance.save()
            history = HistoryUser(
                user=request.user,
                description='Creo el perfiles de egresados',
                content_type=instance
            )
            history.save()
        return response


class GraduateProfileListAPIView(generics.ListAPIView):
    serializer_class = GraduateProfileDetailSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter, SearchFilter]
    filterset_fields = [
        'user__username',
        'user__email',
        'is_active',
        'email',
        'identificator__name',
        'cui',
    ]
    search_fields = [
        '$user__username',
        '$user__first_name',
        '$user__last_name',
        '$user__email',
        '$cell_phone',
        '$email',
        '$cui',
    ]
    ordering_fields = ['user__username', 'user__first_name', 'user__last_name', 'cui', 'email']
    permission_classes = [GraduateProfilePermission]

    def get_queryset(self):
        professional_program_id = self.request.query_params.get('professional_program_id', None)
        area_id = self.request.query_params.get('area_id', None)
        faculty_id = self.request.query_params.get('faculty_id', None)
        not_in_activity_id = self.request.query_params.get('not_in_activity_id', None)
        qs = GraduateProfile.objects.filter(is_active=True)
        try:
            if area_id:
                graduate_in_instance = GraduateDataAcademic.get_graduate_by_area(int(area_id))
                qs = GraduateProfile.objects.filter(id__in=graduate_in_instance)

            if faculty_id:
                graduate_in_instance = GraduateDataAcademic.get_graduate_by_faculty(int(faculty_id))
                qs = GraduateProfile.objects.filter(id__in=graduate_in_instance)

            if professional_program_id:
                graduate_in_instance = GraduateDataAcademic.get_graduate_by_professional_program(
                    int(professional_program_id))
                qs = GraduateProfile.objects.filter(id__in=graduate_in_instance)

            if not_in_activity_id:
                graduate_in_activity = UserActivity.get_graduate_with_inscription_activity(int(not_in_activity_id))
                qs = qs.exclude(user__id__in=graduate_in_activity)

        except ValueError:
            pass
        return super(GraduateProfileListAPIView, self).filter_queryset(qs)


class GraduateProfileRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GraduateProfile.objects.all()
    serializer_class = GraduateProfileSerializer
    retrieve_serializer_class = GraduateProfileDetailSerializer

    permission_classes = [GraduateProfilePermission]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.retrieve_serializer_class(
            instance=instance,
            context=self.get_serializer_context()
        )
        response = Response(serializer.data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            history = HistoryUser(
                user=request.user,
                description='Recupero un Perfil de Egresado',
                content_object=instance
            )
            history.save()
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            instance.updated_by = request.user
            instance.save()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un perfil de Egresado',
                content_object=instance
            )
            history.save()
        response = Response(data=self.retrieve_serializer_class(self.get_object()).data)
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for enable and disable admin profile
        """
        instance = self.get_object()
        instance.is_active = False
        instance.save()
        response = Response(
            self.serializer_class(
                instance=instance,
                context=self.get_serializer_context()
            ).data,
            status=status.HTTP_200_OK
        )
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Borro de manera logica un perfil de egresado',
                content_object=instance
            )
            history.save()
        response = Response(data=self.retrieve_serializer_class(self.get_object()).data)
        return response


class GraduateProfileReactivateAPIView(generics.UpdateAPIView):
    queryset = GraduateProfile.objects.filter(is_active=False)
    serializer_class = GraduateProfileSerializer
    retrieve_serializer_class = GraduateProfileDetailSerializer

    permission_classes = [GraduateProfilePermission]

    def update(self, request, *args, **kwargs):
        """
        PUT or PATCH method for API View class
        """
        instance = self.get_object()
        instance.is_active = True
        instance.updated_by = request.user
        instance.save()
        response = Response(self.serializer_class(instance).data)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Activo de manera logica un perfil de egresado',
                content_object=instance
            )
            history.save()
        response = Response(
            data=self.retrieve_serializer_class(get_object_or_404(GraduateProfile, pk=kwargs['pk'])).data
        )
        return response


class CompanyProfileRetrieveAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CompanyProfile.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = CompanyProfileDetailSerializer
    model_class = CompanyProfile

    def retrieve(self, request, *args, **kwargs):
        response = super(CompanyProfileRetrieveAPIView, self).retrieve(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            log_entry_history_user(
                request_user=request.user,
                message='Recupero perfil de Empresa',
                instance=get_object_or_404(self.model_class, pk=self.kwargs['pk'])
            )
        return response

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            instance = self.get_object()
            instance.updated_by = request.user
            instance.save()
            history = HistoryUser(
                user=request.user,
                description='Actualizo un perfil de Empresa',
                content_object=instance
            )
            history.save()
        response = Response(data=self.retrieve_serializer_class(self.get_object()).data)
        return response

    def destroy(self, request, *args, **kwargs):
        """
        Method DELETE of HTTP for enable and disable company profile
        """
        instance = self.get_object()
        instance.is_active = False
        instance.save()
        response = Response(
            self.serializer_class(
                instance=instance,
                context=self.get_serializer_context()
            ).data,
            status=status.HTTP_200_OK
        )
        if response.status_code == status.HTTP_200_OK and \
                isinstance(request.user, User):
            history = HistoryUser(
                user=request.user,
                description='Borro de manera logica un perfil de Empresa',
                content_object=instance
            )
            history.save()
        response = Response(data=self.retrieve_serializer_class(self.get_object()).data)
        return response


class ValidateDataProfileView(generics.CreateAPIView):
    serializer_class = ValidateDataSerializer
    queryset = UserProfile.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        if data['type'] == 'graduateprofile':
            serializer_o = GraduateProfilePreviousValidationSerializer(data=data['data'])
            serializer_o.is_valid(raise_exception=True)
            return Response(status=status.HTTP_200_OK, data={"message": "Datos Correctos"})
        elif data['type'] == 'companyprofile':
            serializer_o = CompanyProfilePreviousValidationSerializer(data=data['data'])
            serializer_o.is_valid(raise_exception=True)
            return Response(status=status.HTTP_200_OK, data={"message": "Datos Correctos"})
        elif data['type'] == 'adminprofile':
            serializer_o = AdminPreviousValidationSerializer(data=data['data'])
            serializer_o.is_valid(raise_exception=True)
            return Response(status=status.HTTP_200_OK, data={ "message": "Datos Correctos" })
        elif data['type'] == 'superadminprofile':
            serializer_o = SuperAdminPreviousValidationSerializer(data=data['data'])
            serializer_o.is_valid(raise_exception=True)
            return Response(status=status.HTTP_200_OK, data={ "message": "Datos Correctos" })
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": "No es un tipo de perfil válido"})


class GraduateCVUpdateAPIView(generics.UpdateAPIView):
    serializer_class = GraduateCVSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        profile = get_object_or_404(GraduateProfile, user=self.request.user)
        return profile

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            instance = self.get_object()
            instance.updated_by = request.user
            instance.save()
            log_entry_history_user(
                request_user=request.user,
                message='Actualizo su CV',
                instance=instance
            )
        return response


class UserCompanyReportAPIView(APIView):
    permission_classes = [ModelPermission]
    queryset = CompanyProfile.objects.filter(is_active=True)

    def post(self, request):
        date_start = datetime.strptime(self.request.data['start_date']+'+0500', '%Y-%m-%d %H:%M:%S%z')
        date_end = datetime.strptime(self.request.data['start_end']+'+0500', '%Y-%m-%d %H:%M:%S%z')
        self.queryset = self.queryset.filter(
            user__date_joined__gte=date_start,
            user__date_joined__lte=date_end
        )
        df = pandas.DataFrame(columns=['date_joined', 'company_name', 'business_name', 'ruc', 'activity',
       'user_document','user_name',  'user_email', 'user_phone'])
        for profile in self.queryset:
            df = df.append({
                'date_joined': profile.user.date_joined.strftime("%Y-%m-%d %H:%M:%S"),
                'company_name': profile.company.company_name,
                'business_name': profile.company.social_reason,
                'ruc': profile.company.ruc,
                'activity': profile.company.company_activity,
                'user_document': profile.document,
                'user_name': '{} {}'.format(profile.user.first_name, profile.user.last_name),
                'user_email': profile.user.email,
                'user_phone': profile.phone
            }, ignore_index=True)
        columns_name = {
            'date_joined': 'Fecha de Registro',
            'company_name': 'Nombre de la empresa',
            'business_name': 'Razon Social',
            'ruc': 'R.U.C.',
            'activity': 'Rubro',
            'user_document': 'Documento del Encargado',
            'user_name': 'Nombre de encargado',
            'user_email': 'Correo del encargado',
            'user_phone': 'Telefono'
        }
        df.index = df.index + 1
        df = df[self.request.data['columns']]
        df = df.rename(columns=columns_name)
        buffer = BytesIO()
        writer = pandas.ExcelWriter(buffer, engine='xlsxwriter')
        df.to_excel(writer, sheet_name='Empresas')
        workbook = writer.book
        worksheet = writer.sheets['Empresas']
        border_fmt = workbook.add_format({'bottom': 1, 'top': 1, 'left': 1, 'right': 1})
        worksheet.conditional_format(xlsxwriter.utility.xl_range(0, 0, len(df), len(df.columns)),
                                     {'type': 'no_errors', 'format': border_fmt})
        writer.save()
        filename = 'Reporte de Empresas.xlsx'
        response = HttpResponse(
            buffer.getvalue(),
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response
