"""
Urls for group module
"""
from django.urls import path

from .views import (
    UserProfileListCreateAPIView,
    SuperAdminProfileRetrieveUpdateAPIView,
    PerfilReactivateView,
    AdminProfileListCreateAPIView,
    AdminProfileRetrieveUpdateAPIView,
    UserUpdateInformationProfile,
    CompanyProfileCreateAPIView,
    UpdateProfilePictureAuth,
    UpdateCompanyProfileLogoAuth,
    CompanyProfileUpdateAPIView,
    GraduateProfileListCreateAPIView,
    GraduateProfileRetrieveUpdateDestroyAPIView,
    GraduateProfileReactivateAPIView,
    CompanyProfileRetrieveAPIView,
    GraduateProfileListAPIView,
    ValidateDataProfileView,
    GraduateCVUpdateAPIView, UserCompanyReportAPIView
)

urlpatterns = [
    path('super-admin/', UserProfileListCreateAPIView.as_view()),
    path('super-admin/<int:pk>', SuperAdminProfileRetrieveUpdateAPIView.as_view()),
    path('super-admin/reactivate/<int:pk>', PerfilReactivateView.as_view()),
    path('admin/', AdminProfileListCreateAPIView.as_view()),
    path('admin/<int:pk>', AdminProfileRetrieveUpdateAPIView.as_view()),
    path('update-current-profile/', UserUpdateInformationProfile.as_view()),
    path('company/', CompanyProfileCreateAPIView.as_view()),
    path('company/<int:pk>', CompanyProfileRetrieveAPIView.as_view()),
    path('update-picture/auth/', UpdateProfilePictureAuth.as_view()),
    path('company/update-logo/auth/', UpdateCompanyProfileLogoAuth.as_view()),
    path('company/update/<int:pk>', CompanyProfileUpdateAPIView.as_view()),
    path('graduate/', GraduateProfileListCreateAPIView.as_view()),
    path('graduate-user/', GraduateProfileListAPIView.as_view()),
    path('graduate/<int:pk>', GraduateProfileRetrieveUpdateDestroyAPIView.as_view()),
    path('graduate/update-cv/', GraduateCVUpdateAPIView.as_view()),
    path('graduate/reactivate/<int:pk>', GraduateProfileReactivateAPIView.as_view()),
    path('validate-data/', ValidateDataProfileView.as_view()),
    path('company/report/', UserCompanyReportAPIView.as_view())
]
