from django.urls import path

from apps.reset_password.views import ResetPasswordRequestCreateAPIView, ResetPasswordRetrieveUpdateAPIView

urlpatterns = [
    path('', ResetPasswordRequestCreateAPIView.as_view()),
    path('<uuid>', ResetPasswordRetrieveUpdateAPIView.as_view())
]