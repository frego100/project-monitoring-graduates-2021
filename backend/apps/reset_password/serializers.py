from abc import ABC

from rest_framework import serializers

from apps.reset_password.models import ResetPassword


class ResetPasswordRequestSerializer(serializers.Serializer):
    redirect_to = serializers.URLField(required=True)
    email = serializers.EmailField(required=True)


class ResetPasswordSerializer(serializers.ModelSerializer):

    class Meta:
        model = ResetPassword
        fields = '__all__'

class ResetPasswordSetSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)
