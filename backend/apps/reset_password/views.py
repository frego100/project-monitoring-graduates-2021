import pytz
from django.contrib.auth.models import User
from rest_framework import generics, status

# Create your views here.
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from apps.email_sender.utils import EmailSender
from apps.reset_password.models import ResetPassword
from apps.reset_password.serializers import ResetPasswordRequestSerializer, ResetPasswordSerializer, \
    ResetPasswordSetSerializer


class ResetPasswordRequestCreateAPIView(generics.CreateAPIView):
    serializer_class = ResetPasswordRequestSerializer
    queryset = ResetPassword.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        users = User.objects.filter(email=data['email'])
        instances = []
        for user in users:
            instance = ResetPassword.objects.create(user=user)
            instances.append(instance)
            template_name = 'request_reset_password.html'
            sender = EmailSender()
            subject = 'SOLICITUD DE CAMBIO DE CONTRASEÑA: {}'.format(
                instance.timestamp.astimezone(pytz.timezone('America/Lima')).strftime('%x %X')
            )
            emails = [instance.user.email]
            data = {
                'request': instance,
                'redirect_to': data['redirect_to']
            }
            sender.send_email(
                subject,
                emails,
                template_name,
                data,
                file=None
            )
        return Response(
            ResetPasswordSerializer(instances, many=True).data
        )


class ResetPasswordRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = ResetPasswordSerializer
    queryset = ResetPassword.objects.all()

    def get_object(self):
        uuid_ = self.request.parser_context.get('kwargs')['uuid']
        return get_object_or_404(self.queryset, uuid=uuid_)

    def update(self, request, *args, **kwargs):
        request_password = self.get_object()
        serializer = ResetPasswordSetSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        new_password = serializer.validated_data['password']
        request_password.user.set_password(new_password)
        request_password.user.save()
        request_password.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

