import uuid

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class ResetPassword(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True,
        verbose_name='UUID'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=False,
        verbose_name='Usuario'
    )
    timestamp = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Fecha de Solicitud'
    )

    class Meta:
        verbose_name = 'Cambio de Contraseña'
