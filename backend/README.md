# Backend API - Seguimiento de Egresados

## Setup

The first thing to do is going to file and clone repository, is better use ssh:

```sh
$ git clone git@gitlab.com:frego100/project-monitoring-graduates-2021.git
$ cd project-monitoring-graduates-2021/backend
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv2 --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```sh
(env)$ pip install -r requirements.txt
```
Note the `(env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv2`.

Once `pip` has finished downloading the dependencies:
```sh
(env)$ cd project
(env)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/`.

Remember configure file ".env"


### Authorizations Admin

For this project a user was set up for team use, the credentials of the same were shared by slack in the EP channel

Going to https://app.slack.com/


## Tests

To make testing, `cd` into the directory where `manage.py` is:
```sh
(env)$ ./manage.py test
```

To run pylint
```sh
(env)$ pylint ./* --ignore=requirements.txt,manage.py,migrations
```


## Deploy Server

The project for development issues is deployed in:
http://dev.unsa-egresados.eesp-system.net/
this is the development environment. Pre-pred and prod environments are shared privately
See the wikis for more information about EP's
